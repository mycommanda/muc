/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"js/app": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/scripts/index.js","js/vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/components/header/header.js":
/*!*****************************************!*\
  !*** ./src/components/header/header.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return HeaderComponent; });
/* harmony import */ var _scripts_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../scripts/base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




var HeaderComponent =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(HeaderComponent, _BaseComponent);

  function HeaderComponent() {
    _classCallCheck(this, HeaderComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(HeaderComponent).apply(this, arguments));
  }

  _createClass(HeaderComponent, [{
    key: "init",
    value: function init() {
      var hamburgerButton = this.$element.find('.js-hamburger');
      var navigation = this.$element.find('.js-navigation');
      hamburgerButton.on('click', function () {
        navigation.toggleClass('header__part_navigation_active');
        hamburgerButton.toggleClass('header__hamburger_active');
      });
    }
  }]);

  return HeaderComponent;
}(_scripts_base_component__WEBPACK_IMPORTED_MODULE_0__["default"]);



/***/ }),

/***/ "./src/components/main-slider/main-slider.js":
/*!***************************************************!*\
  !*** ./src/components/main-slider/main-slider.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MainSliderComponent; });
/* harmony import */ var _scripts_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../scripts/base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! slick-carousel */ "./node_modules/slick-carousel/slick/slick.js");
/* harmony import */ var slick_carousel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(slick_carousel__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var MainSliderComponent =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(MainSliderComponent, _BaseComponent);

  function MainSliderComponent() {
    _classCallCheck(this, MainSliderComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(MainSliderComponent).apply(this, arguments));
  }

  _createClass(MainSliderComponent, [{
    key: "init",
    value: function init() {
      this.$element.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        // prevArrow: "<a href=\"javascript:void(0);\" class='partners__prev arrow arrow_prev'></a>",
        // nextArrow: "<a href=\"javascript:void(0);\" class='partners__next arrow arrow_next'></a>",
        autoplay: true,
        arrows: false,
        autoplaySpeed: 2000,
        infinite: true
      });
    }
  }]);

  return MainSliderComponent;
}(_scripts_base_component__WEBPACK_IMPORTED_MODULE_0__["default"]);



/***/ }),

/***/ "./src/components/partners/partners.js":
/*!*********************************************!*\
  !*** ./src/components/partners/partners.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PartnersComponent; });
/* harmony import */ var _scripts_base_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../scripts/base-component */ "./src/scripts/base-component.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var slick_slider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! slick-slider */ "./node_modules/slick-slider/slick/slick.js");
/* harmony import */ var slick_slider__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(slick_slider__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var PartnersComponent =
/*#__PURE__*/
function (_BaseComponent) {
  _inherits(PartnersComponent, _BaseComponent);

  function PartnersComponent() {
    _classCallCheck(this, PartnersComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(PartnersComponent).apply(this, arguments));
  }

  _createClass(PartnersComponent, [{
    key: "init",
    value: function init() {
      var slider = this.$element.find('.js-partner-slider');
      slider.slick({
        slidesToShow: 4,
        slidesToScroll: 2,
        prevArrow: '<a href="javascript:void(0);" class="partners__arrow partners__arrow_left circle-arrow"></a>',
        nextArrow: '<a href="javascript:void(0);" class="partners__arrow partners__arrow_right circle-arrow"></a>',
        autoplay: true,
        autoplaySpeed: 2000,
        infinite: true,
        lazyLoad: 'ondemand',
        responsive: [{
          breakpoint: 1190,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }, {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            arrows: false
          }
        }, {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            arrows: false
          }
        }]
      });
    }
  }]);

  return PartnersComponent;
}(_scripts_base_component__WEBPACK_IMPORTED_MODULE_0__["default"]);



/***/ }),

/***/ "./src/scripts/base-component.js":
/*!***************************************!*\
  !*** ./src/scripts/base-component.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BaseComponent; });
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var BaseComponent =
/*#__PURE__*/
function () {
  function BaseComponent(element) {
    _classCallCheck(this, BaseComponent);

    this.$element = jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0___default()(element);
    this.init();
  }

  _createClass(BaseComponent, [{
    key: "init",
    value: function init() {}
  }]);

  return BaseComponent;
}();



/***/ }),

/***/ "./src/scripts/base-page.js":
/*!**********************************!*\
  !*** ./src/scripts/base-page.js ***!
  \**********************************/
/*! exports provided: BasePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasePage", function() { return BasePage; });
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var BasePage =
/*#__PURE__*/
function () {
  function BasePage(element) {
    var _this = this;

    _classCallCheck(this, BasePage);

    jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_0___default()(window).ready(function ($) {
      _this.$element = $('body');

      _this.init();
    });
  }

  _createClass(BasePage, [{
    key: "init",
    value: function init() {//abstract
    }
  }]);

  return BasePage;
}();



/***/ }),

/***/ "./src/scripts/index.js":
/*!******************************!*\
  !*** ./src/scripts/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _page_index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./page/index.js */ "./src/scripts/page/index.js");


/***/ }),

/***/ "./src/scripts/page/index.js":
/*!***********************************!*\
  !*** ./src/scripts/page/index.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../base-page */ "./src/scripts/base-page.js");
/* harmony import */ var _components_header_header_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../components/header/header.js */ "./src/components/header/header.js");
/* harmony import */ var _components_main_slider_main_slider_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/main-slider/main-slider.js */ "./src/components/main-slider/main-slider.js");
/* harmony import */ var _components_partners_partners_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/partners/partners.js */ "./src/components/partners/partners.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jquery/dist/jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(jquery_dist_jquery__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _vendor_izimodal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../vendor/izimodal */ "./src/scripts/vendor/izimodal.js");
/* harmony import */ var _vendor_izimodal__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_vendor_izimodal__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var select2_dist_js_select2_full_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! select2/dist/js/select2.full.js */ "./node_modules/select2/dist/js/select2.full.js");
/* harmony import */ var select2_dist_js_select2_full_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(select2_dist_js_select2_full_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _vendor_autosize_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../vendor/autosize.js */ "./src/scripts/vendor/autosize.js");
/* harmony import */ var _vendor_autosize_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_vendor_autosize_js__WEBPACK_IMPORTED_MODULE_7__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }










var IndexPage =
/*#__PURE__*/
function (_BasePage) {
  _inherits(IndexPage, _BasePage);

  function IndexPage() {
    _classCallCheck(this, IndexPage);

    return _possibleConstructorReturn(this, _getPrototypeOf(IndexPage).apply(this, arguments));
  }

  _createClass(IndexPage, [{
    key: "init",
    value: function init() {
      this.$element.find('.js-header').each(function (i, el) {
        new _components_header_header_js__WEBPACK_IMPORTED_MODULE_1__["default"](el);
      });
      this.$element.find('.js-mainslider').each(function (i, el) {
        new _components_main_slider_main_slider_js__WEBPACK_IMPORTED_MODULE_2__["default"](el);
      });
      this.$element.find('.js-partners').each(function (i, el) {
        new _components_partners_partners_js__WEBPACK_IMPORTED_MODULE_3__["default"](el);
      });
    }
  }]);

  return IndexPage;
}(_base_page__WEBPACK_IMPORTED_MODULE_0__["BasePage"]);

var page = new IndexPage();

/***/ }),

/***/ "./src/scripts/vendor/autosize.js":
/*!****************************************!*\
  !*** ./src/scripts/vendor/autosize.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($, jQuery) {var Plugins;

(function (Plugins) {
  var AutosizeInputOptions = function () {
    function AutosizeInputOptions(space) {
      if (typeof space === "undefined") {
        space = 30;
      }

      this.space = space;
    }

    return AutosizeInputOptions;
  }();

  Plugins.AutosizeInputOptions = AutosizeInputOptions;

  var AutosizeInput = function () {
    function AutosizeInput(input, options) {
      var _this = this;

      this._input = $(input);
      this._options = $.extend({}, AutosizeInput.getDefaultOptions(), options); // Init mirror

      this._mirror = $('<span style="position:absolute; top:-999px; left:0; white-space:pre;"/>'); // Copy to mirror

      $.each(['fontFamily', 'fontSize', 'fontWeight', 'fontStyle', 'letterSpacing', 'textTransform', 'wordSpacing', 'textIndent'], function (i, val) {
        _this._mirror[0].style[val] = _this._input.css(val);
      });
      $("body").append(this._mirror); // Bind events - change update paste click mousedown mouseup focus blur
      // IE 9 need keydown to keep updating while deleting (keeping backspace in - else it will first update when backspace is released)
      // IE 9 need keyup incase text is selected and backspace/deleted is hit - keydown is to early
      // How to fix problem with hitting the delete "X" in the box - but not updating!? mouseup is apparently to early
      // Could bind separatly and set timer
      // Add so it automatically updates if value of input is changed http://stackoverflow.com/a/1848414/58524

      this._input.on("keydown keyup input propertychange change", function (e) {
        _this.update();
      }); // Update


      (function () {
        _this.update();
      })();
    }

    AutosizeInput.prototype.getOptions = function () {
      return this._options;
    };

    AutosizeInput.prototype.update = function () {
      var value = this._input.val() || "";

      if (value === this._mirror.text()) {
        // Nothing have changed - skip
        return;
      } // Update mirror


      this._mirror.text(value); // Calculate the width


      var newWidth = this._mirror.width() + this._options.space; // Update the width


      this._input.width(newWidth);
    };

    AutosizeInput.getDefaultOptions = function () {
      return this._defaultOptions;
    };

    AutosizeInput.getInstanceKey = function () {
      // Use camelcase because .data()['autosize-input-instance'] will not work
      return "autosizeInputInstance";
    };

    AutosizeInput._defaultOptions = new AutosizeInputOptions();
    return AutosizeInput;
  }();

  Plugins.AutosizeInput = AutosizeInput; // jQuery Plugin

  (function ($) {
    var pluginDataAttributeName = "autosize-input";
    var validTypes = ["text", "password", "search", "url", "tel", "email", "number"]; // jQuery Plugin

    $.fn.autosizeInput = function (options) {
      return this.each(function () {
        // Make sure it is only applied to input elements of valid type
        // Or let it be the responsibility of the programmer to only select and apply to valid elements?
        if (!(this.tagName == "INPUT" && $.inArray(this.type, validTypes) > -1)) {
          // Skip - if not input and of valid type
          return;
        }

        var $this = $(this);

        if (!$this.data(Plugins.AutosizeInput.getInstanceKey())) {
          // If instance not already created and attached
          if (options == undefined) {
            // Try get options from attribute
            options = $this.data(pluginDataAttributeName);
          } // Create and attach instance


          $this.data(Plugins.AutosizeInput.getInstanceKey(), new Plugins.AutosizeInput(this, options));
        }
      });
    }; // On Document Ready


    $(function () {
      // Instantiate for all with data-provide=autosize-input attribute
      $("input[data-" + pluginDataAttributeName + "]").autosizeInput();
    }); // Alternative to use On Document Ready and creating the instance immediately
    //$(document).on('focus.autosize-input', 'input[data-autosize-input]', function (e)
    //{
    //	$(this).autosizeInput();
    //});
  })(jQuery);
})(Plugins || (Plugins = {}));
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"), __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./src/scripts/vendor/izimodal.js":
/*!****************************************!*\
  !*** ./src/scripts/vendor/izimodal.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*
* iziModal | v1.5.1
* http://izimodal.marcelodolce.com
* by Marcelo Dolce.
*/
(function (factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
})(function ($) {
  var $window = $(window),
      $document = $(document),
      PLUGIN_NAME = 'iziModal',
      STATES = {
    CLOSING: 'closing',
    CLOSED: 'closed',
    OPENING: 'opening',
    OPENED: 'opened',
    DESTROYED: 'destroyed'
  };

  function whichAnimationEvent() {
    var t,
        el = document.createElement("fakeelement"),
        animations = {
      "animation": "animationend",
      "OAnimation": "oAnimationEnd",
      "MozAnimation": "animationend",
      "WebkitAnimation": "webkitAnimationEnd"
    };

    for (t in animations) {
      if (el.style[t] !== undefined) {
        return animations[t];
      }
    }
  }

  function isIE(version) {
    if (version === 9) {
      return navigator.appVersion.indexOf("MSIE 9.") !== -1;
    } else {
      userAgent = navigator.userAgent;
      return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1;
    }
  }

  function clearValue(value) {
    var separators = /%|px|em|cm|vh|vw/;
    return parseInt(String(value).split(separators)[0]);
  }

  var animationEvent = whichAnimationEvent(),
      isMobile = /Mobi/.test(navigator.userAgent) ? true : false;
  window.$iziModal = {};
  window.$iziModal.autoOpen = 0;
  window.$iziModal.history = false;

  var iziModal = function iziModal(element, options) {
    this.init(element, options);
  };

  iziModal.prototype = {
    constructor: iziModal,
    init: function init(element, options) {
      var that = this;
      this.$element = $(element);

      if (this.$element[0].id !== undefined && this.$element[0].id !== '') {
        this.id = this.$element[0].id;
      } else {
        this.id = PLUGIN_NAME + Math.floor(Math.random() * 10000000 + 1);
        this.$element.attr('id', this.id);
      }

      this.classes = this.$element.attr('class') !== undefined ? this.$element.attr('class') : '';
      this.content = this.$element.html();
      this.state = STATES.CLOSED;
      this.options = options;
      this.width = 0;
      this.timer = null;
      this.timerTimeout = null;
      this.progressBar = null;
      this.isPaused = false;
      this.isFullscreen = false;
      this.headerHeight = 0;
      this.modalHeight = 0;
      this.$overlay = $('<div class="' + PLUGIN_NAME + '-overlay" style="background-color:' + options.overlayColor + '"></div>');
      this.$navigate = $('<div class="' + PLUGIN_NAME + '-navigate"><div class="' + PLUGIN_NAME + '-navigate-caption">Use</div><button class="' + PLUGIN_NAME + '-navigate-prev"></button><button class="' + PLUGIN_NAME + '-navigate-next"></button></div>');
      this.group = {
        name: this.$element.attr('data-' + PLUGIN_NAME + '-group'),
        index: null,
        ids: []
      };
      this.$element.attr('aria-hidden', 'true');
      this.$element.attr('aria-labelledby', this.id);
      this.$element.attr('role', 'dialog');

      if (!this.$element.hasClass('iziModal')) {
        this.$element.addClass('iziModal');
      }

      if (this.group.name === undefined && options.group !== "") {
        this.group.name = options.group;
        this.$element.attr('data-' + PLUGIN_NAME + '-group', options.group);
      }

      if (this.options.loop === true) {
        this.$element.attr('data-' + PLUGIN_NAME + '-loop', true);
      }

      $.each(this.options, function (index, val) {
        var attr = that.$element.attr('data-' + PLUGIN_NAME + '-' + index);

        try {
          if (_typeof(attr) !== ( true ? "undefined" : undefined)) {
            if (attr === "" || attr == "true") {
              options[index] = true;
            } else if (attr == "false") {
              options[index] = false;
            } else if (typeof val == 'function') {
              options[index] = new Function(attr);
            } else {
              options[index] = attr;
            }
          }
        } catch (exc) {}
      });

      if (options.appendTo !== false) {
        this.$element.appendTo(options.appendTo);
      }

      if (options.iframe === true) {
        this.$element.html('<div class="' + PLUGIN_NAME + '-wrap"><div class="' + PLUGIN_NAME + '-content"><iframe class="' + PLUGIN_NAME + '-iframe"></iframe>' + this.content + "</div></div>");

        if (options.iframeHeight !== null) {
          this.$element.find('.' + PLUGIN_NAME + '-iframe').css('height', options.iframeHeight);
        }
      } else {
        this.$element.html('<div class="' + PLUGIN_NAME + '-wrap"><div class="' + PLUGIN_NAME + '-content">' + this.content + '</div></div>');
      }

      if (this.options.background !== null) {
        this.$element.css('background', this.options.background);
      }

      this.$wrap = this.$element.find('.' + PLUGIN_NAME + '-wrap');

      if (options.zindex !== null && !isNaN(parseInt(options.zindex))) {
        this.$element.css('z-index', options.zindex);
        this.$navigate.css('z-index', options.zindex - 1);
        this.$overlay.css('z-index', options.zindex - 2);
      }

      if (options.radius !== "") {
        this.$element.css('border-radius', options.radius);
      }

      if (options.padding !== "") {
        this.$element.find('.' + PLUGIN_NAME + '-content').css('padding', options.padding);
      }

      if (options.theme !== "") {
        if (options.theme === "light") {
          this.$element.addClass(PLUGIN_NAME + '-light');
        } else {
          this.$element.addClass(options.theme);
        }
      }

      if (options.rtl === true) {
        this.$element.addClass(PLUGIN_NAME + '-rtl');
      }

      if (options.openFullscreen === true) {
        this.isFullscreen = true;
        this.$element.addClass('isFullscreen');
      }

      this.createHeader();
      this.recalcWidth();
      this.recalcVerticalPos();

      if (that.options.afterRender && (typeof that.options.afterRender === "function" || _typeof(that.options.afterRender) === "object")) {
        that.options.afterRender(that);
      }
    },
    createHeader: function createHeader() {
      this.$header = $('<div class="' + PLUGIN_NAME + '-header"><h2 class="' + PLUGIN_NAME + '-header-title">' + this.options.title + '</h2><p class="' + PLUGIN_NAME + '-header-subtitle">' + this.options.subtitle + '</p><div class="' + PLUGIN_NAME + '-header-buttons"></div></div>');

      if (this.options.closeButton === true) {
        this.$header.find('.' + PLUGIN_NAME + '-header-buttons').append('<a href="javascript:void(0)" class="' + PLUGIN_NAME + '-button ' + PLUGIN_NAME + '-button-close" data-' + PLUGIN_NAME + '-close></a>');
      }

      if (this.options.fullscreen === true) {
        this.$header.find('.' + PLUGIN_NAME + '-header-buttons').append('<a href="javascript:void(0)" class="' + PLUGIN_NAME + '-button ' + PLUGIN_NAME + '-button-fullscreen" data-' + PLUGIN_NAME + '-fullscreen></a>');
      }

      if (this.options.timeoutProgressbar === true && !isNaN(parseInt(this.options.timeout)) && this.options.timeout !== false && this.options.timeout !== 0) {
        this.$header.prepend('<div class="' + PLUGIN_NAME + '-progressbar"><div style="background-color:' + this.options.timeoutProgressbarColor + '"></div></div>');
      }

      if (this.options.subtitle === '') {
        this.$header.addClass(PLUGIN_NAME + '-noSubtitle');
      }

      if (this.options.title !== "") {
        if (this.options.headerColor !== null) {
          if (this.options.borderBottom === true) {
            this.$element.css('border-bottom', '3px solid ' + this.options.headerColor + '');
          }

          this.$header.css('background', this.options.headerColor);
        }

        if (this.options.icon !== null || this.options.iconText !== null) {
          this.$header.prepend('<i class="' + PLUGIN_NAME + '-header-icon"></i>');

          if (this.options.icon !== null) {
            this.$header.find('.' + PLUGIN_NAME + '-header-icon').addClass(this.options.icon).css('color', this.options.iconColor);
          }

          if (this.options.iconText !== null) {
            this.$header.find('.' + PLUGIN_NAME + '-header-icon').html(this.options.iconText);
          }
        }

        this.$element.css('overflow', 'hidden').prepend(this.$header);
      }
    },
    setGroup: function setGroup(groupName) {
      var that = this,
          group = this.group.name || groupName;
      this.group.ids = [];

      if (groupName !== undefined && groupName !== this.group.name) {
        group = groupName;
        this.group.name = group;
        this.$element.attr('data-' + PLUGIN_NAME + '-group', group);
      }

      if (group !== undefined && group !== "") {
        var count = 0;
        $.each($('.' + PLUGIN_NAME + '[data-' + PLUGIN_NAME + '-group=' + group + ']'), function (index, val) {
          that.group.ids.push($(this)[0].id);

          if (that.id == $(this)[0].id) {
            that.group.index = count;
          }

          count++;
        });
      }
    },
    toggle: function toggle() {
      if (this.state == STATES.OPENED) {
        this.close();
      }

      if (this.state == STATES.CLOSED) {
        this.open();
      }
    },
    open: function open(param) {
      var that = this;
      $.each($('.' + PLUGIN_NAME), function (index, modal) {
        if ($(modal).data().iziModal !== undefined) {
          var state = $(modal).iziModal('getState');

          if (state == 'opened' || state == 'opening') {
            $(modal).iziModal('close');
          }
        }
      });

      (function urlHash() {
        if (that.options.history) {
          var oldTitle = document.title;
          document.title = oldTitle + " - " + that.options.title;
          document.location.hash = that.id;
          document.title = oldTitle; //history.pushState({}, that.options.title, "#"+that.id);

          window.$iziModal.history = true;
        } else {
          window.$iziModal.history = false;
        }
      })();

      function opened() {
        // console.info('[ '+PLUGIN_NAME+' | '+that.id+' ] Opened.');
        that.state = STATES.OPENED;
        that.$element.trigger(STATES.OPENED);

        if (that.options.onOpened && (typeof that.options.onOpened === "function" || _typeof(that.options.onOpened) === "object")) {
          that.options.onOpened(that);
        }
      }

      function bindEvents() {
        // Close when button pressed
        that.$element.off('click', '[data-' + PLUGIN_NAME + '-close]').on('click', '[data-' + PLUGIN_NAME + '-close]', function (e) {
          e.preventDefault();
          var transition = $(e.currentTarget).attr('data-' + PLUGIN_NAME + '-transitionOut');

          if (transition !== undefined) {
            that.close({
              transition: transition
            });
          } else {
            that.close();
          }
        }); // Expand when button pressed

        that.$element.off('click', '[data-' + PLUGIN_NAME + '-fullscreen]').on('click', '[data-' + PLUGIN_NAME + '-fullscreen]', function (e) {
          e.preventDefault();

          if (that.isFullscreen === true) {
            that.isFullscreen = false;
            that.$element.removeClass('isFullscreen');
          } else {
            that.isFullscreen = true;
            that.$element.addClass('isFullscreen');
          }

          if (that.options.onFullscreen && typeof that.options.onFullscreen === "function") {
            that.options.onFullscreen(that);
          }

          that.$element.trigger('fullscreen', that);
        }); // Next modal

        that.$navigate.off('click', '.' + PLUGIN_NAME + '-navigate-next').on('click', '.' + PLUGIN_NAME + '-navigate-next', function (e) {
          that.next(e);
        });
        that.$element.off('click', '[data-' + PLUGIN_NAME + '-next]').on('click', '[data-' + PLUGIN_NAME + '-next]', function (e) {
          that.next(e);
        }); // Previous modal

        that.$navigate.off('click', '.' + PLUGIN_NAME + '-navigate-prev').on('click', '.' + PLUGIN_NAME + '-navigate-prev', function (e) {
          that.prev(e);
        });
        that.$element.off('click', '[data-' + PLUGIN_NAME + '-prev]').on('click', '[data-' + PLUGIN_NAME + '-prev]', function (e) {
          that.prev(e);
        });
      }

      if (this.state == STATES.CLOSED) {
        bindEvents();
        this.setGroup();
        this.state = STATES.OPENING;
        this.$element.trigger(STATES.OPENING);
        this.$element.attr('aria-hidden', 'false'); // console.info('[ '+PLUGIN_NAME+' | '+this.id+' ] Opening...');

        if (this.options.iframe === true) {
          this.$element.find('.' + PLUGIN_NAME + '-content').addClass(PLUGIN_NAME + '-content-loader');
          this.$element.find('.' + PLUGIN_NAME + '-iframe').on('load', function () {
            $(this).parent().removeClass(PLUGIN_NAME + '-content-loader');
          });
          var href = null;

          try {
            href = $(param.currentTarget).attr('href') !== "" ? $(param.currentTarget).attr('href') : null;
          } catch (e) {// console.warn(e);
          }

          if (this.options.iframeURL !== null && (href === null || href === undefined)) {
            href = this.options.iframeURL;
          }

          if (href === null || href === undefined) {
            throw new Error("Failed to find iframe URL");
          }

          this.$element.find('.' + PLUGIN_NAME + '-iframe').attr('src', href);
        }

        if (this.options.bodyOverflow || isMobile) {
          $('html').addClass(PLUGIN_NAME + '-isOverflow');

          if (isMobile) {
            $('body').css('overflow', 'hidden');
          }
        }

        if (this.options.onOpening && typeof this.options.onOpening === "function") {
          this.options.onOpening(this);
        }

        (function open() {
          if (that.group.ids.length > 1) {
            that.$navigate.appendTo('body');
            that.$navigate.addClass('fadeIn');

            if (that.options.navigateCaption === true) {
              that.$navigate.find('.' + PLUGIN_NAME + '-navigate-caption').show();
            }

            var modalWidth = that.$element.outerWidth();

            if (that.options.navigateArrows !== false) {
              if (that.options.navigateArrows === 'closeScreenEdge') {
                that.$navigate.find('.' + PLUGIN_NAME + '-navigate-prev').css('left', 0).show();
                that.$navigate.find('.' + PLUGIN_NAME + '-navigate-next').css('right', 0).show();
              } else {
                that.$navigate.find('.' + PLUGIN_NAME + '-navigate-prev').css('margin-left', -(modalWidth / 2 + 84)).show();
                that.$navigate.find('.' + PLUGIN_NAME + '-navigate-next').css('margin-right', -(modalWidth / 2 + 84)).show();
              }
            } else {
              that.$navigate.find('.' + PLUGIN_NAME + '-navigate-prev').hide();
              that.$navigate.find('.' + PLUGIN_NAME + '-navigate-next').hide();
            }

            var loop;

            if (that.group.index === 0) {
              loop = $('.' + PLUGIN_NAME + '[data-' + PLUGIN_NAME + '-group="' + that.group.name + '"][data-' + PLUGIN_NAME + '-loop]').length;
              if (loop === 0 && that.options.loop === false) that.$navigate.find('.' + PLUGIN_NAME + '-navigate-prev').hide();
            }

            if (that.group.index + 1 === that.group.ids.length) {
              loop = $('.' + PLUGIN_NAME + '[data-' + PLUGIN_NAME + '-group="' + that.group.name + '"][data-' + PLUGIN_NAME + '-loop]').length;
              if (loop === 0 && that.options.loop === false) that.$navigate.find('.' + PLUGIN_NAME + '-navigate-next').hide();
            }
          }

          if (that.options.overlay === true) {
            if (that.options.appendToOverlay === false) {
              that.$overlay.appendTo('body');
            } else {
              that.$overlay.appendTo(that.options.appendToOverlay);
            }
          }

          if (that.options.transitionInOverlay) {
            that.$overlay.addClass(that.options.transitionInOverlay);
          }

          var transitionIn = that.options.transitionIn;

          if (_typeof(param) == 'object') {
            if (param.transition !== undefined || param.transitionIn !== undefined) {
              transitionIn = param.transition || param.transitionIn;
            }
          }

          if (transitionIn !== '' && animationEvent !== undefined) {
            that.$element.addClass("transitionIn " + transitionIn).show();
            that.$wrap.one(animationEvent, function () {
              that.$element.removeClass(transitionIn + " transitionIn");
              that.$overlay.removeClass(that.options.transitionInOverlay);
              that.$navigate.removeClass('fadeIn');
              opened();
            });
          } else {
            that.$element.show();
            opened();
          }

          if (that.options.pauseOnHover === true && that.options.pauseOnHover === true && that.options.timeout !== false && !isNaN(parseInt(that.options.timeout)) && that.options.timeout !== false && that.options.timeout !== 0) {
            that.$element.off('mouseenter').on('mouseenter', function (event) {
              event.preventDefault();
              that.isPaused = true;
            });
            that.$element.off('mouseleave').on('mouseleave', function (event) {
              event.preventDefault();
              that.isPaused = false;
            });
          }
        })();

        if (this.options.timeout !== false && !isNaN(parseInt(this.options.timeout)) && this.options.timeout !== false && this.options.timeout !== 0) {
          if (this.options.timeoutProgressbar === true) {
            this.progressBar = {
              hideEta: null,
              maxHideTime: null,
              currentTime: new Date().getTime(),
              el: this.$element.find('.' + PLUGIN_NAME + '-progressbar > div'),
              updateProgress: function updateProgress() {
                if (!that.isPaused) {
                  that.progressBar.currentTime = that.progressBar.currentTime + 10;
                  var percentage = (that.progressBar.hideEta - that.progressBar.currentTime) / that.progressBar.maxHideTime * 100;
                  that.progressBar.el.width(percentage + '%');

                  if (percentage < 0) {
                    that.close();
                  }
                }
              }
            };

            if (this.options.timeout > 0) {
              this.progressBar.maxHideTime = parseFloat(this.options.timeout);
              this.progressBar.hideEta = new Date().getTime() + this.progressBar.maxHideTime;
              this.timerTimeout = setInterval(this.progressBar.updateProgress, 10);
            }
          } else {
            this.timerTimeout = setTimeout(function () {
              that.close();
            }, that.options.timeout);
          }
        } // Close on overlay click


        if (this.options.overlayClose && !this.$element.hasClass(this.options.transitionOut)) {
          this.$overlay.click(function () {
            that.close();
          });
        }

        if (this.options.focusInput) {
          this.$element.find(':input:not(button):enabled:visible:first').focus(); // Focus on the first field
        }

        (function updateTimer() {
          that.recalcLayout();
          that.timer = setTimeout(updateTimer, 300);
        })(); // Close when the Escape key is pressed


        $document.on('keydown.' + PLUGIN_NAME, function (e) {
          if (that.options.closeOnEscape && e.keyCode === 27) {
            that.close();
          }
        });
      }
    },
    close: function close(param) {
      var that = this;

      function closed() {
        // console.info('[ '+PLUGIN_NAME+' | '+that.id+' ] Closed.');
        that.state = STATES.CLOSED;
        that.$element.trigger(STATES.CLOSED);

        if (that.options.iframe === true) {
          that.$element.find('.' + PLUGIN_NAME + '-iframe').attr('src', "");
        }

        if (that.options.bodyOverflow || isMobile) {
          $('html').removeClass(PLUGIN_NAME + '-isOverflow');

          if (isMobile) {
            $('body').css('overflow', 'auto');
          }
        }

        if (that.options.onClosed && typeof that.options.onClosed === "function") {
          that.options.onClosed(that);
        }

        if (that.options.restoreDefaultContent === true) {
          that.$element.find('.' + PLUGIN_NAME + '-content').html(that.content);
        }

        if ($('.' + PLUGIN_NAME + ':visible').length === 0) {
          $('html').removeClass(PLUGIN_NAME + '-isAttached');
        }
      }

      if (this.state == STATES.OPENED || this.state == STATES.OPENING) {
        $document.off('keydown.' + PLUGIN_NAME);
        this.state = STATES.CLOSING;
        this.$element.trigger(STATES.CLOSING);
        this.$element.attr('aria-hidden', 'true'); // console.info('[ '+PLUGIN_NAME+' | '+this.id+' ] Closing...');

        clearTimeout(this.timer);
        clearTimeout(this.timerTimeout);

        if (that.options.onClosing && typeof that.options.onClosing === "function") {
          that.options.onClosing(this);
        }

        var transitionOut = this.options.transitionOut;

        if (_typeof(param) == 'object') {
          if (param.transition !== undefined || param.transitionOut !== undefined) {
            transitionOut = param.transition || param.transitionOut;
          }
        }

        if (transitionOut === false || transitionOut === '' || animationEvent === undefined) {
          this.$element.hide();
          this.$overlay.remove();
          this.$navigate.remove();
          closed();
        } else {
          this.$element.attr('class', [this.classes, PLUGIN_NAME, transitionOut, this.options.theme == 'light' ? PLUGIN_NAME + '-light' : this.options.theme, this.isFullscreen === true ? 'isFullscreen' : '', this.options.rtl ? PLUGIN_NAME + '-rtl' : ''].join(' '));
          this.$overlay.attr('class', PLUGIN_NAME + "-overlay " + this.options.transitionOutOverlay);

          if (that.options.navigateArrows !== false) {
            this.$navigate.attr('class', PLUGIN_NAME + "-navigate fadeOut");
          }

          this.$element.one(animationEvent, function () {
            if (that.$element.hasClass(transitionOut)) {
              that.$element.removeClass(transitionOut + " transitionOut").hide();
            }

            that.$overlay.removeClass(that.options.transitionOutOverlay).remove();
            that.$navigate.removeClass('fadeOut').remove();
            closed();
          });
        }
      }
    },
    next: function next(e) {
      var that = this;
      var transitionIn = 'fadeInRight';
      var transitionOut = 'fadeOutLeft';
      var modal = $('.' + PLUGIN_NAME + ':visible');
      var modals = {};
      modals.out = this;

      if (e !== undefined && _typeof(e) !== 'object') {
        e.preventDefault();
        modal = $(e.currentTarget);
        transitionIn = modal.attr('data-' + PLUGIN_NAME + '-transitionIn');
        transitionOut = modal.attr('data-' + PLUGIN_NAME + '-transitionOut');
      } else if (e !== undefined) {
        if (e.transitionIn !== undefined) {
          transitionIn = e.transitionIn;
        }

        if (e.transitionOut !== undefined) {
          transitionOut = e.transitionOut;
        }
      }

      this.close({
        transition: transitionOut
      });
      setTimeout(function () {
        var loop = $('.' + PLUGIN_NAME + '[data-' + PLUGIN_NAME + '-group="' + that.group.name + '"][data-' + PLUGIN_NAME + '-loop]').length;

        for (var i = that.group.index + 1; i <= that.group.ids.length; i++) {
          try {
            modals["in"] = $("#" + that.group.ids[i]).data().iziModal;
          } catch (log) {// console.info('[ '+PLUGIN_NAME+' ] No next modal.');
          }

          if (typeof modals["in"] !== 'undefined') {
            $("#" + that.group.ids[i]).iziModal('open', {
              transition: transitionIn
            });
            break;
          } else {
            if (i == that.group.ids.length && loop > 0 || that.options.loop === true) {
              for (var index = 0; index <= that.group.ids.length; index++) {
                modals["in"] = $("#" + that.group.ids[index]).data().iziModal;

                if (typeof modals["in"] !== 'undefined') {
                  $("#" + that.group.ids[index]).iziModal('open', {
                    transition: transitionIn
                  });
                  break;
                }
              }
            }
          }
        }
      }, 200);
      $(document).trigger(PLUGIN_NAME + "-group-change", modals);
    },
    prev: function prev(e) {
      var that = this;
      var transitionIn = 'fadeInLeft';
      var transitionOut = 'fadeOutRight';
      var modal = $('.' + PLUGIN_NAME + ':visible');
      var modals = {};
      modals.out = this;

      if (e !== undefined && _typeof(e) !== 'object') {
        e.preventDefault();
        modal = $(e.currentTarget);
        transitionIn = modal.attr('data-' + PLUGIN_NAME + '-transitionIn');
        transitionOut = modal.attr('data-' + PLUGIN_NAME + '-transitionOut');
      } else if (e !== undefined) {
        if (e.transitionIn !== undefined) {
          transitionIn = e.transitionIn;
        }

        if (e.transitionOut !== undefined) {
          transitionOut = e.transitionOut;
        }
      }

      this.close({
        transition: transitionOut
      });
      setTimeout(function () {
        var loop = $('.' + PLUGIN_NAME + '[data-' + PLUGIN_NAME + '-group="' + that.group.name + '"][data-' + PLUGIN_NAME + '-loop]').length;

        for (var i = that.group.index; i >= 0; i--) {
          try {
            modals["in"] = $("#" + that.group.ids[i - 1]).data().iziModal;
          } catch (log) {// console.info('[ '+PLUGIN_NAME+' ] No previous modal.');
          }

          if (typeof modals["in"] !== 'undefined') {
            $("#" + that.group.ids[i - 1]).iziModal('open', {
              transition: transitionIn
            });
            break;
          } else {
            if (i === 0 && loop > 0 || that.options.loop === true) {
              for (var index = that.group.ids.length - 1; index >= 0; index--) {
                modals["in"] = $("#" + that.group.ids[index]).data().iziModal;

                if (typeof modals["in"] !== 'undefined') {
                  $("#" + that.group.ids[index]).iziModal('open', {
                    transition: transitionIn
                  });
                  break;
                }
              }
            }
          }
        }
      }, 200);
      $(document).trigger(PLUGIN_NAME + "-group-change", modals);
    },
    destroy: function destroy() {
      var e = $.Event('destroy');
      this.$element.trigger(e);
      $document.off('keydown.' + PLUGIN_NAME);
      clearTimeout(this.timer);
      clearTimeout(this.timerTimeout);

      if (this.options.iframe === true) {
        this.$element.find('.' + PLUGIN_NAME + '-iframe').remove();
      }

      this.$element.html(this.$element.find('.' + PLUGIN_NAME + '-content').html());
      this.$element.off('click', '[data-' + PLUGIN_NAME + '-close]');
      this.$element.off('click', '[data-' + PLUGIN_NAME + '-fullscreen]');
      this.$element.off('.' + PLUGIN_NAME).removeData(PLUGIN_NAME).attr('style', '');
      this.$overlay.remove();
      this.$navigate.remove();
      this.$element.trigger(STATES.DESTROYED);
      this.$element = null;
    },
    getState: function getState() {
      return this.state;
    },
    getGroup: function getGroup() {
      return this.group;
    },
    setWidth: function setWidth(width) {
      this.options.width = width;
      this.recalcWidth();
      var modalWidth = this.$element.outerWidth();

      if (this.options.navigateArrows === true || this.options.navigateArrows == 'closeToModal') {
        this.$navigate.find('.' + PLUGIN_NAME + '-navigate-prev').css('margin-left', -(modalWidth / 2 + 84)).show();
        this.$navigate.find('.' + PLUGIN_NAME + '-navigate-next').css('margin-right', -(modalWidth / 2 + 84)).show();
      }
    },
    setTop: function setTop(top) {
      this.options.top = top;
      this.recalcVerticalPos(false);
    },
    setBottom: function setBottom(bottom) {
      this.options.bottom = bottom;
      this.recalcVerticalPos(false);
    },
    setHeader: function setHeader(status) {
      if (status) {
        this.$element.find('.' + PLUGIN_NAME + '-header').show();
      } else {
        this.headerHeight = 0;
        this.$element.find('.' + PLUGIN_NAME + '-header').hide();
      }
    },
    setTitle: function setTitle(title) {
      this.options.title = title;

      if (this.headerHeight === 0) {
        this.createHeader();
      }

      if (this.$header.find('.' + PLUGIN_NAME + '-header-title').length === 0) {
        this.$header.append('<h2 class="' + PLUGIN_NAME + '-header-title"></h2>');
      }

      this.$header.find('.' + PLUGIN_NAME + '-header-title').html(title);
    },
    setSubtitle: function setSubtitle(subtitle) {
      if (subtitle === '') {
        this.$header.find('.' + PLUGIN_NAME + '-header-subtitle').remove();
        this.$header.addClass(PLUGIN_NAME + '-noSubtitle');
      } else {
        if (this.$header.find('.' + PLUGIN_NAME + '-header-subtitle').length === 0) {
          this.$header.append('<p class="' + PLUGIN_NAME + '-header-subtitle"></p>');
        }

        this.$header.removeClass(PLUGIN_NAME + '-noSubtitle');
      }

      this.$header.find('.' + PLUGIN_NAME + '-header-subtitle').html(subtitle);
      this.options.subtitle = subtitle;
    },
    setIcon: function setIcon(icon) {
      if (this.$header.find('.' + PLUGIN_NAME + '-header-icon').length === 0) {
        this.$header.prepend('<i class="' + PLUGIN_NAME + '-header-icon"></i>');
      }

      this.$header.find('.' + PLUGIN_NAME + '-header-icon').attr('class', PLUGIN_NAME + '-header-icon ' + icon);
      this.options.icon = icon;
    },
    setIconText: function setIconText(iconText) {
      this.$header.find('.' + PLUGIN_NAME + '-header-icon').html(iconText);
      this.options.iconText = iconText;
    },
    setHeaderColor: function setHeaderColor(headerColor) {
      if (this.options.borderBottom === true) {
        this.$element.css('border-bottom', '3px solid ' + headerColor + '');
      }

      this.$header.css('background', headerColor);
      this.options.headerColor = headerColor;
    },
    setBackground: function setBackground(background) {
      if (background === false) {
        this.options.background = null;
        this.$element.css('background', '');
      } else {
        this.$element.css('background', background);
        this.options.background = background;
      }
    },
    setZindex: function setZindex(zIndex) {
      if (!isNaN(parseInt(this.options.zindex))) {
        this.options.zindex = zIndex;
        this.$element.css('z-index', zIndex);
        this.$navigate.css('z-index', zIndex - 1);
        this.$overlay.css('z-index', zIndex - 2);
      }
    },
    setFullscreen: function setFullscreen(value) {
      if (value) {
        this.isFullscreen = true;
        this.$element.addClass('isFullscreen');
      } else {
        this.isFullscreen = false;
        this.$element.removeClass('isFullscreen');
      }
    },
    setContent: function setContent(content) {
      if (_typeof(content) == "object") {
        var replace = content["default"] || false;

        if (replace === true) {
          this.content = content.content;
        }

        content = content.content;
      }

      if (this.options.iframe === false) {
        this.$element.find('.' + PLUGIN_NAME + '-content').html(content);
      }
    },
    setTransitionIn: function setTransitionIn(transition) {
      this.options.transitionIn = transition;
    },
    setTransitionOut: function setTransitionOut(transition) {
      this.options.transitionOut = transition;
    },
    resetContent: function resetContent() {
      this.$element.find('.' + PLUGIN_NAME + '-content').html(this.content);
    },
    startLoading: function startLoading() {
      if (!this.$element.find('.' + PLUGIN_NAME + '-loader').length) {
        this.$element.append('<div class="' + PLUGIN_NAME + '-loader fadeIn"></div>');
      }

      this.$element.find('.' + PLUGIN_NAME + '-loader').css({
        top: this.headerHeight,
        borderRadius: this.options.radius
      });
    },
    stopLoading: function stopLoading() {
      var $loader = this.$element.find('.' + PLUGIN_NAME + '-loader');

      if (!$loader.length) {
        this.$element.prepend('<div class="' + PLUGIN_NAME + '-loader fadeIn"></div>');
        $loader = this.$element.find('.' + PLUGIN_NAME + '-loader').css('border-radius', this.options.radius);
      }

      $loader.removeClass('fadeIn').addClass('fadeOut');
      setTimeout(function () {
        $loader.remove();
      }, 600);
    },
    recalcWidth: function recalcWidth() {
      var that = this;
      this.$element.css('max-width', this.options.width);

      if (isIE()) {
        var modalWidth = that.options.width;

        if (modalWidth.toString().split("%").length > 1) {
          modalWidth = that.$element.outerWidth();
        }

        that.$element.css({
          left: '50%',
          marginLeft: -(modalWidth / 2)
        });
      }
    },
    recalcVerticalPos: function recalcVerticalPos(first) {
      if (this.options.top !== null && this.options.top !== false) {
        this.$element.css('margin-top', this.options.top);

        if (this.options.top === 0) {
          this.$element.css({
            borderTopRightRadius: 0,
            borderTopLeftRadius: 0
          });
        }
      } else {
        if (first === false) {
          this.$element.css({
            marginTop: '',
            borderRadius: this.options.radius
          });
        }
      }

      if (this.options.bottom !== null && this.options.bottom !== false) {
        this.$element.css('margin-bottom', this.options.bottom);

        if (this.options.bottom === 0) {
          this.$element.css({
            borderBottomRightRadius: 0,
            borderBottomLeftRadius: 0
          });
        }
      } else {
        if (first === false) {
          this.$element.css({
            marginBottom: '',
            borderRadius: this.options.radius
          });
        }
      }
    },
    recalcLayout: function recalcLayout() {
      var that = this,
          windowHeight = $window.height(),
          modalHeight = this.$element.outerHeight(),
          modalWidth = this.$element.outerWidth(),
          contentHeight = this.$element.find('.' + PLUGIN_NAME + '-content')[0].scrollHeight,
          outerHeight = contentHeight + this.headerHeight,
          wrapperHeight = this.$element.innerHeight() - this.headerHeight,
          modalMargin = parseInt(-((this.$element.innerHeight() + 1) / 2)) + 'px',
          scrollTop = this.$wrap.scrollTop(),
          borderSize = 0;

      if (isIE()) {
        if (modalWidth >= $window.width() || this.isFullscreen === true) {
          this.$element.css({
            left: '0',
            marginLeft: ''
          });
        } else {
          this.$element.css({
            left: '50%',
            marginLeft: -(modalWidth / 2)
          });
        }
      }

      if (this.options.borderBottom === true && this.options.title !== "") {
        borderSize = 3;
      }

      if (this.$element.find('.' + PLUGIN_NAME + '-header').length && this.$element.find('.' + PLUGIN_NAME + '-header').is(':visible')) {
        this.headerHeight = parseInt(this.$element.find('.' + PLUGIN_NAME + '-header').innerHeight());
        this.$element.css('overflow', 'hidden');
      } else {
        this.headerHeight = 0;
        this.$element.css('overflow', '');
      }

      if (this.$element.find('.' + PLUGIN_NAME + '-loader').length) {
        this.$element.find('.' + PLUGIN_NAME + '-loader').css('top', this.headerHeight);
      }

      if (modalHeight !== this.modalHeight) {
        this.modalHeight = modalHeight;

        if (this.options.onResize && typeof this.options.onResize === "function") {
          this.options.onResize(this);
        }
      }

      if (this.state == STATES.OPENED || this.state == STATES.OPENING) {
        if (this.options.iframe === true) {
          // If the height of the window is smaller than the modal with iframe
          if (windowHeight < this.options.iframeHeight + this.headerHeight + borderSize || this.isFullscreen === true) {
            this.$element.find('.' + PLUGIN_NAME + '-iframe').css('height', windowHeight - (this.headerHeight + borderSize));
          } else {
            this.$element.find('.' + PLUGIN_NAME + '-iframe').css('height', this.options.iframeHeight);
          }
        }

        if (modalHeight == windowHeight) {
          this.$element.addClass('isAttached');
        } else {
          this.$element.removeClass('isAttached');
        }

        if (this.isFullscreen === false && this.$element.width() >= $window.width()) {
          this.$element.find('.' + PLUGIN_NAME + '-button-fullscreen').hide();
        } else {
          this.$element.find('.' + PLUGIN_NAME + '-button-fullscreen').show();
        }

        this.recalcButtons();

        if (this.isFullscreen === false) {
          windowHeight = windowHeight - (clearValue(this.options.top) || 0) - (clearValue(this.options.bottom) || 0);
        } // If the modal is larger than the height of the window..


        if (outerHeight > windowHeight) {
          if (this.options.top > 0 && this.options.bottom === null && contentHeight < $window.height()) {
            this.$element.addClass('isAttachedBottom');
          }

          if (this.options.bottom > 0 && this.options.top === null && contentHeight < $window.height()) {
            this.$element.addClass('isAttachedTop');
          }

          $('html').addClass(PLUGIN_NAME + '-isAttached');
          this.$element.css('height', windowHeight);
        } else {
          this.$element.css('height', contentHeight + (this.headerHeight + borderSize));
          this.$element.removeClass('isAttachedTop isAttachedBottom');
          $('html').removeClass(PLUGIN_NAME + '-isAttached');
        }

        (function applyScroll() {
          if (contentHeight > wrapperHeight && outerHeight > windowHeight) {
            that.$element.addClass('hasScroll');
            that.$wrap.css('height', modalHeight - (that.headerHeight + borderSize));
          } else {
            that.$element.removeClass('hasScroll');
            that.$wrap.css('height', 'auto');
          }
        })();

        (function applyShadow() {
          if (wrapperHeight + scrollTop < contentHeight - 30) {
            that.$element.addClass('hasShadow');
          } else {
            that.$element.removeClass('hasShadow');
          }
        })();
      }
    },
    recalcButtons: function recalcButtons() {
      var widthButtons = this.$header.find('.' + PLUGIN_NAME + '-header-buttons').innerWidth() + 10;

      if (this.options.rtl === true) {
        this.$header.css('padding-left', widthButtons);
      } else {
        this.$header.css('padding-right', widthButtons);
      }
    }
  };
  $window.off('load.' + PLUGIN_NAME).on('load.' + PLUGIN_NAME, function (e) {
    var modalHash = document.location.hash;

    if (window.$iziModal.autoOpen === 0 && !$('.' + PLUGIN_NAME).is(":visible")) {
      try {
        var data = $(modalHash).data();

        if (typeof data !== 'undefined') {
          if (data.iziModal.options.autoOpen !== false) {
            $(modalHash).iziModal("open");
          }
        }
      } catch (exc) {
        /* console.warn(exc); */
      }
    }
  });
  $window.off('hashchange.' + PLUGIN_NAME).on('hashchange.' + PLUGIN_NAME, function (e) {
    var modalHash = document.location.hash;
    var data = $(modalHash).data();

    if (modalHash !== "") {
      try {
        if (typeof data !== 'undefined' && $(modalHash).iziModal('getState') !== 'opening') {
          setTimeout(function () {
            $(modalHash).iziModal("open");
          }, 200);
        }
      } catch (exc) {
        /* console.warn(exc); */
      }
    } else {
      if (window.$iziModal.history) {
        $.each($('.' + PLUGIN_NAME), function (index, modal) {
          if ($(modal).data().iziModal !== undefined) {
            var state = $(modal).iziModal('getState');

            if (state == 'opened' || state == 'opening') {
              $(modal).iziModal('close');
            }
          }
        });
      }
    }
  });
  $document.off('click', '[data-' + PLUGIN_NAME + '-open]').on('click', '[data-' + PLUGIN_NAME + '-open]', function (e) {
    e.preventDefault();
    var modal = $('.' + PLUGIN_NAME + ':visible');
    var openModal = $(e.currentTarget).attr('data-' + PLUGIN_NAME + '-open');
    var transitionIn = $(e.currentTarget).attr('data-' + PLUGIN_NAME + '-transitionIn');
    var transitionOut = $(e.currentTarget).attr('data-' + PLUGIN_NAME + '-transitionOut');

    if (transitionOut !== undefined) {
      modal.iziModal('close', {
        transition: transitionOut
      });
    } else {
      modal.iziModal('close');
    }

    setTimeout(function () {
      if (transitionIn !== undefined) {
        $(openModal).iziModal('open', {
          transition: transitionIn
        });
      } else {
        $(openModal).iziModal('open');
      }
    }, 200);
  });
  $document.off('keyup.' + PLUGIN_NAME).on('keyup.' + PLUGIN_NAME, function (event) {
    if ($('.' + PLUGIN_NAME + ':visible').length) {
      var modal = $('.' + PLUGIN_NAME + ':visible')[0].id,
          group = $("#" + modal).iziModal('getGroup'),
          e = event || window.event,
          target = e.target || e.srcElement,
          modals = {};

      if (modal !== undefined && group.name !== undefined && !e.ctrlKey && !e.metaKey && !e.altKey && target.tagName.toUpperCase() !== 'INPUT' && target.tagName.toUpperCase() != 'TEXTAREA') {
        //&& $(e.target).is('body')
        if (e.keyCode === 37) {
          // left
          $("#" + modal).iziModal('prev', e);
        } else if (e.keyCode === 39) {
          // right
          $("#" + modal).iziModal('next', e);
        }
      }
    }
  });

  $.fn[PLUGIN_NAME] = function (option, args) {
    if (!$(this).length && _typeof(option) == "object") {
      var newEL = {
        $el: document.createElement("div"),
        id: this.selector.split('#'),
        "class": this.selector.split('.')
      };

      if (newEL.id.length > 1) {
        try {
          newEL.$el = document.createElement(id[0]);
        } catch (exc) {}

        newEL.$el.id = this.selector.split('#')[1].trim();
      } else if (newEL["class"].length > 1) {
        try {
          newEL.$el = document.createElement(newEL["class"][0]);
        } catch (exc) {}

        for (var x = 1; x < newEL["class"].length; x++) {
          newEL.$el.classList.add(newEL["class"][x].trim());
        }
      }

      document.body.appendChild(newEL.$el);
      this.push($(this.selector));
    }

    var objs = this;

    for (var i = 0; i < objs.length; i++) {
      var $this = $(objs[i]);
      var data = $this.data(PLUGIN_NAME);
      var options = $.extend({}, $.fn[PLUGIN_NAME].defaults, $this.data(), _typeof(option) == 'object' && option);

      if (!data && (!option || _typeof(option) == 'object')) {
        $this.data(PLUGIN_NAME, data = new iziModal($this, options));
      } else if (typeof option == 'string' && typeof data != 'undefined') {
        return data[option].apply(data, [].concat(args));
      }

      if (options.autoOpen) {
        // Automatically open the modal if autoOpen setted true or ms
        if (!isNaN(parseInt(options.autoOpen))) {
          setTimeout(function () {
            data.open();
          }, options.autoOpen);
        } else if (options.autoOpen === true) {
          data.open();
        }

        window.$iziModal.autoOpen++;
      }
    }

    return this;
  };

  $.fn[PLUGIN_NAME].defaults = {
    title: '',
    subtitle: '',
    headerColor: '#88A0B9',
    background: null,
    theme: '',
    // light
    icon: null,
    iconText: null,
    iconColor: '',
    rtl: false,
    width: 600,
    top: null,
    bottom: null,
    borderBottom: true,
    padding: 0,
    radius: 3,
    zindex: 999,
    iframe: false,
    iframeHeight: 400,
    iframeURL: null,
    focusInput: true,
    group: '',
    loop: false,
    navigateCaption: true,
    navigateArrows: true,
    // Boolean, 'closeToModal', 'closeScreenEdge'
    history: false,
    restoreDefaultContent: false,
    autoOpen: 0,
    // Boolean, Number
    bodyOverflow: false,
    fullscreen: false,
    openFullscreen: false,
    closeOnEscape: true,
    closeButton: true,
    appendTo: 'body',
    // or false
    appendToOverlay: 'body',
    // or false
    overlay: true,
    overlayClose: true,
    overlayColor: 'rgba(0, 0, 0, 0.4)',
    timeout: false,
    timeoutProgressbar: false,
    pauseOnHover: false,
    timeoutProgressbarColor: 'rgba(255,255,255,0.5)',
    transitionIn: 'comingIn',
    // comingIn, bounceInDown, bounceInUp, fadeInDown, fadeInUp, fadeInLeft, fadeInRight, flipInX
    transitionOut: 'comingOut',
    // comingOut, bounceOutDown, bounceOutUp, fadeOutDown, fadeOutUp, , fadeOutLeft, fadeOutRight, flipOutX
    transitionInOverlay: 'fadeIn',
    transitionOutOverlay: 'fadeOut',
    onFullscreen: function onFullscreen() {},
    onResize: function onResize() {},
    onOpening: function onOpening() {},
    onOpened: function onOpened() {},
    onClosing: function onClosing() {},
    onClosed: function onClosed() {},
    afterRender: function afterRender() {}
  };
  $.fn[PLUGIN_NAME].Constructor = iziModal;
  return $.fn.iziModal;
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9tYWluLXNsaWRlci9tYWluLXNsaWRlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9wYXJ0bmVycy9wYXJ0bmVycy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0cy9iYXNlLWNvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0cy9iYXNlLXBhZ2UuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvcGFnZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0cy92ZW5kb3IvYXV0b3NpemUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdHMvdmVuZG9yL2l6aW1vZGFsLmpzIl0sIm5hbWVzIjpbIkhlYWRlckNvbXBvbmVudCIsImhhbWJ1cmdlckJ1dHRvbiIsIiRlbGVtZW50IiwiZmluZCIsIm5hdmlnYXRpb24iLCJvbiIsInRvZ2dsZUNsYXNzIiwiQmFzZUNvbXBvbmVudCIsIk1haW5TbGlkZXJDb21wb25lbnQiLCJzbGljayIsInNsaWRlc1RvU2hvdyIsInNsaWRlc1RvU2Nyb2xsIiwiYXV0b3BsYXkiLCJhcnJvd3MiLCJhdXRvcGxheVNwZWVkIiwiaW5maW5pdGUiLCJQYXJ0bmVyc0NvbXBvbmVudCIsInNsaWRlciIsInByZXZBcnJvdyIsIm5leHRBcnJvdyIsImxhenlMb2FkIiwicmVzcG9uc2l2ZSIsImJyZWFrcG9pbnQiLCJzZXR0aW5ncyIsImVsZW1lbnQiLCIkIiwiaW5pdCIsIkJhc2VQYWdlIiwid2luZG93IiwicmVhZHkiLCJJbmRleFBhZ2UiLCJlYWNoIiwiaSIsImVsIiwicGFnZSIsIlBsdWdpbnMiLCJBdXRvc2l6ZUlucHV0T3B0aW9ucyIsInNwYWNlIiwiQXV0b3NpemVJbnB1dCIsImlucHV0Iiwib3B0aW9ucyIsIl90aGlzIiwiX2lucHV0IiwiX29wdGlvbnMiLCJleHRlbmQiLCJnZXREZWZhdWx0T3B0aW9ucyIsIl9taXJyb3IiLCJ2YWwiLCJzdHlsZSIsImNzcyIsImFwcGVuZCIsImUiLCJ1cGRhdGUiLCJwcm90b3R5cGUiLCJnZXRPcHRpb25zIiwidmFsdWUiLCJ0ZXh0IiwibmV3V2lkdGgiLCJ3aWR0aCIsIl9kZWZhdWx0T3B0aW9ucyIsImdldEluc3RhbmNlS2V5IiwicGx1Z2luRGF0YUF0dHJpYnV0ZU5hbWUiLCJ2YWxpZFR5cGVzIiwiZm4iLCJhdXRvc2l6ZUlucHV0IiwidGFnTmFtZSIsImluQXJyYXkiLCJ0eXBlIiwiJHRoaXMiLCJkYXRhIiwidW5kZWZpbmVkIiwialF1ZXJ5IiwiZmFjdG9yeSIsImRlZmluZSIsIiR3aW5kb3ciLCIkZG9jdW1lbnQiLCJkb2N1bWVudCIsIlBMVUdJTl9OQU1FIiwiU1RBVEVTIiwiQ0xPU0lORyIsIkNMT1NFRCIsIk9QRU5JTkciLCJPUEVORUQiLCJERVNUUk9ZRUQiLCJ3aGljaEFuaW1hdGlvbkV2ZW50IiwidCIsImNyZWF0ZUVsZW1lbnQiLCJhbmltYXRpb25zIiwiaXNJRSIsInZlcnNpb24iLCJuYXZpZ2F0b3IiLCJhcHBWZXJzaW9uIiwiaW5kZXhPZiIsInVzZXJBZ2VudCIsImNsZWFyVmFsdWUiLCJzZXBhcmF0b3JzIiwicGFyc2VJbnQiLCJTdHJpbmciLCJzcGxpdCIsImFuaW1hdGlvbkV2ZW50IiwiaXNNb2JpbGUiLCJ0ZXN0IiwiJGl6aU1vZGFsIiwiYXV0b09wZW4iLCJoaXN0b3J5IiwiaXppTW9kYWwiLCJjb25zdHJ1Y3RvciIsInRoYXQiLCJpZCIsIk1hdGgiLCJmbG9vciIsInJhbmRvbSIsImF0dHIiLCJjbGFzc2VzIiwiY29udGVudCIsImh0bWwiLCJzdGF0ZSIsInRpbWVyIiwidGltZXJUaW1lb3V0IiwicHJvZ3Jlc3NCYXIiLCJpc1BhdXNlZCIsImlzRnVsbHNjcmVlbiIsImhlYWRlckhlaWdodCIsIm1vZGFsSGVpZ2h0IiwiJG92ZXJsYXkiLCJvdmVybGF5Q29sb3IiLCIkbmF2aWdhdGUiLCJncm91cCIsIm5hbWUiLCJpbmRleCIsImlkcyIsImhhc0NsYXNzIiwiYWRkQ2xhc3MiLCJsb29wIiwiRnVuY3Rpb24iLCJleGMiLCJhcHBlbmRUbyIsImlmcmFtZSIsImlmcmFtZUhlaWdodCIsImJhY2tncm91bmQiLCIkd3JhcCIsInppbmRleCIsImlzTmFOIiwicmFkaXVzIiwicGFkZGluZyIsInRoZW1lIiwicnRsIiwib3BlbkZ1bGxzY3JlZW4iLCJjcmVhdGVIZWFkZXIiLCJyZWNhbGNXaWR0aCIsInJlY2FsY1ZlcnRpY2FsUG9zIiwiYWZ0ZXJSZW5kZXIiLCIkaGVhZGVyIiwidGl0bGUiLCJzdWJ0aXRsZSIsImNsb3NlQnV0dG9uIiwiZnVsbHNjcmVlbiIsInRpbWVvdXRQcm9ncmVzc2JhciIsInRpbWVvdXQiLCJwcmVwZW5kIiwidGltZW91dFByb2dyZXNzYmFyQ29sb3IiLCJoZWFkZXJDb2xvciIsImJvcmRlckJvdHRvbSIsImljb24iLCJpY29uVGV4dCIsImljb25Db2xvciIsInNldEdyb3VwIiwiZ3JvdXBOYW1lIiwiY291bnQiLCJwdXNoIiwidG9nZ2xlIiwiY2xvc2UiLCJvcGVuIiwicGFyYW0iLCJtb2RhbCIsInVybEhhc2giLCJvbGRUaXRsZSIsImxvY2F0aW9uIiwiaGFzaCIsIm9wZW5lZCIsInRyaWdnZXIiLCJvbk9wZW5lZCIsImJpbmRFdmVudHMiLCJvZmYiLCJwcmV2ZW50RGVmYXVsdCIsInRyYW5zaXRpb24iLCJjdXJyZW50VGFyZ2V0IiwicmVtb3ZlQ2xhc3MiLCJvbkZ1bGxzY3JlZW4iLCJuZXh0IiwicHJldiIsInBhcmVudCIsImhyZWYiLCJpZnJhbWVVUkwiLCJFcnJvciIsImJvZHlPdmVyZmxvdyIsIm9uT3BlbmluZyIsImxlbmd0aCIsIm5hdmlnYXRlQ2FwdGlvbiIsInNob3ciLCJtb2RhbFdpZHRoIiwib3V0ZXJXaWR0aCIsIm5hdmlnYXRlQXJyb3dzIiwiaGlkZSIsIm92ZXJsYXkiLCJhcHBlbmRUb092ZXJsYXkiLCJ0cmFuc2l0aW9uSW5PdmVybGF5IiwidHJhbnNpdGlvbkluIiwib25lIiwicGF1c2VPbkhvdmVyIiwiZXZlbnQiLCJoaWRlRXRhIiwibWF4SGlkZVRpbWUiLCJjdXJyZW50VGltZSIsIkRhdGUiLCJnZXRUaW1lIiwidXBkYXRlUHJvZ3Jlc3MiLCJwZXJjZW50YWdlIiwicGFyc2VGbG9hdCIsInNldEludGVydmFsIiwic2V0VGltZW91dCIsIm92ZXJsYXlDbG9zZSIsInRyYW5zaXRpb25PdXQiLCJjbGljayIsImZvY3VzSW5wdXQiLCJmb2N1cyIsInVwZGF0ZVRpbWVyIiwicmVjYWxjTGF5b3V0IiwiY2xvc2VPbkVzY2FwZSIsImtleUNvZGUiLCJjbG9zZWQiLCJvbkNsb3NlZCIsInJlc3RvcmVEZWZhdWx0Q29udGVudCIsImNsZWFyVGltZW91dCIsIm9uQ2xvc2luZyIsInJlbW92ZSIsImpvaW4iLCJ0cmFuc2l0aW9uT3V0T3ZlcmxheSIsIm1vZGFscyIsIm91dCIsImxvZyIsImRlc3Ryb3kiLCJFdmVudCIsInJlbW92ZURhdGEiLCJnZXRTdGF0ZSIsImdldEdyb3VwIiwic2V0V2lkdGgiLCJzZXRUb3AiLCJ0b3AiLCJzZXRCb3R0b20iLCJib3R0b20iLCJzZXRIZWFkZXIiLCJzdGF0dXMiLCJzZXRUaXRsZSIsInNldFN1YnRpdGxlIiwic2V0SWNvbiIsInNldEljb25UZXh0Iiwic2V0SGVhZGVyQ29sb3IiLCJzZXRCYWNrZ3JvdW5kIiwic2V0WmluZGV4IiwiekluZGV4Iiwic2V0RnVsbHNjcmVlbiIsInNldENvbnRlbnQiLCJyZXBsYWNlIiwic2V0VHJhbnNpdGlvbkluIiwic2V0VHJhbnNpdGlvbk91dCIsInJlc2V0Q29udGVudCIsInN0YXJ0TG9hZGluZyIsImJvcmRlclJhZGl1cyIsInN0b3BMb2FkaW5nIiwiJGxvYWRlciIsInRvU3RyaW5nIiwibGVmdCIsIm1hcmdpbkxlZnQiLCJmaXJzdCIsImJvcmRlclRvcFJpZ2h0UmFkaXVzIiwiYm9yZGVyVG9wTGVmdFJhZGl1cyIsIm1hcmdpblRvcCIsImJvcmRlckJvdHRvbVJpZ2h0UmFkaXVzIiwiYm9yZGVyQm90dG9tTGVmdFJhZGl1cyIsIm1hcmdpbkJvdHRvbSIsIndpbmRvd0hlaWdodCIsImhlaWdodCIsIm91dGVySGVpZ2h0IiwiY29udGVudEhlaWdodCIsInNjcm9sbEhlaWdodCIsIndyYXBwZXJIZWlnaHQiLCJpbm5lckhlaWdodCIsIm1vZGFsTWFyZ2luIiwic2Nyb2xsVG9wIiwiYm9yZGVyU2l6ZSIsImlzIiwib25SZXNpemUiLCJyZWNhbGNCdXR0b25zIiwiYXBwbHlTY3JvbGwiLCJhcHBseVNoYWRvdyIsIndpZHRoQnV0dG9ucyIsImlubmVyV2lkdGgiLCJtb2RhbEhhc2giLCJvcGVuTW9kYWwiLCJ0YXJnZXQiLCJzcmNFbGVtZW50IiwiY3RybEtleSIsIm1ldGFLZXkiLCJhbHRLZXkiLCJ0b1VwcGVyQ2FzZSIsIm9wdGlvbiIsImFyZ3MiLCJuZXdFTCIsIiRlbCIsInNlbGVjdG9yIiwidHJpbSIsIngiLCJjbGFzc0xpc3QiLCJhZGQiLCJib2R5IiwiYXBwZW5kQ2hpbGQiLCJvYmpzIiwiZGVmYXVsdHMiLCJhcHBseSIsImNvbmNhdCIsIkNvbnN0cnVjdG9yIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnQkFBUSxvQkFBb0I7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBaUIsNEJBQTRCO0FBQzdDO0FBQ0E7QUFDQSwwQkFBa0IsMkJBQTJCO0FBQzdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBZ0IsdUJBQXVCO0FBQ3ZDOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdEpBO0FBRUE7O0lBRXFCQSxlOzs7Ozs7Ozs7Ozs7OzJCQUVWO0FBQ0gsVUFBTUMsZUFBZSxHQUFHLEtBQUtDLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixlQUFuQixDQUF4QjtBQUNBLFVBQU1DLFVBQVUsR0FBRyxLQUFLRixRQUFMLENBQWNDLElBQWQsQ0FBbUIsZ0JBQW5CLENBQW5CO0FBRUFGLHFCQUFlLENBQUNJLEVBQWhCLENBQW1CLE9BQW5CLEVBQTRCLFlBQUs7QUFDN0JELGtCQUFVLENBQUNFLFdBQVgsQ0FBdUIsZ0NBQXZCO0FBQ0FMLHVCQUFlLENBQUNLLFdBQWhCLENBQTRCLDBCQUE1QjtBQUNILE9BSEQ7QUFLSDs7OztFQVh3Q0MsK0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0o3QztBQUVBO0FBQ0E7O0lBRXFCQyxtQjs7Ozs7Ozs7Ozs7OzsyQkFFVjtBQUVOLFdBQUtOLFFBQUwsQ0FBY08sS0FBZCxDQUFvQjtBQUNuQkMsb0JBQVksRUFBRSxDQURLO0FBRW5CQyxzQkFBYyxFQUFFLENBRkc7QUFHbkI7QUFDQTtBQUNBQyxnQkFBUSxFQUFFLElBTFM7QUFNbkJDLGNBQU0sRUFBRSxLQU5XO0FBT25CQyxxQkFBYSxFQUFFLElBUEk7QUFRbkJDLGdCQUFRLEVBQUU7QUFSUyxPQUFwQjtBQVlBOzs7O0VBaEI0Q1IsK0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0xqRDtBQUVBO0FBQ0E7O0lBRXFCUyxpQjs7Ozs7Ozs7Ozs7OzsyQkFFVjtBQUNILFVBQU1DLE1BQU0sR0FBRyxLQUFLZixRQUFMLENBQWNDLElBQWQsQ0FBbUIsb0JBQW5CLENBQWY7QUFFQWMsWUFBTSxDQUFDUixLQUFQLENBQWE7QUFDVEMsb0JBQVksRUFBRSxDQURMO0FBRVRDLHNCQUFjLEVBQUUsQ0FGUDtBQUdUTyxpQkFBUyxFQUFFLDhGQUhGO0FBSVRDLGlCQUFTLEVBQUUsK0ZBSkY7QUFLVFAsZ0JBQVEsRUFBRSxJQUxEO0FBTVRFLHFCQUFhLEVBQUUsSUFOTjtBQU9UQyxnQkFBUSxFQUFFLElBUEQ7QUFRVEssZ0JBQVEsRUFBRSxVQVJEO0FBU1RDLGtCQUFVLEVBQUUsQ0FDUjtBQUNJQyxvQkFBVSxFQUFFLElBRGhCO0FBRUlDLGtCQUFRLEVBQUU7QUFDTmIsd0JBQVksRUFBRSxDQURSO0FBRU5DLDBCQUFjLEVBQUU7QUFGVjtBQUZkLFNBRFEsRUFRUjtBQUNJVyxvQkFBVSxFQUFFLEdBRGhCO0FBRUlDLGtCQUFRLEVBQUU7QUFDTmIsd0JBQVksRUFBRSxDQURSO0FBRU5DLDBCQUFjLEVBQUUsQ0FGVjtBQUdOSSxvQkFBUSxFQUFFLElBSEo7QUFJTkYsa0JBQU0sRUFBRTtBQUpGO0FBRmQsU0FSUSxFQWlCUjtBQUNJUyxvQkFBVSxFQUFFLEdBRGhCO0FBRUlDLGtCQUFRLEVBQUU7QUFDTmIsd0JBQVksRUFBRSxDQURSO0FBRU5DLDBCQUFjLEVBQUUsQ0FGVjtBQUdOSSxvQkFBUSxFQUFFLElBSEo7QUFJTkYsa0JBQU0sRUFBRTtBQUpGO0FBRmQsU0FqQlE7QUFUSCxPQUFiO0FBdUNIOzs7O0VBNUMwQ04sK0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0wvQzs7SUFFcUJBLGE7OztBQUNqQix5QkFBWWlCLE9BQVosRUFBcUI7QUFBQTs7QUFDakIsU0FBS3RCLFFBQUwsR0FBZ0J1Qix5REFBQyxDQUFDRCxPQUFELENBQWpCO0FBQ0EsU0FBS0UsSUFBTDtBQUNIOzs7OzJCQUVNLENBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNSYjs7SUFFTUMsUTs7O0FBQ0wsb0JBQVlILE9BQVosRUFBcUI7QUFBQTs7QUFBQTs7QUFDcEJDLDZEQUFDLENBQUNHLE1BQUQsQ0FBRCxDQUFVQyxLQUFWLENBQWdCLFVBQUNKLENBQUQsRUFBTztBQUN0QixXQUFJLENBQUN2QixRQUFMLEdBQWdCdUIsQ0FBQyxDQUFDLE1BQUQsQ0FBakI7O0FBQ0EsV0FBSSxDQUFDQyxJQUFMO0FBQ0EsS0FIRDtBQUlBOzs7OzJCQUVNLENBQ047QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWkY7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBOztJQUVNSSxTOzs7Ozs7Ozs7Ozs7OzJCQUNLO0FBQ0gsV0FBSzVCLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixZQUFuQixFQUFpQzRCLElBQWpDLENBQXNDLFVBQUNDLENBQUQsRUFBSUMsRUFBSixFQUFXO0FBQzdDLFlBQUlqQyxvRUFBSixDQUFvQmlDLEVBQXBCO0FBQ0gsT0FGRDtBQUlBLFdBQUsvQixRQUFMLENBQWNDLElBQWQsQ0FBbUIsZ0JBQW5CLEVBQXFDNEIsSUFBckMsQ0FBMEMsVUFBQ0MsQ0FBRCxFQUFJQyxFQUFKLEVBQVc7QUFDakQsWUFBSXpCLDhFQUFKLENBQXdCeUIsRUFBeEI7QUFDSCxPQUZEO0FBSUEsV0FBSy9CLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixjQUFuQixFQUFtQzRCLElBQW5DLENBQXdDLFVBQUNDLENBQUQsRUFBSUMsRUFBSixFQUFXO0FBQy9DLFlBQUlqQix3RUFBSixDQUFzQmlCLEVBQXRCO0FBQ0gsT0FGRDtBQU1IOzs7O0VBaEJtQk4sbUQ7O0FBbUJ4QixJQUFNTyxJQUFJLEdBQUcsSUFBSUosU0FBSixFQUFiLEM7Ozs7Ozs7Ozs7O0FDOUJBLHFEQUFJSyxPQUFKOztBQUNBLENBQUMsVUFBVUEsT0FBVixFQUFtQjtBQUNoQixNQUFJQyxvQkFBb0IsR0FBSSxZQUFZO0FBQ3BDLGFBQVNBLG9CQUFULENBQThCQyxLQUE5QixFQUFxQztBQUNqQyxVQUFJLE9BQU9BLEtBQVAsS0FBaUIsV0FBckIsRUFBa0M7QUFBRUEsYUFBSyxHQUFHLEVBQVI7QUFBYTs7QUFDakQsV0FBS0EsS0FBTCxHQUFhQSxLQUFiO0FBQ0g7O0FBQ0QsV0FBT0Qsb0JBQVA7QUFDSCxHQU4wQixFQUEzQjs7QUFPQUQsU0FBTyxDQUFDQyxvQkFBUixHQUErQkEsb0JBQS9COztBQUVBLE1BQUlFLGFBQWEsR0FBSSxZQUFZO0FBQzdCLGFBQVNBLGFBQVQsQ0FBdUJDLEtBQXZCLEVBQThCQyxPQUE5QixFQUF1QztBQUNuQyxVQUFJQyxLQUFLLEdBQUcsSUFBWjs7QUFDQSxXQUFLQyxNQUFMLEdBQWNqQixDQUFDLENBQUNjLEtBQUQsQ0FBZjtBQUNBLFdBQUtJLFFBQUwsR0FBZ0JsQixDQUFDLENBQUNtQixNQUFGLENBQVMsRUFBVCxFQUFhTixhQUFhLENBQUNPLGlCQUFkLEVBQWIsRUFBZ0RMLE9BQWhELENBQWhCLENBSG1DLENBS25DOztBQUNBLFdBQUtNLE9BQUwsR0FBZXJCLENBQUMsQ0FBQyx5RUFBRCxDQUFoQixDQU5tQyxDQVFuQzs7QUFDQUEsT0FBQyxDQUFDTSxJQUFGLENBQU8sQ0FBQyxZQUFELEVBQWUsVUFBZixFQUEyQixZQUEzQixFQUF5QyxXQUF6QyxFQUFzRCxlQUF0RCxFQUF1RSxlQUF2RSxFQUF3RixhQUF4RixFQUF1RyxZQUF2RyxDQUFQLEVBQTZILFVBQVVDLENBQVYsRUFBYWUsR0FBYixFQUFrQjtBQUMzSU4sYUFBSyxDQUFDSyxPQUFOLENBQWMsQ0FBZCxFQUFpQkUsS0FBakIsQ0FBdUJELEdBQXZCLElBQThCTixLQUFLLENBQUNDLE1BQU4sQ0FBYU8sR0FBYixDQUFpQkYsR0FBakIsQ0FBOUI7QUFDSCxPQUZEO0FBR0F0QixPQUFDLENBQUMsTUFBRCxDQUFELENBQVV5QixNQUFWLENBQWlCLEtBQUtKLE9BQXRCLEVBWm1DLENBY25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQSxXQUFLSixNQUFMLENBQVlyQyxFQUFaLENBQWUsMkNBQWYsRUFBNEQsVUFBVThDLENBQVYsRUFBYTtBQUNyRVYsYUFBSyxDQUFDVyxNQUFOO0FBQ0gsT0FGRCxFQXBCbUMsQ0F3Qm5DOzs7QUFDQSxPQUFDLFlBQVk7QUFDVFgsYUFBSyxDQUFDVyxNQUFOO0FBQ0gsT0FGRDtBQUdIOztBQUNEZCxpQkFBYSxDQUFDZSxTQUFkLENBQXdCQyxVQUF4QixHQUFxQyxZQUFZO0FBQzdDLGFBQU8sS0FBS1gsUUFBWjtBQUNILEtBRkQ7O0FBSUFMLGlCQUFhLENBQUNlLFNBQWQsQ0FBd0JELE1BQXhCLEdBQWlDLFlBQVk7QUFDekMsVUFBSUcsS0FBSyxHQUFHLEtBQUtiLE1BQUwsQ0FBWUssR0FBWixNQUFxQixFQUFqQzs7QUFFQSxVQUFJUSxLQUFLLEtBQUssS0FBS1QsT0FBTCxDQUFhVSxJQUFiLEVBQWQsRUFBbUM7QUFDL0I7QUFDQTtBQUNILE9BTndDLENBUXpDOzs7QUFDQSxXQUFLVixPQUFMLENBQWFVLElBQWIsQ0FBa0JELEtBQWxCLEVBVHlDLENBV3pDOzs7QUFDQSxVQUFJRSxRQUFRLEdBQUcsS0FBS1gsT0FBTCxDQUFhWSxLQUFiLEtBQXVCLEtBQUtmLFFBQUwsQ0FBY04sS0FBcEQsQ0FaeUMsQ0FjekM7OztBQUNBLFdBQUtLLE1BQUwsQ0FBWWdCLEtBQVosQ0FBa0JELFFBQWxCO0FBQ0gsS0FoQkQ7O0FBa0JBbkIsaUJBQWEsQ0FBQ08saUJBQWQsR0FBa0MsWUFBWTtBQUMxQyxhQUFPLEtBQUtjLGVBQVo7QUFDSCxLQUZEOztBQUlBckIsaUJBQWEsQ0FBQ3NCLGNBQWQsR0FBK0IsWUFBWTtBQUN2QztBQUNBLGFBQU8sdUJBQVA7QUFDSCxLQUhEOztBQUlBdEIsaUJBQWEsQ0FBQ3FCLGVBQWQsR0FBZ0MsSUFBSXZCLG9CQUFKLEVBQWhDO0FBQ0EsV0FBT0UsYUFBUDtBQUNILEdBOURtQixFQUFwQjs7QUErREFILFNBQU8sQ0FBQ0csYUFBUixHQUF3QkEsYUFBeEIsQ0F6RWdCLENBMkVoQjs7QUFDQSxHQUFDLFVBQVViLENBQVYsRUFBYTtBQUNWLFFBQUlvQyx1QkFBdUIsR0FBRyxnQkFBOUI7QUFDQSxRQUFJQyxVQUFVLEdBQUcsQ0FBQyxNQUFELEVBQVMsVUFBVCxFQUFxQixRQUFyQixFQUErQixLQUEvQixFQUFzQyxLQUF0QyxFQUE2QyxPQUE3QyxFQUFzRCxRQUF0RCxDQUFqQixDQUZVLENBSVY7O0FBQ0FyQyxLQUFDLENBQUNzQyxFQUFGLENBQUtDLGFBQUwsR0FBcUIsVUFBVXhCLE9BQVYsRUFBbUI7QUFDcEMsYUFBTyxLQUFLVCxJQUFMLENBQVUsWUFBWTtBQUN6QjtBQUNBO0FBQ0EsWUFBSSxFQUFFLEtBQUtrQyxPQUFMLElBQWdCLE9BQWhCLElBQTJCeEMsQ0FBQyxDQUFDeUMsT0FBRixDQUFVLEtBQUtDLElBQWYsRUFBcUJMLFVBQXJCLElBQW1DLENBQUMsQ0FBakUsQ0FBSixFQUF5RTtBQUNyRTtBQUNBO0FBQ0g7O0FBRUQsWUFBSU0sS0FBSyxHQUFHM0MsQ0FBQyxDQUFDLElBQUQsQ0FBYjs7QUFFQSxZQUFJLENBQUMyQyxLQUFLLENBQUNDLElBQU4sQ0FBV2xDLE9BQU8sQ0FBQ0csYUFBUixDQUFzQnNCLGNBQXRCLEVBQVgsQ0FBTCxFQUF5RDtBQUNyRDtBQUNBLGNBQUlwQixPQUFPLElBQUk4QixTQUFmLEVBQTBCO0FBQ3RCO0FBQ0E5QixtQkFBTyxHQUFHNEIsS0FBSyxDQUFDQyxJQUFOLENBQVdSLHVCQUFYLENBQVY7QUFDSCxXQUxvRCxDQU9yRDs7O0FBQ0FPLGVBQUssQ0FBQ0MsSUFBTixDQUFXbEMsT0FBTyxDQUFDRyxhQUFSLENBQXNCc0IsY0FBdEIsRUFBWCxFQUFtRCxJQUFJekIsT0FBTyxDQUFDRyxhQUFaLENBQTBCLElBQTFCLEVBQWdDRSxPQUFoQyxDQUFuRDtBQUNIO0FBQ0osT0FwQk0sQ0FBUDtBQXFCSCxLQXRCRCxDQUxVLENBNkJWOzs7QUFDQWYsS0FBQyxDQUFDLFlBQVk7QUFDVjtBQUNBQSxPQUFDLENBQUMsZ0JBQWdCb0MsdUJBQWhCLEdBQTBDLEdBQTNDLENBQUQsQ0FBaURHLGFBQWpEO0FBQ0gsS0FIQSxDQUFELENBOUJVLENBa0NWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSCxHQXZDRCxFQXVDR08sTUF2Q0g7QUF3Q0gsQ0FwSEQsRUFvSEdwQyxPQUFPLEtBQUtBLE9BQU8sR0FBRyxFQUFmLENBcEhWLEU7Ozs7Ozs7Ozs7Ozs7O0FDREE7Ozs7O0FBS0MsV0FBVXFDLE9BQVYsRUFBbUI7QUFDaEIsTUFBSSxJQUFKLEVBQWdEO0FBQzVDQyxxQ0FBTyxDQUFDLHlFQUFELENBQUQsb0NBQWFELE9BQWI7QUFBQTtBQUFBO0FBQUEsb0dBQU47QUFDSCxHQUZELE1BRU8sRUFlTjtBQUNKLENBbkJBLEVBbUJDLFVBQVUvQyxDQUFWLEVBQWE7QUFFWCxNQUFJaUQsT0FBTyxHQUFHakQsQ0FBQyxDQUFDRyxNQUFELENBQWY7QUFBQSxNQUNJK0MsU0FBUyxHQUFHbEQsQ0FBQyxDQUFDbUQsUUFBRCxDQURqQjtBQUFBLE1BRUlDLFdBQVcsR0FBRyxVQUZsQjtBQUFBLE1BR0lDLE1BQU0sR0FBRztBQUNMQyxXQUFPLEVBQUUsU0FESjtBQUVMQyxVQUFNLEVBQUUsUUFGSDtBQUdMQyxXQUFPLEVBQUUsU0FISjtBQUlMQyxVQUFNLEVBQUUsUUFKSDtBQUtMQyxhQUFTLEVBQUU7QUFMTixHQUhiOztBQVdBLFdBQVNDLG1CQUFULEdBQThCO0FBQzFCLFFBQUlDLENBQUo7QUFBQSxRQUNJcEQsRUFBRSxHQUFHMkMsUUFBUSxDQUFDVSxhQUFULENBQXVCLGFBQXZCLENBRFQ7QUFBQSxRQUVJQyxVQUFVLEdBQUc7QUFDVCxtQkFBbUIsY0FEVjtBQUVULG9CQUFtQixlQUZWO0FBR1Qsc0JBQW1CLGNBSFY7QUFJVCx5QkFBbUI7QUFKVixLQUZqQjs7QUFRQSxTQUFLRixDQUFMLElBQVVFLFVBQVYsRUFBcUI7QUFDakIsVUFBSXRELEVBQUUsQ0FBQ2UsS0FBSCxDQUFTcUMsQ0FBVCxNQUFnQmYsU0FBcEIsRUFBOEI7QUFDMUIsZUFBT2lCLFVBQVUsQ0FBQ0YsQ0FBRCxDQUFqQjtBQUNIO0FBQ0o7QUFDSjs7QUFFRCxXQUFTRyxJQUFULENBQWNDLE9BQWQsRUFBdUI7QUFDbkIsUUFBR0EsT0FBTyxLQUFLLENBQWYsRUFBaUI7QUFDYixhQUFPQyxTQUFTLENBQUNDLFVBQVYsQ0FBcUJDLE9BQXJCLENBQTZCLFNBQTdCLE1BQTRDLENBQUMsQ0FBcEQ7QUFDSCxLQUZELE1BRU87QUFDSEMsZUFBUyxHQUFHSCxTQUFTLENBQUNHLFNBQXRCO0FBQ0EsYUFBT0EsU0FBUyxDQUFDRCxPQUFWLENBQWtCLE9BQWxCLElBQTZCLENBQUMsQ0FBOUIsSUFBbUNDLFNBQVMsQ0FBQ0QsT0FBVixDQUFrQixVQUFsQixJQUFnQyxDQUFDLENBQTNFO0FBQ0g7QUFDSjs7QUFFRCxXQUFTRSxVQUFULENBQW9CdkMsS0FBcEIsRUFBMEI7QUFDdEIsUUFBSXdDLFVBQVUsR0FBRyxrQkFBakI7QUFDQSxXQUFPQyxRQUFRLENBQUNDLE1BQU0sQ0FBQzFDLEtBQUQsQ0FBTixDQUFjMkMsS0FBZCxDQUFvQkgsVUFBcEIsRUFBZ0MsQ0FBaEMsQ0FBRCxDQUFmO0FBQ0g7O0FBRUQsTUFBSUksY0FBYyxHQUFHZixtQkFBbUIsRUFBeEM7QUFBQSxNQUNJZ0IsUUFBUSxHQUFJLE9BQU9DLElBQVAsQ0FBWVgsU0FBUyxDQUFDRyxTQUF0QixDQUFELEdBQXFDLElBQXJDLEdBQTRDLEtBRDNEO0FBR0FqRSxRQUFNLENBQUMwRSxTQUFQLEdBQW1CLEVBQW5CO0FBQ0ExRSxRQUFNLENBQUMwRSxTQUFQLENBQWlCQyxRQUFqQixHQUE0QixDQUE1QjtBQUNBM0UsUUFBTSxDQUFDMEUsU0FBUCxDQUFpQkUsT0FBakIsR0FBMkIsS0FBM0I7O0FBRUEsTUFBSUMsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBVWpGLE9BQVYsRUFBbUJnQixPQUFuQixFQUE0QjtBQUN2QyxTQUFLZCxJQUFMLENBQVVGLE9BQVYsRUFBbUJnQixPQUFuQjtBQUNILEdBRkQ7O0FBSUFpRSxVQUFRLENBQUNwRCxTQUFULEdBQXFCO0FBRWpCcUQsZUFBVyxFQUFFRCxRQUZJO0FBSWpCL0UsUUFBSSxFQUFFLGNBQVVGLE9BQVYsRUFBbUJnQixPQUFuQixFQUE0QjtBQUU5QixVQUFJbUUsSUFBSSxHQUFHLElBQVg7QUFDQSxXQUFLekcsUUFBTCxHQUFnQnVCLENBQUMsQ0FBQ0QsT0FBRCxDQUFqQjs7QUFFQSxVQUFHLEtBQUt0QixRQUFMLENBQWMsQ0FBZCxFQUFpQjBHLEVBQWpCLEtBQXdCdEMsU0FBeEIsSUFBcUMsS0FBS3BFLFFBQUwsQ0FBYyxDQUFkLEVBQWlCMEcsRUFBakIsS0FBd0IsRUFBaEUsRUFBbUU7QUFDL0QsYUFBS0EsRUFBTCxHQUFVLEtBQUsxRyxRQUFMLENBQWMsQ0FBZCxFQUFpQjBHLEVBQTNCO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsYUFBS0EsRUFBTCxHQUFVL0IsV0FBVyxHQUFDZ0MsSUFBSSxDQUFDQyxLQUFMLENBQVlELElBQUksQ0FBQ0UsTUFBTCxLQUFnQixRQUFqQixHQUE2QixDQUF4QyxDQUF0QjtBQUNBLGFBQUs3RyxRQUFMLENBQWM4RyxJQUFkLENBQW1CLElBQW5CLEVBQXlCLEtBQUtKLEVBQTlCO0FBQ0g7O0FBQ0QsV0FBS0ssT0FBTCxHQUFpQixLQUFLL0csUUFBTCxDQUFjOEcsSUFBZCxDQUFtQixPQUFuQixNQUFnQzFDLFNBQWxDLEdBQWdELEtBQUtwRSxRQUFMLENBQWM4RyxJQUFkLENBQW1CLE9BQW5CLENBQWhELEdBQThFLEVBQTdGO0FBQ0EsV0FBS0UsT0FBTCxHQUFlLEtBQUtoSCxRQUFMLENBQWNpSCxJQUFkLEVBQWY7QUFDQSxXQUFLQyxLQUFMLEdBQWF0QyxNQUFNLENBQUNFLE1BQXBCO0FBQ0EsV0FBS3hDLE9BQUwsR0FBZUEsT0FBZjtBQUNBLFdBQUtrQixLQUFMLEdBQWEsQ0FBYjtBQUNBLFdBQUsyRCxLQUFMLEdBQWEsSUFBYjtBQUNBLFdBQUtDLFlBQUwsR0FBb0IsSUFBcEI7QUFDQSxXQUFLQyxXQUFMLEdBQW1CLElBQW5CO0FBQ0EsV0FBS0MsUUFBTCxHQUFnQixLQUFoQjtBQUNBLFdBQUtDLFlBQUwsR0FBb0IsS0FBcEI7QUFDQSxXQUFLQyxZQUFMLEdBQW9CLENBQXBCO0FBQ0EsV0FBS0MsV0FBTCxHQUFtQixDQUFuQjtBQUNBLFdBQUtDLFFBQUwsR0FBZ0JuRyxDQUFDLENBQUMsaUJBQWVvRCxXQUFmLEdBQTJCLG9DQUEzQixHQUFnRXJDLE9BQU8sQ0FBQ3FGLFlBQXhFLEdBQXFGLFVBQXRGLENBQWpCO0FBQ0EsV0FBS0MsU0FBTCxHQUFpQnJHLENBQUMsQ0FBQyxpQkFBZW9ELFdBQWYsR0FBMkIseUJBQTNCLEdBQXFEQSxXQUFyRCxHQUFpRSw2Q0FBakUsR0FBK0dBLFdBQS9HLEdBQTJILDBDQUEzSCxHQUFzS0EsV0FBdEssR0FBa0wsaUNBQW5MLENBQWxCO0FBQ0EsV0FBS2tELEtBQUwsR0FBYTtBQUNUQyxZQUFJLEVBQUUsS0FBSzlILFFBQUwsQ0FBYzhHLElBQWQsQ0FBbUIsVUFBUW5DLFdBQVIsR0FBb0IsUUFBdkMsQ0FERztBQUVUb0QsYUFBSyxFQUFFLElBRkU7QUFHVEMsV0FBRyxFQUFFO0FBSEksT0FBYjtBQUtBLFdBQUtoSSxRQUFMLENBQWM4RyxJQUFkLENBQW1CLGFBQW5CLEVBQWtDLE1BQWxDO0FBQ0EsV0FBSzlHLFFBQUwsQ0FBYzhHLElBQWQsQ0FBbUIsaUJBQW5CLEVBQXNDLEtBQUtKLEVBQTNDO0FBQ0EsV0FBSzFHLFFBQUwsQ0FBYzhHLElBQWQsQ0FBbUIsTUFBbkIsRUFBMkIsUUFBM0I7O0FBRUEsVUFBSSxDQUFDLEtBQUs5RyxRQUFMLENBQWNpSSxRQUFkLENBQXVCLFVBQXZCLENBQUwsRUFBeUM7QUFDckMsYUFBS2pJLFFBQUwsQ0FBY2tJLFFBQWQsQ0FBdUIsVUFBdkI7QUFDSDs7QUFFRCxVQUFHLEtBQUtMLEtBQUwsQ0FBV0MsSUFBWCxLQUFvQjFELFNBQXBCLElBQWlDOUIsT0FBTyxDQUFDdUYsS0FBUixLQUFrQixFQUF0RCxFQUF5RDtBQUNyRCxhQUFLQSxLQUFMLENBQVdDLElBQVgsR0FBa0J4RixPQUFPLENBQUN1RixLQUExQjtBQUNBLGFBQUs3SCxRQUFMLENBQWM4RyxJQUFkLENBQW1CLFVBQVFuQyxXQUFSLEdBQW9CLFFBQXZDLEVBQWlEckMsT0FBTyxDQUFDdUYsS0FBekQ7QUFDSDs7QUFDRCxVQUFHLEtBQUt2RixPQUFMLENBQWE2RixJQUFiLEtBQXNCLElBQXpCLEVBQThCO0FBQzFCLGFBQUtuSSxRQUFMLENBQWM4RyxJQUFkLENBQW1CLFVBQVFuQyxXQUFSLEdBQW9CLE9BQXZDLEVBQWdELElBQWhEO0FBQ0g7O0FBRURwRCxPQUFDLENBQUNNLElBQUYsQ0FBUSxLQUFLUyxPQUFiLEVBQXVCLFVBQVN5RixLQUFULEVBQWdCbEYsR0FBaEIsRUFBcUI7QUFDeEMsWUFBSWlFLElBQUksR0FBR0wsSUFBSSxDQUFDekcsUUFBTCxDQUFjOEcsSUFBZCxDQUFtQixVQUFRbkMsV0FBUixHQUFvQixHQUFwQixHQUF3Qm9ELEtBQTNDLENBQVg7O0FBQ0EsWUFBSTtBQUNBLGNBQUcsUUFBT2pCLElBQVAsdUNBQUgsRUFBb0M7QUFFaEMsZ0JBQUdBLElBQUksS0FBSyxFQUFULElBQWVBLElBQUksSUFBSSxNQUExQixFQUFpQztBQUM3QnhFLHFCQUFPLENBQUN5RixLQUFELENBQVAsR0FBaUIsSUFBakI7QUFDSCxhQUZELE1BRU8sSUFBSWpCLElBQUksSUFBSSxPQUFaLEVBQXFCO0FBQ3hCeEUscUJBQU8sQ0FBQ3lGLEtBQUQsQ0FBUCxHQUFpQixLQUFqQjtBQUNILGFBRk0sTUFFQSxJQUFJLE9BQU9sRixHQUFQLElBQWMsVUFBbEIsRUFBOEI7QUFDakNQLHFCQUFPLENBQUN5RixLQUFELENBQVAsR0FBaUIsSUFBSUssUUFBSixDQUFhdEIsSUFBYixDQUFqQjtBQUNILGFBRk0sTUFFQTtBQUNIeEUscUJBQU8sQ0FBQ3lGLEtBQUQsQ0FBUCxHQUFpQmpCLElBQWpCO0FBQ0g7QUFDSjtBQUNKLFNBYkQsQ0FhRSxPQUFNdUIsR0FBTixFQUFVLENBQUU7QUFDakIsT0FoQkQ7O0FBa0JBLFVBQUcvRixPQUFPLENBQUNnRyxRQUFSLEtBQXFCLEtBQXhCLEVBQThCO0FBQzFCLGFBQUt0SSxRQUFMLENBQWNzSSxRQUFkLENBQXVCaEcsT0FBTyxDQUFDZ0csUUFBL0I7QUFDSDs7QUFFRCxVQUFJaEcsT0FBTyxDQUFDaUcsTUFBUixLQUFtQixJQUF2QixFQUE2QjtBQUN6QixhQUFLdkksUUFBTCxDQUFjaUgsSUFBZCxDQUFtQixpQkFBZXRDLFdBQWYsR0FBMkIscUJBQTNCLEdBQWlEQSxXQUFqRCxHQUE2RCwyQkFBN0QsR0FBeUZBLFdBQXpGLEdBQXFHLG9CQUFyRyxHQUE0SCxLQUFLcUMsT0FBakksR0FBMkksY0FBOUo7O0FBRUEsWUFBSTFFLE9BQU8sQ0FBQ2tHLFlBQVIsS0FBeUIsSUFBN0IsRUFBbUM7QUFDL0IsZUFBS3hJLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixNQUFJMEUsV0FBSixHQUFnQixTQUFuQyxFQUE4QzVCLEdBQTlDLENBQWtELFFBQWxELEVBQTREVCxPQUFPLENBQUNrRyxZQUFwRTtBQUNIO0FBQ0osT0FORCxNQU1PO0FBQ0gsYUFBS3hJLFFBQUwsQ0FBY2lILElBQWQsQ0FBbUIsaUJBQWV0QyxXQUFmLEdBQTJCLHFCQUEzQixHQUFpREEsV0FBakQsR0FBNkQsWUFBN0QsR0FBNEUsS0FBS3FDLE9BQWpGLEdBQTJGLGNBQTlHO0FBQ0g7O0FBRUQsVUFBSSxLQUFLMUUsT0FBTCxDQUFhbUcsVUFBYixLQUE0QixJQUFoQyxFQUFzQztBQUNsQyxhQUFLekksUUFBTCxDQUFjK0MsR0FBZCxDQUFrQixZQUFsQixFQUFnQyxLQUFLVCxPQUFMLENBQWFtRyxVQUE3QztBQUNIOztBQUVELFdBQUtDLEtBQUwsR0FBYSxLQUFLMUksUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLE9BQW5DLENBQWI7O0FBRUEsVUFBR3JDLE9BQU8sQ0FBQ3FHLE1BQVIsS0FBbUIsSUFBbkIsSUFBMkIsQ0FBQ0MsS0FBSyxDQUFDOUMsUUFBUSxDQUFDeEQsT0FBTyxDQUFDcUcsTUFBVCxDQUFULENBQXBDLEVBQWdFO0FBQzVELGFBQUszSSxRQUFMLENBQWMrQyxHQUFkLENBQWtCLFNBQWxCLEVBQTZCVCxPQUFPLENBQUNxRyxNQUFyQztBQUNBLGFBQUtmLFNBQUwsQ0FBZTdFLEdBQWYsQ0FBbUIsU0FBbkIsRUFBOEJULE9BQU8sQ0FBQ3FHLE1BQVIsR0FBZSxDQUE3QztBQUNBLGFBQUtqQixRQUFMLENBQWMzRSxHQUFkLENBQWtCLFNBQWxCLEVBQTZCVCxPQUFPLENBQUNxRyxNQUFSLEdBQWUsQ0FBNUM7QUFDSDs7QUFFRCxVQUFHckcsT0FBTyxDQUFDdUcsTUFBUixLQUFtQixFQUF0QixFQUF5QjtBQUNyQixhQUFLN0ksUUFBTCxDQUFjK0MsR0FBZCxDQUFrQixlQUFsQixFQUFtQ1QsT0FBTyxDQUFDdUcsTUFBM0M7QUFDSDs7QUFFRCxVQUFHdkcsT0FBTyxDQUFDd0csT0FBUixLQUFvQixFQUF2QixFQUEwQjtBQUN0QixhQUFLOUksUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLFVBQW5DLEVBQStDNUIsR0FBL0MsQ0FBbUQsU0FBbkQsRUFBOERULE9BQU8sQ0FBQ3dHLE9BQXRFO0FBQ0g7O0FBRUQsVUFBR3hHLE9BQU8sQ0FBQ3lHLEtBQVIsS0FBa0IsRUFBckIsRUFBd0I7QUFDcEIsWUFBR3pHLE9BQU8sQ0FBQ3lHLEtBQVIsS0FBa0IsT0FBckIsRUFBNkI7QUFDekIsZUFBSy9JLFFBQUwsQ0FBY2tJLFFBQWQsQ0FBdUJ2RCxXQUFXLEdBQUMsUUFBbkM7QUFDSCxTQUZELE1BRU87QUFDSCxlQUFLM0UsUUFBTCxDQUFja0ksUUFBZCxDQUF1QjVGLE9BQU8sQ0FBQ3lHLEtBQS9CO0FBQ0g7QUFDSjs7QUFFRCxVQUFHekcsT0FBTyxDQUFDMEcsR0FBUixLQUFnQixJQUFuQixFQUF5QjtBQUNyQixhQUFLaEosUUFBTCxDQUFja0ksUUFBZCxDQUF1QnZELFdBQVcsR0FBQyxNQUFuQztBQUNIOztBQUVELFVBQUdyQyxPQUFPLENBQUMyRyxjQUFSLEtBQTJCLElBQTlCLEVBQW1DO0FBQy9CLGFBQUsxQixZQUFMLEdBQW9CLElBQXBCO0FBQ0EsYUFBS3ZILFFBQUwsQ0FBY2tJLFFBQWQsQ0FBdUIsY0FBdkI7QUFDSDs7QUFFRCxXQUFLZ0IsWUFBTDtBQUNBLFdBQUtDLFdBQUw7QUFDQSxXQUFLQyxpQkFBTDs7QUFFQSxVQUFJM0MsSUFBSSxDQUFDbkUsT0FBTCxDQUFhK0csV0FBYixLQUE4QixPQUFPNUMsSUFBSSxDQUFDbkUsT0FBTCxDQUFhK0csV0FBcEIsS0FBcUMsVUFBckMsSUFBbUQsUUFBTzVDLElBQUksQ0FBQ25FLE9BQUwsQ0FBYStHLFdBQXBCLE1BQXFDLFFBQXRILENBQUosRUFBdUk7QUFDbkk1QyxZQUFJLENBQUNuRSxPQUFMLENBQWErRyxXQUFiLENBQXlCNUMsSUFBekI7QUFDSDtBQUVKLEtBL0hnQjtBQWlJakJ5QyxnQkFBWSxFQUFFLHdCQUFVO0FBRXBCLFdBQUtJLE9BQUwsR0FBZS9ILENBQUMsQ0FBQyxpQkFBZW9ELFdBQWYsR0FBMkIsc0JBQTNCLEdBQWtEQSxXQUFsRCxHQUE4RCxpQkFBOUQsR0FBa0YsS0FBS3JDLE9BQUwsQ0FBYWlILEtBQS9GLEdBQXVHLGlCQUF2RyxHQUF5SDVFLFdBQXpILEdBQXFJLG9CQUFySSxHQUE0SixLQUFLckMsT0FBTCxDQUFha0gsUUFBekssR0FBb0wsa0JBQXBMLEdBQXVNN0UsV0FBdk0sR0FBbU4sK0JBQXBOLENBQWhCOztBQUVBLFVBQUksS0FBS3JDLE9BQUwsQ0FBYW1ILFdBQWIsS0FBNkIsSUFBakMsRUFBdUM7QUFDbkMsYUFBS0gsT0FBTCxDQUFhckosSUFBYixDQUFrQixNQUFJMEUsV0FBSixHQUFnQixpQkFBbEMsRUFBcUQzQixNQUFyRCxDQUE0RCx5Q0FBdUMyQixXQUF2QyxHQUFtRCxVQUFuRCxHQUE4REEsV0FBOUQsR0FBMEUsc0JBQTFFLEdBQWlHQSxXQUFqRyxHQUE2RyxhQUF6SztBQUNIOztBQUVELFVBQUksS0FBS3JDLE9BQUwsQ0FBYW9ILFVBQWIsS0FBNEIsSUFBaEMsRUFBc0M7QUFDbEMsYUFBS0osT0FBTCxDQUFhckosSUFBYixDQUFrQixNQUFJMEUsV0FBSixHQUFnQixpQkFBbEMsRUFBcUQzQixNQUFyRCxDQUE0RCx5Q0FBdUMyQixXQUF2QyxHQUFtRCxVQUFuRCxHQUE4REEsV0FBOUQsR0FBMEUsMkJBQTFFLEdBQXNHQSxXQUF0RyxHQUFrSCxrQkFBOUs7QUFDSDs7QUFFRCxVQUFJLEtBQUtyQyxPQUFMLENBQWFxSCxrQkFBYixLQUFvQyxJQUFwQyxJQUE0QyxDQUFDZixLQUFLLENBQUM5QyxRQUFRLENBQUMsS0FBS3hELE9BQUwsQ0FBYXNILE9BQWQsQ0FBVCxDQUFsRCxJQUFzRixLQUFLdEgsT0FBTCxDQUFhc0gsT0FBYixLQUF5QixLQUEvRyxJQUF3SCxLQUFLdEgsT0FBTCxDQUFhc0gsT0FBYixLQUF5QixDQUFySixFQUF3SjtBQUNwSixhQUFLTixPQUFMLENBQWFPLE9BQWIsQ0FBcUIsaUJBQWVsRixXQUFmLEdBQTJCLDZDQUEzQixHQUF5RSxLQUFLckMsT0FBTCxDQUFhd0gsdUJBQXRGLEdBQThHLGdCQUFuSTtBQUNIOztBQUVELFVBQUksS0FBS3hILE9BQUwsQ0FBYWtILFFBQWIsS0FBMEIsRUFBOUIsRUFBa0M7QUFDOUIsYUFBS0YsT0FBTCxDQUFhcEIsUUFBYixDQUFzQnZELFdBQVcsR0FBQyxhQUFsQztBQUNIOztBQUVELFVBQUksS0FBS3JDLE9BQUwsQ0FBYWlILEtBQWIsS0FBdUIsRUFBM0IsRUFBK0I7QUFFM0IsWUFBSSxLQUFLakgsT0FBTCxDQUFheUgsV0FBYixLQUE2QixJQUFqQyxFQUF1QztBQUNuQyxjQUFHLEtBQUt6SCxPQUFMLENBQWEwSCxZQUFiLEtBQThCLElBQWpDLEVBQXNDO0FBQ2xDLGlCQUFLaEssUUFBTCxDQUFjK0MsR0FBZCxDQUFrQixlQUFsQixFQUFtQyxlQUFlLEtBQUtULE9BQUwsQ0FBYXlILFdBQTVCLEdBQTBDLEVBQTdFO0FBQ0g7O0FBQ0QsZUFBS1QsT0FBTCxDQUFhdkcsR0FBYixDQUFpQixZQUFqQixFQUErQixLQUFLVCxPQUFMLENBQWF5SCxXQUE1QztBQUNIOztBQUNELFlBQUksS0FBS3pILE9BQUwsQ0FBYTJILElBQWIsS0FBc0IsSUFBdEIsSUFBOEIsS0FBSzNILE9BQUwsQ0FBYTRILFFBQWIsS0FBMEIsSUFBNUQsRUFBaUU7QUFFN0QsZUFBS1osT0FBTCxDQUFhTyxPQUFiLENBQXFCLGVBQWFsRixXQUFiLEdBQXlCLG9CQUE5Qzs7QUFFQSxjQUFJLEtBQUtyQyxPQUFMLENBQWEySCxJQUFiLEtBQXNCLElBQTFCLEVBQWdDO0FBQzVCLGlCQUFLWCxPQUFMLENBQWFySixJQUFiLENBQWtCLE1BQUkwRSxXQUFKLEdBQWdCLGNBQWxDLEVBQWtEdUQsUUFBbEQsQ0FBMkQsS0FBSzVGLE9BQUwsQ0FBYTJILElBQXhFLEVBQThFbEgsR0FBOUUsQ0FBa0YsT0FBbEYsRUFBMkYsS0FBS1QsT0FBTCxDQUFhNkgsU0FBeEc7QUFDSDs7QUFDRCxjQUFJLEtBQUs3SCxPQUFMLENBQWE0SCxRQUFiLEtBQTBCLElBQTlCLEVBQW1DO0FBQy9CLGlCQUFLWixPQUFMLENBQWFySixJQUFiLENBQWtCLE1BQUkwRSxXQUFKLEdBQWdCLGNBQWxDLEVBQWtEc0MsSUFBbEQsQ0FBdUQsS0FBSzNFLE9BQUwsQ0FBYTRILFFBQXBFO0FBQ0g7QUFDSjs7QUFDRCxhQUFLbEssUUFBTCxDQUFjK0MsR0FBZCxDQUFrQixVQUFsQixFQUE4QixRQUE5QixFQUF3QzhHLE9BQXhDLENBQWdELEtBQUtQLE9BQXJEO0FBQ0g7QUFDSixLQTFLZ0I7QUE0S2pCYyxZQUFRLEVBQUUsa0JBQVNDLFNBQVQsRUFBbUI7QUFFekIsVUFBSTVELElBQUksR0FBRyxJQUFYO0FBQUEsVUFDSW9CLEtBQUssR0FBRyxLQUFLQSxLQUFMLENBQVdDLElBQVgsSUFBbUJ1QyxTQUQvQjtBQUVBLFdBQUt4QyxLQUFMLENBQVdHLEdBQVgsR0FBaUIsRUFBakI7O0FBRUEsVUFBSXFDLFNBQVMsS0FBS2pHLFNBQWQsSUFBMkJpRyxTQUFTLEtBQUssS0FBS3hDLEtBQUwsQ0FBV0MsSUFBeEQsRUFBNkQ7QUFDekRELGFBQUssR0FBR3dDLFNBQVI7QUFDQSxhQUFLeEMsS0FBTCxDQUFXQyxJQUFYLEdBQWtCRCxLQUFsQjtBQUNBLGFBQUs3SCxRQUFMLENBQWM4RyxJQUFkLENBQW1CLFVBQVFuQyxXQUFSLEdBQW9CLFFBQXZDLEVBQWlEa0QsS0FBakQ7QUFDSDs7QUFDRCxVQUFHQSxLQUFLLEtBQUt6RCxTQUFWLElBQXVCeUQsS0FBSyxLQUFLLEVBQXBDLEVBQXVDO0FBRW5DLFlBQUl5QyxLQUFLLEdBQUcsQ0FBWjtBQUNBL0ksU0FBQyxDQUFDTSxJQUFGLENBQVFOLENBQUMsQ0FBQyxNQUFJb0QsV0FBSixHQUFnQixRQUFoQixHQUF5QkEsV0FBekIsR0FBcUMsU0FBckMsR0FBK0NrRCxLQUEvQyxHQUFxRCxHQUF0RCxDQUFULEVBQXNFLFVBQVNFLEtBQVQsRUFBZ0JsRixHQUFoQixFQUFxQjtBQUV2RjRELGNBQUksQ0FBQ29CLEtBQUwsQ0FBV0csR0FBWCxDQUFldUMsSUFBZixDQUFvQmhKLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUSxDQUFSLEVBQVdtRixFQUEvQjs7QUFFQSxjQUFHRCxJQUFJLENBQUNDLEVBQUwsSUFBV25GLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUSxDQUFSLEVBQVdtRixFQUF6QixFQUE0QjtBQUN4QkQsZ0JBQUksQ0FBQ29CLEtBQUwsQ0FBV0UsS0FBWCxHQUFtQnVDLEtBQW5CO0FBQ0g7O0FBQ0RBLGVBQUs7QUFDUixTQVJEO0FBU0g7QUFDSixLQXBNZ0I7QUFzTWpCRSxVQUFNLEVBQUUsa0JBQVk7QUFFaEIsVUFBRyxLQUFLdEQsS0FBTCxJQUFjdEMsTUFBTSxDQUFDSSxNQUF4QixFQUErQjtBQUMzQixhQUFLeUYsS0FBTDtBQUNIOztBQUNELFVBQUcsS0FBS3ZELEtBQUwsSUFBY3RDLE1BQU0sQ0FBQ0UsTUFBeEIsRUFBK0I7QUFDM0IsYUFBSzRGLElBQUw7QUFDSDtBQUNKLEtBOU1nQjtBQWdOakJBLFFBQUksRUFBRSxjQUFVQyxLQUFWLEVBQWlCO0FBRW5CLFVBQUlsRSxJQUFJLEdBQUcsSUFBWDtBQUVBbEYsT0FBQyxDQUFDTSxJQUFGLENBQVFOLENBQUMsQ0FBQyxNQUFJb0QsV0FBTCxDQUFULEVBQTZCLFVBQVNvRCxLQUFULEVBQWdCNkMsS0FBaEIsRUFBdUI7QUFDaEQsWUFBSXJKLENBQUMsQ0FBQ3FKLEtBQUQsQ0FBRCxDQUFTekcsSUFBVCxHQUFnQm9DLFFBQWhCLEtBQTZCbkMsU0FBakMsRUFBNEM7QUFDeEMsY0FBSThDLEtBQUssR0FBRzNGLENBQUMsQ0FBQ3FKLEtBQUQsQ0FBRCxDQUFTckUsUUFBVCxDQUFrQixVQUFsQixDQUFaOztBQUNBLGNBQUdXLEtBQUssSUFBSSxRQUFULElBQXFCQSxLQUFLLElBQUksU0FBakMsRUFBMkM7QUFDdkMzRixhQUFDLENBQUNxSixLQUFELENBQUQsQ0FBU3JFLFFBQVQsQ0FBa0IsT0FBbEI7QUFDSDtBQUNKO0FBQ0osT0FQRDs7QUFTQSxPQUFDLFNBQVNzRSxPQUFULEdBQWtCO0FBQ2YsWUFBR3BFLElBQUksQ0FBQ25FLE9BQUwsQ0FBYWdFLE9BQWhCLEVBQXdCO0FBQ3BCLGNBQUl3RSxRQUFRLEdBQUdwRyxRQUFRLENBQUM2RSxLQUF4QjtBQUNBN0Usa0JBQVEsQ0FBQzZFLEtBQVQsR0FBaUJ1QixRQUFRLEdBQUcsS0FBWCxHQUFtQnJFLElBQUksQ0FBQ25FLE9BQUwsQ0FBYWlILEtBQWpEO0FBQ0E3RSxrQkFBUSxDQUFDcUcsUUFBVCxDQUFrQkMsSUFBbEIsR0FBeUJ2RSxJQUFJLENBQUNDLEVBQTlCO0FBQ0FoQyxrQkFBUSxDQUFDNkUsS0FBVCxHQUFpQnVCLFFBQWpCLENBSm9CLENBS3BCOztBQUNBcEosZ0JBQU0sQ0FBQzBFLFNBQVAsQ0FBaUJFLE9BQWpCLEdBQTJCLElBQTNCO0FBQ0gsU0FQRCxNQU9PO0FBQ0g1RSxnQkFBTSxDQUFDMEUsU0FBUCxDQUFpQkUsT0FBakIsR0FBMkIsS0FBM0I7QUFDSDtBQUNKLE9BWEQ7O0FBYUEsZUFBUzJFLE1BQVQsR0FBaUI7QUFFYjtBQUVBeEUsWUFBSSxDQUFDUyxLQUFMLEdBQWF0QyxNQUFNLENBQUNJLE1BQXBCO0FBQ0F5QixZQUFJLENBQUN6RyxRQUFMLENBQWNrTCxPQUFkLENBQXNCdEcsTUFBTSxDQUFDSSxNQUE3Qjs7QUFFQSxZQUFJeUIsSUFBSSxDQUFDbkUsT0FBTCxDQUFhNkksUUFBYixLQUEyQixPQUFPMUUsSUFBSSxDQUFDbkUsT0FBTCxDQUFhNkksUUFBcEIsS0FBa0MsVUFBbEMsSUFBZ0QsUUFBTzFFLElBQUksQ0FBQ25FLE9BQUwsQ0FBYTZJLFFBQXBCLE1BQWtDLFFBQTdHLENBQUosRUFBOEg7QUFDMUgxRSxjQUFJLENBQUNuRSxPQUFMLENBQWE2SSxRQUFiLENBQXNCMUUsSUFBdEI7QUFDSDtBQUNKOztBQUVELGVBQVMyRSxVQUFULEdBQXFCO0FBRWpCO0FBQ0EzRSxZQUFJLENBQUN6RyxRQUFMLENBQWNxTCxHQUFkLENBQWtCLE9BQWxCLEVBQTJCLFdBQVMxRyxXQUFULEdBQXFCLFNBQWhELEVBQTJEeEUsRUFBM0QsQ0FBOEQsT0FBOUQsRUFBdUUsV0FBU3dFLFdBQVQsR0FBcUIsU0FBNUYsRUFBdUcsVUFBVTFCLENBQVYsRUFBYTtBQUNoSEEsV0FBQyxDQUFDcUksY0FBRjtBQUVBLGNBQUlDLFVBQVUsR0FBR2hLLENBQUMsQ0FBQzBCLENBQUMsQ0FBQ3VJLGFBQUgsQ0FBRCxDQUFtQjFFLElBQW5CLENBQXdCLFVBQVFuQyxXQUFSLEdBQW9CLGdCQUE1QyxDQUFqQjs7QUFFQSxjQUFHNEcsVUFBVSxLQUFLbkgsU0FBbEIsRUFBNEI7QUFDeEJxQyxnQkFBSSxDQUFDZ0UsS0FBTCxDQUFXO0FBQUNjLHdCQUFVLEVBQUNBO0FBQVosYUFBWDtBQUNILFdBRkQsTUFFTztBQUNIOUUsZ0JBQUksQ0FBQ2dFLEtBQUw7QUFDSDtBQUNKLFNBVkQsRUFIaUIsQ0FlakI7O0FBQ0FoRSxZQUFJLENBQUN6RyxRQUFMLENBQWNxTCxHQUFkLENBQWtCLE9BQWxCLEVBQTJCLFdBQVMxRyxXQUFULEdBQXFCLGNBQWhELEVBQWdFeEUsRUFBaEUsQ0FBbUUsT0FBbkUsRUFBNEUsV0FBU3dFLFdBQVQsR0FBcUIsY0FBakcsRUFBaUgsVUFBVTFCLENBQVYsRUFBYTtBQUMxSEEsV0FBQyxDQUFDcUksY0FBRjs7QUFDQSxjQUFHN0UsSUFBSSxDQUFDYyxZQUFMLEtBQXNCLElBQXpCLEVBQThCO0FBQzFCZCxnQkFBSSxDQUFDYyxZQUFMLEdBQW9CLEtBQXBCO0FBQ0FkLGdCQUFJLENBQUN6RyxRQUFMLENBQWN5TCxXQUFkLENBQTBCLGNBQTFCO0FBQ0gsV0FIRCxNQUdPO0FBQ0hoRixnQkFBSSxDQUFDYyxZQUFMLEdBQW9CLElBQXBCO0FBQ0FkLGdCQUFJLENBQUN6RyxRQUFMLENBQWNrSSxRQUFkLENBQXVCLGNBQXZCO0FBQ0g7O0FBQ0QsY0FBSXpCLElBQUksQ0FBQ25FLE9BQUwsQ0FBYW9KLFlBQWIsSUFBNkIsT0FBT2pGLElBQUksQ0FBQ25FLE9BQUwsQ0FBYW9KLFlBQXBCLEtBQXNDLFVBQXZFLEVBQW1GO0FBQy9FakYsZ0JBQUksQ0FBQ25FLE9BQUwsQ0FBYW9KLFlBQWIsQ0FBMEJqRixJQUExQjtBQUNIOztBQUNEQSxjQUFJLENBQUN6RyxRQUFMLENBQWNrTCxPQUFkLENBQXNCLFlBQXRCLEVBQW9DekUsSUFBcEM7QUFDSCxTQWJELEVBaEJpQixDQStCakI7O0FBQ0FBLFlBQUksQ0FBQ21CLFNBQUwsQ0FBZXlELEdBQWYsQ0FBbUIsT0FBbkIsRUFBNEIsTUFBSTFHLFdBQUosR0FBZ0IsZ0JBQTVDLEVBQThEeEUsRUFBOUQsQ0FBaUUsT0FBakUsRUFBMEUsTUFBSXdFLFdBQUosR0FBZ0IsZ0JBQTFGLEVBQTRHLFVBQVUxQixDQUFWLEVBQWE7QUFDckh3RCxjQUFJLENBQUNrRixJQUFMLENBQVUxSSxDQUFWO0FBQ0gsU0FGRDtBQUdBd0QsWUFBSSxDQUFDekcsUUFBTCxDQUFjcUwsR0FBZCxDQUFrQixPQUFsQixFQUEyQixXQUFTMUcsV0FBVCxHQUFxQixRQUFoRCxFQUEwRHhFLEVBQTFELENBQTZELE9BQTdELEVBQXNFLFdBQVN3RSxXQUFULEdBQXFCLFFBQTNGLEVBQXFHLFVBQVUxQixDQUFWLEVBQWE7QUFDOUd3RCxjQUFJLENBQUNrRixJQUFMLENBQVUxSSxDQUFWO0FBQ0gsU0FGRCxFQW5DaUIsQ0F1Q2pCOztBQUNBd0QsWUFBSSxDQUFDbUIsU0FBTCxDQUFleUQsR0FBZixDQUFtQixPQUFuQixFQUE0QixNQUFJMUcsV0FBSixHQUFnQixnQkFBNUMsRUFBOER4RSxFQUE5RCxDQUFpRSxPQUFqRSxFQUEwRSxNQUFJd0UsV0FBSixHQUFnQixnQkFBMUYsRUFBNEcsVUFBVTFCLENBQVYsRUFBYTtBQUNySHdELGNBQUksQ0FBQ21GLElBQUwsQ0FBVTNJLENBQVY7QUFDSCxTQUZEO0FBR0F3RCxZQUFJLENBQUN6RyxRQUFMLENBQWNxTCxHQUFkLENBQWtCLE9BQWxCLEVBQTJCLFdBQVMxRyxXQUFULEdBQXFCLFFBQWhELEVBQTBEeEUsRUFBMUQsQ0FBNkQsT0FBN0QsRUFBc0UsV0FBU3dFLFdBQVQsR0FBcUIsUUFBM0YsRUFBcUcsVUFBVTFCLENBQVYsRUFBYTtBQUM5R3dELGNBQUksQ0FBQ21GLElBQUwsQ0FBVTNJLENBQVY7QUFDSCxTQUZEO0FBR0g7O0FBRUQsVUFBRyxLQUFLaUUsS0FBTCxJQUFjdEMsTUFBTSxDQUFDRSxNQUF4QixFQUErQjtBQUUzQnNHLGtCQUFVO0FBRVYsYUFBS2hCLFFBQUw7QUFDQSxhQUFLbEQsS0FBTCxHQUFhdEMsTUFBTSxDQUFDRyxPQUFwQjtBQUNBLGFBQUsvRSxRQUFMLENBQWNrTCxPQUFkLENBQXNCdEcsTUFBTSxDQUFDRyxPQUE3QjtBQUNBLGFBQUsvRSxRQUFMLENBQWM4RyxJQUFkLENBQW1CLGFBQW5CLEVBQWtDLE9BQWxDLEVBUDJCLENBUzNCOztBQUVBLFlBQUcsS0FBS3hFLE9BQUwsQ0FBYWlHLE1BQWIsS0FBd0IsSUFBM0IsRUFBZ0M7QUFFNUIsZUFBS3ZJLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixNQUFJMEUsV0FBSixHQUFnQixVQUFuQyxFQUErQ3VELFFBQS9DLENBQXdEdkQsV0FBVyxHQUFDLGlCQUFwRTtBQUVBLGVBQUszRSxRQUFMLENBQWNDLElBQWQsQ0FBbUIsTUFBSTBFLFdBQUosR0FBZ0IsU0FBbkMsRUFBOEN4RSxFQUE5QyxDQUFpRCxNQUFqRCxFQUF5RCxZQUFVO0FBQy9Eb0IsYUFBQyxDQUFDLElBQUQsQ0FBRCxDQUFRc0ssTUFBUixHQUFpQkosV0FBakIsQ0FBNkI5RyxXQUFXLEdBQUMsaUJBQXpDO0FBQ0gsV0FGRDtBQUlBLGNBQUltSCxJQUFJLEdBQUcsSUFBWDs7QUFDQSxjQUFJO0FBQ0FBLGdCQUFJLEdBQUd2SyxDQUFDLENBQUNvSixLQUFLLENBQUNhLGFBQVAsQ0FBRCxDQUF1QjFFLElBQXZCLENBQTRCLE1BQTVCLE1BQXdDLEVBQXhDLEdBQTZDdkYsQ0FBQyxDQUFDb0osS0FBSyxDQUFDYSxhQUFQLENBQUQsQ0FBdUIxRSxJQUF2QixDQUE0QixNQUE1QixDQUE3QyxHQUFtRixJQUExRjtBQUNILFdBRkQsQ0FFRSxPQUFNN0QsQ0FBTixFQUFTLENBQ1A7QUFDSDs7QUFDRCxjQUFLLEtBQUtYLE9BQUwsQ0FBYXlKLFNBQWIsS0FBMkIsSUFBNUIsS0FBc0NELElBQUksS0FBSyxJQUFULElBQWlCQSxJQUFJLEtBQUsxSCxTQUFoRSxDQUFKLEVBQStFO0FBQzNFMEgsZ0JBQUksR0FBRyxLQUFLeEosT0FBTCxDQUFheUosU0FBcEI7QUFDSDs7QUFDRCxjQUFHRCxJQUFJLEtBQUssSUFBVCxJQUFpQkEsSUFBSSxLQUFLMUgsU0FBN0IsRUFBdUM7QUFDbkMsa0JBQU0sSUFBSTRILEtBQUosQ0FBVSwyQkFBVixDQUFOO0FBQ0g7O0FBQ0QsZUFBS2hNLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixNQUFJMEUsV0FBSixHQUFnQixTQUFuQyxFQUE4Q21DLElBQTlDLENBQW1ELEtBQW5ELEVBQTBEZ0YsSUFBMUQ7QUFDSDs7QUFHRCxZQUFJLEtBQUt4SixPQUFMLENBQWEySixZQUFiLElBQTZCL0YsUUFBakMsRUFBMEM7QUFDdEMzRSxXQUFDLENBQUMsTUFBRCxDQUFELENBQVUyRyxRQUFWLENBQW1CdkQsV0FBVyxHQUFDLGFBQS9COztBQUNBLGNBQUd1QixRQUFILEVBQVk7QUFDUjNFLGFBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVXdCLEdBQVYsQ0FBYyxVQUFkLEVBQTBCLFFBQTFCO0FBQ0g7QUFDSjs7QUFFRCxZQUFJLEtBQUtULE9BQUwsQ0FBYTRKLFNBQWIsSUFBMEIsT0FBTyxLQUFLNUosT0FBTCxDQUFhNEosU0FBcEIsS0FBbUMsVUFBakUsRUFBNkU7QUFDekUsZUFBSzVKLE9BQUwsQ0FBYTRKLFNBQWIsQ0FBdUIsSUFBdkI7QUFDSDs7QUFDRCxTQUFDLFNBQVN4QixJQUFULEdBQWU7QUFFWixjQUFHakUsSUFBSSxDQUFDb0IsS0FBTCxDQUFXRyxHQUFYLENBQWVtRSxNQUFmLEdBQXdCLENBQTNCLEVBQThCO0FBRTFCMUYsZ0JBQUksQ0FBQ21CLFNBQUwsQ0FBZVUsUUFBZixDQUF3QixNQUF4QjtBQUNBN0IsZ0JBQUksQ0FBQ21CLFNBQUwsQ0FBZU0sUUFBZixDQUF3QixRQUF4Qjs7QUFFQSxnQkFBR3pCLElBQUksQ0FBQ25FLE9BQUwsQ0FBYThKLGVBQWIsS0FBaUMsSUFBcEMsRUFBeUM7QUFDckMzRixrQkFBSSxDQUFDbUIsU0FBTCxDQUFlM0gsSUFBZixDQUFvQixNQUFJMEUsV0FBSixHQUFnQixtQkFBcEMsRUFBeUQwSCxJQUF6RDtBQUNIOztBQUVELGdCQUFJQyxVQUFVLEdBQUc3RixJQUFJLENBQUN6RyxRQUFMLENBQWN1TSxVQUFkLEVBQWpCOztBQUNBLGdCQUFHOUYsSUFBSSxDQUFDbkUsT0FBTCxDQUFha0ssY0FBYixLQUFnQyxLQUFuQyxFQUF5QztBQUNyQyxrQkFBSS9GLElBQUksQ0FBQ25FLE9BQUwsQ0FBYWtLLGNBQWIsS0FBZ0MsaUJBQXBDLEVBQXNEO0FBQ2xEL0Ysb0JBQUksQ0FBQ21CLFNBQUwsQ0FBZTNILElBQWYsQ0FBb0IsTUFBSTBFLFdBQUosR0FBZ0IsZ0JBQXBDLEVBQXNENUIsR0FBdEQsQ0FBMEQsTUFBMUQsRUFBa0UsQ0FBbEUsRUFBcUVzSixJQUFyRTtBQUNBNUYsb0JBQUksQ0FBQ21CLFNBQUwsQ0FBZTNILElBQWYsQ0FBb0IsTUFBSTBFLFdBQUosR0FBZ0IsZ0JBQXBDLEVBQXNENUIsR0FBdEQsQ0FBMEQsT0FBMUQsRUFBbUUsQ0FBbkUsRUFBc0VzSixJQUF0RTtBQUNILGVBSEQsTUFHTztBQUNINUYsb0JBQUksQ0FBQ21CLFNBQUwsQ0FBZTNILElBQWYsQ0FBb0IsTUFBSTBFLFdBQUosR0FBZ0IsZ0JBQXBDLEVBQXNENUIsR0FBdEQsQ0FBMEQsYUFBMUQsRUFBeUUsRUFBR3VKLFVBQVUsR0FBQyxDQUFaLEdBQWUsRUFBakIsQ0FBekUsRUFBK0ZELElBQS9GO0FBQ0E1RixvQkFBSSxDQUFDbUIsU0FBTCxDQUFlM0gsSUFBZixDQUFvQixNQUFJMEUsV0FBSixHQUFnQixnQkFBcEMsRUFBc0Q1QixHQUF0RCxDQUEwRCxjQUExRCxFQUEwRSxFQUFHdUosVUFBVSxHQUFDLENBQVosR0FBZSxFQUFqQixDQUExRSxFQUFnR0QsSUFBaEc7QUFDSDtBQUNKLGFBUkQsTUFRTztBQUNINUYsa0JBQUksQ0FBQ21CLFNBQUwsQ0FBZTNILElBQWYsQ0FBb0IsTUFBSTBFLFdBQUosR0FBZ0IsZ0JBQXBDLEVBQXNEOEgsSUFBdEQ7QUFDQWhHLGtCQUFJLENBQUNtQixTQUFMLENBQWUzSCxJQUFmLENBQW9CLE1BQUkwRSxXQUFKLEdBQWdCLGdCQUFwQyxFQUFzRDhILElBQXREO0FBQ0g7O0FBRUQsZ0JBQUl0RSxJQUFKOztBQUNBLGdCQUFHMUIsSUFBSSxDQUFDb0IsS0FBTCxDQUFXRSxLQUFYLEtBQXFCLENBQXhCLEVBQTBCO0FBRXRCSSxrQkFBSSxHQUFHNUcsQ0FBQyxDQUFDLE1BQUlvRCxXQUFKLEdBQWdCLFFBQWhCLEdBQXlCQSxXQUF6QixHQUFxQyxVQUFyQyxHQUFnRDhCLElBQUksQ0FBQ29CLEtBQUwsQ0FBV0MsSUFBM0QsR0FBZ0UsVUFBaEUsR0FBMkVuRCxXQUEzRSxHQUF1RixRQUF4RixDQUFELENBQW1Hd0gsTUFBMUc7QUFFQSxrQkFBR2hFLElBQUksS0FBSyxDQUFULElBQWMxQixJQUFJLENBQUNuRSxPQUFMLENBQWE2RixJQUFiLEtBQXNCLEtBQXZDLEVBQ0kxQixJQUFJLENBQUNtQixTQUFMLENBQWUzSCxJQUFmLENBQW9CLE1BQUkwRSxXQUFKLEdBQWdCLGdCQUFwQyxFQUFzRDhILElBQXREO0FBQ1A7O0FBQ0QsZ0JBQUdoRyxJQUFJLENBQUNvQixLQUFMLENBQVdFLEtBQVgsR0FBaUIsQ0FBakIsS0FBdUJ0QixJQUFJLENBQUNvQixLQUFMLENBQVdHLEdBQVgsQ0FBZW1FLE1BQXpDLEVBQWdEO0FBRTVDaEUsa0JBQUksR0FBRzVHLENBQUMsQ0FBQyxNQUFJb0QsV0FBSixHQUFnQixRQUFoQixHQUF5QkEsV0FBekIsR0FBcUMsVUFBckMsR0FBZ0Q4QixJQUFJLENBQUNvQixLQUFMLENBQVdDLElBQTNELEdBQWdFLFVBQWhFLEdBQTJFbkQsV0FBM0UsR0FBdUYsUUFBeEYsQ0FBRCxDQUFtR3dILE1BQTFHO0FBRUEsa0JBQUdoRSxJQUFJLEtBQUssQ0FBVCxJQUFjMUIsSUFBSSxDQUFDbkUsT0FBTCxDQUFhNkYsSUFBYixLQUFzQixLQUF2QyxFQUNJMUIsSUFBSSxDQUFDbUIsU0FBTCxDQUFlM0gsSUFBZixDQUFvQixNQUFJMEUsV0FBSixHQUFnQixnQkFBcEMsRUFBc0Q4SCxJQUF0RDtBQUNQO0FBQ0o7O0FBRUQsY0FBR2hHLElBQUksQ0FBQ25FLE9BQUwsQ0FBYW9LLE9BQWIsS0FBeUIsSUFBNUIsRUFBa0M7QUFFOUIsZ0JBQUdqRyxJQUFJLENBQUNuRSxPQUFMLENBQWFxSyxlQUFiLEtBQWlDLEtBQXBDLEVBQTBDO0FBQ3RDbEcsa0JBQUksQ0FBQ2lCLFFBQUwsQ0FBY1ksUUFBZCxDQUF1QixNQUF2QjtBQUNILGFBRkQsTUFFTztBQUNIN0Isa0JBQUksQ0FBQ2lCLFFBQUwsQ0FBY1ksUUFBZCxDQUF3QjdCLElBQUksQ0FBQ25FLE9BQUwsQ0FBYXFLLGVBQXJDO0FBQ0g7QUFDSjs7QUFFRCxjQUFJbEcsSUFBSSxDQUFDbkUsT0FBTCxDQUFhc0ssbUJBQWpCLEVBQXNDO0FBQ2xDbkcsZ0JBQUksQ0FBQ2lCLFFBQUwsQ0FBY1EsUUFBZCxDQUF1QnpCLElBQUksQ0FBQ25FLE9BQUwsQ0FBYXNLLG1CQUFwQztBQUNIOztBQUVELGNBQUlDLFlBQVksR0FBR3BHLElBQUksQ0FBQ25FLE9BQUwsQ0FBYXVLLFlBQWhDOztBQUVBLGNBQUksUUFBT2xDLEtBQVAsS0FBZ0IsUUFBcEIsRUFBOEI7QUFDMUIsZ0JBQUdBLEtBQUssQ0FBQ1ksVUFBTixLQUFxQm5ILFNBQXJCLElBQWtDdUcsS0FBSyxDQUFDa0MsWUFBTixLQUF1QnpJLFNBQTVELEVBQXNFO0FBQ2xFeUksMEJBQVksR0FBR2xDLEtBQUssQ0FBQ1ksVUFBTixJQUFvQlosS0FBSyxDQUFDa0MsWUFBekM7QUFDSDtBQUNKOztBQUVELGNBQUlBLFlBQVksS0FBSyxFQUFqQixJQUF1QjVHLGNBQWMsS0FBSzdCLFNBQTlDLEVBQXlEO0FBRXJEcUMsZ0JBQUksQ0FBQ3pHLFFBQUwsQ0FBY2tJLFFBQWQsQ0FBdUIsa0JBQWdCMkUsWUFBdkMsRUFBcURSLElBQXJEO0FBQ0E1RixnQkFBSSxDQUFDaUMsS0FBTCxDQUFXb0UsR0FBWCxDQUFlN0csY0FBZixFQUErQixZQUFZO0FBRXZDUSxrQkFBSSxDQUFDekcsUUFBTCxDQUFjeUwsV0FBZCxDQUEwQm9CLFlBQVksR0FBRyxlQUF6QztBQUNBcEcsa0JBQUksQ0FBQ2lCLFFBQUwsQ0FBYytELFdBQWQsQ0FBMEJoRixJQUFJLENBQUNuRSxPQUFMLENBQWFzSyxtQkFBdkM7QUFDQW5HLGtCQUFJLENBQUNtQixTQUFMLENBQWU2RCxXQUFmLENBQTJCLFFBQTNCO0FBRUFSLG9CQUFNO0FBQ1QsYUFQRDtBQVNILFdBWkQsTUFZTztBQUVIeEUsZ0JBQUksQ0FBQ3pHLFFBQUwsQ0FBY3FNLElBQWQ7QUFDQXBCLGtCQUFNO0FBQ1Q7O0FBRUQsY0FBR3hFLElBQUksQ0FBQ25FLE9BQUwsQ0FBYXlLLFlBQWIsS0FBOEIsSUFBOUIsSUFBc0N0RyxJQUFJLENBQUNuRSxPQUFMLENBQWF5SyxZQUFiLEtBQThCLElBQXBFLElBQTRFdEcsSUFBSSxDQUFDbkUsT0FBTCxDQUFhc0gsT0FBYixLQUF5QixLQUFyRyxJQUE4RyxDQUFDaEIsS0FBSyxDQUFDOUMsUUFBUSxDQUFDVyxJQUFJLENBQUNuRSxPQUFMLENBQWFzSCxPQUFkLENBQVQsQ0FBcEgsSUFBd0puRCxJQUFJLENBQUNuRSxPQUFMLENBQWFzSCxPQUFiLEtBQXlCLEtBQWpMLElBQTBMbkQsSUFBSSxDQUFDbkUsT0FBTCxDQUFhc0gsT0FBYixLQUF5QixDQUF0TixFQUF3TjtBQUVwTm5ELGdCQUFJLENBQUN6RyxRQUFMLENBQWNxTCxHQUFkLENBQWtCLFlBQWxCLEVBQWdDbEwsRUFBaEMsQ0FBbUMsWUFBbkMsRUFBaUQsVUFBUzZNLEtBQVQsRUFBZ0I7QUFDN0RBLG1CQUFLLENBQUMxQixjQUFOO0FBQ0E3RSxrQkFBSSxDQUFDYSxRQUFMLEdBQWdCLElBQWhCO0FBQ0gsYUFIRDtBQUlBYixnQkFBSSxDQUFDekcsUUFBTCxDQUFjcUwsR0FBZCxDQUFrQixZQUFsQixFQUFnQ2xMLEVBQWhDLENBQW1DLFlBQW5DLEVBQWlELFVBQVM2TSxLQUFULEVBQWdCO0FBQzdEQSxtQkFBSyxDQUFDMUIsY0FBTjtBQUNBN0Usa0JBQUksQ0FBQ2EsUUFBTCxHQUFnQixLQUFoQjtBQUNILGFBSEQ7QUFJSDtBQUVKLFNBN0ZEOztBQStGQSxZQUFJLEtBQUtoRixPQUFMLENBQWFzSCxPQUFiLEtBQXlCLEtBQXpCLElBQWtDLENBQUNoQixLQUFLLENBQUM5QyxRQUFRLENBQUMsS0FBS3hELE9BQUwsQ0FBYXNILE9BQWQsQ0FBVCxDQUF4QyxJQUE0RSxLQUFLdEgsT0FBTCxDQUFhc0gsT0FBYixLQUF5QixLQUFyRyxJQUE4RyxLQUFLdEgsT0FBTCxDQUFhc0gsT0FBYixLQUF5QixDQUEzSSxFQUE4STtBQUUxSSxjQUFJLEtBQUt0SCxPQUFMLENBQWFxSCxrQkFBYixLQUFvQyxJQUF4QyxFQUE4QztBQUUxQyxpQkFBS3RDLFdBQUwsR0FBbUI7QUFDZjRGLHFCQUFPLEVBQUUsSUFETTtBQUVmQyx5QkFBVyxFQUFFLElBRkU7QUFHZkMseUJBQVcsRUFBRSxJQUFJQyxJQUFKLEdBQVdDLE9BQVgsRUFIRTtBQUlmdEwsZ0JBQUUsRUFBRSxLQUFLL0IsUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLG9CQUFuQyxDQUpXO0FBS2YySSw0QkFBYyxFQUFFLDBCQUNoQjtBQUNJLG9CQUFHLENBQUM3RyxJQUFJLENBQUNhLFFBQVQsRUFBa0I7QUFFZGIsc0JBQUksQ0FBQ1ksV0FBTCxDQUFpQjhGLFdBQWpCLEdBQStCMUcsSUFBSSxDQUFDWSxXQUFMLENBQWlCOEYsV0FBakIsR0FBNkIsRUFBNUQ7QUFFQSxzQkFBSUksVUFBVSxHQUFJLENBQUM5RyxJQUFJLENBQUNZLFdBQUwsQ0FBaUI0RixPQUFqQixHQUE0QnhHLElBQUksQ0FBQ1ksV0FBTCxDQUFpQjhGLFdBQTlDLElBQThEMUcsSUFBSSxDQUFDWSxXQUFMLENBQWlCNkYsV0FBaEYsR0FBK0YsR0FBaEg7QUFDQXpHLHNCQUFJLENBQUNZLFdBQUwsQ0FBaUJ0RixFQUFqQixDQUFvQnlCLEtBQXBCLENBQTBCK0osVUFBVSxHQUFHLEdBQXZDOztBQUNBLHNCQUFHQSxVQUFVLEdBQUcsQ0FBaEIsRUFBa0I7QUFDZDlHLHdCQUFJLENBQUNnRSxLQUFMO0FBQ0g7QUFDSjtBQUNKO0FBakJjLGFBQW5COztBQW1CQSxnQkFBSSxLQUFLbkksT0FBTCxDQUFhc0gsT0FBYixHQUF1QixDQUEzQixFQUE4QjtBQUUxQixtQkFBS3ZDLFdBQUwsQ0FBaUI2RixXQUFqQixHQUErQk0sVUFBVSxDQUFDLEtBQUtsTCxPQUFMLENBQWFzSCxPQUFkLENBQXpDO0FBQ0EsbUJBQUt2QyxXQUFMLENBQWlCNEYsT0FBakIsR0FBMkIsSUFBSUcsSUFBSixHQUFXQyxPQUFYLEtBQXVCLEtBQUtoRyxXQUFMLENBQWlCNkYsV0FBbkU7QUFDQSxtQkFBSzlGLFlBQUwsR0FBb0JxRyxXQUFXLENBQUMsS0FBS3BHLFdBQUwsQ0FBaUJpRyxjQUFsQixFQUFrQyxFQUFsQyxDQUEvQjtBQUNIO0FBRUosV0E1QkQsTUE0Qk87QUFFSCxpQkFBS2xHLFlBQUwsR0FBb0JzRyxVQUFVLENBQUMsWUFBVTtBQUNyQ2pILGtCQUFJLENBQUNnRSxLQUFMO0FBQ0gsYUFGNkIsRUFFM0JoRSxJQUFJLENBQUNuRSxPQUFMLENBQWFzSCxPQUZjLENBQTlCO0FBR0g7QUFDSixTQWhMMEIsQ0FrTDNCOzs7QUFDQSxZQUFJLEtBQUt0SCxPQUFMLENBQWFxTCxZQUFiLElBQTZCLENBQUMsS0FBSzNOLFFBQUwsQ0FBY2lJLFFBQWQsQ0FBdUIsS0FBSzNGLE9BQUwsQ0FBYXNMLGFBQXBDLENBQWxDLEVBQXNGO0FBQ2xGLGVBQUtsRyxRQUFMLENBQWNtRyxLQUFkLENBQW9CLFlBQVk7QUFDNUJwSCxnQkFBSSxDQUFDZ0UsS0FBTDtBQUNILFdBRkQ7QUFHSDs7QUFFRCxZQUFJLEtBQUtuSSxPQUFMLENBQWF3TCxVQUFqQixFQUE0QjtBQUN4QixlQUFLOU4sUUFBTCxDQUFjQyxJQUFkLENBQW1CLDBDQUFuQixFQUErRDhOLEtBQS9ELEdBRHdCLENBQ2dEO0FBQzNFOztBQUVELFNBQUMsU0FBU0MsV0FBVCxHQUFzQjtBQUNuQnZILGNBQUksQ0FBQ3dILFlBQUw7QUFDQXhILGNBQUksQ0FBQ1UsS0FBTCxHQUFhdUcsVUFBVSxDQUFDTSxXQUFELEVBQWMsR0FBZCxDQUF2QjtBQUNILFNBSEQsSUE3TDJCLENBa00zQjs7O0FBQ0F2SixpQkFBUyxDQUFDdEUsRUFBVixDQUFhLGFBQVd3RSxXQUF4QixFQUFxQyxVQUFVMUIsQ0FBVixFQUFhO0FBQzlDLGNBQUl3RCxJQUFJLENBQUNuRSxPQUFMLENBQWE0TCxhQUFiLElBQThCakwsQ0FBQyxDQUFDa0wsT0FBRixLQUFjLEVBQWhELEVBQW9EO0FBQ2hEMUgsZ0JBQUksQ0FBQ2dFLEtBQUw7QUFDSDtBQUNKLFNBSkQ7QUFNSDtBQUVKLEtBamZnQjtBQW1makJBLFNBQUssRUFBRSxlQUFVRSxLQUFWLEVBQWlCO0FBRXBCLFVBQUlsRSxJQUFJLEdBQUcsSUFBWDs7QUFFQSxlQUFTMkgsTUFBVCxHQUFpQjtBQUViO0FBRUEzSCxZQUFJLENBQUNTLEtBQUwsR0FBYXRDLE1BQU0sQ0FBQ0UsTUFBcEI7QUFDQTJCLFlBQUksQ0FBQ3pHLFFBQUwsQ0FBY2tMLE9BQWQsQ0FBc0J0RyxNQUFNLENBQUNFLE1BQTdCOztBQUVBLFlBQUkyQixJQUFJLENBQUNuRSxPQUFMLENBQWFpRyxNQUFiLEtBQXdCLElBQTVCLEVBQWtDO0FBQzlCOUIsY0FBSSxDQUFDekcsUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLFNBQW5DLEVBQThDbUMsSUFBOUMsQ0FBbUQsS0FBbkQsRUFBMEQsRUFBMUQ7QUFDSDs7QUFFRCxZQUFJTCxJQUFJLENBQUNuRSxPQUFMLENBQWEySixZQUFiLElBQTZCL0YsUUFBakMsRUFBMEM7QUFDdEMzRSxXQUFDLENBQUMsTUFBRCxDQUFELENBQVVrSyxXQUFWLENBQXNCOUcsV0FBVyxHQUFDLGFBQWxDOztBQUNBLGNBQUd1QixRQUFILEVBQVk7QUFDUjNFLGFBQUMsQ0FBQyxNQUFELENBQUQsQ0FBVXdCLEdBQVYsQ0FBYyxVQUFkLEVBQXlCLE1BQXpCO0FBQ0g7QUFDSjs7QUFFRCxZQUFJMEQsSUFBSSxDQUFDbkUsT0FBTCxDQUFhK0wsUUFBYixJQUF5QixPQUFPNUgsSUFBSSxDQUFDbkUsT0FBTCxDQUFhK0wsUUFBcEIsS0FBa0MsVUFBL0QsRUFBMkU7QUFDdkU1SCxjQUFJLENBQUNuRSxPQUFMLENBQWErTCxRQUFiLENBQXNCNUgsSUFBdEI7QUFDSDs7QUFFRCxZQUFHQSxJQUFJLENBQUNuRSxPQUFMLENBQWFnTSxxQkFBYixLQUF1QyxJQUExQyxFQUErQztBQUMzQzdILGNBQUksQ0FBQ3pHLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixNQUFJMEUsV0FBSixHQUFnQixVQUFuQyxFQUErQ3NDLElBQS9DLENBQXFEUixJQUFJLENBQUNPLE9BQTFEO0FBQ0g7O0FBRUQsWUFBSXpGLENBQUMsQ0FBQyxNQUFJb0QsV0FBSixHQUFnQixVQUFqQixDQUFELENBQThCd0gsTUFBOUIsS0FBeUMsQ0FBN0MsRUFBZ0Q7QUFDNUM1SyxXQUFDLENBQUMsTUFBRCxDQUFELENBQVVrSyxXQUFWLENBQXNCOUcsV0FBVyxHQUFDLGFBQWxDO0FBQ0g7QUFDSjs7QUFFRCxVQUFHLEtBQUt1QyxLQUFMLElBQWN0QyxNQUFNLENBQUNJLE1BQXJCLElBQStCLEtBQUtrQyxLQUFMLElBQWN0QyxNQUFNLENBQUNHLE9BQXZELEVBQStEO0FBRTNETixpQkFBUyxDQUFDNEcsR0FBVixDQUFjLGFBQVcxRyxXQUF6QjtBQUVBLGFBQUt1QyxLQUFMLEdBQWF0QyxNQUFNLENBQUNDLE9BQXBCO0FBQ0EsYUFBSzdFLFFBQUwsQ0FBY2tMLE9BQWQsQ0FBc0J0RyxNQUFNLENBQUNDLE9BQTdCO0FBQ0EsYUFBSzdFLFFBQUwsQ0FBYzhHLElBQWQsQ0FBbUIsYUFBbkIsRUFBa0MsTUFBbEMsRUFOMkQsQ0FRM0Q7O0FBRUF5SCxvQkFBWSxDQUFDLEtBQUtwSCxLQUFOLENBQVo7QUFDQW9ILG9CQUFZLENBQUMsS0FBS25ILFlBQU4sQ0FBWjs7QUFFQSxZQUFJWCxJQUFJLENBQUNuRSxPQUFMLENBQWFrTSxTQUFiLElBQTBCLE9BQU8vSCxJQUFJLENBQUNuRSxPQUFMLENBQWFrTSxTQUFwQixLQUFtQyxVQUFqRSxFQUE2RTtBQUN6RS9ILGNBQUksQ0FBQ25FLE9BQUwsQ0FBYWtNLFNBQWIsQ0FBdUIsSUFBdkI7QUFDSDs7QUFFRCxZQUFJWixhQUFhLEdBQUcsS0FBS3RMLE9BQUwsQ0FBYXNMLGFBQWpDOztBQUVBLFlBQUksUUFBT2pELEtBQVAsS0FBZ0IsUUFBcEIsRUFBOEI7QUFDMUIsY0FBR0EsS0FBSyxDQUFDWSxVQUFOLEtBQXFCbkgsU0FBckIsSUFBa0N1RyxLQUFLLENBQUNpRCxhQUFOLEtBQXdCeEosU0FBN0QsRUFBdUU7QUFDbkV3Six5QkFBYSxHQUFHakQsS0FBSyxDQUFDWSxVQUFOLElBQW9CWixLQUFLLENBQUNpRCxhQUExQztBQUNIO0FBQ0o7O0FBRUQsWUFBS0EsYUFBYSxLQUFLLEtBQWxCLElBQTJCQSxhQUFhLEtBQUssRUFBOUMsSUFBc0QzSCxjQUFjLEtBQUs3QixTQUE3RSxFQUF1RjtBQUVuRixlQUFLcEUsUUFBTCxDQUFjeU0sSUFBZDtBQUNBLGVBQUsvRSxRQUFMLENBQWMrRyxNQUFkO0FBQ0EsZUFBSzdHLFNBQUwsQ0FBZTZHLE1BQWY7QUFDQUwsZ0JBQU07QUFFVCxTQVBELE1BT087QUFFSCxlQUFLcE8sUUFBTCxDQUFjOEcsSUFBZCxDQUFtQixPQUFuQixFQUE0QixDQUN4QixLQUFLQyxPQURtQixFQUV4QnBDLFdBRndCLEVBR3hCaUosYUFId0IsRUFJeEIsS0FBS3RMLE9BQUwsQ0FBYXlHLEtBQWIsSUFBc0IsT0FBdEIsR0FBZ0NwRSxXQUFXLEdBQUMsUUFBNUMsR0FBdUQsS0FBS3JDLE9BQUwsQ0FBYXlHLEtBSjVDLEVBS3hCLEtBQUt4QixZQUFMLEtBQXNCLElBQXRCLEdBQTZCLGNBQTdCLEdBQThDLEVBTHRCLEVBTXhCLEtBQUtqRixPQUFMLENBQWEwRyxHQUFiLEdBQW1CckUsV0FBVyxHQUFDLE1BQS9CLEdBQXdDLEVBTmhCLEVBTzFCK0osSUFQMEIsQ0FPckIsR0FQcUIsQ0FBNUI7QUFTQSxlQUFLaEgsUUFBTCxDQUFjWixJQUFkLENBQW1CLE9BQW5CLEVBQTRCbkMsV0FBVyxHQUFHLFdBQWQsR0FBNEIsS0FBS3JDLE9BQUwsQ0FBYXFNLG9CQUFyRTs7QUFFQSxjQUFHbEksSUFBSSxDQUFDbkUsT0FBTCxDQUFha0ssY0FBYixLQUFnQyxLQUFuQyxFQUF5QztBQUNyQyxpQkFBSzVFLFNBQUwsQ0FBZWQsSUFBZixDQUFvQixPQUFwQixFQUE2Qm5DLFdBQVcsR0FBRyxtQkFBM0M7QUFDSDs7QUFFRCxlQUFLM0UsUUFBTCxDQUFjOE0sR0FBZCxDQUFrQjdHLGNBQWxCLEVBQWtDLFlBQVk7QUFFMUMsZ0JBQUlRLElBQUksQ0FBQ3pHLFFBQUwsQ0FBY2lJLFFBQWQsQ0FBdUIyRixhQUF2QixDQUFKLEVBQTJDO0FBQ3ZDbkgsa0JBQUksQ0FBQ3pHLFFBQUwsQ0FBY3lMLFdBQWQsQ0FBMEJtQyxhQUFhLEdBQUcsZ0JBQTFDLEVBQTREbkIsSUFBNUQ7QUFDSDs7QUFDRGhHLGdCQUFJLENBQUNpQixRQUFMLENBQWMrRCxXQUFkLENBQTBCaEYsSUFBSSxDQUFDbkUsT0FBTCxDQUFhcU0sb0JBQXZDLEVBQTZERixNQUE3RDtBQUNBaEksZ0JBQUksQ0FBQ21CLFNBQUwsQ0FBZTZELFdBQWYsQ0FBMkIsU0FBM0IsRUFBc0NnRCxNQUF0QztBQUNBTCxrQkFBTTtBQUNULFdBUkQ7QUFVSDtBQUVKO0FBQ0osS0FwbEJnQjtBQXNsQmpCekMsUUFBSSxFQUFFLGNBQVUxSSxDQUFWLEVBQVk7QUFFZCxVQUFJd0QsSUFBSSxHQUFHLElBQVg7QUFDQSxVQUFJb0csWUFBWSxHQUFHLGFBQW5CO0FBQ0EsVUFBSWUsYUFBYSxHQUFHLGFBQXBCO0FBQ0EsVUFBSWhELEtBQUssR0FBR3JKLENBQUMsQ0FBQyxNQUFJb0QsV0FBSixHQUFnQixVQUFqQixDQUFiO0FBQ0EsVUFBSWlLLE1BQU0sR0FBRyxFQUFiO0FBQ0FBLFlBQU0sQ0FBQ0MsR0FBUCxHQUFhLElBQWI7O0FBRUEsVUFBRzVMLENBQUMsS0FBS21CLFNBQU4sSUFBbUIsUUFBT25CLENBQVAsTUFBYSxRQUFuQyxFQUE0QztBQUN4Q0EsU0FBQyxDQUFDcUksY0FBRjtBQUNBVixhQUFLLEdBQUdySixDQUFDLENBQUMwQixDQUFDLENBQUN1SSxhQUFILENBQVQ7QUFDQXFCLG9CQUFZLEdBQUdqQyxLQUFLLENBQUM5RCxJQUFOLENBQVcsVUFBUW5DLFdBQVIsR0FBb0IsZUFBL0IsQ0FBZjtBQUNBaUoscUJBQWEsR0FBR2hELEtBQUssQ0FBQzlELElBQU4sQ0FBVyxVQUFRbkMsV0FBUixHQUFvQixnQkFBL0IsQ0FBaEI7QUFDSCxPQUxELE1BS08sSUFBRzFCLENBQUMsS0FBS21CLFNBQVQsRUFBbUI7QUFDdEIsWUFBR25CLENBQUMsQ0FBQzRKLFlBQUYsS0FBbUJ6SSxTQUF0QixFQUFnQztBQUM1QnlJLHNCQUFZLEdBQUc1SixDQUFDLENBQUM0SixZQUFqQjtBQUNIOztBQUNELFlBQUc1SixDQUFDLENBQUMySyxhQUFGLEtBQW9CeEosU0FBdkIsRUFBaUM7QUFDN0J3Six1QkFBYSxHQUFHM0ssQ0FBQyxDQUFDMkssYUFBbEI7QUFDSDtBQUNKOztBQUVELFdBQUtuRCxLQUFMLENBQVc7QUFBQ2Msa0JBQVUsRUFBQ3FDO0FBQVosT0FBWDtBQUVBRixnQkFBVSxDQUFDLFlBQVU7QUFFakIsWUFBSXZGLElBQUksR0FBRzVHLENBQUMsQ0FBQyxNQUFJb0QsV0FBSixHQUFnQixRQUFoQixHQUF5QkEsV0FBekIsR0FBcUMsVUFBckMsR0FBZ0Q4QixJQUFJLENBQUNvQixLQUFMLENBQVdDLElBQTNELEdBQWdFLFVBQWhFLEdBQTJFbkQsV0FBM0UsR0FBdUYsUUFBeEYsQ0FBRCxDQUFtR3dILE1BQTlHOztBQUNBLGFBQUssSUFBSXJLLENBQUMsR0FBRzJFLElBQUksQ0FBQ29CLEtBQUwsQ0FBV0UsS0FBWCxHQUFpQixDQUE5QixFQUFpQ2pHLENBQUMsSUFBSTJFLElBQUksQ0FBQ29CLEtBQUwsQ0FBV0csR0FBWCxDQUFlbUUsTUFBckQsRUFBNkRySyxDQUFDLEVBQTlELEVBQWtFO0FBRTlELGNBQUk7QUFDQThNLGtCQUFNLE1BQU4sR0FBWXJOLENBQUMsQ0FBQyxNQUFJa0YsSUFBSSxDQUFDb0IsS0FBTCxDQUFXRyxHQUFYLENBQWVsRyxDQUFmLENBQUwsQ0FBRCxDQUF5QnFDLElBQXpCLEdBQWdDb0MsUUFBNUM7QUFDSCxXQUZELENBRUUsT0FBTXVJLEdBQU4sRUFBVyxDQUNUO0FBQ0g7O0FBQ0QsY0FBRyxPQUFPRixNQUFNLE1BQWIsS0FBcUIsV0FBeEIsRUFBb0M7QUFFaENyTixhQUFDLENBQUMsTUFBSWtGLElBQUksQ0FBQ29CLEtBQUwsQ0FBV0csR0FBWCxDQUFlbEcsQ0FBZixDQUFMLENBQUQsQ0FBeUJ5RSxRQUF6QixDQUFrQyxNQUFsQyxFQUEwQztBQUFFZ0Ysd0JBQVUsRUFBRXNCO0FBQWQsYUFBMUM7QUFDQTtBQUVILFdBTEQsTUFLTztBQUVILGdCQUFHL0ssQ0FBQyxJQUFJMkUsSUFBSSxDQUFDb0IsS0FBTCxDQUFXRyxHQUFYLENBQWVtRSxNQUFwQixJQUE4QmhFLElBQUksR0FBRyxDQUFyQyxJQUEwQzFCLElBQUksQ0FBQ25FLE9BQUwsQ0FBYTZGLElBQWIsS0FBc0IsSUFBbkUsRUFBd0U7QUFFcEUsbUJBQUssSUFBSUosS0FBSyxHQUFHLENBQWpCLEVBQW9CQSxLQUFLLElBQUl0QixJQUFJLENBQUNvQixLQUFMLENBQVdHLEdBQVgsQ0FBZW1FLE1BQTVDLEVBQW9EcEUsS0FBSyxFQUF6RCxFQUE2RDtBQUV6RDZHLHNCQUFNLE1BQU4sR0FBWXJOLENBQUMsQ0FBQyxNQUFJa0YsSUFBSSxDQUFDb0IsS0FBTCxDQUFXRyxHQUFYLENBQWVELEtBQWYsQ0FBTCxDQUFELENBQTZCNUQsSUFBN0IsR0FBb0NvQyxRQUFoRDs7QUFDQSxvQkFBRyxPQUFPcUksTUFBTSxNQUFiLEtBQXFCLFdBQXhCLEVBQW9DO0FBQ2hDck4sbUJBQUMsQ0FBQyxNQUFJa0YsSUFBSSxDQUFDb0IsS0FBTCxDQUFXRyxHQUFYLENBQWVELEtBQWYsQ0FBTCxDQUFELENBQTZCeEIsUUFBN0IsQ0FBc0MsTUFBdEMsRUFBOEM7QUFBRWdGLDhCQUFVLEVBQUVzQjtBQUFkLG1CQUE5QztBQUNBO0FBQ0g7QUFDSjtBQUNKO0FBQ0o7QUFDSjtBQUVKLE9BL0JTLEVBK0JQLEdBL0JPLENBQVY7QUFpQ0F0TCxPQUFDLENBQUNtRCxRQUFELENBQUQsQ0FBWXdHLE9BQVosQ0FBcUJ2RyxXQUFXLEdBQUcsZUFBbkMsRUFBb0RpSyxNQUFwRDtBQUNILEtBanBCZ0I7QUFtcEJqQmhELFFBQUksRUFBRSxjQUFVM0ksQ0FBVixFQUFZO0FBQ2QsVUFBSXdELElBQUksR0FBRyxJQUFYO0FBQ0EsVUFBSW9HLFlBQVksR0FBRyxZQUFuQjtBQUNBLFVBQUllLGFBQWEsR0FBRyxjQUFwQjtBQUNBLFVBQUloRCxLQUFLLEdBQUdySixDQUFDLENBQUMsTUFBSW9ELFdBQUosR0FBZ0IsVUFBakIsQ0FBYjtBQUNBLFVBQUlpSyxNQUFNLEdBQUcsRUFBYjtBQUNBQSxZQUFNLENBQUNDLEdBQVAsR0FBYSxJQUFiOztBQUVBLFVBQUc1TCxDQUFDLEtBQUttQixTQUFOLElBQW1CLFFBQU9uQixDQUFQLE1BQWEsUUFBbkMsRUFBNEM7QUFDeENBLFNBQUMsQ0FBQ3FJLGNBQUY7QUFDQVYsYUFBSyxHQUFHckosQ0FBQyxDQUFDMEIsQ0FBQyxDQUFDdUksYUFBSCxDQUFUO0FBQ0FxQixvQkFBWSxHQUFHakMsS0FBSyxDQUFDOUQsSUFBTixDQUFXLFVBQVFuQyxXQUFSLEdBQW9CLGVBQS9CLENBQWY7QUFDQWlKLHFCQUFhLEdBQUdoRCxLQUFLLENBQUM5RCxJQUFOLENBQVcsVUFBUW5DLFdBQVIsR0FBb0IsZ0JBQS9CLENBQWhCO0FBRUgsT0FORCxNQU1PLElBQUcxQixDQUFDLEtBQUttQixTQUFULEVBQW1CO0FBRXRCLFlBQUduQixDQUFDLENBQUM0SixZQUFGLEtBQW1CekksU0FBdEIsRUFBZ0M7QUFDNUJ5SSxzQkFBWSxHQUFHNUosQ0FBQyxDQUFDNEosWUFBakI7QUFDSDs7QUFDRCxZQUFHNUosQ0FBQyxDQUFDMkssYUFBRixLQUFvQnhKLFNBQXZCLEVBQWlDO0FBQzdCd0osdUJBQWEsR0FBRzNLLENBQUMsQ0FBQzJLLGFBQWxCO0FBQ0g7QUFDSjs7QUFFRCxXQUFLbkQsS0FBTCxDQUFXO0FBQUNjLGtCQUFVLEVBQUNxQztBQUFaLE9BQVg7QUFFQUYsZ0JBQVUsQ0FBQyxZQUFVO0FBRWpCLFlBQUl2RixJQUFJLEdBQUc1RyxDQUFDLENBQUMsTUFBSW9ELFdBQUosR0FBZ0IsUUFBaEIsR0FBeUJBLFdBQXpCLEdBQXFDLFVBQXJDLEdBQWdEOEIsSUFBSSxDQUFDb0IsS0FBTCxDQUFXQyxJQUEzRCxHQUFnRSxVQUFoRSxHQUEyRW5ELFdBQTNFLEdBQXVGLFFBQXhGLENBQUQsQ0FBbUd3SCxNQUE5Rzs7QUFFQSxhQUFLLElBQUlySyxDQUFDLEdBQUcyRSxJQUFJLENBQUNvQixLQUFMLENBQVdFLEtBQXhCLEVBQStCakcsQ0FBQyxJQUFJLENBQXBDLEVBQXVDQSxDQUFDLEVBQXhDLEVBQTRDO0FBRXhDLGNBQUk7QUFDQThNLGtCQUFNLE1BQU4sR0FBWXJOLENBQUMsQ0FBQyxNQUFJa0YsSUFBSSxDQUFDb0IsS0FBTCxDQUFXRyxHQUFYLENBQWVsRyxDQUFDLEdBQUMsQ0FBakIsQ0FBTCxDQUFELENBQTJCcUMsSUFBM0IsR0FBa0NvQyxRQUE5QztBQUNILFdBRkQsQ0FFRSxPQUFNdUksR0FBTixFQUFXLENBQ1Q7QUFDSDs7QUFDRCxjQUFHLE9BQU9GLE1BQU0sTUFBYixLQUFxQixXQUF4QixFQUFvQztBQUVoQ3JOLGFBQUMsQ0FBQyxNQUFJa0YsSUFBSSxDQUFDb0IsS0FBTCxDQUFXRyxHQUFYLENBQWVsRyxDQUFDLEdBQUMsQ0FBakIsQ0FBTCxDQUFELENBQTJCeUUsUUFBM0IsQ0FBb0MsTUFBcEMsRUFBNEM7QUFBRWdGLHdCQUFVLEVBQUVzQjtBQUFkLGFBQTVDO0FBQ0E7QUFFSCxXQUxELE1BS087QUFFSCxnQkFBRy9LLENBQUMsS0FBSyxDQUFOLElBQVdxRyxJQUFJLEdBQUcsQ0FBbEIsSUFBdUIxQixJQUFJLENBQUNuRSxPQUFMLENBQWE2RixJQUFiLEtBQXNCLElBQWhELEVBQXFEO0FBRWpELG1CQUFLLElBQUlKLEtBQUssR0FBR3RCLElBQUksQ0FBQ29CLEtBQUwsQ0FBV0csR0FBWCxDQUFlbUUsTUFBZixHQUFzQixDQUF2QyxFQUEwQ3BFLEtBQUssSUFBSSxDQUFuRCxFQUFzREEsS0FBSyxFQUEzRCxFQUErRDtBQUUzRDZHLHNCQUFNLE1BQU4sR0FBWXJOLENBQUMsQ0FBQyxNQUFJa0YsSUFBSSxDQUFDb0IsS0FBTCxDQUFXRyxHQUFYLENBQWVELEtBQWYsQ0FBTCxDQUFELENBQTZCNUQsSUFBN0IsR0FBb0NvQyxRQUFoRDs7QUFDQSxvQkFBRyxPQUFPcUksTUFBTSxNQUFiLEtBQXFCLFdBQXhCLEVBQW9DO0FBQ2hDck4sbUJBQUMsQ0FBQyxNQUFJa0YsSUFBSSxDQUFDb0IsS0FBTCxDQUFXRyxHQUFYLENBQWVELEtBQWYsQ0FBTCxDQUFELENBQTZCeEIsUUFBN0IsQ0FBc0MsTUFBdEMsRUFBOEM7QUFBRWdGLDhCQUFVLEVBQUVzQjtBQUFkLG1CQUE5QztBQUNBO0FBQ0g7QUFDSjtBQUNKO0FBQ0o7QUFDSjtBQUVKLE9BaENTLEVBZ0NQLEdBaENPLENBQVY7QUFrQ0F0TCxPQUFDLENBQUNtRCxRQUFELENBQUQsQ0FBWXdHLE9BQVosQ0FBcUJ2RyxXQUFXLEdBQUcsZUFBbkMsRUFBb0RpSyxNQUFwRDtBQUNILEtBaHRCZ0I7QUFrdEJqQkcsV0FBTyxFQUFFLG1CQUFZO0FBQ2pCLFVBQUk5TCxDQUFDLEdBQUcxQixDQUFDLENBQUN5TixLQUFGLENBQVEsU0FBUixDQUFSO0FBRUEsV0FBS2hQLFFBQUwsQ0FBY2tMLE9BQWQsQ0FBc0JqSSxDQUF0QjtBQUVBd0IsZUFBUyxDQUFDNEcsR0FBVixDQUFjLGFBQVcxRyxXQUF6QjtBQUVBNEosa0JBQVksQ0FBQyxLQUFLcEgsS0FBTixDQUFaO0FBQ0FvSCxrQkFBWSxDQUFDLEtBQUtuSCxZQUFOLENBQVo7O0FBRUEsVUFBSSxLQUFLOUUsT0FBTCxDQUFhaUcsTUFBYixLQUF3QixJQUE1QixFQUFrQztBQUM5QixhQUFLdkksUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLFNBQW5DLEVBQThDOEosTUFBOUM7QUFDSDs7QUFDRCxXQUFLek8sUUFBTCxDQUFjaUgsSUFBZCxDQUFtQixLQUFLakgsUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLFVBQW5DLEVBQStDc0MsSUFBL0MsRUFBbkI7QUFFQSxXQUFLakgsUUFBTCxDQUFjcUwsR0FBZCxDQUFrQixPQUFsQixFQUEyQixXQUFTMUcsV0FBVCxHQUFxQixTQUFoRDtBQUNBLFdBQUszRSxRQUFMLENBQWNxTCxHQUFkLENBQWtCLE9BQWxCLEVBQTJCLFdBQVMxRyxXQUFULEdBQXFCLGNBQWhEO0FBRUEsV0FBSzNFLFFBQUwsQ0FDS3FMLEdBREwsQ0FDUyxNQUFJMUcsV0FEYixFQUVLc0ssVUFGTCxDQUVnQnRLLFdBRmhCLEVBR0ttQyxJQUhMLENBR1UsT0FIVixFQUdtQixFQUhuQjtBQUtBLFdBQUtZLFFBQUwsQ0FBYytHLE1BQWQ7QUFDQSxXQUFLN0csU0FBTCxDQUFlNkcsTUFBZjtBQUNBLFdBQUt6TyxRQUFMLENBQWNrTCxPQUFkLENBQXNCdEcsTUFBTSxDQUFDSyxTQUE3QjtBQUNBLFdBQUtqRixRQUFMLEdBQWdCLElBQWhCO0FBQ0gsS0E3dUJnQjtBQSt1QmpCa1AsWUFBUSxFQUFFLG9CQUFVO0FBRWhCLGFBQU8sS0FBS2hJLEtBQVo7QUFDSCxLQWx2QmdCO0FBb3ZCakJpSSxZQUFRLEVBQUUsb0JBQVU7QUFFaEIsYUFBTyxLQUFLdEgsS0FBWjtBQUNILEtBdnZCZ0I7QUF5dkJqQnVILFlBQVEsRUFBRSxrQkFBUzVMLEtBQVQsRUFBZTtBQUVyQixXQUFLbEIsT0FBTCxDQUFha0IsS0FBYixHQUFxQkEsS0FBckI7QUFFQSxXQUFLMkYsV0FBTDtBQUVBLFVBQUltRCxVQUFVLEdBQUcsS0FBS3RNLFFBQUwsQ0FBY3VNLFVBQWQsRUFBakI7O0FBQ0EsVUFBRyxLQUFLakssT0FBTCxDQUFha0ssY0FBYixLQUFnQyxJQUFoQyxJQUF3QyxLQUFLbEssT0FBTCxDQUFha0ssY0FBYixJQUErQixjQUExRSxFQUF5RjtBQUNyRixhQUFLNUUsU0FBTCxDQUFlM0gsSUFBZixDQUFvQixNQUFJMEUsV0FBSixHQUFnQixnQkFBcEMsRUFBc0Q1QixHQUF0RCxDQUEwRCxhQUExRCxFQUF5RSxFQUFHdUosVUFBVSxHQUFDLENBQVosR0FBZSxFQUFqQixDQUF6RSxFQUErRkQsSUFBL0Y7QUFDQSxhQUFLekUsU0FBTCxDQUFlM0gsSUFBZixDQUFvQixNQUFJMEUsV0FBSixHQUFnQixnQkFBcEMsRUFBc0Q1QixHQUF0RCxDQUEwRCxjQUExRCxFQUEwRSxFQUFHdUosVUFBVSxHQUFDLENBQVosR0FBZSxFQUFqQixDQUExRSxFQUFnR0QsSUFBaEc7QUFDSDtBQUVKLEtBcndCZ0I7QUF1d0JqQmdELFVBQU0sRUFBRSxnQkFBU0MsR0FBVCxFQUFhO0FBRWpCLFdBQUtoTixPQUFMLENBQWFnTixHQUFiLEdBQW1CQSxHQUFuQjtBQUVBLFdBQUtsRyxpQkFBTCxDQUF1QixLQUF2QjtBQUNILEtBNXdCZ0I7QUE4d0JqQm1HLGFBQVMsRUFBRSxtQkFBU0MsTUFBVCxFQUFnQjtBQUV2QixXQUFLbE4sT0FBTCxDQUFha04sTUFBYixHQUFzQkEsTUFBdEI7QUFFQSxXQUFLcEcsaUJBQUwsQ0FBdUIsS0FBdkI7QUFFSCxLQXB4QmdCO0FBc3hCakJxRyxhQUFTLEVBQUUsbUJBQVNDLE1BQVQsRUFBZ0I7QUFFdkIsVUFBR0EsTUFBSCxFQUFVO0FBQ04sYUFBSzFQLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixNQUFJMEUsV0FBSixHQUFnQixTQUFuQyxFQUE4QzBILElBQTlDO0FBQ0gsT0FGRCxNQUVPO0FBQ0gsYUFBSzdFLFlBQUwsR0FBb0IsQ0FBcEI7QUFDQSxhQUFLeEgsUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLFNBQW5DLEVBQThDOEgsSUFBOUM7QUFDSDtBQUNKLEtBOXhCZ0I7QUFneUJqQmtELFlBQVEsRUFBRSxrQkFBU3BHLEtBQVQsRUFBZTtBQUVyQixXQUFLakgsT0FBTCxDQUFhaUgsS0FBYixHQUFxQkEsS0FBckI7O0FBRUEsVUFBRyxLQUFLL0IsWUFBTCxLQUFzQixDQUF6QixFQUEyQjtBQUN2QixhQUFLMEIsWUFBTDtBQUNIOztBQUVELFVBQUksS0FBS0ksT0FBTCxDQUFhckosSUFBYixDQUFrQixNQUFJMEUsV0FBSixHQUFnQixlQUFsQyxFQUFtRHdILE1BQW5ELEtBQThELENBQWxFLEVBQXFFO0FBQ2pFLGFBQUs3QyxPQUFMLENBQWF0RyxNQUFiLENBQW9CLGdCQUFjMkIsV0FBZCxHQUEwQixzQkFBOUM7QUFDSDs7QUFFRCxXQUFLMkUsT0FBTCxDQUFhckosSUFBYixDQUFrQixNQUFJMEUsV0FBSixHQUFnQixlQUFsQyxFQUFtRHNDLElBQW5ELENBQXdEc0MsS0FBeEQ7QUFDSCxLQTd5QmdCO0FBK3lCakJxRyxlQUFXLEVBQUUscUJBQVNwRyxRQUFULEVBQWtCO0FBRTNCLFVBQUdBLFFBQVEsS0FBSyxFQUFoQixFQUFtQjtBQUVmLGFBQUtGLE9BQUwsQ0FBYXJKLElBQWIsQ0FBa0IsTUFBSTBFLFdBQUosR0FBZ0Isa0JBQWxDLEVBQXNEOEosTUFBdEQ7QUFDQSxhQUFLbkYsT0FBTCxDQUFhcEIsUUFBYixDQUFzQnZELFdBQVcsR0FBQyxhQUFsQztBQUVILE9BTEQsTUFLTztBQUVILFlBQUksS0FBSzJFLE9BQUwsQ0FBYXJKLElBQWIsQ0FBa0IsTUFBSTBFLFdBQUosR0FBZ0Isa0JBQWxDLEVBQXNEd0gsTUFBdEQsS0FBaUUsQ0FBckUsRUFBd0U7QUFDcEUsZUFBSzdDLE9BQUwsQ0FBYXRHLE1BQWIsQ0FBb0IsZUFBYTJCLFdBQWIsR0FBeUIsd0JBQTdDO0FBQ0g7O0FBQ0QsYUFBSzJFLE9BQUwsQ0FBYW1DLFdBQWIsQ0FBeUI5RyxXQUFXLEdBQUMsYUFBckM7QUFFSDs7QUFFRCxXQUFLMkUsT0FBTCxDQUFhckosSUFBYixDQUFrQixNQUFJMEUsV0FBSixHQUFnQixrQkFBbEMsRUFBc0RzQyxJQUF0RCxDQUEyRHVDLFFBQTNEO0FBQ0EsV0FBS2xILE9BQUwsQ0FBYWtILFFBQWIsR0FBd0JBLFFBQXhCO0FBQ0gsS0FqMEJnQjtBQW0wQmpCcUcsV0FBTyxFQUFFLGlCQUFTNUYsSUFBVCxFQUFjO0FBRW5CLFVBQUksS0FBS1gsT0FBTCxDQUFhckosSUFBYixDQUFrQixNQUFJMEUsV0FBSixHQUFnQixjQUFsQyxFQUFrRHdILE1BQWxELEtBQTZELENBQWpFLEVBQW9FO0FBQ2hFLGFBQUs3QyxPQUFMLENBQWFPLE9BQWIsQ0FBcUIsZUFBYWxGLFdBQWIsR0FBeUIsb0JBQTlDO0FBQ0g7O0FBQ0QsV0FBSzJFLE9BQUwsQ0FBYXJKLElBQWIsQ0FBa0IsTUFBSTBFLFdBQUosR0FBZ0IsY0FBbEMsRUFBa0RtQyxJQUFsRCxDQUF1RCxPQUF2RCxFQUFnRW5DLFdBQVcsR0FBQyxlQUFaLEdBQThCc0YsSUFBOUY7QUFDQSxXQUFLM0gsT0FBTCxDQUFhMkgsSUFBYixHQUFvQkEsSUFBcEI7QUFDSCxLQTEwQmdCO0FBNDBCakI2RixlQUFXLEVBQUUscUJBQVM1RixRQUFULEVBQWtCO0FBRTNCLFdBQUtaLE9BQUwsQ0FBYXJKLElBQWIsQ0FBa0IsTUFBSTBFLFdBQUosR0FBZ0IsY0FBbEMsRUFBa0RzQyxJQUFsRCxDQUF1RGlELFFBQXZEO0FBQ0EsV0FBSzVILE9BQUwsQ0FBYTRILFFBQWIsR0FBd0JBLFFBQXhCO0FBQ0gsS0FoMUJnQjtBQWsxQmpCNkYsa0JBQWMsRUFBRSx3QkFBU2hHLFdBQVQsRUFBcUI7QUFDakMsVUFBRyxLQUFLekgsT0FBTCxDQUFhMEgsWUFBYixLQUE4QixJQUFqQyxFQUFzQztBQUNsQyxhQUFLaEssUUFBTCxDQUFjK0MsR0FBZCxDQUFrQixlQUFsQixFQUFtQyxlQUFlZ0gsV0FBZixHQUE2QixFQUFoRTtBQUNIOztBQUNELFdBQUtULE9BQUwsQ0FBYXZHLEdBQWIsQ0FBaUIsWUFBakIsRUFBK0JnSCxXQUEvQjtBQUNBLFdBQUt6SCxPQUFMLENBQWF5SCxXQUFiLEdBQTJCQSxXQUEzQjtBQUNILEtBeDFCZ0I7QUEwMUJqQmlHLGlCQUFhLEVBQUUsdUJBQVN2SCxVQUFULEVBQW9CO0FBQy9CLFVBQUdBLFVBQVUsS0FBSyxLQUFsQixFQUF3QjtBQUNwQixhQUFLbkcsT0FBTCxDQUFhbUcsVUFBYixHQUEwQixJQUExQjtBQUNBLGFBQUt6SSxRQUFMLENBQWMrQyxHQUFkLENBQWtCLFlBQWxCLEVBQWdDLEVBQWhDO0FBQ0gsT0FIRCxNQUdNO0FBQ0YsYUFBSy9DLFFBQUwsQ0FBYytDLEdBQWQsQ0FBa0IsWUFBbEIsRUFBZ0MwRixVQUFoQztBQUNBLGFBQUtuRyxPQUFMLENBQWFtRyxVQUFiLEdBQTBCQSxVQUExQjtBQUNIO0FBQ0osS0FsMkJnQjtBQW8yQmpCd0gsYUFBUyxFQUFFLG1CQUFTQyxNQUFULEVBQWdCO0FBRXZCLFVBQUksQ0FBQ3RILEtBQUssQ0FBQzlDLFFBQVEsQ0FBQyxLQUFLeEQsT0FBTCxDQUFhcUcsTUFBZCxDQUFULENBQVYsRUFBMkM7QUFDdkMsYUFBS3JHLE9BQUwsQ0FBYXFHLE1BQWIsR0FBc0J1SCxNQUF0QjtBQUNBLGFBQUtsUSxRQUFMLENBQWMrQyxHQUFkLENBQWtCLFNBQWxCLEVBQTZCbU4sTUFBN0I7QUFDQSxhQUFLdEksU0FBTCxDQUFlN0UsR0FBZixDQUFtQixTQUFuQixFQUE4Qm1OLE1BQU0sR0FBQyxDQUFyQztBQUNBLGFBQUt4SSxRQUFMLENBQWMzRSxHQUFkLENBQWtCLFNBQWxCLEVBQTZCbU4sTUFBTSxHQUFDLENBQXBDO0FBQ0g7QUFDSixLQTUyQmdCO0FBODJCakJDLGlCQUFhLEVBQUUsdUJBQVM5TSxLQUFULEVBQWU7QUFFMUIsVUFBR0EsS0FBSCxFQUFTO0FBQ0wsYUFBS2tFLFlBQUwsR0FBb0IsSUFBcEI7QUFDQSxhQUFLdkgsUUFBTCxDQUFja0ksUUFBZCxDQUF1QixjQUF2QjtBQUNILE9BSEQsTUFHTztBQUNILGFBQUtYLFlBQUwsR0FBb0IsS0FBcEI7QUFDQSxhQUFLdkgsUUFBTCxDQUFjeUwsV0FBZCxDQUEwQixjQUExQjtBQUNIO0FBRUosS0F4M0JnQjtBQTAzQmpCMkUsY0FBVSxFQUFFLG9CQUFTcEosT0FBVCxFQUFpQjtBQUV6QixVQUFJLFFBQU9BLE9BQVAsS0FBa0IsUUFBdEIsRUFBZ0M7QUFDNUIsWUFBSXFKLE9BQU8sR0FBR3JKLE9BQU8sV0FBUCxJQUFtQixLQUFqQzs7QUFDQSxZQUFHcUosT0FBTyxLQUFLLElBQWYsRUFBb0I7QUFDaEIsZUFBS3JKLE9BQUwsR0FBZUEsT0FBTyxDQUFDQSxPQUF2QjtBQUNIOztBQUNEQSxlQUFPLEdBQUdBLE9BQU8sQ0FBQ0EsT0FBbEI7QUFDSDs7QUFDRCxVQUFJLEtBQUsxRSxPQUFMLENBQWFpRyxNQUFiLEtBQXdCLEtBQTVCLEVBQW1DO0FBQy9CLGFBQUt2SSxRQUFMLENBQWNDLElBQWQsQ0FBbUIsTUFBSTBFLFdBQUosR0FBZ0IsVUFBbkMsRUFBK0NzQyxJQUEvQyxDQUFvREQsT0FBcEQ7QUFDSDtBQUVKLEtBdjRCZ0I7QUF5NEJqQnNKLG1CQUFlLEVBQUUseUJBQVMvRSxVQUFULEVBQW9CO0FBRWpDLFdBQUtqSixPQUFMLENBQWF1SyxZQUFiLEdBQTRCdEIsVUFBNUI7QUFDSCxLQTU0QmdCO0FBODRCakJnRixvQkFBZ0IsRUFBRSwwQkFBU2hGLFVBQVQsRUFBb0I7QUFFbEMsV0FBS2pKLE9BQUwsQ0FBYXNMLGFBQWIsR0FBNkJyQyxVQUE3QjtBQUNILEtBajVCZ0I7QUFtNUJqQmlGLGdCQUFZLEVBQUUsd0JBQVU7QUFFcEIsV0FBS3hRLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixNQUFJMEUsV0FBSixHQUFnQixVQUFuQyxFQUErQ3NDLElBQS9DLENBQW9ELEtBQUtELE9BQXpEO0FBRUgsS0F2NUJnQjtBQXk1QmpCeUosZ0JBQVksRUFBRSx3QkFBVTtBQUVwQixVQUFJLENBQUMsS0FBS3pRLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixNQUFJMEUsV0FBSixHQUFnQixTQUFuQyxFQUE4Q3dILE1BQW5ELEVBQTJEO0FBQ3ZELGFBQUtuTSxRQUFMLENBQWNnRCxNQUFkLENBQXFCLGlCQUFlMkIsV0FBZixHQUEyQix3QkFBaEQ7QUFDSDs7QUFDRCxXQUFLM0UsUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLFNBQW5DLEVBQThDNUIsR0FBOUMsQ0FBa0Q7QUFDOUN1TSxXQUFHLEVBQUUsS0FBSzlILFlBRG9DO0FBRTlDa0osb0JBQVksRUFBRSxLQUFLcE8sT0FBTCxDQUFhdUc7QUFGbUIsT0FBbEQ7QUFJSCxLQWw2QmdCO0FBbzZCakI4SCxlQUFXLEVBQUUsdUJBQVU7QUFFbkIsVUFBSUMsT0FBTyxHQUFHLEtBQUs1USxRQUFMLENBQWNDLElBQWQsQ0FBbUIsTUFBSTBFLFdBQUosR0FBZ0IsU0FBbkMsQ0FBZDs7QUFFQSxVQUFJLENBQUNpTSxPQUFPLENBQUN6RSxNQUFiLEVBQXFCO0FBQ2pCLGFBQUtuTSxRQUFMLENBQWM2SixPQUFkLENBQXNCLGlCQUFlbEYsV0FBZixHQUEyQix3QkFBakQ7QUFDQWlNLGVBQU8sR0FBRyxLQUFLNVEsUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLFNBQW5DLEVBQThDNUIsR0FBOUMsQ0FBa0QsZUFBbEQsRUFBbUUsS0FBS1QsT0FBTCxDQUFhdUcsTUFBaEYsQ0FBVjtBQUNIOztBQUNEK0gsYUFBTyxDQUFDbkYsV0FBUixDQUFvQixRQUFwQixFQUE4QnZELFFBQTlCLENBQXVDLFNBQXZDO0FBQ0F3RixnQkFBVSxDQUFDLFlBQVU7QUFDakJrRCxlQUFPLENBQUNuQyxNQUFSO0FBQ0gsT0FGUyxFQUVSLEdBRlEsQ0FBVjtBQUdILEtBaDdCZ0I7QUFrN0JqQnRGLGVBQVcsRUFBRSx1QkFBVTtBQUVuQixVQUFJMUMsSUFBSSxHQUFHLElBQVg7QUFFQSxXQUFLekcsUUFBTCxDQUFjK0MsR0FBZCxDQUFrQixXQUFsQixFQUErQixLQUFLVCxPQUFMLENBQWFrQixLQUE1Qzs7QUFFQSxVQUFHOEIsSUFBSSxFQUFQLEVBQVU7QUFDTixZQUFJZ0gsVUFBVSxHQUFHN0YsSUFBSSxDQUFDbkUsT0FBTCxDQUFha0IsS0FBOUI7O0FBRUEsWUFBRzhJLFVBQVUsQ0FBQ3VFLFFBQVgsR0FBc0I3SyxLQUF0QixDQUE0QixHQUE1QixFQUFpQ21HLE1BQWpDLEdBQTBDLENBQTdDLEVBQStDO0FBQzNDRyxvQkFBVSxHQUFHN0YsSUFBSSxDQUFDekcsUUFBTCxDQUFjdU0sVUFBZCxFQUFiO0FBQ0g7O0FBQ0Q5RixZQUFJLENBQUN6RyxRQUFMLENBQWMrQyxHQUFkLENBQWtCO0FBQ2QrTixjQUFJLEVBQUUsS0FEUTtBQUVkQyxvQkFBVSxFQUFFLEVBQUV6RSxVQUFVLEdBQUMsQ0FBYjtBQUZFLFNBQWxCO0FBSUg7QUFDSixLQW44QmdCO0FBcThCakJsRCxxQkFBaUIsRUFBRSwyQkFBUzRILEtBQVQsRUFBZTtBQUU5QixVQUFHLEtBQUsxTyxPQUFMLENBQWFnTixHQUFiLEtBQXFCLElBQXJCLElBQTZCLEtBQUtoTixPQUFMLENBQWFnTixHQUFiLEtBQXFCLEtBQXJELEVBQTJEO0FBQ3ZELGFBQUt0UCxRQUFMLENBQWMrQyxHQUFkLENBQWtCLFlBQWxCLEVBQWdDLEtBQUtULE9BQUwsQ0FBYWdOLEdBQTdDOztBQUNBLFlBQUcsS0FBS2hOLE9BQUwsQ0FBYWdOLEdBQWIsS0FBcUIsQ0FBeEIsRUFBMEI7QUFDdEIsZUFBS3RQLFFBQUwsQ0FBYytDLEdBQWQsQ0FBa0I7QUFDZGtPLGdDQUFvQixFQUFFLENBRFI7QUFFZEMsK0JBQW1CLEVBQUU7QUFGUCxXQUFsQjtBQUlIO0FBQ0osT0FSRCxNQVFPO0FBQ0gsWUFBR0YsS0FBSyxLQUFLLEtBQWIsRUFBbUI7QUFDZixlQUFLaFIsUUFBTCxDQUFjK0MsR0FBZCxDQUFrQjtBQUNkb08scUJBQVMsRUFBRSxFQURHO0FBRWRULHdCQUFZLEVBQUUsS0FBS3BPLE9BQUwsQ0FBYXVHO0FBRmIsV0FBbEI7QUFJSDtBQUNKOztBQUNELFVBQUksS0FBS3ZHLE9BQUwsQ0FBYWtOLE1BQWIsS0FBd0IsSUFBeEIsSUFBZ0MsS0FBS2xOLE9BQUwsQ0FBYWtOLE1BQWIsS0FBd0IsS0FBNUQsRUFBa0U7QUFDOUQsYUFBS3hQLFFBQUwsQ0FBYytDLEdBQWQsQ0FBa0IsZUFBbEIsRUFBbUMsS0FBS1QsT0FBTCxDQUFha04sTUFBaEQ7O0FBQ0EsWUFBRyxLQUFLbE4sT0FBTCxDQUFha04sTUFBYixLQUF3QixDQUEzQixFQUE2QjtBQUN6QixlQUFLeFAsUUFBTCxDQUFjK0MsR0FBZCxDQUFrQjtBQUNkcU8sbUNBQXVCLEVBQUUsQ0FEWDtBQUVkQyxrQ0FBc0IsRUFBRTtBQUZWLFdBQWxCO0FBSUg7QUFDSixPQVJELE1BUU87QUFDSCxZQUFHTCxLQUFLLEtBQUssS0FBYixFQUFtQjtBQUNmLGVBQUtoUixRQUFMLENBQWMrQyxHQUFkLENBQWtCO0FBQ2R1Tyx3QkFBWSxFQUFFLEVBREE7QUFFZFosd0JBQVksRUFBRSxLQUFLcE8sT0FBTCxDQUFhdUc7QUFGYixXQUFsQjtBQUlIO0FBQ0o7QUFFSixLQXgrQmdCO0FBMCtCakJvRixnQkFBWSxFQUFFLHdCQUFVO0FBRXBCLFVBQUl4SCxJQUFJLEdBQUcsSUFBWDtBQUFBLFVBQ0k4SyxZQUFZLEdBQUcvTSxPQUFPLENBQUNnTixNQUFSLEVBRG5CO0FBQUEsVUFFSS9KLFdBQVcsR0FBRyxLQUFLekgsUUFBTCxDQUFjeVIsV0FBZCxFQUZsQjtBQUFBLFVBR0luRixVQUFVLEdBQUcsS0FBS3RNLFFBQUwsQ0FBY3VNLFVBQWQsRUFIakI7QUFBQSxVQUlJbUYsYUFBYSxHQUFHLEtBQUsxUixRQUFMLENBQWNDLElBQWQsQ0FBbUIsTUFBSTBFLFdBQUosR0FBZ0IsVUFBbkMsRUFBK0MsQ0FBL0MsRUFBa0RnTixZQUp0RTtBQUFBLFVBS0lGLFdBQVcsR0FBR0MsYUFBYSxHQUFHLEtBQUtsSyxZQUx2QztBQUFBLFVBTUlvSyxhQUFhLEdBQUcsS0FBSzVSLFFBQUwsQ0FBYzZSLFdBQWQsS0FBOEIsS0FBS3JLLFlBTnZEO0FBQUEsVUFPSXNLLFdBQVcsR0FBR2hNLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSzlGLFFBQUwsQ0FBYzZSLFdBQWQsS0FBOEIsQ0FBL0IsSUFBb0MsQ0FBdEMsQ0FBRCxDQUFSLEdBQXFELElBUHZFO0FBQUEsVUFRSUUsU0FBUyxHQUFHLEtBQUtySixLQUFMLENBQVdxSixTQUFYLEVBUmhCO0FBQUEsVUFTSUMsVUFBVSxHQUFHLENBVGpCOztBQVdBLFVBQUcxTSxJQUFJLEVBQVAsRUFBVTtBQUNOLFlBQUlnSCxVQUFVLElBQUk5SCxPQUFPLENBQUNoQixLQUFSLEVBQWQsSUFBaUMsS0FBSytELFlBQUwsS0FBc0IsSUFBM0QsRUFBaUU7QUFDN0QsZUFBS3ZILFFBQUwsQ0FBYytDLEdBQWQsQ0FBa0I7QUFDZCtOLGdCQUFJLEVBQUUsR0FEUTtBQUVkQyxzQkFBVSxFQUFFO0FBRkUsV0FBbEI7QUFJSCxTQUxELE1BS087QUFDSCxlQUFLL1EsUUFBTCxDQUFjK0MsR0FBZCxDQUFrQjtBQUNkK04sZ0JBQUksRUFBRSxLQURRO0FBRWRDLHNCQUFVLEVBQUUsRUFBRXpFLFVBQVUsR0FBQyxDQUFiO0FBRkUsV0FBbEI7QUFJSDtBQUNKOztBQUVELFVBQUcsS0FBS2hLLE9BQUwsQ0FBYTBILFlBQWIsS0FBOEIsSUFBOUIsSUFBc0MsS0FBSzFILE9BQUwsQ0FBYWlILEtBQWIsS0FBdUIsRUFBaEUsRUFBbUU7QUFDL0R5SSxrQkFBVSxHQUFHLENBQWI7QUFDSDs7QUFFRCxVQUFHLEtBQUtoUyxRQUFMLENBQWNDLElBQWQsQ0FBbUIsTUFBSTBFLFdBQUosR0FBZ0IsU0FBbkMsRUFBOEN3SCxNQUE5QyxJQUF3RCxLQUFLbk0sUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLFNBQW5DLEVBQThDc04sRUFBOUMsQ0FBaUQsVUFBakQsQ0FBM0QsRUFBeUg7QUFDckgsYUFBS3pLLFlBQUwsR0FBb0IxQixRQUFRLENBQUMsS0FBSzlGLFFBQUwsQ0FBY0MsSUFBZCxDQUFtQixNQUFJMEUsV0FBSixHQUFnQixTQUFuQyxFQUE4Q2tOLFdBQTlDLEVBQUQsQ0FBNUI7QUFDQSxhQUFLN1IsUUFBTCxDQUFjK0MsR0FBZCxDQUFrQixVQUFsQixFQUE4QixRQUE5QjtBQUNILE9BSEQsTUFHTztBQUNILGFBQUt5RSxZQUFMLEdBQW9CLENBQXBCO0FBQ0EsYUFBS3hILFFBQUwsQ0FBYytDLEdBQWQsQ0FBa0IsVUFBbEIsRUFBOEIsRUFBOUI7QUFDSDs7QUFFRCxVQUFHLEtBQUsvQyxRQUFMLENBQWNDLElBQWQsQ0FBbUIsTUFBSTBFLFdBQUosR0FBZ0IsU0FBbkMsRUFBOEN3SCxNQUFqRCxFQUF3RDtBQUNwRCxhQUFLbk0sUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLFNBQW5DLEVBQThDNUIsR0FBOUMsQ0FBa0QsS0FBbEQsRUFBeUQsS0FBS3lFLFlBQTlEO0FBQ0g7O0FBRUQsVUFBR0MsV0FBVyxLQUFLLEtBQUtBLFdBQXhCLEVBQW9DO0FBQ2hDLGFBQUtBLFdBQUwsR0FBbUJBLFdBQW5COztBQUVBLFlBQUksS0FBS25GLE9BQUwsQ0FBYTRQLFFBQWIsSUFBeUIsT0FBTyxLQUFLNVAsT0FBTCxDQUFhNFAsUUFBcEIsS0FBa0MsVUFBL0QsRUFBMkU7QUFDdkUsZUFBSzVQLE9BQUwsQ0FBYTRQLFFBQWIsQ0FBc0IsSUFBdEI7QUFDSDtBQUNKOztBQUVELFVBQUcsS0FBS2hMLEtBQUwsSUFBY3RDLE1BQU0sQ0FBQ0ksTUFBckIsSUFBK0IsS0FBS2tDLEtBQUwsSUFBY3RDLE1BQU0sQ0FBQ0csT0FBdkQsRUFBK0Q7QUFFM0QsWUFBSSxLQUFLekMsT0FBTCxDQUFhaUcsTUFBYixLQUF3QixJQUE1QixFQUFrQztBQUU5QjtBQUNBLGNBQUdnSixZQUFZLEdBQUksS0FBS2pQLE9BQUwsQ0FBYWtHLFlBQWIsR0FBNEIsS0FBS2hCLFlBQWpDLEdBQThDd0ssVUFBOUQsSUFBNkUsS0FBS3pLLFlBQUwsS0FBc0IsSUFBdEcsRUFBMkc7QUFDdkcsaUJBQUt2SCxRQUFMLENBQWNDLElBQWQsQ0FBbUIsTUFBSTBFLFdBQUosR0FBZ0IsU0FBbkMsRUFBOEM1QixHQUE5QyxDQUFtRCxRQUFuRCxFQUE2RHdPLFlBQVksSUFBSSxLQUFLL0osWUFBTCxHQUFrQndLLFVBQXRCLENBQXpFO0FBQ0gsV0FGRCxNQUVPO0FBQ0gsaUJBQUtoUyxRQUFMLENBQWNDLElBQWQsQ0FBbUIsTUFBSTBFLFdBQUosR0FBZ0IsU0FBbkMsRUFBOEM1QixHQUE5QyxDQUFtRCxRQUFuRCxFQUE2RCxLQUFLVCxPQUFMLENBQWFrRyxZQUExRTtBQUNIO0FBQ0o7O0FBRUQsWUFBR2YsV0FBVyxJQUFJOEosWUFBbEIsRUFBK0I7QUFDM0IsZUFBS3ZSLFFBQUwsQ0FBY2tJLFFBQWQsQ0FBdUIsWUFBdkI7QUFDSCxTQUZELE1BRU87QUFDSCxlQUFLbEksUUFBTCxDQUFjeUwsV0FBZCxDQUEwQixZQUExQjtBQUNIOztBQUVELFlBQUcsS0FBS2xFLFlBQUwsS0FBc0IsS0FBdEIsSUFBK0IsS0FBS3ZILFFBQUwsQ0FBY3dELEtBQWQsTUFBeUJnQixPQUFPLENBQUNoQixLQUFSLEVBQTNELEVBQTRFO0FBQ3hFLGVBQUt4RCxRQUFMLENBQWNDLElBQWQsQ0FBbUIsTUFBSTBFLFdBQUosR0FBZ0Isb0JBQW5DLEVBQXlEOEgsSUFBekQ7QUFDSCxTQUZELE1BRU87QUFDSCxlQUFLek0sUUFBTCxDQUFjQyxJQUFkLENBQW1CLE1BQUkwRSxXQUFKLEdBQWdCLG9CQUFuQyxFQUF5RDBILElBQXpEO0FBQ0g7O0FBQ0QsYUFBSzhGLGFBQUw7O0FBRUEsWUFBRyxLQUFLNUssWUFBTCxLQUFzQixLQUF6QixFQUErQjtBQUMzQmdLLHNCQUFZLEdBQUdBLFlBQVksSUFBSTNMLFVBQVUsQ0FBQyxLQUFLdEQsT0FBTCxDQUFhZ04sR0FBZCxDQUFWLElBQWdDLENBQXBDLENBQVosSUFBc0QxSixVQUFVLENBQUMsS0FBS3RELE9BQUwsQ0FBYWtOLE1BQWQsQ0FBVixJQUFtQyxDQUF6RixDQUFmO0FBQ0gsU0EzQjBELENBNEIzRDs7O0FBQ0EsWUFBSWlDLFdBQVcsR0FBR0YsWUFBbEIsRUFBZ0M7QUFDNUIsY0FBRyxLQUFLalAsT0FBTCxDQUFhZ04sR0FBYixHQUFtQixDQUFuQixJQUF3QixLQUFLaE4sT0FBTCxDQUFha04sTUFBYixLQUF3QixJQUFoRCxJQUF3RGtDLGFBQWEsR0FBR2xOLE9BQU8sQ0FBQ2dOLE1BQVIsRUFBM0UsRUFBNEY7QUFDeEYsaUJBQUt4UixRQUFMLENBQWNrSSxRQUFkLENBQXVCLGtCQUF2QjtBQUNIOztBQUNELGNBQUcsS0FBSzVGLE9BQUwsQ0FBYWtOLE1BQWIsR0FBc0IsQ0FBdEIsSUFBMkIsS0FBS2xOLE9BQUwsQ0FBYWdOLEdBQWIsS0FBcUIsSUFBaEQsSUFBd0RvQyxhQUFhLEdBQUdsTixPQUFPLENBQUNnTixNQUFSLEVBQTNFLEVBQTRGO0FBQ3hGLGlCQUFLeFIsUUFBTCxDQUFja0ksUUFBZCxDQUF1QixlQUF2QjtBQUNIOztBQUNEM0csV0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVMkcsUUFBVixDQUFtQnZELFdBQVcsR0FBQyxhQUEvQjtBQUNBLGVBQUszRSxRQUFMLENBQWMrQyxHQUFkLENBQW1CLFFBQW5CLEVBQTZCd08sWUFBN0I7QUFFSCxTQVZELE1BVU87QUFDSCxlQUFLdlIsUUFBTCxDQUFjK0MsR0FBZCxDQUFrQixRQUFsQixFQUE0QjJPLGFBQWEsSUFBSSxLQUFLbEssWUFBTCxHQUFrQndLLFVBQXRCLENBQXpDO0FBQ0EsZUFBS2hTLFFBQUwsQ0FBY3lMLFdBQWQsQ0FBMEIsZ0NBQTFCO0FBQ0FsSyxXQUFDLENBQUMsTUFBRCxDQUFELENBQVVrSyxXQUFWLENBQXNCOUcsV0FBVyxHQUFDLGFBQWxDO0FBQ0g7O0FBRUQsU0FBQyxTQUFTeU4sV0FBVCxHQUFzQjtBQUNuQixjQUFHVixhQUFhLEdBQUdFLGFBQWhCLElBQWlDSCxXQUFXLEdBQUdGLFlBQWxELEVBQStEO0FBQzNEOUssZ0JBQUksQ0FBQ3pHLFFBQUwsQ0FBY2tJLFFBQWQsQ0FBdUIsV0FBdkI7QUFDQXpCLGdCQUFJLENBQUNpQyxLQUFMLENBQVczRixHQUFYLENBQWUsUUFBZixFQUF5QjBFLFdBQVcsSUFBSWhCLElBQUksQ0FBQ2UsWUFBTCxHQUFrQndLLFVBQXRCLENBQXBDO0FBQ0gsV0FIRCxNQUdPO0FBQ0h2TCxnQkFBSSxDQUFDekcsUUFBTCxDQUFjeUwsV0FBZCxDQUEwQixXQUExQjtBQUNBaEYsZ0JBQUksQ0FBQ2lDLEtBQUwsQ0FBVzNGLEdBQVgsQ0FBZSxRQUFmLEVBQXlCLE1BQXpCO0FBQ0g7QUFDSixTQVJEOztBQVVBLFNBQUMsU0FBU3NQLFdBQVQsR0FBc0I7QUFDbkIsY0FBSVQsYUFBYSxHQUFHRyxTQUFoQixHQUE2QkwsYUFBYSxHQUFHLEVBQWpELEVBQXNEO0FBQ2xEakwsZ0JBQUksQ0FBQ3pHLFFBQUwsQ0FBY2tJLFFBQWQsQ0FBdUIsV0FBdkI7QUFDSCxXQUZELE1BRU87QUFDSHpCLGdCQUFJLENBQUN6RyxRQUFMLENBQWN5TCxXQUFkLENBQTBCLFdBQTFCO0FBQ0g7QUFDSixTQU5EO0FBUUg7QUFDSixLQTdsQ2dCO0FBK2xDakIwRyxpQkFBYSxFQUFFLHlCQUFVO0FBQ3JCLFVBQUlHLFlBQVksR0FBRyxLQUFLaEosT0FBTCxDQUFhckosSUFBYixDQUFrQixNQUFJMEUsV0FBSixHQUFnQixpQkFBbEMsRUFBcUQ0TixVQUFyRCxLQUFrRSxFQUFyRjs7QUFDQSxVQUFHLEtBQUtqUSxPQUFMLENBQWEwRyxHQUFiLEtBQXFCLElBQXhCLEVBQTZCO0FBQ3pCLGFBQUtNLE9BQUwsQ0FBYXZHLEdBQWIsQ0FBaUIsY0FBakIsRUFBaUN1UCxZQUFqQztBQUNILE9BRkQsTUFFTztBQUNILGFBQUtoSixPQUFMLENBQWF2RyxHQUFiLENBQWlCLGVBQWpCLEVBQWtDdVAsWUFBbEM7QUFDSDtBQUNKO0FBdG1DZ0IsR0FBckI7QUEybUNBOU4sU0FBTyxDQUFDNkcsR0FBUixDQUFZLFVBQVExRyxXQUFwQixFQUFpQ3hFLEVBQWpDLENBQW9DLFVBQVF3RSxXQUE1QyxFQUF5RCxVQUFTMUIsQ0FBVCxFQUFZO0FBRWpFLFFBQUl1UCxTQUFTLEdBQUc5TixRQUFRLENBQUNxRyxRQUFULENBQWtCQyxJQUFsQzs7QUFFQSxRQUFHdEosTUFBTSxDQUFDMEUsU0FBUCxDQUFpQkMsUUFBakIsS0FBOEIsQ0FBOUIsSUFBbUMsQ0FBQzlFLENBQUMsQ0FBQyxNQUFJb0QsV0FBTCxDQUFELENBQW1Cc04sRUFBbkIsQ0FBc0IsVUFBdEIsQ0FBdkMsRUFBeUU7QUFFckUsVUFBSTtBQUNBLFlBQUk5TixJQUFJLEdBQUc1QyxDQUFDLENBQUNpUixTQUFELENBQUQsQ0FBYXJPLElBQWIsRUFBWDs7QUFDQSxZQUFHLE9BQU9BLElBQVAsS0FBZ0IsV0FBbkIsRUFBK0I7QUFDM0IsY0FBR0EsSUFBSSxDQUFDb0MsUUFBTCxDQUFjakUsT0FBZCxDQUFzQitELFFBQXRCLEtBQW1DLEtBQXRDLEVBQTRDO0FBQ3hDOUUsYUFBQyxDQUFDaVIsU0FBRCxDQUFELENBQWFqTSxRQUFiLENBQXNCLE1BQXRCO0FBQ0g7QUFDSjtBQUNKLE9BUEQsQ0FPRSxPQUFNOEIsR0FBTixFQUFXO0FBQUU7QUFBMEI7QUFDNUM7QUFFSixHQWhCRDtBQWtCQTdELFNBQU8sQ0FBQzZHLEdBQVIsQ0FBWSxnQkFBYzFHLFdBQTFCLEVBQXVDeEUsRUFBdkMsQ0FBMEMsZ0JBQWN3RSxXQUF4RCxFQUFxRSxVQUFTMUIsQ0FBVCxFQUFZO0FBRTdFLFFBQUl1UCxTQUFTLEdBQUc5TixRQUFRLENBQUNxRyxRQUFULENBQWtCQyxJQUFsQztBQUNBLFFBQUk3RyxJQUFJLEdBQUc1QyxDQUFDLENBQUNpUixTQUFELENBQUQsQ0FBYXJPLElBQWIsRUFBWDs7QUFFQSxRQUFHcU8sU0FBUyxLQUFLLEVBQWpCLEVBQW9CO0FBQ2hCLFVBQUk7QUFDQSxZQUFHLE9BQU9yTyxJQUFQLEtBQWdCLFdBQWhCLElBQStCNUMsQ0FBQyxDQUFDaVIsU0FBRCxDQUFELENBQWFqTSxRQUFiLENBQXNCLFVBQXRCLE1BQXNDLFNBQXhFLEVBQWtGO0FBRTlFbUgsb0JBQVUsQ0FBQyxZQUFVO0FBQ2pCbk0sYUFBQyxDQUFDaVIsU0FBRCxDQUFELENBQWFqTSxRQUFiLENBQXNCLE1BQXRCO0FBQ0gsV0FGUyxFQUVSLEdBRlEsQ0FBVjtBQUdIO0FBQ0osT0FQRCxDQU9FLE9BQU04QixHQUFOLEVBQVc7QUFBRTtBQUEwQjtBQUU1QyxLQVZELE1BVU87QUFFSCxVQUFHM0csTUFBTSxDQUFDMEUsU0FBUCxDQUFpQkUsT0FBcEIsRUFBNEI7QUFDeEIvRSxTQUFDLENBQUNNLElBQUYsQ0FBUU4sQ0FBQyxDQUFDLE1BQUlvRCxXQUFMLENBQVQsRUFBNkIsVUFBU29ELEtBQVQsRUFBZ0I2QyxLQUFoQixFQUF1QjtBQUNoRCxjQUFJckosQ0FBQyxDQUFDcUosS0FBRCxDQUFELENBQVN6RyxJQUFULEdBQWdCb0MsUUFBaEIsS0FBNkJuQyxTQUFqQyxFQUE0QztBQUN4QyxnQkFBSThDLEtBQUssR0FBRzNGLENBQUMsQ0FBQ3FKLEtBQUQsQ0FBRCxDQUFTckUsUUFBVCxDQUFrQixVQUFsQixDQUFaOztBQUNBLGdCQUFHVyxLQUFLLElBQUksUUFBVCxJQUFxQkEsS0FBSyxJQUFJLFNBQWpDLEVBQTJDO0FBQ3ZDM0YsZUFBQyxDQUFDcUosS0FBRCxDQUFELENBQVNyRSxRQUFULENBQWtCLE9BQWxCO0FBQ0g7QUFDSjtBQUNKLFNBUEQ7QUFRSDtBQUNKO0FBR0osR0E5QkQ7QUFnQ0E5QixXQUFTLENBQUM0RyxHQUFWLENBQWMsT0FBZCxFQUF1QixXQUFTMUcsV0FBVCxHQUFxQixRQUE1QyxFQUFzRHhFLEVBQXRELENBQXlELE9BQXpELEVBQWtFLFdBQVN3RSxXQUFULEdBQXFCLFFBQXZGLEVBQWlHLFVBQVMxQixDQUFULEVBQVk7QUFDekdBLEtBQUMsQ0FBQ3FJLGNBQUY7QUFFQSxRQUFJVixLQUFLLEdBQUdySixDQUFDLENBQUMsTUFBSW9ELFdBQUosR0FBZ0IsVUFBakIsQ0FBYjtBQUNBLFFBQUk4TixTQUFTLEdBQUdsUixDQUFDLENBQUMwQixDQUFDLENBQUN1SSxhQUFILENBQUQsQ0FBbUIxRSxJQUFuQixDQUF3QixVQUFRbkMsV0FBUixHQUFvQixPQUE1QyxDQUFoQjtBQUNBLFFBQUlrSSxZQUFZLEdBQUd0TCxDQUFDLENBQUMwQixDQUFDLENBQUN1SSxhQUFILENBQUQsQ0FBbUIxRSxJQUFuQixDQUF3QixVQUFRbkMsV0FBUixHQUFvQixlQUE1QyxDQUFuQjtBQUNBLFFBQUlpSixhQUFhLEdBQUdyTSxDQUFDLENBQUMwQixDQUFDLENBQUN1SSxhQUFILENBQUQsQ0FBbUIxRSxJQUFuQixDQUF3QixVQUFRbkMsV0FBUixHQUFvQixnQkFBNUMsQ0FBcEI7O0FBRUEsUUFBR2lKLGFBQWEsS0FBS3hKLFNBQXJCLEVBQStCO0FBQzNCd0csV0FBSyxDQUFDckUsUUFBTixDQUFlLE9BQWYsRUFBd0I7QUFDcEJnRixrQkFBVSxFQUFFcUM7QUFEUSxPQUF4QjtBQUdILEtBSkQsTUFJTztBQUNIaEQsV0FBSyxDQUFDckUsUUFBTixDQUFlLE9BQWY7QUFDSDs7QUFFRG1ILGNBQVUsQ0FBQyxZQUFVO0FBQ2pCLFVBQUdiLFlBQVksS0FBS3pJLFNBQXBCLEVBQThCO0FBQzFCN0MsU0FBQyxDQUFDa1IsU0FBRCxDQUFELENBQWFsTSxRQUFiLENBQXNCLE1BQXRCLEVBQThCO0FBQzFCZ0Ysb0JBQVUsRUFBRXNCO0FBRGMsU0FBOUI7QUFHSCxPQUpELE1BSU87QUFDSHRMLFNBQUMsQ0FBQ2tSLFNBQUQsQ0FBRCxDQUFhbE0sUUFBYixDQUFzQixNQUF0QjtBQUNIO0FBQ0osS0FSUyxFQVFQLEdBUk8sQ0FBVjtBQVNILEdBekJEO0FBMkJBOUIsV0FBUyxDQUFDNEcsR0FBVixDQUFjLFdBQVMxRyxXQUF2QixFQUFvQ3hFLEVBQXBDLENBQXVDLFdBQVN3RSxXQUFoRCxFQUE2RCxVQUFTcUksS0FBVCxFQUFnQjtBQUV6RSxRQUFJekwsQ0FBQyxDQUFDLE1BQUlvRCxXQUFKLEdBQWdCLFVBQWpCLENBQUQsQ0FBOEJ3SCxNQUFsQyxFQUEwQztBQUN0QyxVQUFJdkIsS0FBSyxHQUFHckosQ0FBQyxDQUFDLE1BQUlvRCxXQUFKLEdBQWdCLFVBQWpCLENBQUQsQ0FBOEIsQ0FBOUIsRUFBaUMrQixFQUE3QztBQUFBLFVBQ0ltQixLQUFLLEdBQUd0RyxDQUFDLENBQUMsTUFBSXFKLEtBQUwsQ0FBRCxDQUFhckUsUUFBYixDQUFzQixVQUF0QixDQURaO0FBQUEsVUFFSXRELENBQUMsR0FBRytKLEtBQUssSUFBSXRMLE1BQU0sQ0FBQ3NMLEtBRnhCO0FBQUEsVUFHSTBGLE1BQU0sR0FBR3pQLENBQUMsQ0FBQ3lQLE1BQUYsSUFBWXpQLENBQUMsQ0FBQzBQLFVBSDNCO0FBQUEsVUFJSS9ELE1BQU0sR0FBRyxFQUpiOztBQU1BLFVBQUdoRSxLQUFLLEtBQUt4RyxTQUFWLElBQXVCeUQsS0FBSyxDQUFDQyxJQUFOLEtBQWUxRCxTQUF0QyxJQUFtRCxDQUFDbkIsQ0FBQyxDQUFDMlAsT0FBdEQsSUFBaUUsQ0FBQzNQLENBQUMsQ0FBQzRQLE9BQXBFLElBQStFLENBQUM1UCxDQUFDLENBQUM2UCxNQUFsRixJQUE0RkosTUFBTSxDQUFDM08sT0FBUCxDQUFlZ1AsV0FBZixPQUFpQyxPQUE3SCxJQUF3SUwsTUFBTSxDQUFDM08sT0FBUCxDQUFlZ1AsV0FBZixNQUFnQyxVQUEzSyxFQUFzTDtBQUFFO0FBRXBMLFlBQUc5UCxDQUFDLENBQUNrTCxPQUFGLEtBQWMsRUFBakIsRUFBcUI7QUFBRTtBQUVuQjVNLFdBQUMsQ0FBQyxNQUFJcUosS0FBTCxDQUFELENBQWFyRSxRQUFiLENBQXNCLE1BQXRCLEVBQThCdEQsQ0FBOUI7QUFDSCxTQUhELE1BSUssSUFBR0EsQ0FBQyxDQUFDa0wsT0FBRixLQUFjLEVBQWpCLEVBQXNCO0FBQUU7QUFFekI1TSxXQUFDLENBQUMsTUFBSXFKLEtBQUwsQ0FBRCxDQUFhckUsUUFBYixDQUFzQixNQUF0QixFQUE4QnRELENBQTlCO0FBRUg7QUFDSjtBQUNKO0FBQ0osR0F0QkQ7O0FBd0JBMUIsR0FBQyxDQUFDc0MsRUFBRixDQUFLYyxXQUFMLElBQW9CLFVBQVNxTyxNQUFULEVBQWlCQyxJQUFqQixFQUF1QjtBQUd2QyxRQUFJLENBQUMxUixDQUFDLENBQUMsSUFBRCxDQUFELENBQVE0SyxNQUFULElBQW1CLFFBQU82RyxNQUFQLEtBQWlCLFFBQXhDLEVBQWlEO0FBRTdDLFVBQUlFLEtBQUssR0FBRztBQUNSQyxXQUFHLEVBQUV6TyxRQUFRLENBQUNVLGFBQVQsQ0FBdUIsS0FBdkIsQ0FERztBQUVSc0IsVUFBRSxFQUFFLEtBQUswTSxRQUFMLENBQWNwTixLQUFkLENBQW9CLEdBQXBCLENBRkk7QUFHUixpQkFBTyxLQUFLb04sUUFBTCxDQUFjcE4sS0FBZCxDQUFvQixHQUFwQjtBQUhDLE9BQVo7O0FBTUEsVUFBR2tOLEtBQUssQ0FBQ3hNLEVBQU4sQ0FBU3lGLE1BQVQsR0FBa0IsQ0FBckIsRUFBdUI7QUFDbkIsWUFBRztBQUNDK0csZUFBSyxDQUFDQyxHQUFOLEdBQVl6TyxRQUFRLENBQUNVLGFBQVQsQ0FBdUJzQixFQUFFLENBQUMsQ0FBRCxDQUF6QixDQUFaO0FBQ0gsU0FGRCxDQUVFLE9BQU0yQixHQUFOLEVBQVUsQ0FBRzs7QUFFZjZLLGFBQUssQ0FBQ0MsR0FBTixDQUFVek0sRUFBVixHQUFlLEtBQUswTSxRQUFMLENBQWNwTixLQUFkLENBQW9CLEdBQXBCLEVBQXlCLENBQXpCLEVBQTRCcU4sSUFBNUIsRUFBZjtBQUVILE9BUEQsTUFPTyxJQUFHSCxLQUFLLFNBQUwsQ0FBWS9HLE1BQVosR0FBcUIsQ0FBeEIsRUFBMEI7QUFDN0IsWUFBRztBQUNDK0csZUFBSyxDQUFDQyxHQUFOLEdBQVl6TyxRQUFRLENBQUNVLGFBQVQsQ0FBdUI4TixLQUFLLFNBQUwsQ0FBWSxDQUFaLENBQXZCLENBQVo7QUFDSCxTQUZELENBRUUsT0FBTTdLLEdBQU4sRUFBVSxDQUFHOztBQUVmLGFBQUssSUFBSWlMLENBQUMsR0FBQyxDQUFYLEVBQWNBLENBQUMsR0FBQ0osS0FBSyxTQUFMLENBQVkvRyxNQUE1QixFQUFvQ21ILENBQUMsRUFBckMsRUFBeUM7QUFDckNKLGVBQUssQ0FBQ0MsR0FBTixDQUFVSSxTQUFWLENBQW9CQyxHQUFwQixDQUF3Qk4sS0FBSyxTQUFMLENBQVlJLENBQVosRUFBZUQsSUFBZixFQUF4QjtBQUNIO0FBQ0o7O0FBQ0QzTyxjQUFRLENBQUMrTyxJQUFULENBQWNDLFdBQWQsQ0FBMEJSLEtBQUssQ0FBQ0MsR0FBaEM7QUFFQSxXQUFLNUksSUFBTCxDQUFVaEosQ0FBQyxDQUFDLEtBQUs2UixRQUFOLENBQVg7QUFDSDs7QUFDRCxRQUFJTyxJQUFJLEdBQUcsSUFBWDs7QUFFQSxTQUFLLElBQUk3UixDQUFDLEdBQUMsQ0FBWCxFQUFjQSxDQUFDLEdBQUM2UixJQUFJLENBQUN4SCxNQUFyQixFQUE2QnJLLENBQUMsRUFBOUIsRUFBa0M7QUFFOUIsVUFBSW9DLEtBQUssR0FBRzNDLENBQUMsQ0FBQ29TLElBQUksQ0FBQzdSLENBQUQsQ0FBTCxDQUFiO0FBQ0EsVUFBSXFDLElBQUksR0FBR0QsS0FBSyxDQUFDQyxJQUFOLENBQVdRLFdBQVgsQ0FBWDtBQUNBLFVBQUlyQyxPQUFPLEdBQUdmLENBQUMsQ0FBQ21CLE1BQUYsQ0FBUyxFQUFULEVBQWFuQixDQUFDLENBQUNzQyxFQUFGLENBQUtjLFdBQUwsRUFBa0JpUCxRQUEvQixFQUF5QzFQLEtBQUssQ0FBQ0MsSUFBTixFQUF6QyxFQUF1RCxRQUFPNk8sTUFBUCxLQUFpQixRQUFqQixJQUE2QkEsTUFBcEYsQ0FBZDs7QUFFQSxVQUFJLENBQUM3TyxJQUFELEtBQVUsQ0FBQzZPLE1BQUQsSUFBVyxRQUFPQSxNQUFQLEtBQWlCLFFBQXRDLENBQUosRUFBb0Q7QUFFaEQ5TyxhQUFLLENBQUNDLElBQU4sQ0FBV1EsV0FBWCxFQUF5QlIsSUFBSSxHQUFHLElBQUlvQyxRQUFKLENBQWFyQyxLQUFiLEVBQW9CNUIsT0FBcEIsQ0FBaEM7QUFDSCxPQUhELE1BSUssSUFBSSxPQUFPMFEsTUFBUCxJQUFpQixRQUFqQixJQUE2QixPQUFPN08sSUFBUCxJQUFlLFdBQWhELEVBQTREO0FBRTdELGVBQU9BLElBQUksQ0FBQzZPLE1BQUQsQ0FBSixDQUFhYSxLQUFiLENBQW1CMVAsSUFBbkIsRUFBeUIsR0FBRzJQLE1BQUgsQ0FBVWIsSUFBVixDQUF6QixDQUFQO0FBQ0g7O0FBQ0QsVUFBSTNRLE9BQU8sQ0FBQytELFFBQVosRUFBcUI7QUFBRTtBQUVuQixZQUFJLENBQUN1QyxLQUFLLENBQUM5QyxRQUFRLENBQUN4RCxPQUFPLENBQUMrRCxRQUFULENBQVQsQ0FBVixFQUF3QztBQUVwQ3FILG9CQUFVLENBQUMsWUFBVTtBQUNqQnZKLGdCQUFJLENBQUN1RyxJQUFMO0FBQ0gsV0FGUyxFQUVQcEksT0FBTyxDQUFDK0QsUUFGRCxDQUFWO0FBSUgsU0FORCxNQU1PLElBQUcvRCxPQUFPLENBQUMrRCxRQUFSLEtBQXFCLElBQXhCLEVBQStCO0FBRWxDbEMsY0FBSSxDQUFDdUcsSUFBTDtBQUNIOztBQUNEaEosY0FBTSxDQUFDMEUsU0FBUCxDQUFpQkMsUUFBakI7QUFDSDtBQUNKOztBQUVELFdBQU8sSUFBUDtBQUNILEdBaEVEOztBQWtFQTlFLEdBQUMsQ0FBQ3NDLEVBQUYsQ0FBS2MsV0FBTCxFQUFrQmlQLFFBQWxCLEdBQTZCO0FBQ3pCckssU0FBSyxFQUFFLEVBRGtCO0FBRXpCQyxZQUFRLEVBQUUsRUFGZTtBQUd6Qk8sZUFBVyxFQUFFLFNBSFk7QUFJekJ0QixjQUFVLEVBQUUsSUFKYTtBQUt6Qk0sU0FBSyxFQUFFLEVBTGtCO0FBS2I7QUFDWmtCLFFBQUksRUFBRSxJQU5tQjtBQU96QkMsWUFBUSxFQUFFLElBUGU7QUFRekJDLGFBQVMsRUFBRSxFQVJjO0FBU3pCbkIsT0FBRyxFQUFFLEtBVG9CO0FBVXpCeEYsU0FBSyxFQUFFLEdBVmtCO0FBV3pCOEwsT0FBRyxFQUFFLElBWG9CO0FBWXpCRSxVQUFNLEVBQUUsSUFaaUI7QUFhekJ4RixnQkFBWSxFQUFFLElBYlc7QUFjekJsQixXQUFPLEVBQUUsQ0FkZ0I7QUFlekJELFVBQU0sRUFBRSxDQWZpQjtBQWdCekJGLFVBQU0sRUFBRSxHQWhCaUI7QUFpQnpCSixVQUFNLEVBQUUsS0FqQmlCO0FBa0J6QkMsZ0JBQVksRUFBRSxHQWxCVztBQW1CekJ1RCxhQUFTLEVBQUUsSUFuQmM7QUFvQnpCK0IsY0FBVSxFQUFFLElBcEJhO0FBcUJ6QmpHLFNBQUssRUFBRSxFQXJCa0I7QUFzQnpCTSxRQUFJLEVBQUUsS0F0Qm1CO0FBdUJ6QmlFLG1CQUFlLEVBQUUsSUF2QlE7QUF3QnpCSSxrQkFBYyxFQUFFLElBeEJTO0FBd0JIO0FBQ3RCbEcsV0FBTyxFQUFFLEtBekJnQjtBQTBCekJnSSx5QkFBcUIsRUFBRSxLQTFCRTtBQTJCekJqSSxZQUFRLEVBQUUsQ0EzQmU7QUEyQlo7QUFDYjRGLGdCQUFZLEVBQUUsS0E1Qlc7QUE2QnpCdkMsY0FBVSxFQUFFLEtBN0JhO0FBOEJ6QlQsa0JBQWMsRUFBRSxLQTlCUztBQStCekJpRixpQkFBYSxFQUFFLElBL0JVO0FBZ0N6QnpFLGVBQVcsRUFBRSxJQWhDWTtBQWlDekJuQixZQUFRLEVBQUUsTUFqQ2U7QUFpQ1A7QUFDbEJxRSxtQkFBZSxFQUFFLE1BbENRO0FBa0NBO0FBQ3pCRCxXQUFPLEVBQUUsSUFuQ2dCO0FBb0N6QmlCLGdCQUFZLEVBQUUsSUFwQ1c7QUFxQ3pCaEcsZ0JBQVksRUFBRSxvQkFyQ1c7QUFzQ3pCaUMsV0FBTyxFQUFFLEtBdENnQjtBQXVDekJELHNCQUFrQixFQUFFLEtBdkNLO0FBd0N6Qm9ELGdCQUFZLEVBQUUsS0F4Q1c7QUF5Q3pCakQsMkJBQXVCLEVBQUUsdUJBekNBO0FBMEN6QitDLGdCQUFZLEVBQUUsVUExQ1c7QUEwQ0c7QUFDNUJlLGlCQUFhLEVBQUUsV0EzQ1U7QUEyQ0c7QUFDNUJoQix1QkFBbUIsRUFBRSxRQTVDSTtBQTZDekIrQix3QkFBb0IsRUFBRSxTQTdDRztBQThDekJqRCxnQkFBWSxFQUFFLHdCQUFVLENBQUUsQ0E5Q0Q7QUErQ3pCd0csWUFBUSxFQUFFLG9CQUFVLENBQUUsQ0EvQ0c7QUFnRHpCaEcsYUFBUyxFQUFFLHFCQUFVLENBQUUsQ0FoREU7QUFpRHpCZixZQUFRLEVBQUUsb0JBQVUsQ0FBRSxDQWpERztBQWtEekJxRCxhQUFTLEVBQUUscUJBQVUsQ0FBRSxDQWxERTtBQW1EekJILFlBQVEsRUFBRSxvQkFBVSxDQUFFLENBbkRHO0FBb0R6QmhGLGVBQVcsRUFBRSx1QkFBVSxDQUFFO0FBcERBLEdBQTdCO0FBdURBOUgsR0FBQyxDQUFDc0MsRUFBRixDQUFLYyxXQUFMLEVBQWtCb1AsV0FBbEIsR0FBZ0N4TixRQUFoQztBQUVBLFNBQU9oRixDQUFDLENBQUNzQyxFQUFGLENBQUswQyxRQUFaO0FBRUgsQ0F0NUNBLENBQUQsQyIsImZpbGUiOiJqcy9hcHAuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gaW5zdGFsbCBhIEpTT05QIGNhbGxiYWNrIGZvciBjaHVuayBsb2FkaW5nXG4gXHRmdW5jdGlvbiB3ZWJwYWNrSnNvbnBDYWxsYmFjayhkYXRhKSB7XG4gXHRcdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG4gXHRcdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG4gXHRcdHZhciBleGVjdXRlTW9kdWxlcyA9IGRhdGFbMl07XG5cbiBcdFx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG4gXHRcdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuIFx0XHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwLCByZXNvbHZlcyA9IFtdO1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcbiBcdFx0XHRcdHJlc29sdmVzLnB1c2goaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKTtcbiBcdFx0XHR9XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuIFx0XHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0gPSBtb3JlTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdGlmKHBhcmVudEpzb25wRnVuY3Rpb24pIHBhcmVudEpzb25wRnVuY3Rpb24oZGF0YSk7XG5cbiBcdFx0d2hpbGUocmVzb2x2ZXMubGVuZ3RoKSB7XG4gXHRcdFx0cmVzb2x2ZXMuc2hpZnQoKSgpO1xuIFx0XHR9XG5cbiBcdFx0Ly8gYWRkIGVudHJ5IG1vZHVsZXMgZnJvbSBsb2FkZWQgY2h1bmsgdG8gZGVmZXJyZWQgbGlzdFxuIFx0XHRkZWZlcnJlZE1vZHVsZXMucHVzaC5hcHBseShkZWZlcnJlZE1vZHVsZXMsIGV4ZWN1dGVNb2R1bGVzIHx8IFtdKTtcblxuIFx0XHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIGFsbCBjaHVua3MgcmVhZHlcbiBcdFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4gXHR9O1xuIFx0ZnVuY3Rpb24gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKSB7XG4gXHRcdHZhciByZXN1bHQ7XG4gXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZE1vZHVsZXMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHR2YXIgZGVmZXJyZWRNb2R1bGUgPSBkZWZlcnJlZE1vZHVsZXNbaV07XG4gXHRcdFx0dmFyIGZ1bGZpbGxlZCA9IHRydWU7XG4gXHRcdFx0Zm9yKHZhciBqID0gMTsgaiA8IGRlZmVycmVkTW9kdWxlLmxlbmd0aDsgaisrKSB7XG4gXHRcdFx0XHR2YXIgZGVwSWQgPSBkZWZlcnJlZE1vZHVsZVtqXTtcbiBcdFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tkZXBJZF0gIT09IDApIGZ1bGZpbGxlZCA9IGZhbHNlO1xuIFx0XHRcdH1cbiBcdFx0XHRpZihmdWxmaWxsZWQpIHtcbiBcdFx0XHRcdGRlZmVycmVkTW9kdWxlcy5zcGxpY2UoaS0tLCAxKTtcbiBcdFx0XHRcdHJlc3VsdCA9IF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gZGVmZXJyZWRNb2R1bGVbMF0pO1xuIFx0XHRcdH1cbiBcdFx0fVxuIFx0XHRyZXR1cm4gcmVzdWx0O1xuIFx0fVxuXG4gXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBvYmplY3QgdG8gc3RvcmUgbG9hZGVkIGFuZCBsb2FkaW5nIGNodW5rc1xuIFx0Ly8gdW5kZWZpbmVkID0gY2h1bmsgbm90IGxvYWRlZCwgbnVsbCA9IGNodW5rIHByZWxvYWRlZC9wcmVmZXRjaGVkXG4gXHQvLyBQcm9taXNlID0gY2h1bmsgbG9hZGluZywgMCA9IGNodW5rIGxvYWRlZFxuIFx0dmFyIGluc3RhbGxlZENodW5rcyA9IHtcbiBcdFx0XCJqcy9hcHBcIjogMFxuIFx0fTtcblxuIFx0dmFyIGRlZmVycmVkTW9kdWxlcyA9IFtdO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHR2YXIganNvbnBBcnJheSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSB8fCBbXTtcbiBcdHZhciBvbGRKc29ucEZ1bmN0aW9uID0ganNvbnBBcnJheS5wdXNoLmJpbmQoanNvbnBBcnJheSk7XG4gXHRqc29ucEFycmF5LnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjaztcbiBcdGpzb25wQXJyYXkgPSBqc29ucEFycmF5LnNsaWNlKCk7XG4gXHRmb3IodmFyIGkgPSAwOyBpIDwganNvbnBBcnJheS5sZW5ndGg7IGkrKykgd2VicGFja0pzb25wQ2FsbGJhY2soanNvbnBBcnJheVtpXSk7XG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IG9sZEpzb25wRnVuY3Rpb247XG5cblxuIFx0Ly8gYWRkIGVudHJ5IG1vZHVsZSB0byBkZWZlcnJlZCBsaXN0XG4gXHRkZWZlcnJlZE1vZHVsZXMucHVzaChbXCIuL3NyYy9zY3JpcHRzL2luZGV4LmpzXCIsXCJqcy92ZW5kb3JzXCJdKTtcbiBcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gcmVhZHlcbiBcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIiwiaW1wb3J0IEJhc2VDb21wb25lbnQgZnJvbSAnLi4vLi4vc2NyaXB0cy9iYXNlLWNvbXBvbmVudCc7XG5cbmltcG9ydCAkIGZyb20gJ2pxdWVyeS9kaXN0L2pxdWVyeSc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEhlYWRlckNvbXBvbmVudCBleHRlbmRzIEJhc2VDb21wb25lbnQge1xuXG4gICAgaW5pdCgpIHtcbiAgICAgICAgY29uc3QgaGFtYnVyZ2VyQnV0dG9uID0gdGhpcy4kZWxlbWVudC5maW5kKCcuanMtaGFtYnVyZ2VyJyk7XG4gICAgICAgIGNvbnN0IG5hdmlnYXRpb24gPSB0aGlzLiRlbGVtZW50LmZpbmQoJy5qcy1uYXZpZ2F0aW9uJyk7XG5cbiAgICAgICAgaGFtYnVyZ2VyQnV0dG9uLm9uKCdjbGljaycsICgpPT4ge1xuICAgICAgICAgICAgbmF2aWdhdGlvbi50b2dnbGVDbGFzcygnaGVhZGVyX19wYXJ0X25hdmlnYXRpb25fYWN0aXZlJyk7XG4gICAgICAgICAgICBoYW1idXJnZXJCdXR0b24udG9nZ2xlQ2xhc3MoJ2hlYWRlcl9faGFtYnVyZ2VyX2FjdGl2ZScpO1xuICAgICAgICB9KTtcblxuICAgIH1cbn1cbiIsImltcG9ydCBCYXNlQ29tcG9uZW50IGZyb20gJy4uLy4uL3NjcmlwdHMvYmFzZS1jb21wb25lbnQnO1xuXG5pbXBvcnQgJCBmcm9tICdqcXVlcnkvZGlzdC9qcXVlcnknO1xuaW1wb3J0IFwic2xpY2stY2Fyb3VzZWxcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTWFpblNsaWRlckNvbXBvbmVudCBleHRlbmRzIEJhc2VDb21wb25lbnQge1xuXG4gICAgaW5pdCgpIHtcblxuXHQgICAgdGhpcy4kZWxlbWVudC5zbGljayh7XG5cdFx0ICAgIHNsaWRlc1RvU2hvdzogMSxcblx0XHQgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG5cdFx0ICAgIC8vIHByZXZBcnJvdzogXCI8YSBocmVmPVxcXCJqYXZhc2NyaXB0OnZvaWQoMCk7XFxcIiBjbGFzcz0ncGFydG5lcnNfX3ByZXYgYXJyb3cgYXJyb3dfcHJldic+PC9hPlwiLFxuXHRcdCAgICAvLyBuZXh0QXJyb3c6IFwiPGEgaHJlZj1cXFwiamF2YXNjcmlwdDp2b2lkKDApO1xcXCIgY2xhc3M9J3BhcnRuZXJzX19uZXh0IGFycm93IGFycm93X25leHQnPjwvYT5cIixcblx0XHQgICAgYXV0b3BsYXk6IHRydWUsXG5cdFx0ICAgIGFycm93czogZmFsc2UsXG5cdFx0ICAgIGF1dG9wbGF5U3BlZWQ6IDIwMDAsXG5cdFx0ICAgIGluZmluaXRlOiB0cnVlLFxuXHQgICAgfSk7XG5cblxuICAgIH1cbn1cbiIsImltcG9ydCBCYXNlQ29tcG9uZW50IGZyb20gJy4uLy4uL3NjcmlwdHMvYmFzZS1jb21wb25lbnQnO1xuXG5pbXBvcnQgJCBmcm9tICdqcXVlcnkvZGlzdC9qcXVlcnknO1xuaW1wb3J0IFwic2xpY2stc2xpZGVyXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFBhcnRuZXJzQ29tcG9uZW50IGV4dGVuZHMgQmFzZUNvbXBvbmVudCB7XG5cbiAgICBpbml0KCkge1xuICAgICAgICBjb25zdCBzbGlkZXIgPSB0aGlzLiRlbGVtZW50LmZpbmQoJy5qcy1wYXJ0bmVyLXNsaWRlcicpO1xuXG4gICAgICAgIHNsaWRlci5zbGljayh7XG4gICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDQsXG4gICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMixcbiAgICAgICAgICAgIHByZXZBcnJvdzogJzxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMCk7XCIgY2xhc3M9XCJwYXJ0bmVyc19fYXJyb3cgcGFydG5lcnNfX2Fycm93X2xlZnQgY2lyY2xlLWFycm93XCI+PC9hPicsXG4gICAgICAgICAgICBuZXh0QXJyb3c6ICc8YSBocmVmPVwiamF2YXNjcmlwdDp2b2lkKDApO1wiIGNsYXNzPVwicGFydG5lcnNfX2Fycm93IHBhcnRuZXJzX19hcnJvd19yaWdodCBjaXJjbGUtYXJyb3dcIj48L2E+JyxcbiAgICAgICAgICAgIGF1dG9wbGF5OiB0cnVlLFxuICAgICAgICAgICAgYXV0b3BsYXlTcGVlZDogMjAwMCxcbiAgICAgICAgICAgIGluZmluaXRlOiB0cnVlLFxuICAgICAgICAgICAgbGF6eUxvYWQ6ICdvbmRlbWFuZCcsXG4gICAgICAgICAgICByZXNwb25zaXZlOiBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBicmVha3BvaW50OiAxMTkwLFxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWtwb2ludDogNzY4LFxuICAgICAgICAgICAgICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGFycm93czogZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBicmVha3BvaW50OiA2MDAsXG4gICAgICAgICAgICAgICAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGluZmluaXRlOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgYXJyb3dzOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgfSk7XG5cblxuICAgIH1cbn1cbiIsImltcG9ydCAkIGZyb20gXCJqcXVlcnkvZGlzdC9qcXVlcnlcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQmFzZUNvbXBvbmVudCB7XG4gICAgY29uc3RydWN0b3IoZWxlbWVudCkge1xuICAgICAgICB0aGlzLiRlbGVtZW50ID0gJChlbGVtZW50KTtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgfVxuXG4gICAgaW5pdCgpIHt9XG59XG4iLCJpbXBvcnQgJCBmcm9tICdqcXVlcnkvZGlzdC9qcXVlcnknO1xuXG5jbGFzcyBCYXNlUGFnZSB7XG5cdGNvbnN0cnVjdG9yKGVsZW1lbnQpIHtcblx0XHQkKHdpbmRvdykucmVhZHkoKCQpID0+IHtcblx0XHRcdHRoaXMuJGVsZW1lbnQgPSAkKCdib2R5Jyk7XG5cdFx0XHR0aGlzLmluaXQoKTtcblx0XHR9KTtcblx0fVxuXG5cdGluaXQoKSB7XG5cdFx0Ly9hYnN0cmFjdFxuXHR9XG59XG5cbmV4cG9ydCB7QmFzZVBhZ2V9OyIsImltcG9ydCAnLi9wYWdlL2luZGV4LmpzJzsiLCJpbXBvcnQge0Jhc2VQYWdlfSBmcm9tIFwiLi4vYmFzZS1wYWdlXCI7XG5pbXBvcnQgSGVhZGVyQ29tcG9uZW50IGZyb20gXCIuLi8uLi9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuanNcIjtcbmltcG9ydCBNYWluU2xpZGVyQ29tcG9uZW50IGZyb20gXCIuLi8uLi9jb21wb25lbnRzL21haW4tc2xpZGVyL21haW4tc2xpZGVyLmpzXCI7XG5pbXBvcnQgUGFydG5lcnNDb21wb25lbnQgZnJvbSBcIi4uLy4uL2NvbXBvbmVudHMvcGFydG5lcnMvcGFydG5lcnMuanNcIjtcblxuXG5pbXBvcnQgJCBmcm9tIFwianF1ZXJ5L2Rpc3QvanF1ZXJ5XCI7XG5pbXBvcnQgXCIuLi92ZW5kb3IvaXppbW9kYWxcIjtcbmltcG9ydCBcInNlbGVjdDIvZGlzdC9qcy9zZWxlY3QyLmZ1bGwuanNcIjtcbmltcG9ydCBcIi4uL3ZlbmRvci9hdXRvc2l6ZS5qc1wiO1xuXG5jbGFzcyBJbmRleFBhZ2UgZXh0ZW5kcyBCYXNlUGFnZSB7XG4gICAgaW5pdCgpIHtcbiAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuanMtaGVhZGVyJykuZWFjaCgoaSwgZWwpID0+IHtcbiAgICAgICAgICAgIG5ldyBIZWFkZXJDb21wb25lbnQoZWwpO1xuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLiRlbGVtZW50LmZpbmQoJy5qcy1tYWluc2xpZGVyJykuZWFjaCgoaSwgZWwpID0+IHtcbiAgICAgICAgICAgIG5ldyBNYWluU2xpZGVyQ29tcG9uZW50KGVsKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuanMtcGFydG5lcnMnKS5lYWNoKChpLCBlbCkgPT4ge1xuICAgICAgICAgICAgbmV3IFBhcnRuZXJzQ29tcG9uZW50KGVsKTtcbiAgICAgICAgfSk7XG5cblxuXG4gICAgfVxufVxuXG5jb25zdCBwYWdlID0gbmV3IEluZGV4UGFnZSgpOyIsInZhciBQbHVnaW5zO1xuKGZ1bmN0aW9uIChQbHVnaW5zKSB7XG4gICAgdmFyIEF1dG9zaXplSW5wdXRPcHRpb25zID0gKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgZnVuY3Rpb24gQXV0b3NpemVJbnB1dE9wdGlvbnMoc3BhY2UpIHtcbiAgICAgICAgICAgIGlmICh0eXBlb2Ygc3BhY2UgPT09IFwidW5kZWZpbmVkXCIpIHsgc3BhY2UgPSAzMDsgfVxuICAgICAgICAgICAgdGhpcy5zcGFjZSA9IHNwYWNlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBBdXRvc2l6ZUlucHV0T3B0aW9ucztcbiAgICB9KSgpO1xuICAgIFBsdWdpbnMuQXV0b3NpemVJbnB1dE9wdGlvbnMgPSBBdXRvc2l6ZUlucHV0T3B0aW9ucztcblxuICAgIHZhciBBdXRvc2l6ZUlucHV0ID0gKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgZnVuY3Rpb24gQXV0b3NpemVJbnB1dChpbnB1dCwgb3B0aW9ucykge1xuICAgICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICAgIHRoaXMuX2lucHV0ID0gJChpbnB1dCk7XG4gICAgICAgICAgICB0aGlzLl9vcHRpb25zID0gJC5leHRlbmQoe30sIEF1dG9zaXplSW5wdXQuZ2V0RGVmYXVsdE9wdGlvbnMoKSwgb3B0aW9ucyk7XG5cbiAgICAgICAgICAgIC8vIEluaXQgbWlycm9yXG4gICAgICAgICAgICB0aGlzLl9taXJyb3IgPSAkKCc8c3BhbiBzdHlsZT1cInBvc2l0aW9uOmFic29sdXRlOyB0b3A6LTk5OXB4OyBsZWZ0OjA7IHdoaXRlLXNwYWNlOnByZTtcIi8+Jyk7XG5cbiAgICAgICAgICAgIC8vIENvcHkgdG8gbWlycm9yXG4gICAgICAgICAgICAkLmVhY2goWydmb250RmFtaWx5JywgJ2ZvbnRTaXplJywgJ2ZvbnRXZWlnaHQnLCAnZm9udFN0eWxlJywgJ2xldHRlclNwYWNpbmcnLCAndGV4dFRyYW5zZm9ybScsICd3b3JkU3BhY2luZycsICd0ZXh0SW5kZW50J10sIGZ1bmN0aW9uIChpLCB2YWwpIHtcbiAgICAgICAgICAgICAgICBfdGhpcy5fbWlycm9yWzBdLnN0eWxlW3ZhbF0gPSBfdGhpcy5faW5wdXQuY3NzKHZhbCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoXCJib2R5XCIpLmFwcGVuZCh0aGlzLl9taXJyb3IpO1xuXG4gICAgICAgICAgICAvLyBCaW5kIGV2ZW50cyAtIGNoYW5nZSB1cGRhdGUgcGFzdGUgY2xpY2sgbW91c2Vkb3duIG1vdXNldXAgZm9jdXMgYmx1clxuICAgICAgICAgICAgLy8gSUUgOSBuZWVkIGtleWRvd24gdG8ga2VlcCB1cGRhdGluZyB3aGlsZSBkZWxldGluZyAoa2VlcGluZyBiYWNrc3BhY2UgaW4gLSBlbHNlIGl0IHdpbGwgZmlyc3QgdXBkYXRlIHdoZW4gYmFja3NwYWNlIGlzIHJlbGVhc2VkKVxuICAgICAgICAgICAgLy8gSUUgOSBuZWVkIGtleXVwIGluY2FzZSB0ZXh0IGlzIHNlbGVjdGVkIGFuZCBiYWNrc3BhY2UvZGVsZXRlZCBpcyBoaXQgLSBrZXlkb3duIGlzIHRvIGVhcmx5XG4gICAgICAgICAgICAvLyBIb3cgdG8gZml4IHByb2JsZW0gd2l0aCBoaXR0aW5nIHRoZSBkZWxldGUgXCJYXCIgaW4gdGhlIGJveCAtIGJ1dCBub3QgdXBkYXRpbmchPyBtb3VzZXVwIGlzIGFwcGFyZW50bHkgdG8gZWFybHlcbiAgICAgICAgICAgIC8vIENvdWxkIGJpbmQgc2VwYXJhdGx5IGFuZCBzZXQgdGltZXJcbiAgICAgICAgICAgIC8vIEFkZCBzbyBpdCBhdXRvbWF0aWNhbGx5IHVwZGF0ZXMgaWYgdmFsdWUgb2YgaW5wdXQgaXMgY2hhbmdlZCBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8xODQ4NDE0LzU4NTI0XG4gICAgICAgICAgICB0aGlzLl9pbnB1dC5vbihcImtleWRvd24ga2V5dXAgaW5wdXQgcHJvcGVydHljaGFuZ2UgY2hhbmdlXCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMudXBkYXRlKCk7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgLy8gVXBkYXRlXG4gICAgICAgICAgICAoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIF90aGlzLnVwZGF0ZSgpO1xuICAgICAgICAgICAgfSkoKTtcbiAgICAgICAgfVxuICAgICAgICBBdXRvc2l6ZUlucHV0LnByb3RvdHlwZS5nZXRPcHRpb25zID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX29wdGlvbnM7XG4gICAgICAgIH07XG5cbiAgICAgICAgQXV0b3NpemVJbnB1dC5wcm90b3R5cGUudXBkYXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIHZhbHVlID0gdGhpcy5faW5wdXQudmFsKCkgfHwgXCJcIjtcblxuICAgICAgICAgICAgaWYgKHZhbHVlID09PSB0aGlzLl9taXJyb3IudGV4dCgpKSB7XG4gICAgICAgICAgICAgICAgLy8gTm90aGluZyBoYXZlIGNoYW5nZWQgLSBza2lwXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBVcGRhdGUgbWlycm9yXG4gICAgICAgICAgICB0aGlzLl9taXJyb3IudGV4dCh2YWx1ZSk7XG5cbiAgICAgICAgICAgIC8vIENhbGN1bGF0ZSB0aGUgd2lkdGhcbiAgICAgICAgICAgIHZhciBuZXdXaWR0aCA9IHRoaXMuX21pcnJvci53aWR0aCgpICsgdGhpcy5fb3B0aW9ucy5zcGFjZTtcblxuICAgICAgICAgICAgLy8gVXBkYXRlIHRoZSB3aWR0aFxuICAgICAgICAgICAgdGhpcy5faW5wdXQud2lkdGgobmV3V2lkdGgpO1xuICAgICAgICB9O1xuXG4gICAgICAgIEF1dG9zaXplSW5wdXQuZ2V0RGVmYXVsdE9wdGlvbnMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fZGVmYXVsdE9wdGlvbnM7XG4gICAgICAgIH07XG5cbiAgICAgICAgQXV0b3NpemVJbnB1dC5nZXRJbnN0YW5jZUtleSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIC8vIFVzZSBjYW1lbGNhc2UgYmVjYXVzZSAuZGF0YSgpWydhdXRvc2l6ZS1pbnB1dC1pbnN0YW5jZSddIHdpbGwgbm90IHdvcmtcbiAgICAgICAgICAgIHJldHVybiBcImF1dG9zaXplSW5wdXRJbnN0YW5jZVwiO1xuICAgICAgICB9O1xuICAgICAgICBBdXRvc2l6ZUlucHV0Ll9kZWZhdWx0T3B0aW9ucyA9IG5ldyBBdXRvc2l6ZUlucHV0T3B0aW9ucygpO1xuICAgICAgICByZXR1cm4gQXV0b3NpemVJbnB1dDtcbiAgICB9KSgpO1xuICAgIFBsdWdpbnMuQXV0b3NpemVJbnB1dCA9IEF1dG9zaXplSW5wdXQ7XG5cbiAgICAvLyBqUXVlcnkgUGx1Z2luXG4gICAgKGZ1bmN0aW9uICgkKSB7XG4gICAgICAgIHZhciBwbHVnaW5EYXRhQXR0cmlidXRlTmFtZSA9IFwiYXV0b3NpemUtaW5wdXRcIjtcbiAgICAgICAgdmFyIHZhbGlkVHlwZXMgPSBbXCJ0ZXh0XCIsIFwicGFzc3dvcmRcIiwgXCJzZWFyY2hcIiwgXCJ1cmxcIiwgXCJ0ZWxcIiwgXCJlbWFpbFwiLCBcIm51bWJlclwiXTtcblxuICAgICAgICAvLyBqUXVlcnkgUGx1Z2luXG4gICAgICAgICQuZm4uYXV0b3NpemVJbnB1dCA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAvLyBNYWtlIHN1cmUgaXQgaXMgb25seSBhcHBsaWVkIHRvIGlucHV0IGVsZW1lbnRzIG9mIHZhbGlkIHR5cGVcbiAgICAgICAgICAgICAgICAvLyBPciBsZXQgaXQgYmUgdGhlIHJlc3BvbnNpYmlsaXR5IG9mIHRoZSBwcm9ncmFtbWVyIHRvIG9ubHkgc2VsZWN0IGFuZCBhcHBseSB0byB2YWxpZCBlbGVtZW50cz9cbiAgICAgICAgICAgICAgICBpZiAoISh0aGlzLnRhZ05hbWUgPT0gXCJJTlBVVFwiICYmICQuaW5BcnJheSh0aGlzLnR5cGUsIHZhbGlkVHlwZXMpID4gLTEpKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIFNraXAgLSBpZiBub3QgaW5wdXQgYW5kIG9mIHZhbGlkIHR5cGVcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyk7XG5cbiAgICAgICAgICAgICAgICBpZiAoISR0aGlzLmRhdGEoUGx1Z2lucy5BdXRvc2l6ZUlucHV0LmdldEluc3RhbmNlS2V5KCkpKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIElmIGluc3RhbmNlIG5vdCBhbHJlYWR5IGNyZWF0ZWQgYW5kIGF0dGFjaGVkXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcHRpb25zID09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gVHJ5IGdldCBvcHRpb25zIGZyb20gYXR0cmlidXRlXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zID0gJHRoaXMuZGF0YShwbHVnaW5EYXRhQXR0cmlidXRlTmFtZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAvLyBDcmVhdGUgYW5kIGF0dGFjaCBpbnN0YW5jZVxuICAgICAgICAgICAgICAgICAgICAkdGhpcy5kYXRhKFBsdWdpbnMuQXV0b3NpemVJbnB1dC5nZXRJbnN0YW5jZUtleSgpLCBuZXcgUGx1Z2lucy5BdXRvc2l6ZUlucHV0KHRoaXMsIG9wdGlvbnMpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAvLyBPbiBEb2N1bWVudCBSZWFkeVxuICAgICAgICAkKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIC8vIEluc3RhbnRpYXRlIGZvciBhbGwgd2l0aCBkYXRhLXByb3ZpZGU9YXV0b3NpemUtaW5wdXQgYXR0cmlidXRlXG4gICAgICAgICAgICAkKFwiaW5wdXRbZGF0YS1cIiArIHBsdWdpbkRhdGFBdHRyaWJ1dGVOYW1lICsgXCJdXCIpLmF1dG9zaXplSW5wdXQoKTtcbiAgICAgICAgfSk7XG4gICAgICAgIC8vIEFsdGVybmF0aXZlIHRvIHVzZSBPbiBEb2N1bWVudCBSZWFkeSBhbmQgY3JlYXRpbmcgdGhlIGluc3RhbmNlIGltbWVkaWF0ZWx5XG4gICAgICAgIC8vJChkb2N1bWVudCkub24oJ2ZvY3VzLmF1dG9zaXplLWlucHV0JywgJ2lucHV0W2RhdGEtYXV0b3NpemUtaW5wdXRdJywgZnVuY3Rpb24gKGUpXG4gICAgICAgIC8ve1xuICAgICAgICAvL1x0JCh0aGlzKS5hdXRvc2l6ZUlucHV0KCk7XG4gICAgICAgIC8vfSk7XG4gICAgfSkoalF1ZXJ5KTtcbn0pKFBsdWdpbnMgfHwgKFBsdWdpbnMgPSB7fSkpOyIsIi8qXG4qIGl6aU1vZGFsIHwgdjEuNS4xXG4qIGh0dHA6Ly9pemltb2RhbC5tYXJjZWxvZG9sY2UuY29tXG4qIGJ5IE1hcmNlbG8gRG9sY2UuXG4qL1xuKGZ1bmN0aW9uIChmYWN0b3J5KSB7XG4gICAgaWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xuICAgICAgICBkZWZpbmUoWydqcXVlcnknXSwgZmFjdG9yeSk7XG4gICAgfSBlbHNlIGlmICh0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0JyAmJiBtb2R1bGUuZXhwb3J0cykge1xuICAgICAgICBtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKCByb290LCBqUXVlcnkgKSB7XG4gICAgICAgICAgICBpZiAoIGpRdWVyeSA9PT0gdW5kZWZpbmVkICkge1xuICAgICAgICAgICAgICAgIGlmICggdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgKSB7XG4gICAgICAgICAgICAgICAgICAgIGpRdWVyeSA9IHJlcXVpcmUoJ2pxdWVyeScpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgalF1ZXJ5ID0gcmVxdWlyZSgnanF1ZXJ5Jykocm9vdCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZmFjdG9yeShqUXVlcnkpO1xuICAgICAgICAgICAgcmV0dXJuIGpRdWVyeTtcbiAgICAgICAgfTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBmYWN0b3J5KGpRdWVyeSk7XG4gICAgfVxufShmdW5jdGlvbiAoJCkge1xuXG4gICAgdmFyICR3aW5kb3cgPSAkKHdpbmRvdyksXG4gICAgICAgICRkb2N1bWVudCA9ICQoZG9jdW1lbnQpLFxuICAgICAgICBQTFVHSU5fTkFNRSA9ICdpemlNb2RhbCcsXG4gICAgICAgIFNUQVRFUyA9IHtcbiAgICAgICAgICAgIENMT1NJTkc6ICdjbG9zaW5nJyxcbiAgICAgICAgICAgIENMT1NFRDogJ2Nsb3NlZCcsXG4gICAgICAgICAgICBPUEVOSU5HOiAnb3BlbmluZycsXG4gICAgICAgICAgICBPUEVORUQ6ICdvcGVuZWQnLFxuICAgICAgICAgICAgREVTVFJPWUVEOiAnZGVzdHJveWVkJ1xuICAgICAgICB9O1xuXG4gICAgZnVuY3Rpb24gd2hpY2hBbmltYXRpb25FdmVudCgpe1xuICAgICAgICB2YXIgdCxcbiAgICAgICAgICAgIGVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImZha2VlbGVtZW50XCIpLFxuICAgICAgICAgICAgYW5pbWF0aW9ucyA9IHtcbiAgICAgICAgICAgICAgICBcImFuaW1hdGlvblwiICAgICAgOiBcImFuaW1hdGlvbmVuZFwiLFxuICAgICAgICAgICAgICAgIFwiT0FuaW1hdGlvblwiICAgICA6IFwib0FuaW1hdGlvbkVuZFwiLFxuICAgICAgICAgICAgICAgIFwiTW96QW5pbWF0aW9uXCIgICA6IFwiYW5pbWF0aW9uZW5kXCIsXG4gICAgICAgICAgICAgICAgXCJXZWJraXRBbmltYXRpb25cIjogXCJ3ZWJraXRBbmltYXRpb25FbmRcIlxuICAgICAgICAgICAgfTtcbiAgICAgICAgZm9yICh0IGluIGFuaW1hdGlvbnMpe1xuICAgICAgICAgICAgaWYgKGVsLnN0eWxlW3RdICE9PSB1bmRlZmluZWQpe1xuICAgICAgICAgICAgICAgIHJldHVybiBhbmltYXRpb25zW3RdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gaXNJRSh2ZXJzaW9uKSB7XG4gICAgICAgIGlmKHZlcnNpb24gPT09IDkpe1xuICAgICAgICAgICAgcmV0dXJuIG5hdmlnYXRvci5hcHBWZXJzaW9uLmluZGV4T2YoXCJNU0lFIDkuXCIpICE9PSAtMTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHVzZXJBZ2VudCA9IG5hdmlnYXRvci51c2VyQWdlbnQ7XG4gICAgICAgICAgICByZXR1cm4gdXNlckFnZW50LmluZGV4T2YoXCJNU0lFIFwiKSA+IC0xIHx8IHVzZXJBZ2VudC5pbmRleE9mKFwiVHJpZGVudC9cIikgPiAtMTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNsZWFyVmFsdWUodmFsdWUpe1xuICAgICAgICB2YXIgc2VwYXJhdG9ycyA9IC8lfHB4fGVtfGNtfHZofHZ3LztcbiAgICAgICAgcmV0dXJuIHBhcnNlSW50KFN0cmluZyh2YWx1ZSkuc3BsaXQoc2VwYXJhdG9ycylbMF0pO1xuICAgIH1cblxuICAgIHZhciBhbmltYXRpb25FdmVudCA9IHdoaWNoQW5pbWF0aW9uRXZlbnQoKSxcbiAgICAgICAgaXNNb2JpbGUgPSAoL01vYmkvLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCkpID8gdHJ1ZSA6IGZhbHNlO1xuXG4gICAgd2luZG93LiRpemlNb2RhbCA9IHt9O1xuICAgIHdpbmRvdy4kaXppTW9kYWwuYXV0b09wZW4gPSAwO1xuICAgIHdpbmRvdy4kaXppTW9kYWwuaGlzdG9yeSA9IGZhbHNlO1xuXG4gICAgdmFyIGl6aU1vZGFsID0gZnVuY3Rpb24gKGVsZW1lbnQsIG9wdGlvbnMpIHtcbiAgICAgICAgdGhpcy5pbml0KGVsZW1lbnQsIG9wdGlvbnMpO1xuICAgIH07XG5cbiAgICBpemlNb2RhbC5wcm90b3R5cGUgPSB7XG5cbiAgICAgICAgY29uc3RydWN0b3I6IGl6aU1vZGFsLFxuXG4gICAgICAgIGluaXQ6IGZ1bmN0aW9uIChlbGVtZW50LCBvcHRpb25zKSB7XG5cbiAgICAgICAgICAgIHZhciB0aGF0ID0gdGhpcztcbiAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQgPSAkKGVsZW1lbnQpO1xuXG4gICAgICAgICAgICBpZih0aGlzLiRlbGVtZW50WzBdLmlkICE9PSB1bmRlZmluZWQgJiYgdGhpcy4kZWxlbWVudFswXS5pZCAhPT0gJycpe1xuICAgICAgICAgICAgICAgIHRoaXMuaWQgPSB0aGlzLiRlbGVtZW50WzBdLmlkO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLmlkID0gUExVR0lOX05BTUUrTWF0aC5mbG9vcigoTWF0aC5yYW5kb20oKSAqIDEwMDAwMDAwKSArIDEpO1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuYXR0cignaWQnLCB0aGlzLmlkKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuY2xhc3NlcyA9ICggdGhpcy4kZWxlbWVudC5hdHRyKCdjbGFzcycpICE9PSB1bmRlZmluZWQgKSA/IHRoaXMuJGVsZW1lbnQuYXR0cignY2xhc3MnKSA6ICcnO1xuICAgICAgICAgICAgdGhpcy5jb250ZW50ID0gdGhpcy4kZWxlbWVudC5odG1sKCk7XG4gICAgICAgICAgICB0aGlzLnN0YXRlID0gU1RBVEVTLkNMT1NFRDtcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucyA9IG9wdGlvbnM7XG4gICAgICAgICAgICB0aGlzLndpZHRoID0gMDtcbiAgICAgICAgICAgIHRoaXMudGltZXIgPSBudWxsO1xuICAgICAgICAgICAgdGhpcy50aW1lclRpbWVvdXQgPSBudWxsO1xuICAgICAgICAgICAgdGhpcy5wcm9ncmVzc0JhciA9IG51bGw7XG4gICAgICAgICAgICB0aGlzLmlzUGF1c2VkID0gZmFsc2U7XG4gICAgICAgICAgICB0aGlzLmlzRnVsbHNjcmVlbiA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5oZWFkZXJIZWlnaHQgPSAwO1xuICAgICAgICAgICAgdGhpcy5tb2RhbEhlaWdodCA9IDA7XG4gICAgICAgICAgICB0aGlzLiRvdmVybGF5ID0gJCgnPGRpdiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1vdmVybGF5XCIgc3R5bGU9XCJiYWNrZ3JvdW5kLWNvbG9yOicrb3B0aW9ucy5vdmVybGF5Q29sb3IrJ1wiPjwvZGl2PicpO1xuICAgICAgICAgICAgdGhpcy4kbmF2aWdhdGUgPSAkKCc8ZGl2IGNsYXNzPVwiJytQTFVHSU5fTkFNRSsnLW5hdmlnYXRlXCI+PGRpdiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1uYXZpZ2F0ZS1jYXB0aW9uXCI+VXNlPC9kaXY+PGJ1dHRvbiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1uYXZpZ2F0ZS1wcmV2XCI+PC9idXR0b24+PGJ1dHRvbiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1uYXZpZ2F0ZS1uZXh0XCI+PC9idXR0b24+PC9kaXY+Jyk7XG4gICAgICAgICAgICB0aGlzLmdyb3VwID0ge1xuICAgICAgICAgICAgICAgIG5hbWU6IHRoaXMuJGVsZW1lbnQuYXR0cignZGF0YS0nK1BMVUdJTl9OQU1FKyctZ3JvdXAnKSxcbiAgICAgICAgICAgICAgICBpbmRleDogbnVsbCxcbiAgICAgICAgICAgICAgICBpZHM6IFtdXG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5hdHRyKCdhcmlhLWhpZGRlbicsICd0cnVlJyk7XG4gICAgICAgICAgICB0aGlzLiRlbGVtZW50LmF0dHIoJ2FyaWEtbGFiZWxsZWRieScsIHRoaXMuaWQpO1xuICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5hdHRyKCdyb2xlJywgJ2RpYWxvZycpO1xuXG4gICAgICAgICAgICBpZiggIXRoaXMuJGVsZW1lbnQuaGFzQ2xhc3MoJ2l6aU1vZGFsJykgKXtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmFkZENsYXNzKCdpemlNb2RhbCcpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZih0aGlzLmdyb3VwLm5hbWUgPT09IHVuZGVmaW5lZCAmJiBvcHRpb25zLmdyb3VwICE9PSBcIlwiKXtcbiAgICAgICAgICAgICAgICB0aGlzLmdyb3VwLm5hbWUgPSBvcHRpb25zLmdyb3VwO1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuYXR0cignZGF0YS0nK1BMVUdJTl9OQU1FKyctZ3JvdXAnLCBvcHRpb25zLmdyb3VwKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKHRoaXMub3B0aW9ucy5sb29wID09PSB0cnVlKXtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmF0dHIoJ2RhdGEtJytQTFVHSU5fTkFNRSsnLWxvb3AnLCB0cnVlKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgJC5lYWNoKCB0aGlzLm9wdGlvbnMgLCBmdW5jdGlvbihpbmRleCwgdmFsKSB7XG4gICAgICAgICAgICAgICAgdmFyIGF0dHIgPSB0aGF0LiRlbGVtZW50LmF0dHIoJ2RhdGEtJytQTFVHSU5fTkFNRSsnLScraW5kZXgpO1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBhdHRyICE9PSB0eXBlb2YgdW5kZWZpbmVkKXtcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoYXR0ciA9PT0gXCJcIiB8fCBhdHRyID09IFwidHJ1ZVwiKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zW2luZGV4XSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGF0dHIgPT0gXCJmYWxzZVwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uc1tpbmRleF0gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbCA9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uc1tpbmRleF0gPSBuZXcgRnVuY3Rpb24oYXR0cik7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnNbaW5kZXhdID0gYXR0cjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gY2F0Y2goZXhjKXt9XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgaWYob3B0aW9ucy5hcHBlbmRUbyAhPT0gZmFsc2Upe1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuYXBwZW5kVG8ob3B0aW9ucy5hcHBlbmRUbyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChvcHRpb25zLmlmcmFtZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuaHRtbCgnPGRpdiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy13cmFwXCI+PGRpdiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1jb250ZW50XCI+PGlmcmFtZSBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1pZnJhbWVcIj48L2lmcmFtZT4nICsgdGhpcy5jb250ZW50ICsgXCI8L2Rpdj48L2Rpdj5cIik7XG5cbiAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5pZnJhbWVIZWlnaHQgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWlmcmFtZScpLmNzcygnaGVpZ2h0Jywgb3B0aW9ucy5pZnJhbWVIZWlnaHQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5odG1sKCc8ZGl2IGNsYXNzPVwiJytQTFVHSU5fTkFNRSsnLXdyYXBcIj48ZGl2IGNsYXNzPVwiJytQTFVHSU5fTkFNRSsnLWNvbnRlbnRcIj4nICsgdGhpcy5jb250ZW50ICsgJzwvZGl2PjwvZGl2PicpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmJhY2tncm91bmQgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmNzcygnYmFja2dyb3VuZCcsIHRoaXMub3B0aW9ucy5iYWNrZ3JvdW5kKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy4kd3JhcCA9IHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy13cmFwJyk7XG5cbiAgICAgICAgICAgIGlmKG9wdGlvbnMuemluZGV4ICE9PSBudWxsICYmICFpc05hTihwYXJzZUludChvcHRpb25zLnppbmRleCkpICl7XG4gICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5jc3MoJ3otaW5kZXgnLCBvcHRpb25zLnppbmRleCk7XG4gICAgICAgICAgICAgICAgdGhpcy4kbmF2aWdhdGUuY3NzKCd6LWluZGV4Jywgb3B0aW9ucy56aW5kZXgtMSk7XG4gICAgICAgICAgICAgICAgdGhpcy4kb3ZlcmxheS5jc3MoJ3otaW5kZXgnLCBvcHRpb25zLnppbmRleC0yKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYob3B0aW9ucy5yYWRpdXMgIT09IFwiXCIpe1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuY3NzKCdib3JkZXItcmFkaXVzJywgb3B0aW9ucy5yYWRpdXMpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZihvcHRpb25zLnBhZGRpbmcgIT09IFwiXCIpe1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy1jb250ZW50JykuY3NzKCdwYWRkaW5nJywgb3B0aW9ucy5wYWRkaW5nKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYob3B0aW9ucy50aGVtZSAhPT0gXCJcIil7XG4gICAgICAgICAgICAgICAgaWYob3B0aW9ucy50aGVtZSA9PT0gXCJsaWdodFwiKXtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5hZGRDbGFzcyhQTFVHSU5fTkFNRSsnLWxpZ2h0Jyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5hZGRDbGFzcyhvcHRpb25zLnRoZW1lKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmKG9wdGlvbnMucnRsID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5hZGRDbGFzcyhQTFVHSU5fTkFNRSsnLXJ0bCcpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZihvcHRpb25zLm9wZW5GdWxsc2NyZWVuID09PSB0cnVlKXtcbiAgICAgICAgICAgICAgICB0aGlzLmlzRnVsbHNjcmVlbiA9IHRydWU7XG4gICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5hZGRDbGFzcygnaXNGdWxsc2NyZWVuJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuY3JlYXRlSGVhZGVyKCk7XG4gICAgICAgICAgICB0aGlzLnJlY2FsY1dpZHRoKCk7XG4gICAgICAgICAgICB0aGlzLnJlY2FsY1ZlcnRpY2FsUG9zKCk7XG5cbiAgICAgICAgICAgIGlmICh0aGF0Lm9wdGlvbnMuYWZ0ZXJSZW5kZXIgJiYgKCB0eXBlb2YodGhhdC5vcHRpb25zLmFmdGVyUmVuZGVyKSA9PT0gXCJmdW5jdGlvblwiIHx8IHR5cGVvZih0aGF0Lm9wdGlvbnMuYWZ0ZXJSZW5kZXIpID09PSBcIm9iamVjdFwiICkgKSB7XG4gICAgICAgICAgICAgICAgdGhhdC5vcHRpb25zLmFmdGVyUmVuZGVyKHRoYXQpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgY3JlYXRlSGVhZGVyOiBmdW5jdGlvbigpe1xuXG4gICAgICAgICAgICB0aGlzLiRoZWFkZXIgPSAkKCc8ZGl2IGNsYXNzPVwiJytQTFVHSU5fTkFNRSsnLWhlYWRlclwiPjxoMiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1oZWFkZXItdGl0bGVcIj4nICsgdGhpcy5vcHRpb25zLnRpdGxlICsgJzwvaDI+PHAgY2xhc3M9XCInK1BMVUdJTl9OQU1FKyctaGVhZGVyLXN1YnRpdGxlXCI+JyArIHRoaXMub3B0aW9ucy5zdWJ0aXRsZSArICc8L3A+PGRpdiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1oZWFkZXItYnV0dG9uc1wiPjwvZGl2PjwvZGl2PicpO1xuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmNsb3NlQnV0dG9uID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kaGVhZGVyLmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctaGVhZGVyLWJ1dHRvbnMnKS5hcHBlbmQoJzxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1idXR0b24gJytQTFVHSU5fTkFNRSsnLWJ1dHRvbi1jbG9zZVwiIGRhdGEtJytQTFVHSU5fTkFNRSsnLWNsb3NlPjwvYT4nKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5mdWxsc2NyZWVuID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kaGVhZGVyLmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctaGVhZGVyLWJ1dHRvbnMnKS5hcHBlbmQoJzxhIGhyZWY9XCJqYXZhc2NyaXB0OnZvaWQoMClcIiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1idXR0b24gJytQTFVHSU5fTkFNRSsnLWJ1dHRvbi1mdWxsc2NyZWVuXCIgZGF0YS0nK1BMVUdJTl9OQU1FKyctZnVsbHNjcmVlbj48L2E+Jyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMudGltZW91dFByb2dyZXNzYmFyID09PSB0cnVlICYmICFpc05hTihwYXJzZUludCh0aGlzLm9wdGlvbnMudGltZW91dCkpICYmIHRoaXMub3B0aW9ucy50aW1lb3V0ICE9PSBmYWxzZSAmJiB0aGlzLm9wdGlvbnMudGltZW91dCAhPT0gMCkge1xuICAgICAgICAgICAgICAgIHRoaXMuJGhlYWRlci5wcmVwZW5kKCc8ZGl2IGNsYXNzPVwiJytQTFVHSU5fTkFNRSsnLXByb2dyZXNzYmFyXCI+PGRpdiBzdHlsZT1cImJhY2tncm91bmQtY29sb3I6Jyt0aGlzLm9wdGlvbnMudGltZW91dFByb2dyZXNzYmFyQ29sb3IrJ1wiPjwvZGl2PjwvZGl2PicpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnN1YnRpdGxlID09PSAnJykge1xuICAgICAgICAgICAgICAgIHRoaXMuJGhlYWRlci5hZGRDbGFzcyhQTFVHSU5fTkFNRSsnLW5vU3VidGl0bGUnKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy50aXRsZSAhPT0gXCJcIikge1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5oZWFkZXJDb2xvciAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICBpZih0aGlzLm9wdGlvbnMuYm9yZGVyQm90dG9tID09PSB0cnVlKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuY3NzKCdib3JkZXItYm90dG9tJywgJzNweCBzb2xpZCAnICsgdGhpcy5vcHRpb25zLmhlYWRlckNvbG9yICsgJycpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGhlYWRlci5jc3MoJ2JhY2tncm91bmQnLCB0aGlzLm9wdGlvbnMuaGVhZGVyQ29sb3IpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmljb24gIT09IG51bGwgfHwgdGhpcy5vcHRpb25zLmljb25UZXh0ICE9PSBudWxsKXtcblxuICAgICAgICAgICAgICAgICAgICB0aGlzLiRoZWFkZXIucHJlcGVuZCgnPGkgY2xhc3M9XCInK1BMVUdJTl9OQU1FKyctaGVhZGVyLWljb25cIj48L2k+Jyk7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5pY29uICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRoZWFkZXIuZmluZCgnLicrUExVR0lOX05BTUUrJy1oZWFkZXItaWNvbicpLmFkZENsYXNzKHRoaXMub3B0aW9ucy5pY29uKS5jc3MoJ2NvbG9yJywgdGhpcy5vcHRpb25zLmljb25Db2xvcik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5pY29uVGV4dCAhPT0gbnVsbCl7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRoZWFkZXIuZmluZCgnLicrUExVR0lOX05BTUUrJy1oZWFkZXItaWNvbicpLmh0bWwodGhpcy5vcHRpb25zLmljb25UZXh0KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmNzcygnb3ZlcmZsb3cnLCAnaGlkZGVuJykucHJlcGVuZCh0aGlzLiRoZWFkZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldEdyb3VwOiBmdW5jdGlvbihncm91cE5hbWUpe1xuXG4gICAgICAgICAgICB2YXIgdGhhdCA9IHRoaXMsXG4gICAgICAgICAgICAgICAgZ3JvdXAgPSB0aGlzLmdyb3VwLm5hbWUgfHwgZ3JvdXBOYW1lO1xuICAgICAgICAgICAgdGhpcy5ncm91cC5pZHMgPSBbXTtcblxuICAgICAgICAgICAgaWYoIGdyb3VwTmFtZSAhPT0gdW5kZWZpbmVkICYmIGdyb3VwTmFtZSAhPT0gdGhpcy5ncm91cC5uYW1lKXtcbiAgICAgICAgICAgICAgICBncm91cCA9IGdyb3VwTmFtZTtcbiAgICAgICAgICAgICAgICB0aGlzLmdyb3VwLm5hbWUgPSBncm91cDtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmF0dHIoJ2RhdGEtJytQTFVHSU5fTkFNRSsnLWdyb3VwJywgZ3JvdXApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoZ3JvdXAgIT09IHVuZGVmaW5lZCAmJiBncm91cCAhPT0gXCJcIil7XG5cbiAgICAgICAgICAgICAgICB2YXIgY291bnQgPSAwO1xuICAgICAgICAgICAgICAgICQuZWFjaCggJCgnLicrUExVR0lOX05BTUUrJ1tkYXRhLScrUExVR0lOX05BTUUrJy1ncm91cD0nK2dyb3VwKyddJykgLCBmdW5jdGlvbihpbmRleCwgdmFsKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhhdC5ncm91cC5pZHMucHVzaCgkKHRoaXMpWzBdLmlkKTtcblxuICAgICAgICAgICAgICAgICAgICBpZih0aGF0LmlkID09ICQodGhpcylbMF0uaWQpe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC5ncm91cC5pbmRleCA9IGNvdW50O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNvdW50Kys7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG5cbiAgICAgICAgdG9nZ2xlOiBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgIGlmKHRoaXMuc3RhdGUgPT0gU1RBVEVTLk9QRU5FRCl7XG4gICAgICAgICAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYodGhpcy5zdGF0ZSA9PSBTVEFURVMuQ0xPU0VEKXtcbiAgICAgICAgICAgICAgICB0aGlzLm9wZW4oKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBvcGVuOiBmdW5jdGlvbiAocGFyYW0pIHtcblxuICAgICAgICAgICAgdmFyIHRoYXQgPSB0aGlzO1xuXG4gICAgICAgICAgICAkLmVhY2goICQoJy4nK1BMVUdJTl9OQU1FKSAsIGZ1bmN0aW9uKGluZGV4LCBtb2RhbCkge1xuICAgICAgICAgICAgICAgIGlmKCAkKG1vZGFsKS5kYXRhKCkuaXppTW9kYWwgIT09IHVuZGVmaW5lZCApe1xuICAgICAgICAgICAgICAgICAgICB2YXIgc3RhdGUgPSAkKG1vZGFsKS5pemlNb2RhbCgnZ2V0U3RhdGUnKTtcbiAgICAgICAgICAgICAgICAgICAgaWYoc3RhdGUgPT0gJ29wZW5lZCcgfHwgc3RhdGUgPT0gJ29wZW5pbmcnKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICQobW9kYWwpLml6aU1vZGFsKCdjbG9zZScpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIChmdW5jdGlvbiB1cmxIYXNoKCl7XG4gICAgICAgICAgICAgICAgaWYodGhhdC5vcHRpb25zLmhpc3Rvcnkpe1xuICAgICAgICAgICAgICAgICAgICB2YXIgb2xkVGl0bGUgPSBkb2N1bWVudC50aXRsZTtcbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQudGl0bGUgPSBvbGRUaXRsZSArIFwiIC0gXCIgKyB0aGF0Lm9wdGlvbnMudGl0bGU7XG4gICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LmxvY2F0aW9uLmhhc2ggPSB0aGF0LmlkO1xuICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC50aXRsZSA9IG9sZFRpdGxlO1xuICAgICAgICAgICAgICAgICAgICAvL2hpc3RvcnkucHVzaFN0YXRlKHt9LCB0aGF0Lm9wdGlvbnMudGl0bGUsIFwiI1wiK3RoYXQuaWQpO1xuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuJGl6aU1vZGFsLmhpc3RvcnkgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy4kaXppTW9kYWwuaGlzdG9yeSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pKCk7XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIG9wZW5lZCgpe1xuXG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5pbmZvKCdbICcrUExVR0lOX05BTUUrJyB8ICcrdGhhdC5pZCsnIF0gT3BlbmVkLicpO1xuXG4gICAgICAgICAgICAgICAgdGhhdC5zdGF0ZSA9IFNUQVRFUy5PUEVORUQ7XG4gICAgICAgICAgICAgICAgdGhhdC4kZWxlbWVudC50cmlnZ2VyKFNUQVRFUy5PUEVORUQpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoYXQub3B0aW9ucy5vbk9wZW5lZCAmJiAoIHR5cGVvZih0aGF0Lm9wdGlvbnMub25PcGVuZWQpID09PSBcImZ1bmN0aW9uXCIgfHwgdHlwZW9mKHRoYXQub3B0aW9ucy5vbk9wZW5lZCkgPT09IFwib2JqZWN0XCIgKSApIHtcbiAgICAgICAgICAgICAgICAgICAgdGhhdC5vcHRpb25zLm9uT3BlbmVkKHRoYXQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gYmluZEV2ZW50cygpe1xuXG4gICAgICAgICAgICAgICAgLy8gQ2xvc2Ugd2hlbiBidXR0b24gcHJlc3NlZFxuICAgICAgICAgICAgICAgIHRoYXQuJGVsZW1lbnQub2ZmKCdjbGljaycsICdbZGF0YS0nK1BMVUdJTl9OQU1FKyctY2xvc2VdJykub24oJ2NsaWNrJywgJ1tkYXRhLScrUExVR0lOX05BTUUrJy1jbG9zZV0nLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICAgICAgICAgICAgdmFyIHRyYW5zaXRpb24gPSAkKGUuY3VycmVudFRhcmdldCkuYXR0cignZGF0YS0nK1BMVUdJTl9OQU1FKyctdHJhbnNpdGlvbk91dCcpO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmKHRyYW5zaXRpb24gIT09IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGF0LmNsb3NlKHt0cmFuc2l0aW9uOnRyYW5zaXRpb259KTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuY2xvc2UoKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgLy8gRXhwYW5kIHdoZW4gYnV0dG9uIHByZXNzZWRcbiAgICAgICAgICAgICAgICB0aGF0LiRlbGVtZW50Lm9mZignY2xpY2snLCAnW2RhdGEtJytQTFVHSU5fTkFNRSsnLWZ1bGxzY3JlZW5dJykub24oJ2NsaWNrJywgJ1tkYXRhLScrUExVR0lOX05BTUUrJy1mdWxsc2NyZWVuXScsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYodGhhdC5pc0Z1bGxzY3JlZW4gPT09IHRydWUpe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC5pc0Z1bGxzY3JlZW4gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuJGVsZW1lbnQucmVtb3ZlQ2xhc3MoJ2lzRnVsbHNjcmVlbicpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC5pc0Z1bGxzY3JlZW4gPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kZWxlbWVudC5hZGRDbGFzcygnaXNGdWxsc2NyZWVuJyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoYXQub3B0aW9ucy5vbkZ1bGxzY3JlZW4gJiYgdHlwZW9mKHRoYXQub3B0aW9ucy5vbkZ1bGxzY3JlZW4pID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQub3B0aW9ucy5vbkZ1bGxzY3JlZW4odGhhdCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdGhhdC4kZWxlbWVudC50cmlnZ2VyKCdmdWxsc2NyZWVuJywgdGhhdCk7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAvLyBOZXh0IG1vZGFsXG4gICAgICAgICAgICAgICAgdGhhdC4kbmF2aWdhdGUub2ZmKCdjbGljaycsICcuJytQTFVHSU5fTkFNRSsnLW5hdmlnYXRlLW5leHQnKS5vbignY2xpY2snLCAnLicrUExVR0lOX05BTUUrJy1uYXZpZ2F0ZS1uZXh0JywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhhdC5uZXh0KGUpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHRoYXQuJGVsZW1lbnQub2ZmKCdjbGljaycsICdbZGF0YS0nK1BMVUdJTl9OQU1FKyctbmV4dF0nKS5vbignY2xpY2snLCAnW2RhdGEtJytQTFVHSU5fTkFNRSsnLW5leHRdJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhhdC5uZXh0KGUpO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgLy8gUHJldmlvdXMgbW9kYWxcbiAgICAgICAgICAgICAgICB0aGF0LiRuYXZpZ2F0ZS5vZmYoJ2NsaWNrJywgJy4nK1BMVUdJTl9OQU1FKyctbmF2aWdhdGUtcHJldicpLm9uKCdjbGljaycsICcuJytQTFVHSU5fTkFNRSsnLW5hdmlnYXRlLXByZXYnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgICB0aGF0LnByZXYoZSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgdGhhdC4kZWxlbWVudC5vZmYoJ2NsaWNrJywgJ1tkYXRhLScrUExVR0lOX05BTUUrJy1wcmV2XScpLm9uKCdjbGljaycsICdbZGF0YS0nK1BMVUdJTl9OQU1FKyctcHJldl0nLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgICB0aGF0LnByZXYoZSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmKHRoaXMuc3RhdGUgPT0gU1RBVEVTLkNMT1NFRCl7XG5cbiAgICAgICAgICAgICAgICBiaW5kRXZlbnRzKCk7XG5cbiAgICAgICAgICAgICAgICB0aGlzLnNldEdyb3VwKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0ZSA9IFNUQVRFUy5PUEVOSU5HO1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQudHJpZ2dlcihTVEFURVMuT1BFTklORyk7XG4gICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5hdHRyKCdhcmlhLWhpZGRlbicsICdmYWxzZScpO1xuXG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5pbmZvKCdbICcrUExVR0lOX05BTUUrJyB8ICcrdGhpcy5pZCsnIF0gT3BlbmluZy4uLicpO1xuXG4gICAgICAgICAgICAgICAgaWYodGhpcy5vcHRpb25zLmlmcmFtZSA9PT0gdHJ1ZSl7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWNvbnRlbnQnKS5hZGRDbGFzcyhQTFVHSU5fTkFNRSsnLWNvbnRlbnQtbG9hZGVyJyk7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWlmcmFtZScpLm9uKCdsb2FkJywgZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICQodGhpcykucGFyZW50KCkucmVtb3ZlQ2xhc3MoUExVR0lOX05BTUUrJy1jb250ZW50LWxvYWRlcicpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICB2YXIgaHJlZiA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBocmVmID0gJChwYXJhbS5jdXJyZW50VGFyZ2V0KS5hdHRyKCdocmVmJykgIT09IFwiXCIgPyAkKHBhcmFtLmN1cnJlbnRUYXJnZXQpLmF0dHIoJ2hyZWYnKSA6IG51bGw7XG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2goZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29uc29sZS53YXJuKGUpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmKCAodGhpcy5vcHRpb25zLmlmcmFtZVVSTCAhPT0gbnVsbCkgJiYgKGhyZWYgPT09IG51bGwgfHwgaHJlZiA9PT0gdW5kZWZpbmVkKSl7XG4gICAgICAgICAgICAgICAgICAgICAgICBocmVmID0gdGhpcy5vcHRpb25zLmlmcmFtZVVSTDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZihocmVmID09PSBudWxsIHx8IGhyZWYgPT09IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJGYWlsZWQgdG8gZmluZCBpZnJhbWUgVVJMXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy1pZnJhbWUnKS5hdHRyKCdzcmMnLCBocmVmKTtcbiAgICAgICAgICAgICAgICB9XG5cblxuICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuYm9keU92ZXJmbG93IHx8IGlzTW9iaWxlKXtcbiAgICAgICAgICAgICAgICAgICAgJCgnaHRtbCcpLmFkZENsYXNzKFBMVUdJTl9OQU1FKyctaXNPdmVyZmxvdycpO1xuICAgICAgICAgICAgICAgICAgICBpZihpc01vYmlsZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKCdib2R5JykuY3NzKCdvdmVyZmxvdycsICdoaWRkZW4nKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMub25PcGVuaW5nICYmIHR5cGVvZih0aGlzLm9wdGlvbnMub25PcGVuaW5nKSA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3B0aW9ucy5vbk9wZW5pbmcodGhpcyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIChmdW5jdGlvbiBvcGVuKCl7XG5cbiAgICAgICAgICAgICAgICAgICAgaWYodGhhdC5ncm91cC5pZHMubGVuZ3RoID4gMSApe1xuXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiRuYXZpZ2F0ZS5hcHBlbmRUbygnYm9keScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kbmF2aWdhdGUuYWRkQ2xhc3MoJ2ZhZGVJbicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZih0aGF0Lm9wdGlvbnMubmF2aWdhdGVDYXB0aW9uID09PSB0cnVlKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiRuYXZpZ2F0ZS5maW5kKCcuJytQTFVHSU5fTkFNRSsnLW5hdmlnYXRlLWNhcHRpb24nKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBtb2RhbFdpZHRoID0gdGhhdC4kZWxlbWVudC5vdXRlcldpZHRoKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZih0aGF0Lm9wdGlvbnMubmF2aWdhdGVBcnJvd3MgIT09IGZhbHNlKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhhdC5vcHRpb25zLm5hdmlnYXRlQXJyb3dzID09PSAnY2xvc2VTY3JlZW5FZGdlJyl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuJG5hdmlnYXRlLmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctbmF2aWdhdGUtcHJldicpLmNzcygnbGVmdCcsIDApLnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kbmF2aWdhdGUuZmluZCgnLicrUExVR0lOX05BTUUrJy1uYXZpZ2F0ZS1uZXh0JykuY3NzKCdyaWdodCcsIDApLnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiRuYXZpZ2F0ZS5maW5kKCcuJytQTFVHSU5fTkFNRSsnLW5hdmlnYXRlLXByZXYnKS5jc3MoJ21hcmdpbi1sZWZ0JywgLSgobW9kYWxXaWR0aC8yKSs4NCkpLnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kbmF2aWdhdGUuZmluZCgnLicrUExVR0lOX05BTUUrJy1uYXZpZ2F0ZS1uZXh0JykuY3NzKCdtYXJnaW4tcmlnaHQnLCAtKChtb2RhbFdpZHRoLzIpKzg0KSkuc2hvdygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kbmF2aWdhdGUuZmluZCgnLicrUExVR0lOX05BTUUrJy1uYXZpZ2F0ZS1wcmV2JykuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuJG5hdmlnYXRlLmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctbmF2aWdhdGUtbmV4dCcpLmhpZGUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGxvb3A7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZih0aGF0Lmdyb3VwLmluZGV4ID09PSAwKXtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxvb3AgPSAkKCcuJytQTFVHSU5fTkFNRSsnW2RhdGEtJytQTFVHSU5fTkFNRSsnLWdyb3VwPVwiJyt0aGF0Lmdyb3VwLm5hbWUrJ1wiXVtkYXRhLScrUExVR0lOX05BTUUrJy1sb29wXScpLmxlbmd0aDtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKGxvb3AgPT09IDAgJiYgdGhhdC5vcHRpb25zLmxvb3AgPT09IGZhbHNlKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiRuYXZpZ2F0ZS5maW5kKCcuJytQTFVHSU5fTkFNRSsnLW5hdmlnYXRlLXByZXYnKS5oaWRlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBpZih0aGF0Lmdyb3VwLmluZGV4KzEgPT09IHRoYXQuZ3JvdXAuaWRzLmxlbmd0aCl7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsb29wID0gJCgnLicrUExVR0lOX05BTUUrJ1tkYXRhLScrUExVR0lOX05BTUUrJy1ncm91cD1cIicrdGhhdC5ncm91cC5uYW1lKydcIl1bZGF0YS0nK1BMVUdJTl9OQU1FKyctbG9vcF0nKS5sZW5ndGg7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihsb29wID09PSAwICYmIHRoYXQub3B0aW9ucy5sb29wID09PSBmYWxzZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kbmF2aWdhdGUuZmluZCgnLicrUExVR0lOX05BTUUrJy1uYXZpZ2F0ZS1uZXh0JykuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgaWYodGhhdC5vcHRpb25zLm92ZXJsYXkgPT09IHRydWUpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYodGhhdC5vcHRpb25zLmFwcGVuZFRvT3ZlcmxheSA9PT0gZmFsc2Upe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuJG92ZXJsYXkuYXBwZW5kVG8oJ2JvZHknKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kb3ZlcmxheS5hcHBlbmRUbyggdGhhdC5vcHRpb25zLmFwcGVuZFRvT3ZlcmxheSApO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoYXQub3B0aW9ucy50cmFuc2l0aW9uSW5PdmVybGF5KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiRvdmVybGF5LmFkZENsYXNzKHRoYXQub3B0aW9ucy50cmFuc2l0aW9uSW5PdmVybGF5KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIHZhciB0cmFuc2l0aW9uSW4gPSB0aGF0Lm9wdGlvbnMudHJhbnNpdGlvbkluO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmKCB0eXBlb2YgcGFyYW0gPT0gJ29iamVjdCcgKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHBhcmFtLnRyYW5zaXRpb24gIT09IHVuZGVmaW5lZCB8fCBwYXJhbS50cmFuc2l0aW9uSW4gIT09IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNpdGlvbkluID0gcGFyYW0udHJhbnNpdGlvbiB8fCBwYXJhbS50cmFuc2l0aW9uSW47XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpZiAodHJhbnNpdGlvbkluICE9PSAnJyAmJiBhbmltYXRpb25FdmVudCAhPT0gdW5kZWZpbmVkKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuJGVsZW1lbnQuYWRkQ2xhc3MoXCJ0cmFuc2l0aW9uSW4gXCIrdHJhbnNpdGlvbkluKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiR3cmFwLm9uZShhbmltYXRpb25FdmVudCwgZnVuY3Rpb24gKCkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kZWxlbWVudC5yZW1vdmVDbGFzcyh0cmFuc2l0aW9uSW4gKyBcIiB0cmFuc2l0aW9uSW5cIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kb3ZlcmxheS5yZW1vdmVDbGFzcyh0aGF0Lm9wdGlvbnMudHJhbnNpdGlvbkluT3ZlcmxheSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kbmF2aWdhdGUucmVtb3ZlQ2xhc3MoJ2ZhZGVJbicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BlbmVkKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiRlbGVtZW50LnNob3coKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wZW5lZCgpO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgaWYodGhhdC5vcHRpb25zLnBhdXNlT25Ib3ZlciA9PT0gdHJ1ZSAmJiB0aGF0Lm9wdGlvbnMucGF1c2VPbkhvdmVyID09PSB0cnVlICYmIHRoYXQub3B0aW9ucy50aW1lb3V0ICE9PSBmYWxzZSAmJiAhaXNOYU4ocGFyc2VJbnQodGhhdC5vcHRpb25zLnRpbWVvdXQpKSAmJiB0aGF0Lm9wdGlvbnMudGltZW91dCAhPT0gZmFsc2UgJiYgdGhhdC5vcHRpb25zLnRpbWVvdXQgIT09IDApe1xuXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiRlbGVtZW50Lm9mZignbW91c2VlbnRlcicpLm9uKCdtb3VzZWVudGVyJywgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuaXNQYXVzZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiRlbGVtZW50Lm9mZignbW91c2VsZWF2ZScpLm9uKCdtb3VzZWxlYXZlJywgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuaXNQYXVzZWQgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9KSgpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy50aW1lb3V0ICE9PSBmYWxzZSAmJiAhaXNOYU4ocGFyc2VJbnQodGhpcy5vcHRpb25zLnRpbWVvdXQpKSAmJiB0aGlzLm9wdGlvbnMudGltZW91dCAhPT0gZmFsc2UgJiYgdGhpcy5vcHRpb25zLnRpbWVvdXQgIT09IDApIHtcblxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnRpbWVvdXRQcm9ncmVzc2JhciA9PT0gdHJ1ZSkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2dyZXNzQmFyID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhpZGVFdGE6IG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4SGlkZVRpbWU6IG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudFRpbWU6IG5ldyBEYXRlKCkuZ2V0VGltZSgpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVsOiB0aGlzLiRlbGVtZW50LmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctcHJvZ3Jlc3NiYXIgPiBkaXYnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cGRhdGVQcm9ncmVzczogZnVuY3Rpb24oKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoIXRoYXQuaXNQYXVzZWQpe1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGF0LnByb2dyZXNzQmFyLmN1cnJlbnRUaW1lID0gdGhhdC5wcm9ncmVzc0Jhci5jdXJyZW50VGltZSsxMDtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHBlcmNlbnRhZ2UgPSAoKHRoYXQucHJvZ3Jlc3NCYXIuaGlkZUV0YSAtICh0aGF0LnByb2dyZXNzQmFyLmN1cnJlbnRUaW1lKSkgLyB0aGF0LnByb2dyZXNzQmFyLm1heEhpZGVUaW1lKSAqIDEwMDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQucHJvZ3Jlc3NCYXIuZWwud2lkdGgocGVyY2VudGFnZSArICclJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihwZXJjZW50YWdlIDwgMCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC5jbG9zZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMudGltZW91dCA+IDApIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZ3Jlc3NCYXIubWF4SGlkZVRpbWUgPSBwYXJzZUZsb2F0KHRoaXMub3B0aW9ucy50aW1lb3V0KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2dyZXNzQmFyLmhpZGVFdGEgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKSArIHRoaXMucHJvZ3Jlc3NCYXIubWF4SGlkZVRpbWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50aW1lclRpbWVvdXQgPSBzZXRJbnRlcnZhbCh0aGlzLnByb2dyZXNzQmFyLnVwZGF0ZVByb2dyZXNzLCAxMCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50aW1lclRpbWVvdXQgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC5jbG9zZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSwgdGhhdC5vcHRpb25zLnRpbWVvdXQpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy8gQ2xvc2Ugb24gb3ZlcmxheSBjbGlja1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMub3ZlcmxheUNsb3NlICYmICF0aGlzLiRlbGVtZW50Lmhhc0NsYXNzKHRoaXMub3B0aW9ucy50cmFuc2l0aW9uT3V0KSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRvdmVybGF5LmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuY2xvc2UoKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5mb2N1c0lucHV0KXtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCc6aW5wdXQ6bm90KGJ1dHRvbik6ZW5hYmxlZDp2aXNpYmxlOmZpcnN0JykuZm9jdXMoKTsgLy8gRm9jdXMgb24gdGhlIGZpcnN0IGZpZWxkXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgKGZ1bmN0aW9uIHVwZGF0ZVRpbWVyKCl7XG4gICAgICAgICAgICAgICAgICAgIHRoYXQucmVjYWxjTGF5b3V0KCk7XG4gICAgICAgICAgICAgICAgICAgIHRoYXQudGltZXIgPSBzZXRUaW1lb3V0KHVwZGF0ZVRpbWVyLCAzMDApO1xuICAgICAgICAgICAgICAgIH0pKCk7XG5cbiAgICAgICAgICAgICAgICAvLyBDbG9zZSB3aGVuIHRoZSBFc2NhcGUga2V5IGlzIHByZXNzZWRcbiAgICAgICAgICAgICAgICAkZG9jdW1lbnQub24oJ2tleWRvd24uJytQTFVHSU5fTkFNRSwgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoYXQub3B0aW9ucy5jbG9zZU9uRXNjYXBlICYmIGUua2V5Q29kZSA9PT0gMjcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuY2xvc2UoKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfSxcblxuICAgICAgICBjbG9zZTogZnVuY3Rpb24gKHBhcmFtKSB7XG5cbiAgICAgICAgICAgIHZhciB0aGF0ID0gdGhpcztcblxuICAgICAgICAgICAgZnVuY3Rpb24gY2xvc2VkKCl7XG5cbiAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmluZm8oJ1sgJytQTFVHSU5fTkFNRSsnIHwgJyt0aGF0LmlkKycgXSBDbG9zZWQuJyk7XG5cbiAgICAgICAgICAgICAgICB0aGF0LnN0YXRlID0gU1RBVEVTLkNMT1NFRDtcbiAgICAgICAgICAgICAgICB0aGF0LiRlbGVtZW50LnRyaWdnZXIoU1RBVEVTLkNMT1NFRCk7XG5cbiAgICAgICAgICAgICAgICBpZiAodGhhdC5vcHRpb25zLmlmcmFtZSA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICB0aGF0LiRlbGVtZW50LmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctaWZyYW1lJykuYXR0cignc3JjJywgXCJcIik7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHRoYXQub3B0aW9ucy5ib2R5T3ZlcmZsb3cgfHwgaXNNb2JpbGUpe1xuICAgICAgICAgICAgICAgICAgICAkKCdodG1sJykucmVtb3ZlQ2xhc3MoUExVR0lOX05BTUUrJy1pc092ZXJmbG93Jyk7XG4gICAgICAgICAgICAgICAgICAgIGlmKGlzTW9iaWxlKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJ2JvZHknKS5jc3MoJ292ZXJmbG93JywnYXV0bycpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHRoYXQub3B0aW9ucy5vbkNsb3NlZCAmJiB0eXBlb2YodGhhdC5vcHRpb25zLm9uQ2xvc2VkKSA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoYXQub3B0aW9ucy5vbkNsb3NlZCh0aGF0KTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZih0aGF0Lm9wdGlvbnMucmVzdG9yZURlZmF1bHRDb250ZW50ID09PSB0cnVlKXtcbiAgICAgICAgICAgICAgICAgICAgdGhhdC4kZWxlbWVudC5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWNvbnRlbnQnKS5odG1sKCB0aGF0LmNvbnRlbnQgKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiggJCgnLicrUExVR0lOX05BTUUrJzp2aXNpYmxlJykubGVuZ3RoID09PSAwICl7XG4gICAgICAgICAgICAgICAgICAgICQoJ2h0bWwnKS5yZW1vdmVDbGFzcyhQTFVHSU5fTkFNRSsnLWlzQXR0YWNoZWQnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmKHRoaXMuc3RhdGUgPT0gU1RBVEVTLk9QRU5FRCB8fCB0aGlzLnN0YXRlID09IFNUQVRFUy5PUEVOSU5HKXtcblxuICAgICAgICAgICAgICAgICRkb2N1bWVudC5vZmYoJ2tleWRvd24uJytQTFVHSU5fTkFNRSk7XG5cbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlID0gU1RBVEVTLkNMT1NJTkc7XG4gICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC50cmlnZ2VyKFNUQVRFUy5DTE9TSU5HKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmF0dHIoJ2FyaWEtaGlkZGVuJywgJ3RydWUnKTtcblxuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUuaW5mbygnWyAnK1BMVUdJTl9OQU1FKycgfCAnK3RoaXMuaWQrJyBdIENsb3NpbmcuLi4nKTtcblxuICAgICAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLnRpbWVyKTtcbiAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy50aW1lclRpbWVvdXQpO1xuXG4gICAgICAgICAgICAgICAgaWYgKHRoYXQub3B0aW9ucy5vbkNsb3NpbmcgJiYgdHlwZW9mKHRoYXQub3B0aW9ucy5vbkNsb3NpbmcpID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhhdC5vcHRpb25zLm9uQ2xvc2luZyh0aGlzKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB2YXIgdHJhbnNpdGlvbk91dCA9IHRoaXMub3B0aW9ucy50cmFuc2l0aW9uT3V0O1xuXG4gICAgICAgICAgICAgICAgaWYoIHR5cGVvZiBwYXJhbSA9PSAnb2JqZWN0JyApe1xuICAgICAgICAgICAgICAgICAgICBpZihwYXJhbS50cmFuc2l0aW9uICE9PSB1bmRlZmluZWQgfHwgcGFyYW0udHJhbnNpdGlvbk91dCAhPT0gdW5kZWZpbmVkKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb25PdXQgPSBwYXJhbS50cmFuc2l0aW9uIHx8IHBhcmFtLnRyYW5zaXRpb25PdXQ7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiggKHRyYW5zaXRpb25PdXQgPT09IGZhbHNlIHx8IHRyYW5zaXRpb25PdXQgPT09ICcnICkgfHwgYW5pbWF0aW9uRXZlbnQgPT09IHVuZGVmaW5lZCl7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5oaWRlKCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJG92ZXJsYXkucmVtb3ZlKCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJG5hdmlnYXRlLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgICAgICBjbG9zZWQoKTtcblxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5hdHRyKCdjbGFzcycsIFtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2xhc3NlcyxcbiAgICAgICAgICAgICAgICAgICAgICAgIFBMVUdJTl9OQU1FLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNpdGlvbk91dCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3B0aW9ucy50aGVtZSA9PSAnbGlnaHQnID8gUExVR0lOX05BTUUrJy1saWdodCcgOiB0aGlzLm9wdGlvbnMudGhlbWUsXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzRnVsbHNjcmVlbiA9PT0gdHJ1ZSA/ICdpc0Z1bGxzY3JlZW4nIDogJycsXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMucnRsID8gUExVR0lOX05BTUUrJy1ydGwnIDogJydcbiAgICAgICAgICAgICAgICAgICAgXS5qb2luKCcgJykpO1xuXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJG92ZXJsYXkuYXR0cignY2xhc3MnLCBQTFVHSU5fTkFNRSArIFwiLW92ZXJsYXkgXCIgKyB0aGlzLm9wdGlvbnMudHJhbnNpdGlvbk91dE92ZXJsYXkpO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmKHRoYXQub3B0aW9ucy5uYXZpZ2F0ZUFycm93cyAhPT0gZmFsc2Upe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kbmF2aWdhdGUuYXR0cignY2xhc3MnLCBQTFVHSU5fTkFNRSArIFwiLW5hdmlnYXRlIGZhZGVPdXRcIik7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50Lm9uZShhbmltYXRpb25FdmVudCwgZnVuY3Rpb24gKCkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiggdGhhdC4kZWxlbWVudC5oYXNDbGFzcyh0cmFuc2l0aW9uT3V0KSApe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuJGVsZW1lbnQucmVtb3ZlQ2xhc3ModHJhbnNpdGlvbk91dCArIFwiIHRyYW5zaXRpb25PdXRcIikuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kb3ZlcmxheS5yZW1vdmVDbGFzcyh0aGF0Lm9wdGlvbnMudHJhbnNpdGlvbk91dE92ZXJsYXkpLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kbmF2aWdhdGUucmVtb3ZlQ2xhc3MoJ2ZhZGVPdXQnKS5yZW1vdmUoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsb3NlZCgpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIG5leHQ6IGZ1bmN0aW9uIChlKXtcblxuICAgICAgICAgICAgdmFyIHRoYXQgPSB0aGlzO1xuICAgICAgICAgICAgdmFyIHRyYW5zaXRpb25JbiA9ICdmYWRlSW5SaWdodCc7XG4gICAgICAgICAgICB2YXIgdHJhbnNpdGlvbk91dCA9ICdmYWRlT3V0TGVmdCc7XG4gICAgICAgICAgICB2YXIgbW9kYWwgPSAkKCcuJytQTFVHSU5fTkFNRSsnOnZpc2libGUnKTtcbiAgICAgICAgICAgIHZhciBtb2RhbHMgPSB7fTtcbiAgICAgICAgICAgIG1vZGFscy5vdXQgPSB0aGlzO1xuXG4gICAgICAgICAgICBpZihlICE9PSB1bmRlZmluZWQgJiYgdHlwZW9mIGUgIT09ICdvYmplY3QnKXtcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgbW9kYWwgPSAkKGUuY3VycmVudFRhcmdldCk7XG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbkluID0gbW9kYWwuYXR0cignZGF0YS0nK1BMVUdJTl9OQU1FKyctdHJhbnNpdGlvbkluJyk7XG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbk91dCA9IG1vZGFsLmF0dHIoJ2RhdGEtJytQTFVHSU5fTkFNRSsnLXRyYW5zaXRpb25PdXQnKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZihlICE9PSB1bmRlZmluZWQpe1xuICAgICAgICAgICAgICAgIGlmKGUudHJhbnNpdGlvbkluICE9PSB1bmRlZmluZWQpe1xuICAgICAgICAgICAgICAgICAgICB0cmFuc2l0aW9uSW4gPSBlLnRyYW5zaXRpb25JbjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYoZS50cmFuc2l0aW9uT3V0ICE9PSB1bmRlZmluZWQpe1xuICAgICAgICAgICAgICAgICAgICB0cmFuc2l0aW9uT3V0ID0gZS50cmFuc2l0aW9uT3V0O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5jbG9zZSh7dHJhbnNpdGlvbjp0cmFuc2l0aW9uT3V0fSk7XG5cbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcblxuICAgICAgICAgICAgICAgIHZhciBsb29wID0gJCgnLicrUExVR0lOX05BTUUrJ1tkYXRhLScrUExVR0lOX05BTUUrJy1ncm91cD1cIicrdGhhdC5ncm91cC5uYW1lKydcIl1bZGF0YS0nK1BMVUdJTl9OQU1FKyctbG9vcF0nKS5sZW5ndGg7XG4gICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IHRoYXQuZ3JvdXAuaW5kZXgrMTsgaSA8PSB0aGF0Lmdyb3VwLmlkcy5sZW5ndGg7IGkrKykge1xuXG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBtb2RhbHMuaW4gPSAkKFwiI1wiK3RoYXQuZ3JvdXAuaWRzW2ldKS5kYXRhKCkuaXppTW9kYWw7XG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2gobG9nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjb25zb2xlLmluZm8oJ1sgJytQTFVHSU5fTkFNRSsnIF0gTm8gbmV4dCBtb2RhbC4nKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZih0eXBlb2YgbW9kYWxzLmluICE9PSAndW5kZWZpbmVkJyl7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjXCIrdGhhdC5ncm91cC5pZHNbaV0pLml6aU1vZGFsKCdvcGVuJywgeyB0cmFuc2l0aW9uOiB0cmFuc2l0aW9uSW4gfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihpID09IHRoYXQuZ3JvdXAuaWRzLmxlbmd0aCAmJiBsb29wID4gMCB8fCB0aGF0Lm9wdGlvbnMubG9vcCA9PT0gdHJ1ZSl7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpbmRleCA9IDA7IGluZGV4IDw9IHRoYXQuZ3JvdXAuaWRzLmxlbmd0aDsgaW5kZXgrKykge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGFscy5pbiA9ICQoXCIjXCIrdGhhdC5ncm91cC5pZHNbaW5kZXhdKS5kYXRhKCkuaXppTW9kYWw7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBtb2RhbHMuaW4gIT09ICd1bmRlZmluZWQnKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjXCIrdGhhdC5ncm91cC5pZHNbaW5kZXhdKS5pemlNb2RhbCgnb3BlbicsIHsgdHJhbnNpdGlvbjogdHJhbnNpdGlvbkluIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0sIDIwMCk7XG5cbiAgICAgICAgICAgICQoZG9jdW1lbnQpLnRyaWdnZXIoIFBMVUdJTl9OQU1FICsgXCItZ3JvdXAtY2hhbmdlXCIsIG1vZGFscyApO1xuICAgICAgICB9LFxuXG4gICAgICAgIHByZXY6IGZ1bmN0aW9uIChlKXtcbiAgICAgICAgICAgIHZhciB0aGF0ID0gdGhpcztcbiAgICAgICAgICAgIHZhciB0cmFuc2l0aW9uSW4gPSAnZmFkZUluTGVmdCc7XG4gICAgICAgICAgICB2YXIgdHJhbnNpdGlvbk91dCA9ICdmYWRlT3V0UmlnaHQnO1xuICAgICAgICAgICAgdmFyIG1vZGFsID0gJCgnLicrUExVR0lOX05BTUUrJzp2aXNpYmxlJyk7XG4gICAgICAgICAgICB2YXIgbW9kYWxzID0ge307XG4gICAgICAgICAgICBtb2RhbHMub3V0ID0gdGhpcztcblxuICAgICAgICAgICAgaWYoZSAhPT0gdW5kZWZpbmVkICYmIHR5cGVvZiBlICE9PSAnb2JqZWN0Jyl7XG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIG1vZGFsID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuICAgICAgICAgICAgICAgIHRyYW5zaXRpb25JbiA9IG1vZGFsLmF0dHIoJ2RhdGEtJytQTFVHSU5fTkFNRSsnLXRyYW5zaXRpb25JbicpO1xuICAgICAgICAgICAgICAgIHRyYW5zaXRpb25PdXQgPSBtb2RhbC5hdHRyKCdkYXRhLScrUExVR0lOX05BTUUrJy10cmFuc2l0aW9uT3V0Jyk7XG5cbiAgICAgICAgICAgIH0gZWxzZSBpZihlICE9PSB1bmRlZmluZWQpe1xuXG4gICAgICAgICAgICAgICAgaWYoZS50cmFuc2l0aW9uSW4gIT09IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb25JbiA9IGUudHJhbnNpdGlvbkluO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZihlLnRyYW5zaXRpb25PdXQgIT09IHVuZGVmaW5lZCl7XG4gICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb25PdXQgPSBlLnRyYW5zaXRpb25PdXQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmNsb3NlKHt0cmFuc2l0aW9uOnRyYW5zaXRpb25PdXR9KTtcblxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuXG4gICAgICAgICAgICAgICAgdmFyIGxvb3AgPSAkKCcuJytQTFVHSU5fTkFNRSsnW2RhdGEtJytQTFVHSU5fTkFNRSsnLWdyb3VwPVwiJyt0aGF0Lmdyb3VwLm5hbWUrJ1wiXVtkYXRhLScrUExVR0lOX05BTUUrJy1sb29wXScpLmxlbmd0aDtcblxuICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSB0aGF0Lmdyb3VwLmluZGV4OyBpID49IDA7IGktLSkge1xuXG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBtb2RhbHMuaW4gPSAkKFwiI1wiK3RoYXQuZ3JvdXAuaWRzW2ktMV0pLmRhdGEoKS5pemlNb2RhbDtcbiAgICAgICAgICAgICAgICAgICAgfSBjYXRjaChsb2cpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUuaW5mbygnWyAnK1BMVUdJTl9OQU1FKycgXSBObyBwcmV2aW91cyBtb2RhbC4nKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZih0eXBlb2YgbW9kYWxzLmluICE9PSAndW5kZWZpbmVkJyl7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjXCIrdGhhdC5ncm91cC5pZHNbaS0xXSkuaXppTW9kYWwoJ29wZW4nLCB7IHRyYW5zaXRpb246IHRyYW5zaXRpb25JbiB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKGkgPT09IDAgJiYgbG9vcCA+IDAgfHwgdGhhdC5vcHRpb25zLmxvb3AgPT09IHRydWUpe1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaW5kZXggPSB0aGF0Lmdyb3VwLmlkcy5sZW5ndGgtMTsgaW5kZXggPj0gMDsgaW5kZXgtLSkge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGFscy5pbiA9ICQoXCIjXCIrdGhhdC5ncm91cC5pZHNbaW5kZXhdKS5kYXRhKCkuaXppTW9kYWw7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBtb2RhbHMuaW4gIT09ICd1bmRlZmluZWQnKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjXCIrdGhhdC5ncm91cC5pZHNbaW5kZXhdKS5pemlNb2RhbCgnb3BlbicsIHsgdHJhbnNpdGlvbjogdHJhbnNpdGlvbkluIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIH0sIDIwMCk7XG5cbiAgICAgICAgICAgICQoZG9jdW1lbnQpLnRyaWdnZXIoIFBMVUdJTl9OQU1FICsgXCItZ3JvdXAtY2hhbmdlXCIsIG1vZGFscyApO1xuICAgICAgICB9LFxuXG4gICAgICAgIGRlc3Ryb3k6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBlID0gJC5FdmVudCgnZGVzdHJveScpO1xuXG4gICAgICAgICAgICB0aGlzLiRlbGVtZW50LnRyaWdnZXIoZSk7XG5cbiAgICAgICAgICAgICRkb2N1bWVudC5vZmYoJ2tleWRvd24uJytQTFVHSU5fTkFNRSk7XG5cbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLnRpbWVyKTtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aGlzLnRpbWVyVGltZW91dCk7XG5cbiAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuaWZyYW1lID09PSB0cnVlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWlmcmFtZScpLnJlbW92ZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5odG1sKHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy1jb250ZW50JykuaHRtbCgpKTtcblxuICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5vZmYoJ2NsaWNrJywgJ1tkYXRhLScrUExVR0lOX05BTUUrJy1jbG9zZV0nKTtcbiAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQub2ZmKCdjbGljaycsICdbZGF0YS0nK1BMVUdJTl9OQU1FKyctZnVsbHNjcmVlbl0nKTtcblxuICAgICAgICAgICAgdGhpcy4kZWxlbWVudFxuICAgICAgICAgICAgICAgIC5vZmYoJy4nK1BMVUdJTl9OQU1FKVxuICAgICAgICAgICAgICAgIC5yZW1vdmVEYXRhKFBMVUdJTl9OQU1FKVxuICAgICAgICAgICAgICAgIC5hdHRyKCdzdHlsZScsICcnKTtcblxuICAgICAgICAgICAgdGhpcy4kb3ZlcmxheS5yZW1vdmUoKTtcbiAgICAgICAgICAgIHRoaXMuJG5hdmlnYXRlLnJlbW92ZSgpO1xuICAgICAgICAgICAgdGhpcy4kZWxlbWVudC50cmlnZ2VyKFNUQVRFUy5ERVNUUk9ZRUQpO1xuICAgICAgICAgICAgdGhpcy4kZWxlbWVudCA9IG51bGw7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0U3RhdGU6IGZ1bmN0aW9uKCl7XG5cbiAgICAgICAgICAgIHJldHVybiB0aGlzLnN0YXRlO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldEdyb3VwOiBmdW5jdGlvbigpe1xuXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5ncm91cDtcbiAgICAgICAgfSxcblxuICAgICAgICBzZXRXaWR0aDogZnVuY3Rpb24od2lkdGgpe1xuXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMud2lkdGggPSB3aWR0aDtcblxuICAgICAgICAgICAgdGhpcy5yZWNhbGNXaWR0aCgpO1xuXG4gICAgICAgICAgICB2YXIgbW9kYWxXaWR0aCA9IHRoaXMuJGVsZW1lbnQub3V0ZXJXaWR0aCgpO1xuICAgICAgICAgICAgaWYodGhpcy5vcHRpb25zLm5hdmlnYXRlQXJyb3dzID09PSB0cnVlIHx8IHRoaXMub3B0aW9ucy5uYXZpZ2F0ZUFycm93cyA9PSAnY2xvc2VUb01vZGFsJyl7XG4gICAgICAgICAgICAgICAgdGhpcy4kbmF2aWdhdGUuZmluZCgnLicrUExVR0lOX05BTUUrJy1uYXZpZ2F0ZS1wcmV2JykuY3NzKCdtYXJnaW4tbGVmdCcsIC0oKG1vZGFsV2lkdGgvMikrODQpKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgdGhpcy4kbmF2aWdhdGUuZmluZCgnLicrUExVR0lOX05BTUUrJy1uYXZpZ2F0ZS1uZXh0JykuY3NzKCdtYXJnaW4tcmlnaHQnLCAtKChtb2RhbFdpZHRoLzIpKzg0KSkuc2hvdygpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0VG9wOiBmdW5jdGlvbih0b3Ape1xuXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMudG9wID0gdG9wO1xuXG4gICAgICAgICAgICB0aGlzLnJlY2FsY1ZlcnRpY2FsUG9zKGZhbHNlKTtcbiAgICAgICAgfSxcblxuICAgICAgICBzZXRCb3R0b206IGZ1bmN0aW9uKGJvdHRvbSl7XG5cbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5ib3R0b20gPSBib3R0b207XG5cbiAgICAgICAgICAgIHRoaXMucmVjYWxjVmVydGljYWxQb3MoZmFsc2UpO1xuXG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0SGVhZGVyOiBmdW5jdGlvbihzdGF0dXMpe1xuXG4gICAgICAgICAgICBpZihzdGF0dXMpe1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy1oZWFkZXInKS5zaG93KCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuaGVhZGVySGVpZ2h0ID0gMDtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctaGVhZGVyJykuaGlkZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldFRpdGxlOiBmdW5jdGlvbih0aXRsZSl7XG5cbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy50aXRsZSA9IHRpdGxlO1xuXG4gICAgICAgICAgICBpZih0aGlzLmhlYWRlckhlaWdodCA9PT0gMCl7XG4gICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVIZWFkZXIoKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYoIHRoaXMuJGhlYWRlci5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWhlYWRlci10aXRsZScpLmxlbmd0aCA9PT0gMCApe1xuICAgICAgICAgICAgICAgIHRoaXMuJGhlYWRlci5hcHBlbmQoJzxoMiBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1oZWFkZXItdGl0bGVcIj48L2gyPicpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLiRoZWFkZXIuZmluZCgnLicrUExVR0lOX05BTUUrJy1oZWFkZXItdGl0bGUnKS5odG1sKHRpdGxlKTtcbiAgICAgICAgfSxcblxuICAgICAgICBzZXRTdWJ0aXRsZTogZnVuY3Rpb24oc3VidGl0bGUpe1xuXG4gICAgICAgICAgICBpZihzdWJ0aXRsZSA9PT0gJycpe1xuXG4gICAgICAgICAgICAgICAgdGhpcy4kaGVhZGVyLmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctaGVhZGVyLXN1YnRpdGxlJykucmVtb3ZlKCk7XG4gICAgICAgICAgICAgICAgdGhpcy4kaGVhZGVyLmFkZENsYXNzKFBMVUdJTl9OQU1FKyctbm9TdWJ0aXRsZScpO1xuXG4gICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgaWYoIHRoaXMuJGhlYWRlci5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWhlYWRlci1zdWJ0aXRsZScpLmxlbmd0aCA9PT0gMCApe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRoZWFkZXIuYXBwZW5kKCc8cCBjbGFzcz1cIicrUExVR0lOX05BTUUrJy1oZWFkZXItc3VidGl0bGVcIj48L3A+Jyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMuJGhlYWRlci5yZW1vdmVDbGFzcyhQTFVHSU5fTkFNRSsnLW5vU3VidGl0bGUnKTtcblxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLiRoZWFkZXIuZmluZCgnLicrUExVR0lOX05BTUUrJy1oZWFkZXItc3VidGl0bGUnKS5odG1sKHN1YnRpdGxlKTtcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5zdWJ0aXRsZSA9IHN1YnRpdGxlO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNldEljb246IGZ1bmN0aW9uKGljb24pe1xuXG4gICAgICAgICAgICBpZiggdGhpcy4kaGVhZGVyLmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctaGVhZGVyLWljb24nKS5sZW5ndGggPT09IDAgKXtcbiAgICAgICAgICAgICAgICB0aGlzLiRoZWFkZXIucHJlcGVuZCgnPGkgY2xhc3M9XCInK1BMVUdJTl9OQU1FKyctaGVhZGVyLWljb25cIj48L2k+Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLiRoZWFkZXIuZmluZCgnLicrUExVR0lOX05BTUUrJy1oZWFkZXItaWNvbicpLmF0dHIoJ2NsYXNzJywgUExVR0lOX05BTUUrJy1oZWFkZXItaWNvbiAnICsgaWNvbik7XG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMuaWNvbiA9IGljb247XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0SWNvblRleHQ6IGZ1bmN0aW9uKGljb25UZXh0KXtcblxuICAgICAgICAgICAgdGhpcy4kaGVhZGVyLmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctaGVhZGVyLWljb24nKS5odG1sKGljb25UZXh0KTtcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5pY29uVGV4dCA9IGljb25UZXh0O1xuICAgICAgICB9LFxuXG4gICAgICAgIHNldEhlYWRlckNvbG9yOiBmdW5jdGlvbihoZWFkZXJDb2xvcil7XG4gICAgICAgICAgICBpZih0aGlzLm9wdGlvbnMuYm9yZGVyQm90dG9tID09PSB0cnVlKXtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmNzcygnYm9yZGVyLWJvdHRvbScsICczcHggc29saWQgJyArIGhlYWRlckNvbG9yICsgJycpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy4kaGVhZGVyLmNzcygnYmFja2dyb3VuZCcsIGhlYWRlckNvbG9yKTtcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5oZWFkZXJDb2xvciA9IGhlYWRlckNvbG9yO1xuICAgICAgICB9LFxuXG4gICAgICAgIHNldEJhY2tncm91bmQ6IGZ1bmN0aW9uKGJhY2tncm91bmQpe1xuICAgICAgICAgICAgaWYoYmFja2dyb3VuZCA9PT0gZmFsc2Upe1xuICAgICAgICAgICAgICAgIHRoaXMub3B0aW9ucy5iYWNrZ3JvdW5kID0gbnVsbDtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmNzcygnYmFja2dyb3VuZCcsICcnKTtcbiAgICAgICAgICAgIH0gZWxzZXtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmNzcygnYmFja2dyb3VuZCcsIGJhY2tncm91bmQpO1xuICAgICAgICAgICAgICAgIHRoaXMub3B0aW9ucy5iYWNrZ3JvdW5kID0gYmFja2dyb3VuZDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBzZXRaaW5kZXg6IGZ1bmN0aW9uKHpJbmRleCl7XG5cbiAgICAgICAgICAgIGlmICghaXNOYU4ocGFyc2VJbnQodGhpcy5vcHRpb25zLnppbmRleCkpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zLnppbmRleCA9IHpJbmRleDtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmNzcygnei1pbmRleCcsIHpJbmRleCk7XG4gICAgICAgICAgICAgICAgdGhpcy4kbmF2aWdhdGUuY3NzKCd6LWluZGV4JywgekluZGV4LTEpO1xuICAgICAgICAgICAgICAgIHRoaXMuJG92ZXJsYXkuY3NzKCd6LWluZGV4JywgekluZGV4LTIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHNldEZ1bGxzY3JlZW46IGZ1bmN0aW9uKHZhbHVlKXtcblxuICAgICAgICAgICAgaWYodmFsdWUpe1xuICAgICAgICAgICAgICAgIHRoaXMuaXNGdWxsc2NyZWVuID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmFkZENsYXNzKCdpc0Z1bGxzY3JlZW4nKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5pc0Z1bGxzY3JlZW4gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LnJlbW92ZUNsYXNzKCdpc0Z1bGxzY3JlZW4nKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9LFxuXG4gICAgICAgIHNldENvbnRlbnQ6IGZ1bmN0aW9uKGNvbnRlbnQpe1xuXG4gICAgICAgICAgICBpZiggdHlwZW9mIGNvbnRlbnQgPT0gXCJvYmplY3RcIiApe1xuICAgICAgICAgICAgICAgIHZhciByZXBsYWNlID0gY29udGVudC5kZWZhdWx0IHx8IGZhbHNlO1xuICAgICAgICAgICAgICAgIGlmKHJlcGxhY2UgPT09IHRydWUpe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnQgPSBjb250ZW50LmNvbnRlbnQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNvbnRlbnQgPSBjb250ZW50LmNvbnRlbnQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmlmcmFtZSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctY29udGVudCcpLmh0bWwoY29udGVudCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfSxcblxuICAgICAgICBzZXRUcmFuc2l0aW9uSW46IGZ1bmN0aW9uKHRyYW5zaXRpb24pe1xuXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMudHJhbnNpdGlvbkluID0gdHJhbnNpdGlvbjtcbiAgICAgICAgfSxcblxuICAgICAgICBzZXRUcmFuc2l0aW9uT3V0OiBmdW5jdGlvbih0cmFuc2l0aW9uKXtcblxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLnRyYW5zaXRpb25PdXQgPSB0cmFuc2l0aW9uO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlc2V0Q29udGVudDogZnVuY3Rpb24oKXtcblxuICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWNvbnRlbnQnKS5odG1sKHRoaXMuY29udGVudCk7XG5cbiAgICAgICAgfSxcblxuICAgICAgICBzdGFydExvYWRpbmc6IGZ1bmN0aW9uKCl7XG5cbiAgICAgICAgICAgIGlmKCAhdGhpcy4kZWxlbWVudC5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWxvYWRlcicpLmxlbmd0aCApe1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiJytQTFVHSU5fTkFNRSsnLWxvYWRlciBmYWRlSW5cIj48L2Rpdj4nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy1sb2FkZXInKS5jc3Moe1xuICAgICAgICAgICAgICAgIHRvcDogdGhpcy5oZWFkZXJIZWlnaHQsXG4gICAgICAgICAgICAgICAgYm9yZGVyUmFkaXVzOiB0aGlzLm9wdGlvbnMucmFkaXVzXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSxcblxuICAgICAgICBzdG9wTG9hZGluZzogZnVuY3Rpb24oKXtcblxuICAgICAgICAgICAgdmFyICRsb2FkZXIgPSB0aGlzLiRlbGVtZW50LmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctbG9hZGVyJyk7XG5cbiAgICAgICAgICAgIGlmKCAhJGxvYWRlci5sZW5ndGggKXtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LnByZXBlbmQoJzxkaXYgY2xhc3M9XCInK1BMVUdJTl9OQU1FKyctbG9hZGVyIGZhZGVJblwiPjwvZGl2PicpO1xuICAgICAgICAgICAgICAgICRsb2FkZXIgPSB0aGlzLiRlbGVtZW50LmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctbG9hZGVyJykuY3NzKCdib3JkZXItcmFkaXVzJywgdGhpcy5vcHRpb25zLnJhZGl1cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAkbG9hZGVyLnJlbW92ZUNsYXNzKCdmYWRlSW4nKS5hZGRDbGFzcygnZmFkZU91dCcpO1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICRsb2FkZXIucmVtb3ZlKCk7XG4gICAgICAgICAgICB9LDYwMCk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVjYWxjV2lkdGg6IGZ1bmN0aW9uKCl7XG5cbiAgICAgICAgICAgIHZhciB0aGF0ID0gdGhpcztcblxuICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5jc3MoJ21heC13aWR0aCcsIHRoaXMub3B0aW9ucy53aWR0aCk7XG5cbiAgICAgICAgICAgIGlmKGlzSUUoKSl7XG4gICAgICAgICAgICAgICAgdmFyIG1vZGFsV2lkdGggPSB0aGF0Lm9wdGlvbnMud2lkdGg7XG5cbiAgICAgICAgICAgICAgICBpZihtb2RhbFdpZHRoLnRvU3RyaW5nKCkuc3BsaXQoXCIlXCIpLmxlbmd0aCA+IDEpe1xuICAgICAgICAgICAgICAgICAgICBtb2RhbFdpZHRoID0gdGhhdC4kZWxlbWVudC5vdXRlcldpZHRoKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoYXQuJGVsZW1lbnQuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgbGVmdDogJzUwJScsXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbkxlZnQ6IC0obW9kYWxXaWR0aC8yKVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuXG4gICAgICAgIHJlY2FsY1ZlcnRpY2FsUG9zOiBmdW5jdGlvbihmaXJzdCl7XG5cbiAgICAgICAgICAgIGlmKHRoaXMub3B0aW9ucy50b3AgIT09IG51bGwgJiYgdGhpcy5vcHRpb25zLnRvcCAhPT0gZmFsc2Upe1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuY3NzKCdtYXJnaW4tdG9wJywgdGhpcy5vcHRpb25zLnRvcCk7XG4gICAgICAgICAgICAgICAgaWYodGhpcy5vcHRpb25zLnRvcCA9PT0gMCl7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlclRvcFJpZ2h0UmFkaXVzOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyVG9wTGVmdFJhZGl1czogMFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmKGZpcnN0ID09PSBmYWxzZSl7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpblRvcDogJycsXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXJSYWRpdXM6IHRoaXMub3B0aW9ucy5yYWRpdXNcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5ib3R0b20gIT09IG51bGwgJiYgdGhpcy5vcHRpb25zLmJvdHRvbSAhPT0gZmFsc2Upe1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuY3NzKCdtYXJnaW4tYm90dG9tJywgdGhpcy5vcHRpb25zLmJvdHRvbSk7XG4gICAgICAgICAgICAgICAgaWYodGhpcy5vcHRpb25zLmJvdHRvbSA9PT0gMCl7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlckJvdHRvbVJpZ2h0UmFkaXVzOiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyQm90dG9tTGVmdFJhZGl1czogMFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmKGZpcnN0ID09PSBmYWxzZSl7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbkJvdHRvbTogJycsXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXJSYWRpdXM6IHRoaXMub3B0aW9ucy5yYWRpdXNcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgcmVjYWxjTGF5b3V0OiBmdW5jdGlvbigpe1xuXG4gICAgICAgICAgICB2YXIgdGhhdCA9IHRoaXMsXG4gICAgICAgICAgICAgICAgd2luZG93SGVpZ2h0ID0gJHdpbmRvdy5oZWlnaHQoKSxcbiAgICAgICAgICAgICAgICBtb2RhbEhlaWdodCA9IHRoaXMuJGVsZW1lbnQub3V0ZXJIZWlnaHQoKSxcbiAgICAgICAgICAgICAgICBtb2RhbFdpZHRoID0gdGhpcy4kZWxlbWVudC5vdXRlcldpZHRoKCksXG4gICAgICAgICAgICAgICAgY29udGVudEhlaWdodCA9IHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy1jb250ZW50JylbMF0uc2Nyb2xsSGVpZ2h0LFxuICAgICAgICAgICAgICAgIG91dGVySGVpZ2h0ID0gY29udGVudEhlaWdodCArIHRoaXMuaGVhZGVySGVpZ2h0LFxuICAgICAgICAgICAgICAgIHdyYXBwZXJIZWlnaHQgPSB0aGlzLiRlbGVtZW50LmlubmVySGVpZ2h0KCkgLSB0aGlzLmhlYWRlckhlaWdodCxcbiAgICAgICAgICAgICAgICBtb2RhbE1hcmdpbiA9IHBhcnNlSW50KC0oKHRoaXMuJGVsZW1lbnQuaW5uZXJIZWlnaHQoKSArIDEpIC8gMikpICsgJ3B4JyxcbiAgICAgICAgICAgICAgICBzY3JvbGxUb3AgPSB0aGlzLiR3cmFwLnNjcm9sbFRvcCgpLFxuICAgICAgICAgICAgICAgIGJvcmRlclNpemUgPSAwO1xuXG4gICAgICAgICAgICBpZihpc0lFKCkpe1xuICAgICAgICAgICAgICAgIGlmKCBtb2RhbFdpZHRoID49ICR3aW5kb3cud2lkdGgoKSB8fCB0aGlzLmlzRnVsbHNjcmVlbiA9PT0gdHJ1ZSApe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmNzcyh7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiAnMCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW5MZWZ0OiAnJ1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmNzcyh7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiAnNTAlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbkxlZnQ6IC0obW9kYWxXaWR0aC8yKVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmKHRoaXMub3B0aW9ucy5ib3JkZXJCb3R0b20gPT09IHRydWUgJiYgdGhpcy5vcHRpb25zLnRpdGxlICE9PSBcIlwiKXtcbiAgICAgICAgICAgICAgICBib3JkZXJTaXplID0gMztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYodGhpcy4kZWxlbWVudC5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWhlYWRlcicpLmxlbmd0aCAmJiB0aGlzLiRlbGVtZW50LmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctaGVhZGVyJykuaXMoJzp2aXNpYmxlJykgKXtcbiAgICAgICAgICAgICAgICB0aGlzLmhlYWRlckhlaWdodCA9IHBhcnNlSW50KHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy1oZWFkZXInKS5pbm5lckhlaWdodCgpKTtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmNzcygnb3ZlcmZsb3cnLCAnaGlkZGVuJyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuaGVhZGVySGVpZ2h0ID0gMDtcbiAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmNzcygnb3ZlcmZsb3cnLCAnJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmKHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy1sb2FkZXInKS5sZW5ndGgpe1xuICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy1sb2FkZXInKS5jc3MoJ3RvcCcsIHRoaXMuaGVhZGVySGVpZ2h0KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYobW9kYWxIZWlnaHQgIT09IHRoaXMubW9kYWxIZWlnaHQpe1xuICAgICAgICAgICAgICAgIHRoaXMubW9kYWxIZWlnaHQgPSBtb2RhbEhlaWdodDtcblxuICAgICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMub25SZXNpemUgJiYgdHlwZW9mKHRoaXMub3B0aW9ucy5vblJlc2l6ZSkgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMub25SZXNpemUodGhpcyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZih0aGlzLnN0YXRlID09IFNUQVRFUy5PUEVORUQgfHwgdGhpcy5zdGF0ZSA9PSBTVEFURVMuT1BFTklORyl7XG5cbiAgICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmlmcmFtZSA9PT0gdHJ1ZSkge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIElmIHRoZSBoZWlnaHQgb2YgdGhlIHdpbmRvdyBpcyBzbWFsbGVyIHRoYW4gdGhlIG1vZGFsIHdpdGggaWZyYW1lXG4gICAgICAgICAgICAgICAgICAgIGlmKHdpbmRvd0hlaWdodCA8ICh0aGlzLm9wdGlvbnMuaWZyYW1lSGVpZ2h0ICsgdGhpcy5oZWFkZXJIZWlnaHQrYm9yZGVyU2l6ZSkgfHwgdGhpcy5pc0Z1bGxzY3JlZW4gPT09IHRydWUpe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWlmcmFtZScpLmNzcyggJ2hlaWdodCcsIHdpbmRvd0hlaWdodCAtICh0aGlzLmhlYWRlckhlaWdodCtib3JkZXJTaXplKSk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctaWZyYW1lJykuY3NzKCAnaGVpZ2h0JywgdGhpcy5vcHRpb25zLmlmcmFtZUhlaWdodCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZihtb2RhbEhlaWdodCA9PSB3aW5kb3dIZWlnaHQpe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmFkZENsYXNzKCdpc0F0dGFjaGVkJyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5yZW1vdmVDbGFzcygnaXNBdHRhY2hlZCcpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmKHRoaXMuaXNGdWxsc2NyZWVuID09PSBmYWxzZSAmJiB0aGlzLiRlbGVtZW50LndpZHRoKCkgPj0gJHdpbmRvdy53aWR0aCgpICl7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuZmluZCgnLicrUExVR0lOX05BTUUrJy1idXR0b24tZnVsbHNjcmVlbicpLmhpZGUoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLiRlbGVtZW50LmZpbmQoJy4nK1BMVUdJTl9OQU1FKyctYnV0dG9uLWZ1bGxzY3JlZW4nKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMucmVjYWxjQnV0dG9ucygpO1xuXG4gICAgICAgICAgICAgICAgaWYodGhpcy5pc0Z1bGxzY3JlZW4gPT09IGZhbHNlKXtcbiAgICAgICAgICAgICAgICAgICAgd2luZG93SGVpZ2h0ID0gd2luZG93SGVpZ2h0IC0gKGNsZWFyVmFsdWUodGhpcy5vcHRpb25zLnRvcCkgfHwgMCkgLSAoY2xlYXJWYWx1ZSh0aGlzLm9wdGlvbnMuYm90dG9tKSB8fCAwKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLy8gSWYgdGhlIG1vZGFsIGlzIGxhcmdlciB0aGFuIHRoZSBoZWlnaHQgb2YgdGhlIHdpbmRvdy4uXG4gICAgICAgICAgICAgICAgaWYgKG91dGVySGVpZ2h0ID4gd2luZG93SGVpZ2h0KSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKHRoaXMub3B0aW9ucy50b3AgPiAwICYmIHRoaXMub3B0aW9ucy5ib3R0b20gPT09IG51bGwgJiYgY29udGVudEhlaWdodCA8ICR3aW5kb3cuaGVpZ2h0KCkpe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5hZGRDbGFzcygnaXNBdHRhY2hlZEJvdHRvbScpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmKHRoaXMub3B0aW9ucy5ib3R0b20gPiAwICYmIHRoaXMub3B0aW9ucy50b3AgPT09IG51bGwgJiYgY29udGVudEhlaWdodCA8ICR3aW5kb3cuaGVpZ2h0KCkpe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5hZGRDbGFzcygnaXNBdHRhY2hlZFRvcCcpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICQoJ2h0bWwnKS5hZGRDbGFzcyhQTFVHSU5fTkFNRSsnLWlzQXR0YWNoZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5jc3MoICdoZWlnaHQnLCB3aW5kb3dIZWlnaHQgKTtcblxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJGVsZW1lbnQuY3NzKCdoZWlnaHQnLCBjb250ZW50SGVpZ2h0ICsgKHRoaXMuaGVhZGVySGVpZ2h0K2JvcmRlclNpemUpKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy4kZWxlbWVudC5yZW1vdmVDbGFzcygnaXNBdHRhY2hlZFRvcCBpc0F0dGFjaGVkQm90dG9tJyk7XG4gICAgICAgICAgICAgICAgICAgICQoJ2h0bWwnKS5yZW1vdmVDbGFzcyhQTFVHSU5fTkFNRSsnLWlzQXR0YWNoZWQnKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAoZnVuY3Rpb24gYXBwbHlTY3JvbGwoKXtcbiAgICAgICAgICAgICAgICAgICAgaWYoY29udGVudEhlaWdodCA+IHdyYXBwZXJIZWlnaHQgJiYgb3V0ZXJIZWlnaHQgPiB3aW5kb3dIZWlnaHQpe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kZWxlbWVudC5hZGRDbGFzcygnaGFzU2Nyb2xsJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiR3cmFwLmNzcygnaGVpZ2h0JywgbW9kYWxIZWlnaHQgLSAodGhhdC5oZWFkZXJIZWlnaHQrYm9yZGVyU2l6ZSkpO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kZWxlbWVudC5yZW1vdmVDbGFzcygnaGFzU2Nyb2xsJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiR3cmFwLmNzcygnaGVpZ2h0JywgJ2F1dG8nKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pKCk7XG5cbiAgICAgICAgICAgICAgICAoZnVuY3Rpb24gYXBwbHlTaGFkb3coKXtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHdyYXBwZXJIZWlnaHQgKyBzY3JvbGxUb3AgPCAoY29udGVudEhlaWdodCAtIDMwKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhhdC4kZWxlbWVudC5hZGRDbGFzcygnaGFzU2hhZG93Jyk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGF0LiRlbGVtZW50LnJlbW92ZUNsYXNzKCdoYXNTaGFkb3cnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pKCk7XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICByZWNhbGNCdXR0b25zOiBmdW5jdGlvbigpe1xuICAgICAgICAgICAgdmFyIHdpZHRoQnV0dG9ucyA9IHRoaXMuJGhlYWRlci5maW5kKCcuJytQTFVHSU5fTkFNRSsnLWhlYWRlci1idXR0b25zJykuaW5uZXJXaWR0aCgpKzEwO1xuICAgICAgICAgICAgaWYodGhpcy5vcHRpb25zLnJ0bCA9PT0gdHJ1ZSl7XG4gICAgICAgICAgICAgICAgdGhpcy4kaGVhZGVyLmNzcygncGFkZGluZy1sZWZ0Jywgd2lkdGhCdXR0b25zKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy4kaGVhZGVyLmNzcygncGFkZGluZy1yaWdodCcsIHdpZHRoQnV0dG9ucyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgIH07XG5cblxuICAgICR3aW5kb3cub2ZmKCdsb2FkLicrUExVR0lOX05BTUUpLm9uKCdsb2FkLicrUExVR0lOX05BTUUsIGZ1bmN0aW9uKGUpIHtcblxuICAgICAgICB2YXIgbW9kYWxIYXNoID0gZG9jdW1lbnQubG9jYXRpb24uaGFzaDtcblxuICAgICAgICBpZih3aW5kb3cuJGl6aU1vZGFsLmF1dG9PcGVuID09PSAwICYmICEkKCcuJytQTFVHSU5fTkFNRSkuaXMoXCI6dmlzaWJsZVwiKSl7XG5cbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgdmFyIGRhdGEgPSAkKG1vZGFsSGFzaCkuZGF0YSgpO1xuICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBkYXRhICE9PSAndW5kZWZpbmVkJyl7XG4gICAgICAgICAgICAgICAgICAgIGlmKGRhdGEuaXppTW9kYWwub3B0aW9ucy5hdXRvT3BlbiAhPT0gZmFsc2Upe1xuICAgICAgICAgICAgICAgICAgICAgICAgJChtb2RhbEhhc2gpLml6aU1vZGFsKFwib3BlblwiKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gY2F0Y2goZXhjKSB7IC8qIGNvbnNvbGUud2FybihleGMpOyAqLyB9XG4gICAgICAgIH1cblxuICAgIH0pO1xuXG4gICAgJHdpbmRvdy5vZmYoJ2hhc2hjaGFuZ2UuJytQTFVHSU5fTkFNRSkub24oJ2hhc2hjaGFuZ2UuJytQTFVHSU5fTkFNRSwgZnVuY3Rpb24oZSkge1xuXG4gICAgICAgIHZhciBtb2RhbEhhc2ggPSBkb2N1bWVudC5sb2NhdGlvbi5oYXNoO1xuICAgICAgICB2YXIgZGF0YSA9ICQobW9kYWxIYXNoKS5kYXRhKCk7XG5cbiAgICAgICAgaWYobW9kYWxIYXNoICE9PSBcIlwiKXtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgaWYodHlwZW9mIGRhdGEgIT09ICd1bmRlZmluZWQnICYmICQobW9kYWxIYXNoKS5pemlNb2RhbCgnZ2V0U3RhdGUnKSAhPT0gJ29wZW5pbmcnKXtcblxuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKG1vZGFsSGFzaCkuaXppTW9kYWwoXCJvcGVuXCIpO1xuICAgICAgICAgICAgICAgICAgICB9LDIwMCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBjYXRjaChleGMpIHsgLyogY29uc29sZS53YXJuKGV4Yyk7ICovIH1cblxuICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICBpZih3aW5kb3cuJGl6aU1vZGFsLmhpc3Rvcnkpe1xuICAgICAgICAgICAgICAgICQuZWFjaCggJCgnLicrUExVR0lOX05BTUUpICwgZnVuY3Rpb24oaW5kZXgsIG1vZGFsKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKCAkKG1vZGFsKS5kYXRhKCkuaXppTW9kYWwgIT09IHVuZGVmaW5lZCApe1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHN0YXRlID0gJChtb2RhbCkuaXppTW9kYWwoJ2dldFN0YXRlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZihzdGF0ZSA9PSAnb3BlbmVkJyB8fCBzdGF0ZSA9PSAnb3BlbmluZycpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQobW9kYWwpLml6aU1vZGFsKCdjbG9zZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuXG4gICAgfSk7XG5cbiAgICAkZG9jdW1lbnQub2ZmKCdjbGljaycsICdbZGF0YS0nK1BMVUdJTl9OQU1FKyctb3Blbl0nKS5vbignY2xpY2snLCAnW2RhdGEtJytQTFVHSU5fTkFNRSsnLW9wZW5dJywgZnVuY3Rpb24oZSkge1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgdmFyIG1vZGFsID0gJCgnLicrUExVR0lOX05BTUUrJzp2aXNpYmxlJyk7XG4gICAgICAgIHZhciBvcGVuTW9kYWwgPSAkKGUuY3VycmVudFRhcmdldCkuYXR0cignZGF0YS0nK1BMVUdJTl9OQU1FKyctb3BlbicpO1xuICAgICAgICB2YXIgdHJhbnNpdGlvbkluID0gJChlLmN1cnJlbnRUYXJnZXQpLmF0dHIoJ2RhdGEtJytQTFVHSU5fTkFNRSsnLXRyYW5zaXRpb25JbicpO1xuICAgICAgICB2YXIgdHJhbnNpdGlvbk91dCA9ICQoZS5jdXJyZW50VGFyZ2V0KS5hdHRyKCdkYXRhLScrUExVR0lOX05BTUUrJy10cmFuc2l0aW9uT3V0Jyk7XG5cbiAgICAgICAgaWYodHJhbnNpdGlvbk91dCAhPT0gdW5kZWZpbmVkKXtcbiAgICAgICAgICAgIG1vZGFsLml6aU1vZGFsKCdjbG9zZScsIHtcbiAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiB0cmFuc2l0aW9uT3V0XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIG1vZGFsLml6aU1vZGFsKCdjbG9zZScpO1xuICAgICAgICB9XG5cbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICAgICAgaWYodHJhbnNpdGlvbkluICE9PSB1bmRlZmluZWQpe1xuICAgICAgICAgICAgICAgICQob3Blbk1vZGFsKS5pemlNb2RhbCgnb3BlbicsIHtcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogdHJhbnNpdGlvbkluXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICQob3Blbk1vZGFsKS5pemlNb2RhbCgnb3BlbicpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCAyMDApO1xuICAgIH0pO1xuXG4gICAgJGRvY3VtZW50Lm9mZigna2V5dXAuJytQTFVHSU5fTkFNRSkub24oJ2tleXVwLicrUExVR0lOX05BTUUsIGZ1bmN0aW9uKGV2ZW50KSB7XG5cbiAgICAgICAgaWYoICQoJy4nK1BMVUdJTl9OQU1FKyc6dmlzaWJsZScpLmxlbmd0aCApe1xuICAgICAgICAgICAgdmFyIG1vZGFsID0gJCgnLicrUExVR0lOX05BTUUrJzp2aXNpYmxlJylbMF0uaWQsXG4gICAgICAgICAgICAgICAgZ3JvdXAgPSAkKFwiI1wiK21vZGFsKS5pemlNb2RhbCgnZ2V0R3JvdXAnKSxcbiAgICAgICAgICAgICAgICBlID0gZXZlbnQgfHwgd2luZG93LmV2ZW50LFxuICAgICAgICAgICAgICAgIHRhcmdldCA9IGUudGFyZ2V0IHx8IGUuc3JjRWxlbWVudCxcbiAgICAgICAgICAgICAgICBtb2RhbHMgPSB7fTtcblxuICAgICAgICAgICAgaWYobW9kYWwgIT09IHVuZGVmaW5lZCAmJiBncm91cC5uYW1lICE9PSB1bmRlZmluZWQgJiYgIWUuY3RybEtleSAmJiAhZS5tZXRhS2V5ICYmICFlLmFsdEtleSAmJiB0YXJnZXQudGFnTmFtZS50b1VwcGVyQ2FzZSgpICE9PSAnSU5QVVQnICYmIHRhcmdldC50YWdOYW1lLnRvVXBwZXJDYXNlKCkgIT0gJ1RFWFRBUkVBJyl7IC8vJiYgJChlLnRhcmdldCkuaXMoJ2JvZHknKVxuXG4gICAgICAgICAgICAgICAgaWYoZS5rZXlDb2RlID09PSAzNykgeyAvLyBsZWZ0XG5cbiAgICAgICAgICAgICAgICAgICAgJChcIiNcIittb2RhbCkuaXppTW9kYWwoJ3ByZXYnLCBlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSBpZihlLmtleUNvZGUgPT09IDM5ICkgeyAvLyByaWdodFxuXG4gICAgICAgICAgICAgICAgICAgICQoXCIjXCIrbW9kYWwpLml6aU1vZGFsKCduZXh0JywgZSk7XG5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgICQuZm5bUExVR0lOX05BTUVdID0gZnVuY3Rpb24ob3B0aW9uLCBhcmdzKSB7XG5cblxuICAgICAgICBpZiggISQodGhpcykubGVuZ3RoICYmIHR5cGVvZiBvcHRpb24gPT0gXCJvYmplY3RcIil7XG5cbiAgICAgICAgICAgIHZhciBuZXdFTCA9IHtcbiAgICAgICAgICAgICAgICAkZWw6IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIiksXG4gICAgICAgICAgICAgICAgaWQ6IHRoaXMuc2VsZWN0b3Iuc3BsaXQoJyMnKSxcbiAgICAgICAgICAgICAgICBjbGFzczogdGhpcy5zZWxlY3Rvci5zcGxpdCgnLicpXG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBpZihuZXdFTC5pZC5sZW5ndGggPiAxKXtcbiAgICAgICAgICAgICAgICB0cnl7XG4gICAgICAgICAgICAgICAgICAgIG5ld0VMLiRlbCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoaWRbMF0pO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2goZXhjKXsgfVxuXG4gICAgICAgICAgICAgICAgbmV3RUwuJGVsLmlkID0gdGhpcy5zZWxlY3Rvci5zcGxpdCgnIycpWzFdLnRyaW0oKTtcblxuICAgICAgICAgICAgfSBlbHNlIGlmKG5ld0VMLmNsYXNzLmxlbmd0aCA+IDEpe1xuICAgICAgICAgICAgICAgIHRyeXtcbiAgICAgICAgICAgICAgICAgICAgbmV3RUwuJGVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChuZXdFTC5jbGFzc1swXSk7XG4gICAgICAgICAgICAgICAgfSBjYXRjaChleGMpeyB9XG5cbiAgICAgICAgICAgICAgICBmb3IgKHZhciB4PTE7IHg8bmV3RUwuY2xhc3MubGVuZ3RoOyB4KyspIHtcbiAgICAgICAgICAgICAgICAgICAgbmV3RUwuJGVsLmNsYXNzTGlzdC5hZGQobmV3RUwuY2xhc3NbeF0udHJpbSgpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKG5ld0VMLiRlbCk7XG5cbiAgICAgICAgICAgIHRoaXMucHVzaCgkKHRoaXMuc2VsZWN0b3IpKTtcbiAgICAgICAgfVxuICAgICAgICB2YXIgb2JqcyA9IHRoaXM7XG5cbiAgICAgICAgZm9yICh2YXIgaT0wOyBpPG9ianMubGVuZ3RoOyBpKyspIHtcblxuICAgICAgICAgICAgdmFyICR0aGlzID0gJChvYmpzW2ldKTtcbiAgICAgICAgICAgIHZhciBkYXRhID0gJHRoaXMuZGF0YShQTFVHSU5fTkFNRSk7XG4gICAgICAgICAgICB2YXIgb3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCAkLmZuW1BMVUdJTl9OQU1FXS5kZWZhdWx0cywgJHRoaXMuZGF0YSgpLCB0eXBlb2Ygb3B0aW9uID09ICdvYmplY3QnICYmIG9wdGlvbik7XG5cbiAgICAgICAgICAgIGlmICghZGF0YSAmJiAoIW9wdGlvbiB8fCB0eXBlb2Ygb3B0aW9uID09ICdvYmplY3QnKSl7XG5cbiAgICAgICAgICAgICAgICAkdGhpcy5kYXRhKFBMVUdJTl9OQU1FLCAoZGF0YSA9IG5ldyBpemlNb2RhbCgkdGhpcywgb3B0aW9ucykpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBvcHRpb24gPT0gJ3N0cmluZycgJiYgdHlwZW9mIGRhdGEgIT0gJ3VuZGVmaW5lZCcpe1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRhdGFbb3B0aW9uXS5hcHBseShkYXRhLCBbXS5jb25jYXQoYXJncykpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKG9wdGlvbnMuYXV0b09wZW4peyAvLyBBdXRvbWF0aWNhbGx5IG9wZW4gdGhlIG1vZGFsIGlmIGF1dG9PcGVuIHNldHRlZCB0cnVlIG9yIG1zXG5cbiAgICAgICAgICAgICAgICBpZiggIWlzTmFOKHBhcnNlSW50KG9wdGlvbnMuYXV0b09wZW4pKSApe1xuXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEub3BlbigpO1xuICAgICAgICAgICAgICAgICAgICB9LCBvcHRpb25zLmF1dG9PcGVuKTtcblxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZihvcHRpb25zLmF1dG9PcGVuID09PSB0cnVlICkge1xuXG4gICAgICAgICAgICAgICAgICAgIGRhdGEub3BlbigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB3aW5kb3cuJGl6aU1vZGFsLmF1dG9PcGVuKys7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuXG4gICAgJC5mbltQTFVHSU5fTkFNRV0uZGVmYXVsdHMgPSB7XG4gICAgICAgIHRpdGxlOiAnJyxcbiAgICAgICAgc3VidGl0bGU6ICcnLFxuICAgICAgICBoZWFkZXJDb2xvcjogJyM4OEEwQjknLFxuICAgICAgICBiYWNrZ3JvdW5kOiBudWxsLFxuICAgICAgICB0aGVtZTogJycsICAvLyBsaWdodFxuICAgICAgICBpY29uOiBudWxsLFxuICAgICAgICBpY29uVGV4dDogbnVsbCxcbiAgICAgICAgaWNvbkNvbG9yOiAnJyxcbiAgICAgICAgcnRsOiBmYWxzZSxcbiAgICAgICAgd2lkdGg6IDYwMCxcbiAgICAgICAgdG9wOiBudWxsLFxuICAgICAgICBib3R0b206IG51bGwsXG4gICAgICAgIGJvcmRlckJvdHRvbTogdHJ1ZSxcbiAgICAgICAgcGFkZGluZzogMCxcbiAgICAgICAgcmFkaXVzOiAzLFxuICAgICAgICB6aW5kZXg6IDk5OSxcbiAgICAgICAgaWZyYW1lOiBmYWxzZSxcbiAgICAgICAgaWZyYW1lSGVpZ2h0OiA0MDAsXG4gICAgICAgIGlmcmFtZVVSTDogbnVsbCxcbiAgICAgICAgZm9jdXNJbnB1dDogdHJ1ZSxcbiAgICAgICAgZ3JvdXA6ICcnLFxuICAgICAgICBsb29wOiBmYWxzZSxcbiAgICAgICAgbmF2aWdhdGVDYXB0aW9uOiB0cnVlLFxuICAgICAgICBuYXZpZ2F0ZUFycm93czogdHJ1ZSwgLy8gQm9vbGVhbiwgJ2Nsb3NlVG9Nb2RhbCcsICdjbG9zZVNjcmVlbkVkZ2UnXG4gICAgICAgIGhpc3Rvcnk6IGZhbHNlLFxuICAgICAgICByZXN0b3JlRGVmYXVsdENvbnRlbnQ6IGZhbHNlLFxuICAgICAgICBhdXRvT3BlbjogMCwgLy8gQm9vbGVhbiwgTnVtYmVyXG4gICAgICAgIGJvZHlPdmVyZmxvdzogZmFsc2UsXG4gICAgICAgIGZ1bGxzY3JlZW46IGZhbHNlLFxuICAgICAgICBvcGVuRnVsbHNjcmVlbjogZmFsc2UsXG4gICAgICAgIGNsb3NlT25Fc2NhcGU6IHRydWUsXG4gICAgICAgIGNsb3NlQnV0dG9uOiB0cnVlLFxuICAgICAgICBhcHBlbmRUbzogJ2JvZHknLCAvLyBvciBmYWxzZVxuICAgICAgICBhcHBlbmRUb092ZXJsYXk6ICdib2R5JywgLy8gb3IgZmFsc2VcbiAgICAgICAgb3ZlcmxheTogdHJ1ZSxcbiAgICAgICAgb3ZlcmxheUNsb3NlOiB0cnVlLFxuICAgICAgICBvdmVybGF5Q29sb3I6ICdyZ2JhKDAsIDAsIDAsIDAuNCknLFxuICAgICAgICB0aW1lb3V0OiBmYWxzZSxcbiAgICAgICAgdGltZW91dFByb2dyZXNzYmFyOiBmYWxzZSxcbiAgICAgICAgcGF1c2VPbkhvdmVyOiBmYWxzZSxcbiAgICAgICAgdGltZW91dFByb2dyZXNzYmFyQ29sb3I6ICdyZ2JhKDI1NSwyNTUsMjU1LDAuNSknLFxuICAgICAgICB0cmFuc2l0aW9uSW46ICdjb21pbmdJbicsICAgLy8gY29taW5nSW4sIGJvdW5jZUluRG93biwgYm91bmNlSW5VcCwgZmFkZUluRG93biwgZmFkZUluVXAsIGZhZGVJbkxlZnQsIGZhZGVJblJpZ2h0LCBmbGlwSW5YXG4gICAgICAgIHRyYW5zaXRpb25PdXQ6ICdjb21pbmdPdXQnLCAvLyBjb21pbmdPdXQsIGJvdW5jZU91dERvd24sIGJvdW5jZU91dFVwLCBmYWRlT3V0RG93biwgZmFkZU91dFVwLCAsIGZhZGVPdXRMZWZ0LCBmYWRlT3V0UmlnaHQsIGZsaXBPdXRYXG4gICAgICAgIHRyYW5zaXRpb25Jbk92ZXJsYXk6ICdmYWRlSW4nLFxuICAgICAgICB0cmFuc2l0aW9uT3V0T3ZlcmxheTogJ2ZhZGVPdXQnLFxuICAgICAgICBvbkZ1bGxzY3JlZW46IGZ1bmN0aW9uKCl7fSxcbiAgICAgICAgb25SZXNpemU6IGZ1bmN0aW9uKCl7fSxcbiAgICAgICAgb25PcGVuaW5nOiBmdW5jdGlvbigpe30sXG4gICAgICAgIG9uT3BlbmVkOiBmdW5jdGlvbigpe30sXG4gICAgICAgIG9uQ2xvc2luZzogZnVuY3Rpb24oKXt9LFxuICAgICAgICBvbkNsb3NlZDogZnVuY3Rpb24oKXt9LFxuICAgICAgICBhZnRlclJlbmRlcjogZnVuY3Rpb24oKXt9XG4gICAgfTtcblxuICAgICQuZm5bUExVR0lOX05BTUVdLkNvbnN0cnVjdG9yID0gaXppTW9kYWw7XG5cbiAgICByZXR1cm4gJC5mbi5pemlNb2RhbDtcblxufSkpOyJdLCJzb3VyY2VSb290IjoiIn0=