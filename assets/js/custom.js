$(document).ready(function($) {

	/* ----------------------------------------------------------- */
	/*  1. SEARCH FORM
	/* ----------------------------------------------------------- */

	jQuery('#mu-search-icon').on('click', function(event) {
		event.preventDefault();
		$('#mu-search').addClass('mu-search-open');
		$('#mu-search form input[type="search"]').focus();
	});

	jQuery('.mu-search-close').on('click', function(event) {
		$('#mu-search').removeClass('mu-search-open');
	});

	/* ----------------------------------------------------------- */
	/*  2. ABOUT US VIDEO
	/* ----------------------------------------------------------- */
	// WHEN CLICK PLAY BUTTON
	jQuery('#mu-abtus-video').on('click', function(event) {
		event.preventDefault();
		$('body').
				append(
						'<div id=\'about-video-popup\'><span id=\'mu-video-close\' class=\'fa fa-close\'></span><iframe id=\'mutube-video\' name=\'mutube-video\' frameborder=\'0\' allowfullscreen></iframe></div>');
		$('#mutube-video').attr('src', $(this).attr('href'));
	});
	// WHEN CLICK CLOSE BUTTON
	$(document).on('click', '#mu-video-close', function(event) {
		$(this).parent('div').fadeOut(1000);
	});
	// WHEN CLICK OVERLAY BACKGROUND
	$(document).on('click', '#about-video-popup', function(event) {
		$(this).remove();
	});

	/* ----------------------------------------------------------- */
	/*  3. TOP SLIDER (SLICK SLIDER)
	/* ----------------------------------------------------------- */

	jQuery('#mu-slider').slick({
		dots: false,
		infinite: true,
		arrows: true,
		speed: 500,
		autoplay: true,
		cssEase: 'linear',
		lazyLoad: 'ondemand',
	});

	/* ----------------------------------------------------------- */
	/*  4. ABOUT US (SLICK SLIDER)
	/* ----------------------------------------------------------- */

	jQuery('#mu-testimonial-slide').slick({
		dots: true,
		infinite: true,
		arrows: false,
		speed: 500,
		autoplay: true,
		cssEase: 'linear',
	});

	/* ----------------------------------------------------------- */
	/*  5. LATEST COURSE SLIDER (SLICK SLIDER)
	/* ----------------------------------------------------------- */

	jQuery('#mu-latest-course-slide').slick({
		dots: true,
		arrows: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 2,
		autoplay: true,
		autoplaySpeed: 2500,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true,
				},
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		],
	});

	/* ----------------------------------------------------------- */
	/*  6. TESTIMONIAL SLIDER (SLICK SLIDER)
	/* ----------------------------------------------------------- */

	jQuery('.mu-testimonial-slider').slick({
		dots: true,
		infinite: true,
		arrows: false,
		autoplay: true,
		speed: 500,
		cssEase: 'linear',
	});

	/* ----------------------------------------------------------- */
	/*  7. COUNTER
	/* ----------------------------------------------------------- */

	jQuery('.counter').counterUp({
		delay: 10,
		time: 1000,
	});

	/* ----------------------------------------------------------- */
	/*  8. RELATED ITEM SLIDER (SLICK SLIDER)
	/* ----------------------------------------------------------- */

	jQuery('#mu-related-item-slide').slick({
		dots: false,
		arrows: true,
		infinite: true,
		speed: 300,
		slidesToShow: 2,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2500,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: false,
				},
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		],
	});

	/* ----------------------------------------------------------- */
	/*  9. MIXIT FILTER (FOR GALLERY)
	/* ----------------------------------------------------------- */

	// jQuery(function() {
	// 	jQuery('#mixit-container').mixItUp();
	// });

	/* ----------------------------------------------------------- */
	/*  10. FANCYBOX (FOR PORTFOLIO POPUP VIEW)
	/* ----------------------------------------------------------- */

	jQuery(document).ready(function() {
		jQuery('.fancybox').fancybox();
	});

	/* ----------------------------------------------------------- */
	/*  11. HOVER DROPDOWN MENU
	/* ----------------------------------------------------------- */

	// for hover dropdown menu
	jQuery('ul.nav li.dropdown').hover(function() {
		jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
	}, function() {
		jQuery(this).
				find('.dropdown-menu').
				stop(true, true).
				delay(200).
				fadeOut(200);
	});

	/* ----------------------------------------------------------- */
	/*  12. SCROLL TOP BUTTON
	/* ----------------------------------------------------------- */

	//Check to see if the window is top if not then display button

	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > 300) {
			jQuery('.scrollToTop').fadeIn();
		}
		else {
			jQuery('.scrollToTop').fadeOut();
		}
	});

	//Click event to scroll to top

	jQuery('.scrollToTop').click(function() {
		jQuery('html, body').animate({scrollTop: 0}, 800);
		return false;
	});

	// $('.preloader').hide('fade', {}, 500);

	$('.js-courses-tab-link').on('click', function(e) {
		var $el = $(e.target).closest('.js-courses-tab-link');
		$el.siblings().removeClass('active');
		$el.addClass('active');
		$items = $('.js-classes-item');
		$items.hide();
		$items.filter('[data-section="' + $el.data('section') + '"]').show();
	});

	if (window.location.pathname == '/') {
		var sectionActive = $('.js-courses-tab-link').first().data('section');
		$items = $('.js-classes-item');
		$items.hide();
		$items.filter('[data-section="' + sectionActive + '"]').show();
	}

	$('.js-courses-tab-link-all').on('click', function(e) {
		var $el = $(e.target).closest('.js-courses-tab-link-all');
		$el.siblings().removeClass('active');
		$el.addClass('active');
		$('.js-classes-item').removeClass('minimal');
	});

	$('.js-section-link').on('click', function(e) {
		var $el = $(e.target);
		var $wrapEl = $el.closest('.js-section-link-wrapper');
		$('.preloader').show('fade', {}, 500);

		$wrapEl.siblings().removeClass('active');
		$wrapEl.addClass('active');

		$.ajax({
			url: '/local/ajax/courses.php',
			type: 'POST',
			data: {SECTION: $el.data('section')},
			success: function(data) {
				$('.js-content').html(data);
				var $elActive = $('.js-courses-tab-link.active');
				if ($elActive.length == 1) {
					var attr = '.js-classes-item[data-section-' +
							$elActive.data('section') + '="1"]';
					var attrNot = '.js-classes-item[data-section-' +
							$elActive.data('section') +
							'!="1"]';
					$(attrNot).addClass('minimal');
					$(attr).removeClass('minimal');
				}
				$('.preloader').hide('fade', {}, 500);
			},
		});
	});

	$('.js-documents-tab-link').on('click', function(e) {
		var height = $('.js-content').height();
		$('.js-content').height(height);
		console.log(height);
		var $el = $(e.target).closest('.js-documents-tab-link');
		$el.siblings().removeClass('active');
		$el.addClass('active');
		var attr = '.js-documents-item[data-section=' + $el.data('section') + ']';
		var attrNot = '.js-documents-item[data-section!=' + $el.data('section') +
				']';

		$(attrNot).addClass('minimal');
		$(attr).removeClass('minimal');

		function func() {
			$('.js-content').height('auto');
		}

		setTimeout(func, 500);
	});

	$('.modal').iziModal({
		width: 400,
	});

	const $form = $('.js-form');

	$.validator.addMethod('checkPhone', function(value, element) {
		return /\+\d{1} \(\d{3}\) \d{3} \d{2} \d{2}/g.test(value);
	});

	$.extend($.validator.messages, {
		checkPhone: 'Введите правильный номер телефона.',
		required: 'Это поле необходимо заполнить.',
		remote: 'Пожалуйста, введите правильное значение.',
		email: 'Пожалуйста, введите корректный email.',
		url: 'Пожалуйста, введите корректный URL.',
		date: 'Пожалуйста, введите корректную дату.',
		dateISO: 'Пожалуйста, введите корректную дату в формате ISO.',
		number: 'Пожалуйста, введите число.',
		digits: 'Пожалуйста, вводите только цифры.',
		creditcard: 'Пожалуйста, введите правильный номер кредитной карты.',
		equalTo: 'Пожалуйста, введите такое же значение ещё раз.',
		extension: 'Пожалуйста, выберите файл с расширением jpeg, pdf, doc, docx.',
		maxlength: $.validator.format(
				'Пожалуйста, введите не больше {0} символов.'),
		minlength: $.validator.format(
				'Пожалуйста, введите не меньше {0} символов.'),
		rangelength: $.validator.format(
				'Пожалуйста, введите значение длиной от {0} до {1} символов.'),
		range: $.validator.format('Пожалуйста, введите число от {0} до {1}.'),
		max: $.validator.format(
				'Пожалуйста, введите число, меньшее или равное {0}.'),
		min: $.validator.format(
				'Пожалуйста, введите число, большее или равное {0}.'),
		maxsize: 'Максимальный размер файла - 5мб',
	});

	$form.validate({
		errorPlacement: function(error, element) {
			return true;
		},
		success: function(element) {
			return true;
		},
		lang: 'ru',
		rules: {
			name: {
				required: true,
				minlength: 2,
			},
			email: {
				minlength: 2,
				email: true,
				required: true,
			},
			phone: {
				required: true,
				minlength: 17,
			},
			cost: {
				required: true,
			},
			date: {
				required: true,
				minlength: 10,
			},
			fn: {
				required: true,
				minlength: 16,
			},
			fp: {
				required: true,
				minlength: 10,
			},
			fd: {
				required: true,
				minlength: 5,
			},
			time: {
				required: true,
				minlength: 5,
			},
		},
		invalidHandler: function(form) {
			// const modal = $(".modal[data-modal='error']");
			// modal.iziModal('open');
		},
		submitHandler: function(form) {

			var formData = $form.serializeArray();

			formData = new FormData(form);

			$.ajax({
				url: form.action,
				type: 'post',
				data: formData,
				contentType: false,
				processData: false,
				dataType: 'text',
				cache: false,
				success: function(response) {
					$form.trigger('reset');
					$form.slideUp('slow', function() {
						$('.js-thanks').fadeIn();
					});
				},
			});
			$form.trigger('reset');

		},
	});

	$('.js-form-select').on('change', function(e) {
		$el = $(this).children(':selected');
		$('.js-form-radio').prop('disabled', true);
		if ($el.data('section-2')) {
			var s2 = $('.js-form-radio[data-section= "2"]');
			s2.prop('disabled', false);
			s2.prop('checked', true);
		}

		if ($el.data('section-1')) {
			var s1 = $('.js-form-radio[data-section= "1"]');
			s1.prop('disabled', false);
			s1.prop('checked', true);
		}
	});

	$('.js-form-select').select2({
		width: '100%',
	});

	$('[data-fancybox="gallery"]').fancybox({
		// Options will go here
	});

	$(document).ready(function() {
//sv_settings
		var selector = '#content, #content *, .navbar, .navbar *, .nav, .nav*, .container,  .container *, body, footer , footer *, section, section *, .inner, .modal, .modal *';
		//-------------
		$('.fs-outer button').click(function() {
			$(selector).css('font-size', $(this).data('size'));
			$.cookie('font-size', $(this).attr('id'));
			$('.fs-outer button').removeClass('active');
			$(this).addClass('active');

		});

		$('.cs-outer button').click(function() {
			$(selector).css('color', $(this).css('color'));
			$(selector).css('background', $(this).css('background'));
			$.cookie('cs', $(this).attr('id'));
			$('.cs-outer button').removeClass('active');
			$(this).addClass('active');

		});

		$('.img-outer button').click(function() {
			if ($.cookie('img') != 'on') {
				$('img').css('display', 'none');
				$.cookie('img', 'on');
				$('#img-onoff-text').text(' Включить');
				$(this).addClass('active');
			}
			else {
				$('img').css('display', 'block');
				$.cookie('img', 'off');
				$('#img-onoff-text').text(' Выключить');
				$(this).removeClass('active');
			}
		});

		if ($.cookie('sv_on') == 'true') {
			$('.js-vision').hide();
			$('#sv_on').addClass('active');
			$('#sv_settings').css('display', 'block');
			if ($.cookie('font-size') !== null) {
				$('#' + $.cookie('font-size')).click();
			}
			if ($.cookie('cs') !== null) {
				$('#' + $.cookie('cs')).click();
			}

		}

		$('#sv_on').click(
				function() {
					if ($.cookie('sv_on') != 'true') {
						$.cookie('sv_on', 'true');
						if ($.cookie('font-size') == 'null') {
							$('.fs-n').click();
						}
						if ($.cookie('cs') == 'null') {
							$('.cs-bw').click();
						}
					}
					else {
						$.cookie('sv_on', 'false');
					}
					location.reload();
				},
		);

	});

	$('.js-vision').on('click', function() {
		$.cookie('sv_on', 'true');
		$('.js-vision').fadeOut('slow', function() {
			$('.js-vision-block').fadeIn();
		});
	});

	$('.js-vision-off').on('click', function() {
		$.cookie('sv_on', 'false');
		location.reload();
	});

});

