import BaseComponent from '../../scripts/base-component';

import $ from 'jquery/dist/jquery';

export default class HeaderComponent extends BaseComponent {

    init() {
        const hamburgerButton = this.$element.find('.js-hamburger');
        const navigation = this.$element.find('.js-navigation');

        hamburgerButton.on('click', ()=> {
            navigation.toggleClass('header__part_navigation_active');
            hamburgerButton.toggleClass('header__hamburger_active');
        });

    }
}
