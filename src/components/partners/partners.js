import BaseComponent from '../../scripts/base-component';

import $ from 'jquery/dist/jquery';
import "slick-slider";

export default class PartnersComponent extends BaseComponent {

    init() {
        const slider = this.$element.find('.js-partner-slider');

        slider.slick({
            slidesToShow: 4,
            slidesToScroll: 2,
            prevArrow: '<a href="javascript:void(0);" class="partners__arrow partners__arrow_left circle-arrow"></a>',
            nextArrow: '<a href="javascript:void(0);" class="partners__arrow partners__arrow_right circle-arrow"></a>',
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            lazyLoad: 'ondemand',
            responsive: [
                {
                    breakpoint: 1190,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        arrows: false,
                    }
                }
            ]
        });


    }
}
