import BaseComponent from '../../scripts/base-component';

import $ from 'jquery/dist/jquery';
import "slick-carousel";

export default class MainSliderComponent extends BaseComponent {

    init() {

	    this.$element.slick({
		    slidesToShow: 1,
		    slidesToScroll: 1,
		    // prevArrow: "<a href=\"javascript:void(0);\" class='partners__prev arrow arrow_prev'></a>",
		    // nextArrow: "<a href=\"javascript:void(0);\" class='partners__next arrow arrow_next'></a>",
		    autoplay: true,
		    arrows: false,
		    autoplaySpeed: 2000,
		    infinite: true,
	    });


    }
}
