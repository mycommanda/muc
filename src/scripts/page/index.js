import {BasePage} from "../base-page";
import HeaderComponent from "../../components/header/header.js";
import MainSliderComponent from "../../components/main-slider/main-slider.js";
import PartnersComponent from "../../components/partners/partners.js";


import $ from "jquery/dist/jquery";
import "../vendor/izimodal";
import "select2/dist/js/select2.full.js";
import "../vendor/autosize.js";

class IndexPage extends BasePage {
    init() {
        this.$element.find('.js-header').each((i, el) => {
            new HeaderComponent(el);
        });

        this.$element.find('.js-mainslider').each((i, el) => {
            new MainSliderComponent(el);
        });

        this.$element.find('.js-partners').each((i, el) => {
            new PartnersComponent(el);
        });



    }
}

const page = new IndexPage();