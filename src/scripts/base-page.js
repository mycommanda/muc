import $ from 'jquery/dist/jquery';

class BasePage {
	constructor(element) {
		$(window).ready(($) => {
			this.$element = $('body');
			this.init();
		});
	}

	init() {
		//abstract
	}
}

export {BasePage};