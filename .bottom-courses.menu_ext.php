<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

$request = $_POST;
CModule::IncludeModule("iblock");

$arSelect = [
	"ID",
	"IBLOCK_ID",
	"IBLOCK_SECTION_ID",
	"NAME",
	"DESCRIPTION",
	"LIST_PAGE_URL",
	"UF_*",
];
$arFilter = ["IBLOCK_ID" => IntVal(IB_COURSES), "ACTIVE" => "Y", 'LEFT_MARGIN' => '1'];
$res = CIBlockSection::GetList(["SORT" => "ASC"], $arFilter, false, $arSelect);
$aMenuLinks = [];

while ($ob = $res->GetNextElement()) {
	$arFields = $ob->GetFields();
	$aMenuLink = [];
	$aMenuLink[0] = $arFields['NAME'];
	$aMenuLink[1] = '/courses/?section=' . $arFields['ID'];
	$aMenuLinks[] = $aMenuLink;
}
?>