<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Loader;
use \Letsrock\Lib\Property;

Loader::includeModule('letsrock.lib');


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><? $APPLICATION->ShowTitle() ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?= SITE_TEMPLATE_PATH ?>/favicon.ico"/>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|Open+Sans:300,400,600,700&amp;subset=cyrillic"
	      rel="stylesheet">
	<script type="text/javascript" src="/assets/js/jquery.min.js"></script>

	<?
	Asset::getInstance()->addCss("/assets/css/font-awesome.css");
	Asset::getInstance()->addCss("/assets/css/slick.css");
	Asset::getInstance()->addCss("/assets/css/styleold.css");
	Asset::getInstance()->addCss("/assets/css/style.css");
	Asset::getInstance()->addCss("/assets/css/animate.css");
	Asset::getInstance()->addCss("/assets/css/jqueryui.css");
	Asset::getInstance()->addCss("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css");
	Asset::getInstance()->addCss("https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css");

	Asset::getInstance()->addJs("/assets/js/vendors.bundle.js");
	Asset::getInstance()->addJs("/assets/js/app.bundle.js");
	Asset::getInstance()->addJs("https://code.jquery.com/ui/1.12.0/jquery-ui.min.js");
	Asset::getInstance()->addJs("/assets/js/iziModal.min.js");
	Asset::getInstance()->addJs("/assets/js/slick.js");
	Asset::getInstance()->addJs("/assets/js/bootstrap.js");
	Asset::getInstance()->addJs("/assets/js/waypoints.js");
	Asset::getInstance()->addJs("/assets/js/jquery.counterup.js");
	Asset::getInstance()->addJs("/assets/js/jquery.validate.js");
	Asset::getInstance()->addJs("/assets/js/additional-methods.min.js");
	Asset::getInstance()->addJs("/assets/js/jquery.cookie.js");
	Asset::getInstance()->addJs("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js");
	Asset::getInstance()->addJs("https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js");
	Asset::getInstance()->addJs("/assets/js/custom.js");


	?>
	<? $APPLICATION->ShowHead(); ?>
</head>
<body>
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<div class="wrapper wrapper_loading">

	<a class="scrollToTop" href="#">
		<i class="fa fa-angle-up"></i>
	</a>

	<header class="header js-header">
		<div class="header__accessible-block accessible-block">
			<div class="accessible-block__inner container js-vision-link-block">
				<a href="javascript:void(0);" class="accessible-block__link js-vision">Версия для слабовидящих</a>
				<div class="sv_settings text-center js-vision-block" id="sv_settings">
					    <span>Размер шрифта
					        <span class="fs-outer">
					            <button class="btn btn-default fs-n" data-size="16px" id="fs-n">А</button>
					            <button class="btn btn-default fs-m" data-size="18px" id="fs-m">А</button>
					            <button class="btn btn-default fs-l" data-size="20px" id="fs-l">А</button>
					        </span>
					    </span>

					<span class="mgl20">Цветовая схема
				        <span class="cs-outer">
				            <button class="btn btn-default cs-bw" id="cs-bw">А</button>
				            <button class="btn btn-default cs-wb" id="cs-wb">А</button>
				            <button class="btn btn-default cs-gb" id="cs-gb">А</button>
				        </span>
					</span>

					<!--					<span class="mgl20">Изображения-->
					<!--					        <span class="img-outer">-->
					<!--					            <button class="btn btn-default"-->
					<!--					                    id="img-onoff"><span class="glyphicon glyphicon-picture"></span><span id="img-onoff-text"> Отключить</span></button>-->
					<!--					        </span>-->
					<!--					</span>-->

					<a href="javascript:void(0);" class="btn btn-default js-vision-off">Обычный просмотр</a>
				</div>
			</div>
		</div>
		<div class="header__part_welcome">
			<div class="header__inner_welcome container">
				<a href="javascript:void(0);" class="header__hamburger js-hamburger">
					<div class="header__hamburger-inner">
						<span class="line"></span>
						<span class="line"></span>
						<span class="line"></span>
					</div>
				</a>
				<a href="/" class="header__logo"></a>
				<p class="header__logo-text">АНО ДПО «Межотраслевой Учебный Центр»</p>
				<a href="mailto:<?= Property::getText('email-v-shapke') ?>" class="header__link header__link_mail">Напишите
				                                                                                                   нам</a>
				<a href="tel:<?= Property::getText('telefon-v-shapke') ?>"
				   class="header__link header__link_call"><?= Property::getText('telefon-v-shapke') ?></a>
				<a href="javascript:void(0);"
				   class="header__request-btn button button_color_red"
				   data-izimodal-open=".modal[data-modal='request']">Подать заявку</a>
			</div>
		</div>
		<div class="header__part_navigation js-navigation">
			<div class="header__inner_welcome container">
				<? $APPLICATION->IncludeComponent(
					"bitrix:menu",
					"commonnew",
					[
						"ALLOW_MULTI_SELECT"    => "N",
						"CHILD_MENU_TYPE"       => "additional",
						"DELAY"                 => "N",
						"MAX_LEVEL"             => "3",
						"MENU_CACHE_GET_VARS"   => [
						],
						"MENU_CACHE_TIME"       => "3600",
						"MENU_CACHE_TYPE"       => "N",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"MENU_THEME"            => "site",
						"ROOT_MENU_TYPE"        => "top",
						"USE_EXT"               => "Y",
						"COMPONENT_TEMPLATE"    => "commonnew",
					],
					false
				); ?>
			</div>
		</div>
	</header>

