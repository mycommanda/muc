<!-- Start footer -->
<footer id="mu-footer">
	<!-- start footer top -->
	<div class="mu-footer-top">
		<div class="container">
			<div class="mu-footer-top-area">
				<div class="row">
					<div class="col-lg-4 col-md-12">
						<a class="footer-brand" href="/"><img src="/assets/img/logo.png" alt="logo">
							<p>АНО ДПО "Межотраслевой Учебный Центр"</p></a>
						<div class="mu-footer-widget" style="padding: 10px;">
							<? $APPLICATION->IncludeComponent(
								"bitrix:menu",
								"footer",
								[
									"ALLOW_MULTI_SELECT"    => "N",
									"CHILD_MENU_TYPE"       => "left",
									"DELAY"                 => "N",
									"MAX_LEVEL"             => "1",
									"MENU_CACHE_GET_VARS"   => [
									],
									"MENU_CACHE_TIME"       => "3600",
									"MENU_CACHE_TYPE"       => "Y",
									"MENU_CACHE_USE_GROUPS" => "N",
									"MENU_THEME"            => "site",
									"ROOT_MENU_TYPE"        => "additional-sites",
									"USE_EXT"               => "Y",
								],
								false
							); ?>
						</div>
					</div>
					<div class="col-lg-4 col-md-12">
						<div class="mu-footer-widget">
							<h4>Наши направления:</h4>
							<? $APPLICATION->IncludeComponent(
								"bitrix:menu",
								"footer",
								[
									"ALLOW_MULTI_SELECT"    => "N",
									"CHILD_MENU_TYPE"       => "left",
									"DELAY"                 => "N",
									"MAX_LEVEL"             => "1",
									"MENU_CACHE_GET_VARS"   => [
									],
									"MENU_CACHE_TIME"       => "3600",
									"MENU_CACHE_TYPE"       => "Y",
									"MENU_CACHE_USE_GROUPS" => "N",
									"MENU_THEME"            => "site",
									"ROOT_MENU_TYPE"        => "bottom",
									"USE_EXT"               => "Y",
									"COMPONENT_TEMPLATE"    => "footer",
								],
								false
							); ?>
						</div>
					</div>
					<div class="col-lg-3 col-md-12">
						<div class="mu-footer-widget contact">
							<h4>Контакты:</h4>

							<address>
								<a href="#" class="contact__item">
									<i class="fa fa-address-card" aria-hidden="true"></i>
									Россия, Краснодарский край, г.Краснодар, ул. Ставропольская, 8</a>
								<a class="contact__item" href="#"><i class="fa fa-phone"></i> +7 (861) 212-59-48</a>
								<a class="contact__item" href="#"><i class="fa fa-envelope"></i> 2990287.muc@mail.ru</a>
								<br>
								<p><strong>Мененеджеры по работе с клиентами:</strong></p>
								<a class="contact__item" href="#"><i class="fa fa-phone"></i> +7 (861) 212-59-48</a>
							</address>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end footer top -->
	<!-- start footer bottom -->
	<div class="mu-footer-bottom">
		<div class="container">
			<div class="mu-footer-bottom-area">
				<p>&copy; Все права защищены
				</p>
			</div>
		</div>
	</div>
	<!-- end footer bottom -->
</footer>
</div>

<div class="preloader">
	<div class="loader animated zoomIn">
		<div class="circle"></div>
		<div class="circle"></div>
		<div class="circle"></div>
	</div>
</div>

<div class="modal modal_type_success" data-modal="request">
	<div class="modal-header">
		<span class="modal__title">Заявка на обучение</span>
		<a href="javascript:void(0);" data-izimodal-close="" class="modal__exit close"></a>
	</div>
	<div class="modal__inner">
		<form action="/local/ajax/addRequest.php" class="js-form">
			<div class="form-group">
				<label for="name">Ваше имя:</label>
				<input type="text" class="form-control" id="name" name="name">
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" class="form-control" name="email" id="email">
			</div>
			<div class="form-group">
				<? $APPLICATION->IncludeComponent(
					"muc:coursesList",
					"",
					[]);
				?>
			</div>
			<div class="form-group">
				<div class="radio">
					<label><input type="radio" class="js-form-radio" name="type" data-section="1" checked>Повышение
					                                                                                      квалификации</label>
				</div>
				<div class="radio">
					<label><input type="radio" class="js-form-radio" name="type" data-section="2">Профессиональная
					                                                                              переподготовка</label>
				</div>
			</div>
			<button class="btn btn-lg btn-block btn-primary btn-default js-submit">Отправить</button>
		</form>
		<div class="js-thanks" style="display: none;">
			<h2>Спасибо</h2>
			<p>Ваша заявка принята</p>
		</div>
	</div>
</div>
</body>
</html>