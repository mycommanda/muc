﻿function JQuery(r) {

}

JQuery(document).ready(function() {
	$('.js-image_block-top').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.js-image_block-bottom',
	});



	$('.js-image_block-bottom').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.js-image_block-top',
		dots: true,
		centerMode: true,
		focusOnSelect: true,
	});
});