<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"contacts",
	[
		"AREA_FILE_SHOW"   => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE"    => "",
		"PATH"             => "/include/contacts/contacts.php",
	]
); ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>