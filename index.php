<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Межотраслевой Учебный Центр");
$APPLICATION->SetTitle("Межотраслевой Учебный Центр");
?><? $APPLICATION->IncludeComponent(
	"muc:main.slider",
	"new",
	[]
); ?>
<? $APPLICATION->IncludeComponent(
	"muc:main.underSlider",
	"",
	[]
); ?>
<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"main_page",
	[
		"AREA_FILE_SHOW"   => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE"    => "",
		"PATH"             => "/include/main/about.php",
	]
); ?>
<? $APPLICATION->IncludeComponent(
	"muc:main.advantages",
	"",
	[]
); ?>
<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"main_courses",
	[
		"AREA_FILE_SHOW"   => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE"    => "",
		"PATH"             => "/include/main/courses.php",
	]
); ?>
<? $APPLICATION->IncludeComponent(
	"muc:partners",
	"",
	[]
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>