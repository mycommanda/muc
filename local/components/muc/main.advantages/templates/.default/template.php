<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use \Letsrock\Lib\Property;

Loader::includeModule('letsrock.lib');
?>
<section id="mu-abtus-counter" style="background-image: url('<?= Property::getImage('izobrazhenie-v-bloke-preimushchestv')['src'] ?>')">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="mu-abtus-counter-area">
					<div class="row">
						<? foreach ($arResult as $item): ?>
						<!-- Start counter item -->
						<div class="col-lg-3 col-md-3 col-sm-3">
							<div class="mu-abtus-counter-single">
								<span class="fa <?=$item['PROPERTY_CLASS_VALUE']?>"></span>
								<h4 class="counter"><?=$item['PROPERTY_ADV_VALUE']?></h4>
								<p><?=$item['PROPERTY_DEST_VALUE']?></p>
							</div>
						</div>
						<!-- End counter item -->
						<? endforeach; ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>