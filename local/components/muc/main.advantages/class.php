<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class AdvantagesComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
		];
		return $result;
	}

	public function executeComponent()
	{
		if ($this->startResultCache()) {
			CModule::IncludeModule("iblock");
			$arSelect = [
				"ID",
				"NAME",
				"PROPERTY_CLASS",
				"PROPERTY_ADV",
				"PROPERTY_DEST",
			];
			$arFilter = ["IBLOCK_ID" => IntVal(IB_ADVANTAGES), "ACTIVE" => "Y"];
			$res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 3], $arSelect);
			$arFields = [];
			$i = 0;

			while ($i < 3 && $ob = $res->GetNextElement()) {
				$arFields[] = $ob->GetFields();
				$i++;
			}

			$this->arResult = $arFields;
			$this->includeComponentTemplate();
		}

		return $this->arResult;
	}
} ?>