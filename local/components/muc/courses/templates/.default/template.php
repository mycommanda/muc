<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

$str = 0;
?>
<div class="mu-course-content-area">
	<div class="row">
		<div class="col-md-3 col-xs-12">
			<aside class="mu-sidebar">
				<div class="mu-single-sidebar">
					<h3>Уровень образования</h3>
					<ul class="mu-sidebar-catg">
						<? foreach ($arResult['SECTIONS'] as $item): ?>
							<li class="section-link js-section-link-wrapper  <?= ($_GET['section'] == $item['ID']) ? 'active' : ''; ?>">
								<a href="javascript:void(0);" class="js-section-link"
								   data-section="<?= $item['ID'] ?>"><?= $item['NAME'] ?></a></li>
						<? endforeach; ?>
					</ul>
				</div>
			</aside>
		</div>
		<div class="col-md-9 col-xs-12">
			<div class="js-content courses__list">
				<? foreach ($arResult['ITEMS'] as $key => $item): ?>
					<div class="courses__item js-classes-item" <?= ($item['PROPERTY_FILES']) ? 'style="grid-row-start: ' . ($str + 1) . '; grid-row-end: ' . ($str + 3) . ';"' : ""; ?>>
						<div class="panel panel-default card">
							<? if ($str < count($arResult['ITEMS'])): ?>
								<? foreach ($item['PROPERTY_FILES'] as $index => $file): ?>
									<? if ($index == 0): ?>
										<a data-fancybox="gallery"
										   class="card-img-top card-image"
										   href="<?= $file['BIG']['src'] ?>">
											<img src="<?= $file['SMALL']['src'] ?>" alt="<?= $file['NAME'] ?>"></a>
									<? else: ?>
										<a data-fancybox="gallery"
										   href="<?= $file['BIG']['src'] ?>" style="display: none;"></a>
									<? endif; ?>
								<? endforeach; ?>
							<? endif; ?>
							<div class="card-body panel-body">
								<h4 class="card-title"><?= $item['NAME'] ?></h4>
								<p class="card-text"><?= $item['PREVIEW_TEXT'] ?></p>
							</div>
							<div class="card-footer panel-footer">
								<a href="/courses/item/?id=<?= $item['ID'] ?>" class="btn btn-primary">Подробнее</a>
							</div>
						</div>
					</div>
					<? $str = $str + 2; ?>
				<? endforeach; ?>
			</div>
		</div>
	</div>
</div>




