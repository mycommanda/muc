<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class CoursesComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
		];
		return $result;
	}

	public function executeComponent()
	{
		$request = $_GET;

		if ($this->startResultCache(false, $request['section'])) {
			CModule::IncludeModule("iblock");

			$arSelect = [
				"ID",
				"IBLOCK_ID",
				"IBLOCK_SECTION_ID",
				"NAME",
				"DESCRIPTION",
				"LIST_PAGE_URL",
				"UF_*",
			];
			$arFilter = ["IBLOCK_ID" => IntVal(IB_COURSES), "ACTIVE" => "Y", 'LEFT_MARGIN' => '1'];
			$res = CIBlockSection::GetList(["SORT" => "ASC"], $arFilter, false, $arSelect);
			$arFields = [];

			while ($ob = $res->GetNextElement()) {
				$arFields[] = $ob->GetFields();
			}

			$this->arResult['SECTIONS'] = $arFields;

			$request = $_GET;
			$filter = ["IBLOCK_ID" => IB_COURSES];

			if (!empty($request['section'])) {
				$filter['SECTION_ID'] = $request['section'];
			}

			$arFields = [];
			$res = CIBlockElement::GetList(
				["SORT" => "ASC"],
				$filter,
				false,
				[],
				[
					'ID',
					'NAME',
					'PROPERTY_IMAGE_CLASS',
					'PROPERTY_SECTION_1',
					'PROPERTY_SECTION_2',
					'PROPERTY_FILES',
					'PREVIEW_TEXT',
					'DETAIL_PAGE_URL',
				]
			);

			while ($ob = $res->GetNextElement()) {
				$arFields[] = $ob->GetFields();
			}

			foreach ($arFields as $key => $course) {
				foreach ($course["PROPERTY_FILES_VALUE"] as $index => $item) {
					$arFields[$key]['PROPERTY_FILES'][$index]['SMALL'] = CFile::ResizeImageGet($item,
						["width" => 400, "height" => 300], BX_RESIZE_IMAGE_PROPORTIONAL);
					$arFields[$key]['PROPERTY_FILES'][$index]['BIG'] = CFile::ResizeImageGet($item,
						["width" => 1000, "height" => 2000], BX_RESIZE_IMAGE_PROPORTIONAL);
				}
			}

			$this->arResult['ITEMS'] = $arFields;
			$this->includeComponentTemplate();
		}

		return $this->arResult;
	}
} ?>