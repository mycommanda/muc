<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<? if (!empty($arResult)): ?>
	<section id="mu-service">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="mu-service-area">
						<? foreach ($arResult as $item): ?>
							<!-- Start single service -->
							<div class="mu-service-single animated slideInUp">
								<span class="fa <?= $item['PROPERTY_CLASS_VALUE'] ?>"></span>
								<h3><?= $item['NAME'] ?></h3>
								<p><?= $item['PROPERTY_DESC_VALUE'] ?></p>
							</div>
							<!-- Start single service -->
						<? endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<? endif; ?>