<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class UnderSliderComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
		];
		return $result;
	}

	public function executeComponent()
	{
		if ($this->startResultCache()) {
			CModule::IncludeModule("iblock");
			$arSelect = [
				"ID",
				"NAME",
				"PREVIEW_TEXT",
				"PROPERTY_CLASS",
				"PROPERTY_DESC",
			];
			$arFilter = ["IBLOCK_ID" => IntVal(IB_BLOCKS_UNDER_SLIDER), "ACTIVE" => "Y"];
			$res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 3], $arSelect);
			$arFields = [];
			$i = 0;

			while ($i < 3 && $ob = $res->GetNextElement()) {
				$arFields[] = $ob->GetFields();
				$i++;
			}

			foreach ($arFields as $key => $row) {

				$arFields[$key]['PROPERTY_BG_VALUE'] = CFile::ResizeImageGet($row["PROPERTY_BG_VALUE"],
					["width" => 1920, "height" => 1080], BX_RESIZE_IMAGE_PROPORTIONAL);
			}

			$this->arResult = $arFields;
			$this->includeComponentTemplate();
		}

		return $this->arResult;
	}
} ?>