<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<section class="m-slider js-mainslider">
	<? foreach ($arResult as $item): ?>
	<div class="m-slider__slide" style="background-image: url('<?=$item['PROPERTY_BG_VALUE']['src']?>')">
		<div class="m-slider__inner container">
			<h3 class="m-slider__slide-title"><?=$item['NAME']?></h3>
			<p class="m-slider__slide-text"><?=$item['PREVIEW_TEXT']?></p>
		</div>
	</div>
	<? endforeach; ?>
</section>
