<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

$i = 0;
?>
<ul class="main-nav nav nav-pills js-courses-tabs">
	<? foreach ($arResult['TABS'] as $item): ?>
		<li role="presentation"
		    class="js-courses-tab-link <?= ($i == 0) ? "active" : "" ?>"
		    data-section="<?= $item['ID'] ?>">
			<a href="javascript:void(0)"><?= $item['NAME'] ?></a></li>
		<? $i++; endforeach; ?>
</ul>
<div class="mu-features-content">
	<div class="row">
		<? foreach ($arResult['ITEMS'] as $item): ?>
			<div class="col-lg-4 col-md-4 col-sm-6 js-classes-item main-card"
			     data-section="<?= $item['IBLOCK_SECTION_ID'] ?>">
				<div class="mu-single-feature">
					<span class="<?= $item['PROPERTY_UF_ICON_VALUE'] ?>"></span>
					<h4><?= $item['NAME'] ?></h4>
					<p><?= $item['PREVIEW_TEXT'] ?></p>
					<a href="/courses/item/?id=<?= $item['ID'] ?>">Подробнее</a>
				</div>
			</div>
		<? endforeach; ?>
	</div>
</div>


