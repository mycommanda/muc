<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<label for="sel1">Направление обучения:</label>
<select class="form-control" id="sel1" name="edu">
	<? foreach ($arResult['ITEMS'] as $item): ?>
	<option value="<?= $item['ID'] ?>"><?= $item['NAME'] ?></option>
	<? endforeach; ?>
</select>

