<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class MainCoursesComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
		];
		return $result;
	}

	public function executeComponent()
	{
		if ($this->startResultCache()) {
			CModule::IncludeModule("iblock");
			$arSelect = [
				"ID",
				"IBLOCK_ID",
				"IBLOCK_SECTION_ID",
				"NAME",
				"DESCRIPTION",
				"LIST_PAGE_URL",
				"UF_*",
			];
			$arFilter = ["IBLOCK_ID" => IntVal(IB_COURSES), "ACTIVE" => "Y", 'LEFT_MARGIN' => '1'];
			$res = CIBlockSection::GetList(["SORT" => "ASC"], $arFilter, false, $arSelect);
			$arTabs = [];
			$arFields = [];

			while ($ob = $res->GetNextElement()) {
				$arTabs[] = $ob->GetFields();
			}

			$arSelect = [
				"ID",
				"IBLOCK_ID",
				"IBLOCK_SECTION_ID",
				"NAME",
				"DESCRIPTION",
				"LIST_PAGE_URL",
				"PREVIEW_TEXT",
				"PROPERTY_UF_ICON",
			];
			$arFilter = [
				"IBLOCK_ID" => IntVal(IB_COURSES),
			];
			$res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, $arSelect);

			while ($ob = $res->GetNextElement()) {
				$arFields[] = $ob->GetFields();
			}

			$this->arResult['TABS'] = $arTabs;
			$this->arResult['ITEMS'] = $arFields;
			$this->includeComponentTemplate();
		}

		return $this->arResult;
	}
} ?>