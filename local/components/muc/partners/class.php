<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class PartnersSliderComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
		];
		return $result;
	}

	public function executeComponent()
	{
		if ($this->startResultCache()) {
			CModule::IncludeModule("iblock");
			$arSelect = [
				"ID",
				"NAME",
				"PREVIEW_PICTURE",
			];
			$arFilter = ["IBLOCK_ID" => IntVal(IB_PARTNERS), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"];
			$res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 50], $arSelect);
			$arFields = [];

			while ($ob = $res->GetNextElement()) {
				$arFields[] = $ob->GetFields();
			}

			foreach ($arFields as $key => $row) {

				$arFields[$key]['PREVIEW_PICTURE'] = CFile::ResizeImageGet($row["PREVIEW_PICTURE"],
					["width" => 300, "height" => 300], BX_RESIZE_IMAGE_PROPORTIONAL);
			}

			$this->arResult = $arFields;
			$this->includeComponentTemplate();
		}

		return $this->arResult;
	}
} ?>