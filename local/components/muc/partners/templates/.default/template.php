<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="partners js-partners">
	<div class="partners__inner container">
		<h2 class="partners__title section-title">Наши партнеры</h2>
		<div class="partners__slider-wrap">
			<div class="partners__slider js-partner-slider">
				<? foreach ($arResult as $item):?>
				<div class="partners__slide">
					<div class="partners__slide-img" style="background-image: url('<?=$item['PREVIEW_PICTURE']['src']?>');"></div>
				</div>
				<? endforeach; ?>
			</div>
		</div>
	</div>
</div>