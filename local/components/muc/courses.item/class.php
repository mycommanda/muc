<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class CoursesItemComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
		];
		return $result;
	}

	public function executeComponent()
	{
		$request = $_GET;

		if ($this->startResultCache(false, $request['id'])) {
			CModule::IncludeModule("iblock");
			$arSelect = [
				"ID",
				"NAME",
				"PREVIEW_TEXT",
				"DETAIL_TEXT",
				"PROPERTY_SECTION_1",
				"PROPERTY_SECTION_2",
				"PROPERTY_TIME",
				"PROPERTY_FORMAT",
				"PROPERTY_FILES",
				"PROPERTY_IMAGE_CLASS",
				"PROPERTY_TIME"
			];
			$arFilter = ["IBLOCK_ID" => IntVal(IB_COURSES), 'ID' => $request['id'], "ACTIVE" => "Y"];
			$res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 3], $arSelect);
			$arFields = $res->Fetch();

			foreach ($arFields["PROPERTY_FILES_VALUE"] as $index => $item) {
				$arFields['PROPERTY_FILES'][$index]['SMALL'] = CFile::ResizeImageGet($item,
					["width" => 400, "height" => 300], BX_RESIZE_IMAGE_PROPORTIONAL);
				$arFields['PROPERTY_FILES'][$index]['BIG'] = CFile::ResizeImageGet($item,
					["width" => 1000, "height" => 2000], BX_RESIZE_IMAGE_PROPORTIONAL);
			}

			$this->arResult = $arFields;
			$this->includeComponentTemplate();
		}

		return $this->arResult;
	}
} ?>