<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<div class="mu-course-content-area course-detail">
	<div class="row">
		<div class="col-md-9">
			<h3 class="course-detail__title">Информация о курсе</h3>
			<div class="mu-latest-course-single">
				<div class="mu-latest-course-single-content">
					<?= $arResult['DETAIL_TEXT'] ?>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<aside class="mu-sidebar">
				<div class="mu-single-sidebar">
					<h4>Уровень образования</h4>
					<p><?= (empty($arResult['SECTION_1'])) ? "Повышение квалификации" : "Профессиональная переподготовка"; ?></p>
					<h4>Форма обучения</h4>
					<p><?= $arResult['PROPERTY_FORMAT_VALUE'] ?></p>
					<h4>Продолжительность обучения</h4>
					<p><?= $arResult['PROPERTY_TIME_VALUE'] ?></p>
				</div>

				<div class="mu-single-sidebar">
					<h4>Выдаваемый документ</h4>
					<div class="mu-sidebar-popular-courses">
						<div>
							<? foreach ($arResult['PROPERTY_FILES'] as $index => $item): ?>
								<? if ($index == 0): ?>
									<a data-fancybox="gallery"
									   class="card-image card-image_side-bar"
									   href="<?= $item['BIG']['src'] ?>">
										<img src="<?= $item['SMALL']['src'] ?>"></a>
								<? else: ?>
									<a data-fancybox="gallery"
									   class="card-image card-image_side-bar"
									   href="<?= $item['BIG']['src'] ?>" style="display: none;"></a>
								<? endif; ?>
							<? endforeach; ?>
						</div>
					</div>
				</div>
			</aside>
		</div>
	</div>
</div>




