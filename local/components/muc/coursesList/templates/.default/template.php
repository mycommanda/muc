<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<label for="sel1">Направление обучения:</label>
<select class="form-control js-form-select" id="sel1" name="edu">
	<? foreach ($arResult['ITEMS'] as $item): ?>
		<option class="js-form-option"
				data-section-1="<?=($item['PROPERTY_SECTION_1_VALUE'])?"1":"0";?>"
				data-section-2="<?=($item['PROPERTY_SECTION_2_VALUE'])?"1":"0";?>"
				value="<?= $item['ID'] ?>"><?= $item['NAME'] ?></option>
	<? endforeach; ?>
</select>



