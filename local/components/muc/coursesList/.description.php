<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentDescription = array(
    "NAME" => 'Слайдер',
    "DESCRIPTION" => 'Карточки под слайдером на главной',
    "PATH" => array(
        "ID" => "dv_components",
        "CHILD" => array(
            "ID" => "slider",
            "NAME" => "Карточки под слайдером на главной"
        )
    ),
);
?>