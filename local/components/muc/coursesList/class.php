<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class CoursesListComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
		];
		return $result;
	}

	public function executeComponent()
	{
		if ($this->startResultCache()) {
			CModule::IncludeModule("iblock");

			$arSelect = [
				"ID",
				"IBLOCK_ID",
				"IBLOCK_SECTION_ID",
				"NAME",
				"DESCRIPTION",
				"LIST_PAGE_URL",
				"PROPERTY_SECTION_1",
				"PROPERTY_SECTION_2"
			];
			$arFilter = ["IBLOCK_ID" => IntVal(IB_COURSES), "ACTIVE" => "Y"];
			$res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, [], $arSelect);

			$arFields = [];

			while ($ob = $res->GetNextElement()) {
				$arFields[] = $ob->GetFields();
			}

			$this->arResult['ITEMS'] = $arFields;
			$this->includeComponentTemplate();
		}

		return $this->arResult;
	}
} ?>