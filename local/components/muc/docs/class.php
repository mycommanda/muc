<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class DocumentsComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
		];
		return $result;
	}

	public function executeComponent()
	{
		$request = $_GET;

		if ($this->startResultCache(false, $request['id'])) {
			\Bitrix\Main\Loader::includeModule('letsrock.lib');
			$controller = new Letsrock\Lib\Controller\DocumentsController();
			$this->arResult = $controller->getDocs();
			$this->includeComponentTemplate();
		}

		return $this->arResult;
	}
} ?>