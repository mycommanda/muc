<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<? if ($APPLICATION->GetCurPage() != "/"): ?>
	</div>
	</div>
	</div>
<? endif; ?>
<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	[
		"AREA_FILE_SHOW"   => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE"    => "",
		"PATH"             => "/include/footer/footer.php",
	]
); ?>