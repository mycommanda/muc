<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	[
		"AREA_FILE_SHOW"   => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE"    => "",
		"PATH"             => "/include/header/header.php",
	]
); ?>
	<? if ($APPLICATION->GetCurPage() != "/"): ?>
	<section id="mu-page-breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-page-breadcrumb-area">
						<h2><?= $APPLICATION->ShowTitle() ?></h2>
						<? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "muc", [
							"START_FROM"         => "0",
							// Номер пункта, начиная с которого будет построена навигационная цепочка
							"PATH"               => "",
							// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
							"SITE_ID"            => "s1",
							// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
							"COMPONENT_TEMPLATE" => "universal",
						],
							false
						); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="inner" <?= ($APPLICATION->GetCurPage() == "/courses/item/")? 'id="mu-course-content"':"" ; ?>>
		<div class="container">
			<?$APPLICATION->IncludeComponent(
				"bitrix:menu",
				"leftanonimus",
				Array(
					"ALLOW_MULTI_SELECT" => "N",
					"CHILD_MENU_TYPE" => "leftfirst",
					"DELAY" => "N",
					"MAX_LEVEL" => "1",
					"MENU_CACHE_GET_VARS" => array(""),
					"MENU_CACHE_TIME" => "3600",
					"MENU_CACHE_TYPE" => "N",
					"MENU_CACHE_USE_GROUPS" => "Y",
					"MENU_THEME" => "blue",
					"ROOT_MENU_TYPE" => "left",
					"USE_EXT" => "Y"
				)
			);?>
			<div class="content">
		<? endif; ?>
