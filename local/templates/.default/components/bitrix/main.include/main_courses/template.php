<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Loader;
use \Letsrock\Lib\Property;

Loader::includeModule('letsrock.lib');
?>

<section id="mu-features">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="mu-features-area">
					<!-- Start Title -->
					<div class="mu-title">
						<?
						include($arResult["FILE"]);
						?>
					</div>
					<!-- End Title -->
					<!-- Start features content -->

					<? $APPLICATION->IncludeComponent(
						"muc:main.courses",
						"",
						[]
					); ?>
					<!-- End features content -->
				</div>
			</div>
		</div>
	</div>
</section>