<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- Start contact  -->
<section id="mu-contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="mu-contact-area">
					<div class="mu-contact-content">
						<div class="row">
							<div class="col-md-6">
								<div class="mu-contact-left">
									<?
									include($arResult["FILE"]);
									?>
								</div>
							</div>
							<div class="col-md-6">
								<div class="mu-contact-right">
									<a class="dg-widget-link"
									   href="http://2gis.ru/krasnodar/firm/70000001019650126/center/38.98452,45.014306/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть
									                                                                                                                                                               на
									                                                                                                                                                               карте
									                                                                                                                                                               Краснодара</a>
									<div class="dg-widget-link">
										<a href="http://2gis.ru/krasnodar/center/38.98452,45.014306/zoom/16/routeTab/rsType/bus/to/38.98452,45.014306╎Межотраслевой учебный центр?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти
										                                                                                                                                                                                                             проезд
										                                                                                                                                                                                                             до
										                                                                                                                                                                                                             Межотраслевой
										                                                                                                                                                                                                             учебный
										                                                                                                                                                                                                             центр</a>
									</div>
									<script charset="utf-8"
									        src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
									<script charset="utf-8">new DGWidgetLoader({
																				'width': '100%',
																				'height': '100%',
																				'borderColor': 'none',
																				'pos': {'lat': 45.014306, 'lon': 38.98452, 'zoom': 16},
																				'opt': {'city': 'krasnodar'},
																				'org': [{'id': '70000001019650126'}],
																			});</script>
									<noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты
									                                                              использует JavaScript.
									                                                              Включите его в
									                                                              настройках вашего
									                                                              браузера.
									</noscript>
								</div>
							</div>
						</div>
					</div>
					<!-- end contact content -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End contact  -->

