<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use Bitrix\Main\Loader;
use \Letsrock\Lib\Property;

Loader::includeModule('letsrock.lib');
?>

<section id="mu-about-us">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="mu-about-us-area">
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<div class="mu-about-us-left">
								<?
								include($arResult["FILE"]);
								?>
							</div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="mu-about-us-right">
								<img src="<?= Property::getImage('IZOBRAZHENIE-V-BLOKE-O-NAS')['src'] ?>" alt="img">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
