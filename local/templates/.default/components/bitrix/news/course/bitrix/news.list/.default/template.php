<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul class="nav nav-pills js-courses-tabs">
	<li role="presentation" class="active js-courses-tab-link-all"><a href="javascript:void(0)">Все</a></li>
	<li role="presentation" class="js-courses-tab-link" data-section="1"><a href="javascript:void(0)">Повышение квалификации</a></li>
	<li role="presentation" class="js-courses-tab-link" data-section="2"><a href="javascript:void(0);">Профессиональная переподготовка</a></li>
</ul>
<div class="mu-features-content">
	<div class="row">
		<? foreach ($arResult['ITEMS'] as $item): ?>
			<div class="col-lg-4 col-md-4  col-sm-6 js-classes-item"
			     data-section-1="<?= empty($item['PROPERTIES']['SECTION_1']['VALUE']) ? 0 : 1; ?>"
			     data-section-2="<?= empty($item['PROPERTIES']['SECTION_2']['VALUE']) ? 0 : 1; ?>">
				<div class="mu-single-feature">
					<span class="<?= $item['PROPERTIES']['IMAGE_CLASS']['VALUE'] ?>"></span>
					<h4><?= $item['NAME'] ?></h4>
					<p><?= $item['PREVIEW_TEXT'] ?></p>
					<a href="<?= $item['DETAIL_PAGE_URL'] ?>">Подробнее</a>
				</div>
			</div>
		<? endforeach; ?>
	</div>
</div>
