<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);


if (empty($arResult["ALL_ITEMS"]))
    return;


?>

<ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
    <? foreach ($arResult["MENU_STRUCTURE"] as $itemID => $arColumns): ?>
        <? if (empty($arColumns)): ?>
            <li class="navbar-item <?= ($arResult["ALL_ITEMS"][$itemID]["SELECTED"])? "active": "" ?>"><a
                        href="<?= $arResult["ALL_ITEMS"][$itemID]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$itemID]["TEXT"] ?></a>
            </li>
        <? else: ?>
            <li class="navbar-item dropdown">
                <a href="<?= $arResult["ALL_ITEMS"][$itemID]["LINK"] ?>" class="dropdown-toggle"
                   data-toggle="dropdown"><?= $arResult["ALL_ITEMS"][$itemID]["TEXT"] ?> <span
                            class="fa fa-angle-down"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <? foreach ($arColumns as $subItemID => $arSubColumns): ?>
                        <? foreach ($arSubColumns as $subSubItemID => $arSubSubColumns): ?>
                            <li class="<?= ($arResult["ALL_ITEMS"][$subSubItemID]["SELECTED"])? "active": "" ?>">
                                <a href="<?= $arResult["ALL_ITEMS"][$subSubItemID]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$subSubItemID]["TEXT"] ?></a>
                            </li>
                        <? endforeach; ?>
                    <? endforeach; ?>
                </ul>
            </li>
        <? endif; ?>
    <? endforeach; ?>
	<div class="nav-red">
		<a class="btn btn-danger" data-izimodal-open=".modal[data-modal='request']" href="javascript:void(0);">Подать заявку</a>
	</div>
</ul>

