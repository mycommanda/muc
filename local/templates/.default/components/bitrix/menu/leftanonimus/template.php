<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult)) {
	return;
}
?>
<div class="col-md-3 col-xs-12">
	<aside class="mu-sidebar">
		<!-- start single sidebar -->
		<div class="mu-single-sidebar">
			<h3><?$APPLICATION->ShowTitle()?></h3>
			<ul class="mu-sidebar-catg">
				<? foreach ($arResult as $item): ?>
					<li class="section-link">
						<a href="<?= $item['LINK'] ?>"><?= $item['TEXT'] ?></a>
					</li>
				<? endforeach; ?>
			</ul>
		</div>
		<!-- end single sidebar -->
	</aside>
</div>