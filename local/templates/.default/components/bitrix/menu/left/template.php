<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult)) {
	return;
}
?>
<aside class="sidebar">
	<div class="sidebar-item">
		<h3 style="margin-top: 0;">Сведения об образовательной организации</h3>
		<ul class="mu-sidebar-catg">
			<? foreach ($arResult as $item): ?>
				<li class="section-link <?= ($item['SELECTED']) ? 'active' : ''; ?>">
					<a href="<?= $item['LINK'] ?>"><?= $item['TEXT'] ?></a>
				</li>
			<? endforeach; ?>
		</ul>
	</div>
</aside>
