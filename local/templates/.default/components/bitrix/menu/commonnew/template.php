<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);


if (empty($arResult["ALL_ITEMS"]))
    return;


?>
<ul class="header__navigation-wrap">
    <? foreach ($arResult["MENU_STRUCTURE"] as $itemID => $arColumns): ?>
        <? if (empty($arColumns)): ?>
            <li class="header__navigation-item <?= ($arResult["ALL_ITEMS"][$itemID]["SELECTED"])? "header__navigation-item_active": "" ?>">
	            <a class="header__navigation-link <?= ($arResult["ALL_ITEMS"][$itemID]["SELECTED"])? "header__navigation-linkactive": "" ?>" href="<?= $arResult["ALL_ITEMS"][$itemID]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$itemID]["TEXT"] ?></a>
            </li>
        <? else: ?>
            <li class="header__navigation-item header__navigation-item_dropdown">
                <a href="<?= $arResult["ALL_ITEMS"][$itemID]["LINK"] ?>" class="header__navigation-link header__navigation-link_dropdown"
                   ><?= $arResult["ALL_ITEMS"][$itemID]["TEXT"] ?></a>
                <ul class="header__navigation-dropdown-wrap">
                    <? foreach ($arColumns as $subItemID => $arSubColumns): ?>
                        <? foreach ($arSubColumns as $subSubItemID => $arSubSubColumns): ?>
                            <li class="header__navigation-dropdown-item <?= ($arResult["ALL_ITEMS"][$subSubItemID]["SELECTED"])? "header__navigation-dropdown-item_active": "" ?>">
                                <a class="header__navigation-dropdown-link" href="<?= $arResult["ALL_ITEMS"][$subSubItemID]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$subSubItemID]["TEXT"] ?></a>
                            </li>
                        <? endforeach; ?>
                    <? endforeach; ?>
                </ul>
            </li>
        <? endif; ?>
    <? endforeach; ?>
</ul>