<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

$request = $_POST;
CModule::IncludeModule("iblock");
$arFields = [];
$res = CIBlockElement::GetList(
	["SORT" => "ASC"],
	[
		"IBLOCK_ID"  => IB_COURSES,
		"SECTION_ID" => $request['SECTION'],
	],
	false,
	[],
	[
		'ID',
		'NAME',
		'PROPERTY_IMAGE_CLASS',
		'PROPERTY_SECTION_1',
		'PROPERTY_SECTION_2',
		'PROPERTY_FILES',
		'PREVIEW_TEXT',
		'DETAIL_PAGE_URL',
	]
);

while ($ob = $res->GetNextElement()) {
	$arFields[] = $ob->GetFields();
}

foreach ($arFields as $key => $course) {
	foreach ($course["PROPERTY_FILES_VALUE"] as $index => $item) {
		$arFields[$key]['PROPERTY_FILES'][$index]['SMALL'] = CFile::ResizeImageGet($item,
			["width" => 400, "height" => 300], BX_RESIZE_IMAGE_PROPORTIONAL);
		$arFields[$key]['PROPERTY_FILES'][$index]['BIG'] = CFile::ResizeImageGet($item,
			["width" => 1000, "height" => 2000], BX_RESIZE_IMAGE_PROPORTIONAL);
	}
}

if (empty($arFields)) {
	return 0;
}


?>

<? foreach ($arFields as $item): ?>
	<div class="col-xs-12 col-sm-6 js-classes-item"
	     data-section-1="<?= empty($item['PROPERTY_SECTION_1_VALUE']) ? 0 : 1; ?>"
	     data-section-2="<?= empty($item['PROPERTY_SECTION_2_VALUE']) ? 0 : 1; ?>">
		<div class="panel panel-default card">
			<? foreach ($item['PROPERTY_FILES'] as $index => $file): ?>
				<? if ($index == 0): ?>
					<a data-fancybox="gallery"
					   class="card-img-top card-image"
					   href="<?= $file['BIG']['src'] ?>">
						<img src="<?= $file['SMALL']['src'] ?>" alt="<?= $file['NAME'] ?>"></a>
				<? else: ?>
					<a data-fancybox="gallery"
					   href="<?= $file['BIG']['src'] ?>" style="display: none;"></a>
				<? endif; ?>
			<? endforeach; ?>
			<div class="card-body panel-body">
				<h4 class="card-title"><?= $item['NAME'] ?></h4>
				<p class="card-text"><?= $item['PREVIEW_TEXT'] ?></p>
			</div>
			<div class="card-footer panel-footer">
				<a href="/courses/item/?id=<?= $item['ID'] ?>" class="btn btn-primary">Подробнее</a>
			</div>
		</div>
	</div>
<? endforeach; ?>
