<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

CModule::IncludeModule("iblock");

$request = $_POST;
$el = new CIBlockElement;

$props = [];
$props['NAME'] = htmlspecialchars($request['name']);
$props['EMAIL'] = htmlspecialchars($request['email']);
$props['SECTION_1'] = $request['name'];
$props['SECTION_2'] = $request['name'];
$props['EDU'] = htmlspecialchars($request['edu']);

$arLoadProductArray = [
	"MODIFIED_BY"       => $USER->GetID(), // элемент изменен текущим пользователем
	"IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
	"IBLOCK_ID"         => IB_REQUEST,
	"PROPERTY_VALUES"   => $props,
	"NAME"              => "Заявка на обучение от".$props['NAME'],
	"ACTIVE"            => "Y",            // активен
];

if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
	echo "New ID: " . $PRODUCT_ID;
} else {
	echo "Error: " . $el->LAST_ERROR;
}

?>