<?php
/**
 * Created by PhpStorm.
 * User: mihailsarajkin
 * Date: 20.03.19
 * Time: 23:33
 */

namespace Letsrock\Lib\Controller;

use Letsrock\Lib\Models\Controller;
use Letsrock\Lib\Models\DocumentModel;

class DocumentsController extends Controller
{
	protected $dir = __DIR__;
	protected $class = __CLASS__;

	function getDocs()
	{
		$documentModel = new DocumentModel();
		$data = [
			'SECTIONS' => $documentModel->getSections(),
			'ITEMS' => $documentModel->getDocuments()
		];

		return $this->render($data);
	}
}