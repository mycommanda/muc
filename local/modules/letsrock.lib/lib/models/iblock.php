<?php

namespace Letsrock\Lib\Models;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

abstract class Iblock
{
	protected $iblockId;

	function __construct()
	{

	}

	/**
	 * Метод вывода списка элементов инфоблока
	 * @param array $select
	 * @param array $filter
	 * @param array $sort
	 * @param bool $nav
	 * @param bool $groupBy
	 * @return array|string
	 */
	public function get(
		$filter = [],
		$select = ['*'],
		$sort = ['SORT' => 'DESC'],
		$nav = false,
		$groupBy = false
	) {
		$filterAdditional = [
			'IBLOCK_ID'   => $this->iblockId,
			'ACTIVE_DATE' => 'Y',
			'ACTIVE'      => 'Y',
		];
		$filter = array_merge($filter, $filterAdditional);

		if ($select != ['*']) {
			$selectAdditional = ['ID', 'IBLOCK_ID'];
			$select = array_merge($select, $selectAdditional);
		}

		if (!empty($nav) && is_int($nav)) {
			$nav = ['nPageSize' => $nav];
		}

		$res = \CIBlockElement::GetList($sort, $filter, $groupBy, $nav, $select);
		$data = [];
		while ($ob = $res->GetNextElement()) {
			$data[] = $ob->GetFields();
		}

		return $data;
	}

	/**
	 * Метод для получения списка разделов инфоблока
	 * @param array $select
	 * @param $filter
	 * @param array $order
	 * @return array
	 */
	public function getSections(
		$select = ['ID', 'NAME', 'CODE'],
		$filter = ['ACTIVE' => 'Y'],
		$order = ['SORT' => 'ASC']
	) {

		$filter['IBLOCK_ID'] = $this->iblockId;
		$rsSect = \CIBlockSection::GetList($order, $filter, false, $select);
		$data = [];;
		while ($arSect = $rsSect->GetNext()) {
			$data[] = $arSect;
		}

		return $data;
	}
}