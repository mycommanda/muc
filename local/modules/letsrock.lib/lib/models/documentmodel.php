<?php

namespace Letsrock\Lib\Models;

use Letsrock\Lib\Models\Iblock;

//use Letsrock\Lib\Models\Section;


class DocumentModel extends Iblock
{
	protected $iblockId = IB_DOCUMENTS;

	function getDocuments() {
		$documents = $this->get([], ['NAME', 'PROPERTY_IMAGES', 'IBLOCK_SECTION_ID']);
		foreach ($documents as $key => $document) {
			foreach ($document["PROPERTY_IMAGES_VALUE"] as $index => $item) {
				$documents[$key]['PROPERTY_FILES'][$index]['SMALL'] = \CFile::ResizeImageGet($item,
					["width" => 400, "height" => 300], BX_RESIZE_IMAGE_PROPORTIONAL);
				$documents[$key]['PROPERTY_FILES'][$index]['BIG'] = \CFile::ResizeImageGet($item,
					["width" => 1000, "height" => 2000], BX_RESIZE_IMAGE_PROPORTIONAL);
			}
		}

		return $documents;
	}


}