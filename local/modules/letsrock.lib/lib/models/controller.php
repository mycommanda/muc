<?php
/**
 * Created by PhpStorm.
 * User: mihailsarajkin
 * Date: 20.03.19
 * Time: 22:23
 */

namespace Letsrock\Lib\Models;


class Controller
{
	protected $template;
	protected $path;
	protected $dir;
	protected $class;

	function __construct()
	{
		$arPath = explode('/', $this->dir);
		array_pop($arPath);
		$arPath[] = 'views';
		$this->path = implode('/', $arPath);
		$array = explode('\\', $this->class);
		$className = $array[count($array) - 1];
		$fileName = str_replace("Controller", "", $className);
		$this->template = $this->path . '/' . mb_strtolower($fileName) . '.phtml';
	}

	function render($data)
	{
		if (!file_exists($this->template)) {
			return "<h2>error</h2>";
		}

		ob_start();
		require_once($this->template);
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}


}