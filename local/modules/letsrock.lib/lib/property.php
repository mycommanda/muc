<?php

namespace Letsrock\Lib;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');


/*
 * Class Property
 * Класс для работы с настройками
 */

class Property
{
	private function __construct()
	{
		Loader::IncludeModule("iblock");
	}

	public static function getText($code)
	{
		$arSelect = [
			"ID",
			"NAME",
			"PREVIEW_TEXT",
		];
		$arFilter = ["IBLOCK_ID" => IntVal(IB_PROPS), "ACTIVE" => "Y", 'CODE' => $code];
		$res = \CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 1], $arSelect);

		$ob = $res->GetNextElement();
		$arFields = $ob->GetFields();

		return $arFields['PREVIEW_TEXT'];
	}

	public static function getImage($code)
	{
		$arSelect = [
			"ID",
			"NAME",
			"PREVIEW_PICTURE",
		];
		$arFilter = ["IBLOCK_ID" => IntVal(IB_PROPS), "ACTIVE" => "Y", 'CODE' => $code];
		$res = \CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 1], $arSelect);
		$ob = $res->GetNextElement();
		$arFields = $ob->GetFields();

		$arFields['PREVIEW_PICTURE'] = \CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"],
			["width" => 1920, "height" => 1080], BX_RESIZE_IMAGE_PROPORTIONAL);

		return $arFields['PREVIEW_PICTURE'];
	}
}
