<?php
/**
 * Пользовательские константы
 */

//const ASSETS_HOME = '/local/templates/assets/';

/**
 * Highload блоки
 */

//const HL_CITY = 3;

/**
 * Инфоблоки
 */

const IB_SLIDER_MAIN = 3;
const IB_BLOCKS_UNDER_SLIDER = 4;
const IB_PROPS = 5;
const IB_ADVANTAGES = 6;
const IB_COURSES = 7;
const IB_DOCUMENTS = 8;
const IB_REQUEST = 9;
const IB_PARTNERS = 12;

