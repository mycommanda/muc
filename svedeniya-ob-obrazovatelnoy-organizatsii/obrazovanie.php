<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Образование");
?><p>
	 Обучение по образовательным программам ведется на русском языке.
</p>
<p>
	 Перечень образовательных программ, реализуемых в очной, заочной форме обучения с применением дистанционных образовательных технологий и документы, регламентирующие образовательный процесс:
</p>
<div>
	<p>
	</p>
</div>

<table cellspacing="0" cellpadding="0">
 <tbody><tr>
  <td>
  <p align="center">№</p>
  <p align="center">п/п</p>
  </td>
  <td>
  <p align="center">&nbsp;</p>
  <p align="center">Образовательная программа</p>
  </td>
  <td>
  <p align="center">Продолжительность обучения, часов</p>
  </td>
  <td>
  <p align="center">Стоимость обучения, рублей</p>
  </td>
 </tr>
 <tr>
  <td colspan="4">
  <p align="center">Повышение квалификации</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>1.</p>
  </td>
  <td>
  <p>«Охрана труда работников организаций»</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">2500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>2.</p>
  </td>
  <td>
  <p>«Охрана труда при
  работе на высоте» I, II, III группы</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4000</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>3.</p>
  </td>
  <td>
  <p>«Обучение навыкам оказания
  первой помощи»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">600</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>4.</p>
  </td>
  <td>
  <p>№ 1 «Пожарно-технический минимум для
  электрогазосварщиков»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>5.</p>
  </td>
  <td>
  <p>№ 2 «Пожарно-технический минимум для
  рабочих, осуществляющих пожароопасные работы»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>6.</p>
  </td>
  <td>
  <p>№ 3 «Пожарно-технический минимум для руководителей и лиц, ответственных за
  пожарную безопасность сельскохозяйственных организаций»</p>
  </td>
  <td>
  <p align="center">19</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>7.</p>
  </td>
  <td>
  <p>№ 4 «Пожарно-технический минимум для механизаторов, рабочих и служащих
  сельскохозяйственных организаций»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>8.</p>
  </td>
  <td>
  <p>№ 5 «Пожарно-технический минимум для руководителей и лиц, ответственных за
  пожарную безопасность дошкольных учреждений и общеобразовательных школ»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>9.</p>
  </td>
  <td>
  <p>№ 6 «Пожарно-технический минимум для воспитателей дошкольных учреждений»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>10.</p>
  </td>
  <td>
  <p>№ 7 «Пожарно-технический минимум для руководителей и лиц, ответственных за
  пожарную безопасность в лечебных учреждениях»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>11.</p>
  </td>
  <td>
  <p>№ 8 «Пожарно-технический минимум для руководителей и лиц, ответственных за
  пожарную безопасность в жилых домах»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>12.</p>
  </td>
  <td>
  <p>№ 9 «Пожарно-технический минимум для руководителей и лиц, ответственных за
  пожарную безопасность в учреждениях (офисах)»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>13.</p>
  </td>
  <td>
  <p>№ 10 «Пожарно-технический минимум
  для сотрудников, осуществляющих круглосуточную охрану организаций, и
  руководителей подразделений организаций»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>14.</p>
  </td>
  <td>
  <p>№ 11 «Пожарно-технический минимум для
  руководителей и ответственных лиц за пожарную безопасность и проведение
  противопожарного инструктажа на пожароопасных производствах»</p>
  </td>
  <td>
  <p align="center">28</p>
  </td>
  <td>
  <p align="center">920</p>
  <p align="center"> </p>
  </td>
 </tr>
 <tr>
  <td>
  <p>15.</p>
  </td>
  <td>
  <p>№ 12 «Пожарно-технический минимум
  для ответственных лиц за пожарную безопасность вновь строящихся и реконструируемых
  объектов»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>16.</p>
  </td>
  <td>
  <p>№ 13 «Пожарно-технический
  минимум для руководителей и ответственных лиц за пожарную безопасность в
  организациях торговли, общественного питания, на базах и складах»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>17.</p>
  </td>
  <td>
  <p>№ 13.1 «Пожарно-технический минимум для руководителей и ответственных лиц за
  пожарную безопасность в организациях бытового обслуживания»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>18.</p>
  </td>
  <td>
  <p>№ 14 «Пожарно-технический минимум
  для руководителей и ответственных лиц за пожарную безопасность в
  театрально-зрелищных и культурно-просветительных учреждениях, объектов с
  массовым пребыванием людей (свыше 50 человек)»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>19.</p>
  </td>
  <td>
  <p>№ 15 «Пожарно-технический минимум
  для руководителей и работников в организациях, предоставляющих услуги населению (гостиничные,
  парикмахерские, ремонта и пошива одежды и обуви, ремонта и технического
  обслуживания автотранспорта)»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>20.</p>
  </td>
  <td>
  <p>№ 16 «Пожарно-технический минимум
  для руководителей (специалистов), операторов автозаправочных станций»</p>
  </td>
  <td>
  <p align="center">28</p>
  </td>
  <td>
  <p align="center">920</p>
  <p align="center"> </p>
  </td>
 </tr>
 <tr>
  <td>
  <p>21.</p>
  </td>
  <td>
  <p>№ 17 «Пожарно-технический минимум для
  рабочих»</p>
  </td>
  <td>
  <p align="center">16</p>
  </td>
  <td>
  <p align="center">800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>22.</p>
  </td>
  <td>
  <p>«Эксплуатация электроустановок
  потребителей II группа допуска (первоначальная подготовка)»</p>
  </td>
  <td>
  <p align="center"> </p>
  <p align="center">72</p>
  </td>
  <td>
  <p align="center"> </p>
  <p align="center">5 250</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>23.</p>
  </td>
  <td>
  <p>«Эксплуатация электроустановок потребителей II группа допуска
  (подтверждение/повышение),III, IV, V группы допуска»</p>
  </td>
  <td>
  <p align="center"> </p>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center"> </p>
  <p align="center">3 000</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>24.</p>
  </td>
  <td>
  <p>«Общие требования безопасной эксплуатации
  лифтов» (для ответственных лиц)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>25.</p>
  </td>
  <td>
  <p>«Общие
  требования безопасной эксплуатации платформ подъемных для инвалидов» (для
  ответственных лиц)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>26.</p>
  </td>
  <td>
  <p>«Общие
  требования безопасной эксплуатации грузового подъемника» (для ответственных лиц)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>27.</p>
  </td>
  <td>
  <p>« Общие требования безопасной эксплуатации медицинских стерилизаторов»
  (для ответственных лиц)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>28.</p>
  </td>
  <td>
  <p>«Требования к
  эксплуатации и обслуживанию медицинских стерилизаторов» (для персонала)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>29.</p>
  </td>
  <td>
  <p>«Требования к
  обслуживанию оборудования, работающего под избыточным давлением» (для персонала)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>30.</p>
  </td>
  <td>
  <p>«Требования к обслуживанию трубопроводов пара и горячей воды» (для персонала) </p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>31.</p>
  </td>
  <td>
  <p>«Рабочий
  люльки подъемников (вышек)»</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>32.</p>
  </td>
  <td>
  <p>«Безопасная
  эксплуатация стеллажей и складского оборудования. Техническое
  освидетельствование стеллажей»</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>33.</p>
  </td>
  <td>
  <p>«Обеспечение экологической
  безопасности руководителями (специалистами) общехозяйственных систем
  управления»</p>
  <p> </p>
  <p> </p>
  </td>
  <td>
  <p align="center">80</p>
  </td>
  <td>
  <p align="center">7500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>34.</p>
  </td>
  <td>
  <p>«Обеспечение экологической
  безопасности руководителями и специалистами экологических служб и систем
  экологического контроля»</p>
  </td>
  <td>
  <p align="center">112</p>
  </td>
  <td>
  <p align="center"> </p>
  <p align="center">8 000</p>
  <p align="center"> </p>
  </td>
 </tr>
 <tr>
  <td>
  <p>35.</p>
  </td>
  <td>
  <p>«Обеспечение экологической
  безопасности при работах в области обращения с опасными отходами I-IV класса
  опасности»</p>
  </td>
  <td>
  <p align="center">112</p>
  </td>
  <td>
  <p align="center">8
  500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>36.</p>
  </td>
  <td>
  <p>«Государственная служба в РФ и перспективы ее
  развития»</p>
  </td>
  <td>
  <p align="center">72</p>
  </td>
  <td>
  <p align="center">6 300</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>37.</p>
  </td>
  <td>
  <p>«Бухгалтерский учет для начинающих»</p>
  </td>
  <td>
  <p align="center">72</p>
  </td>
  <td>
  <p align="center">6 300</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>38.</p>
  </td>
  <td>
  <p>«Бухгалтерский учет и налогообложение с
  изучением 1С; Бухгалтерия 8.0»</p>
  </td>
  <td>
  <p align="center">72</p>
  </td>
  <td>
  <p align="center">6 300</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>39.</p>
  </td>
  <td>
  <p>«Бухгалтерский учет, анализ и аудит»</p>
  </td>
  <td>
  <p align="center">72</p>
  </td>
  <td>
  <p align="center">6 300</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>40.</p>
  </td>
  <td>
  <p>«Бухгалтер государственного (муниципального)
  учреждения»</p>
  </td>
  <td>
  <p align="center">72</p>
  </td>
  <td>
  <p align="center">6 300</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>41.</p>
  </td>
  <td>
  <p>«1С: Зарплата и управление персоналом» 8.0»</p>
  </td>
  <td>
  <p align="center">36</p>
  </td>
  <td>
  <p align="center">4
  000</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>42.</p>
  </td>
  <td>
  <p>«Бухгалтерский учет УСН»</p>
  </td>
  <td>
  <p align="center">24</p>
  </td>
  <td>
  <p align="center">3
  700</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>43.</p>
  </td>
  <td>
  <p>«1С: Торговля и склад» 8.0»</p>
  </td>
  <td>
  <p align="center">24</p>
  </td>
  <td>
  <p align="center">3
  700</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>44.</p>
  </td>
  <td>
  <p>«Кадровый учет за 3 дня»</p>
  </td>
  <td>
  <p align="center">24</p>
  </td>
  <td>
  <p align="center">3
  700</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>45.</p>
  </td>
  <td>
  <p>«Ежегодные занятия с водителями автотранспортных организаций»</p>
  </td>
  <td>
  <p align="center">20</p>
  </td>
  <td>
  <p align="center">500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>46.</p>
  </td>
  <td>
  <p>«Подготовка и переподготовка специалистов по безопасности движения на
  автомобильном и городском электротранспорте»</p>
  </td>
  <td>
  <p align="center">48</p>
  </td>
  <td>
  <p align="center">4
  800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>47.</p>
  </td>
  <td>
  <p>«Подготовка должностных лиц и
  специалистов ГО и РСЧС» (руководитель организации, руководитель ГО)</p>
  </td>
  <td>
  <p align="center">72</p>
  </td>
  <td>
  <p align="center">3 500</p>
  <p align="center"> </p>
  </td>
 </tr>
 <tr>
  <td>
  <p>48.</p>
  </td>
  <td>
  <p>«Подготовка должностных лиц и
  специалистов ГО и РСЧС» (уполномоченный на решение задач в области ГО и
  защиты от ЧС в организации, председатель комиссии ГО и ЧС организации, члены
  комиссии ГО и ЧС)</p>
  </td>
  <td>
  <p align="center"> </p>
  <p align="center">24</p>
  <p align="center"> </p>
  </td>
  <td>
  <p align="center"> </p>
  <p align="center">1 800</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>49.</p>
  </td>
  <td>
  <p>«Педагог дополнительного профессионального
  образования и профессионального обучения»</p>
  </td>
  <td>
  <p align="center">72</p>
  </td>
  <td>
  <p align="center">4800</p>
  </td>
 </tr>
 <tr>
  <td colspan="4">
  <p align="center">Профессиональная переподготовка</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>50.</p>
  </td>
  <td>
  <p>«Техносферная безопасность на производстве»</p>
  </td>
  <td>
  <p align="center">256</p>
  </td>
  <td>
  <p align="center">15 700</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>51.</p>
  </td>
  <td>
  <p>«Педагог дополнительного профессионального
  образования и профессионального обучения»</p>
  </td>
  <td>
  <p align="center">260</p>
  </td>
  <td>
  <p align="center">16 000</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>52.</p>
  </td>
  <td>
  <p>«Специалист (менеджер) по персоналу и кадровому
  делопроизводству»</p>
  </td>
  <td>
  <p align="center">260</p>
  </td>
  <td>
  <p align="center">16 000</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>53.</p>
  </td>
  <td>
  <p>«Переподготовка профессиональных бухгалтеров»</p>
  </td>
  <td>
  <p align="center">260</p>
  </td>
  <td>
  <p align="center">16 000</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>54.</p>
  </td>
  <td>
  <p>«Контролер технического состояния
  автотранспортных средств»</p>
  </td>
  <td>
  <p align="center">260</p>
  </td>
  <td>
  <p align="center">16 000</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>55.</p>
  </td>
  <td>
  <p>«Сервисное обслуживание автомобильного
  транспорта»</p>
  </td>
  <td>
  <p align="center">260</p>
  </td>
  <td>
  <p align="center">16 000</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>56.</p>
  </td>
  <td>
  <p>«Диспетчер автомобильного и городского наземного
  электрического транспорта»</p>
  </td>
  <td>
  <p align="center">260</p>
  </td>
  <td>
  <p align="center">16 000</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>57.</p>
  </td>
  <td>
  <p>«Специалист, ответственный за обеспечение
  безопасности дорожного движения»</p>
  </td>
  <td>
  <p align="center">260</p>
  </td>
  <td>
  <p align="center">16 000</p>
  </td>
 </tr>
 <tr>
  <td colspan="4">
  <p align="center">Профессиональное обучение</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>58.</p>
  </td>
  <td>
  <p>Аппаратчик химводоочистки</p>
  </td>
  <td>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>59.</p>
  </td>
  <td>
  <p>Водитель погрузчика&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (не более 4 КВт)</p>
  </td>
  <td>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>60.</p>
  </td>
  <td>
  <p>Лифтер</p>
  </td>
  <td>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>61.</p>
  </td>
  <td>
  <p>Машинист крана автомобильного</p>
  </td>
  <td>
  <p align="center">400 </p>
  <p align="center">220 </p>
  </td>
  <td>
  <p align="center">11 200</p>
  <p align="center">5 600</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>62.</p>
  </td>
  <td>
  <p>Машинист автовышки и автогидроподъемника</p>
  </td>
  <td>
  <p align="center">240 </p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center"> </p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>63.</p>
  </td>
  <td>
  <p>Машинист компрессорных установок</p>
  </td>
  <td>
  <p align="center">240 </p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>64.</p>
  </td>
  <td>
  <p>Машинист технологических насосов</p>
  </td>
  <td>
  <p align="center">240 </p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>65.</p>
  </td>
  <td>
  <p>Машинист насосных установок</p>
  </td>
  <td>
  <p align="center">240 </p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>66.</p>
  </td>
  <td>
  <p>Монтажник систем вентиляции, кондиционирования
  воздуха, пневмотранспорта и аспирации</p>
  </td>
  <td>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">4900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>67.</p>
  </td>
  <td>
  <p>Оператор технологических установок</p>
  </td>
  <td>
  <p align="center">240</p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">11 200</p>
  <p align="center">5 600</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>68.</p>
  </td>
  <td>
  <p>Оператор котельной</p>
  </td>
  <td>
  <p align="center">240 </p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>69.</p>
  </td>
  <td>
  <p>Оператор платформы подъемной </p>
  </td>
  <td>
  <p align="center">80 </p>
  </td>
  <td>
  <p align="center">3700</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>70.</p>
  </td>
  <td>
  <div>Пробоотборщик</div>
  </td>
  <td>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">4900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>71.</p>
  </td>
  <td>
  <p>Слесарь
  механосборочных работ</p>
  </td>
  <td>
  <p align="center">240 </p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>72.</p>
  </td>
  <td>
  <p>Слесарь
  по контрольно - измерительным приборам и автоматике</p>
  </td>
  <td>
  <p align="center">240 </p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>73.</p>
  </td>
  <td>
  <p>Слесарь
  по ремонту технологических установок</p>
  </td>
  <td>
  <p align="center">240 </p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>74.</p>
  </td>
  <td>
  <p>Стропальщик</p>
  </td>
  <td>
  <p align="center">140</p>
  <p align="center">80 </p>
  </td>
  <td>
  <p align="center">6 500</p>
  <p align="center">3 700</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>75.</p>
  </td>
  <td>
  <p>Штукатур</p>
  </td>
  <td>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>76.</p>
  </td>
  <td>
  <div>Электрослесарь по ремонту и
  обслуживанию автоматики и средств измерений электростанций</div>
  </td>
  <td>
  <p align="center">240</p>
  <p align="center"> 160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>77.</p>
  </td>
  <td>
  <div>Электрослесарь по ремонту
  электрооборудования электростанций</div>
  </td>
  <td>
  <p align="center">240 </p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>78.</p>
  </td>
  <td>
  <div>Электромонтер по ремонту и
  обслуживанию электрооборудования</div>
  </td>
  <td>
  <p align="center">240 </p>
  <p align="center">160 </p>
  </td>
  <td>
  <p align="center">9 800</p>
  <p align="center">4 900</p>
  </td>
 </tr>
 <tr>
  <td colspan="4">
  <p align="center">Предаттестационная подготовка</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>79.</p>
  </td>
  <td>
  <p>А.1. «Основы промышленной
  безопасности»</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4 500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>80.</p>
  </td>
  <td>
  <p>Б.1. «Требования промышленной безопасности в
  химической, нефтехимической и нефтеперерабатывающей промышленности»</p>
  <p>(Б.1.1.- Б.1.25.)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4 500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>81.</p>
  </td>
  <td>
  <p>Б.2. «Требования промышленной безопасности в
  нефтяной и газовой промышленности» (Б.2.1.-Б.2.18.)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4 500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>82.</p>
  </td>
  <td>
  <p>Б.7. «Требования промышленной безопасности
  на объектах газораспределения и газопотребления» (Б.7.1.- Б.7.9.)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4 500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>83.</p>
  </td>
  <td>
  <p>Б.8. «Требования промышленной безопасности к оборудованию, работающему под
  давлением» (Б.8.21.- Б.8.26.)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4 500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>84.</p>
  </td>
  <td>
  <p>Б.9. «Требования промышленной безопасности к подъемным сооружениям»
  (Б.9.22.- Б.9.36.)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4 500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>85.</p>
  </td>
  <td>
  <p>Б.10. «Требования промышленной безопасности при
  транспортировании опасных веществ» (Б.10.1.- Б.10.2.)</p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4 500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>86.</p>
  </td>
  <td>
  <p>Д.1. Гидротехнические сооружения объектов
  промышленности</p>
  <p>(первоначальная подготовка/подтверждение) </p>
  </td>
  <td>
  <p align="center">72 </p>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4 800 </p>
  <p align="center">3 500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>87.</p>
  </td>
  <td>
  <p>Д.3. Гидротехнические сооружения объектов
  водохозяйственного комплекса (первоначальная подготовка/подтверждение) </p>
  </td>
  <td>
  <p align="center">72 </p>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4 800 </p>
  <p align="center">3 500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>88.</p>
  </td>
  <td>
  <p>Г.2.1 «Требования промышленной безопасности к
  порядку работы на тепловых энергоустановках и тепловых сетях» </p>
  </td>
  <td>
  <p align="center">40</p>
  </td>
  <td>
  <p align="center">4 500</p>
  </td>
 </tr>
 <tr>
  <td>
  <p>89.</p>
  </td>
  <td>
  <p>Г.2.1 «Требования
  промышленной безопасности к порядку работы на тепловых энергоустановках и тепловых сетях»</p>
  </td>
  <td>
  <p align="center"> </p>
  <p align="center">24</p>
  </td>
  <td>
  <p> </p>
  <p align="center">3 500</p>
  </td>
 </tr>
</tbody></table>

<p> </p>

<p>&nbsp;</p>





<p>&nbsp;</p>


<div>
</div>
<div></div>
<p>
	&nbsp;
</p>
<p>
	 &nbsp;
</p>
<p>
</p>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>