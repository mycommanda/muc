<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Документы");
?><h3>Учредительные документы, лицензии, свидетельства</h3>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/Свидетельства.PDF"><u>Свидетельства о государственной регистрации</u></a>&nbsp;<br>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/Лицензия%20(3)%20%2011.05.2018.PDF"><u>Лицензия</u><br>
 </a><a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/Аккредитация%20(Охрана%20Труда).PDF"><u>Аккредитация</u></a><sub></sub><sup></sup><br>
<h3>Положения</h3>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/О%20приеме.PDF" target="_blank"><u>ПОЛОЖЕНИЕ</u><u> о приёме, обучении, выпуске, отчислении, переводе и восстановлении в АНО ДПО "Межотраслевой Учебный Центр"</u><br>
 </a><sub></sub><sup></sup><br>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/О%20порядке%20оформления%20отношений.PDF"><u>ПОЛОЖЕНИЕ о порядке оформления возникновения, изменения и прекращения отношений между АНО ДПО "Межотраслевой Учебный Центр"</u></a><br>
 <br>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/О%20промежуточной%20аттестации.PDF"><u>ПОЛОЖЕНИЕ о промежуточной аттестации в АНО ДПО "Межотраслевой Учебный Центр"</u></a><br>
 <br>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/О%20порядке%20контроля.PDF"><u>ПОЛОЖЕНИЕ о порядке осуществления текущего контроля успеваемости в АНО ДПО "Межотраслевой Учебный Центр"</u></a><br>
 <br>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/О%20порядке%20зачета.PDF"><u>ПОЛОЖЕНИЕ о порядке зачёта АНО ДПО "Межотраслевой Учебный Центр" учебных предметов, курсов, дисциплин (модулей), освоенных в процессе предшествующего обучения по программам профессионального обучения и дополнительного профессионального образования</u></a><br>
 <br>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/Об%20итоговой%20аттестации.PDF"><u>ПОЛОЖЕНИЕ об итоговой аттестации в АНО ДПО "Межотраслевой Учебный Центр"</u></a><br>
 <br>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/Об%20оказании%20платных%20услуг.PDF"><u>ПОЛОЖЕНИЕ об оказании платных образовательных услуг в АНО ДПО "Межотраслевой Учебный Центр"</u></a><br>
 <br>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/О%20комиссии%20по%20урегулированию.PDF"><u>ПОЛОЖЕНИЕ о комиссии по урегулированию споров между участниками образовательных отношений в АНО ДПО "Межотраслевой Учебный Центр"</u></a><br>
 <br>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/Об%20аттестационной%20комиссии.PDF"><u>ПОЛОЖЕНИЕ об аттестационной комиссии АНО ДПО "Межотраслевой Учебный Центр"</u></a><br>
 <br>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/О%20дистанционном%20обучении.PDF"><u>ПОЛОЖЕНИЕ о дистанционном обучении в АНО ДПО "Межотраслевой Учебный Центр"</u></a><br>
 <br>
 <br>
<h3>Правила</h3>
 <a href="/svedeniya-ob-obrazovatelnoy-organizatsii/obrazovanie/Правила%20внутреннего%20распорядка.PDF"><u>ПРАВИЛА внутреннего распорядка для обучающихся в АНО ДПО "Межотраслевой Учебный Центр"</u></a><br>
 <br>
 <br>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>