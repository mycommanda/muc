<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Сведения об образовательной организации");
?>
	<table cellspacing="0" cellpadding="0">
		<colgroup>
			<col span="2">
			<col>
			<col span="5">
		</colgroup>
		<tbody>
		<tr>
			<td colspan="3">
				Полное наименование образовательной организации
			</td>
			<td colspan="5">
				Автономная некоммерческая организация дополнительного профессионального образования "Межотраслевой
				Учебный Центр"
			</td>
		</tr>
		<tr>
			<td colspan="3">
				Сокращенное наименование
			</td>
			<td colspan="5">
				АНО ДПО "Межотраслевой Учебный Центр"
			</td>
		</tr>
		<tr>
			<td colspan="3">
				ОГРН
			</td>
			<td colspan="5">
				1102300003860
			</td>
		</tr>
		<tr>
			<td colspan="3">
				ИНН
			</td>
			<td colspan="5">
				2309122230
			</td>
		</tr>
		<tr>
			<td colspan="3">
				ОКТМО
			</td>
			<td colspan="5">
				3701000
			</td>
		</tr>
		<tr>
			<td colspan="3">
				Юридический адрес (местонахождение исполнительного органа)
			</td>
			<td colspan="5">
				350033, Российская Федерация, Краснодарский край, город Краснодар, Центральный внутригородской округ,
				улица Ставропольская, дом 8
			</td>
		</tr>
		<tr>
			<td colspan="3">
				Режим, график работы
			</td>
			<td colspan="5">
				Понедельник - пятница с 8.00 до 17.00
			</td>
		</tr>
		<tr>
			<td colspan="3">
				Контактные телефоны
			</td>
			<td colspan="5">
				8 (861) 212-59-47, 8 (861) 212-59-48
			</td>
		</tr>
		<tr>
			<td colspan="3">
				Адрес электронной почты
			</td>
			<td colspan="5">
				<a href="mailto:2990287.muc@mail.ru">2990287.muc@mail.ru</a>
			</td>
		</tr>
		</tbody>
	</table>
	<p align="center">
		<br>
	</p>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>