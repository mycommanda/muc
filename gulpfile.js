const gulp = require('gulp');
const sass = require('gulp-sass');

gulp.task('style', function(){
    return gulp.src('markup/scss/style.scss')
        .pipe(sass()) // Using gulp-sass
        .pipe(gulp.dest('assets/css/'))
});

gulp.task('watch', function() {
    gulp.watch('markup/scss/**/*.scss', gulp.parallel('style'));
});